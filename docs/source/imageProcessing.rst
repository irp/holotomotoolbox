Image Processing
----------------

.. _matlab:

.. automodule:: functions.imageProcessing
    :members:

.. toctree::
   :maxdepth: 2
 
   subfolder_alignment
   subfolder_analysis
   subfolder_artefactRemoval
   subfolder_cropPadWindow
