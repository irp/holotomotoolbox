Holographic regime
------------------

.. _matlab:

.. automodule:: functions.phaseRetrieval.holographic
    :members:
