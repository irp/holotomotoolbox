Artefact removal
----------------

.. _matlab:

.. automodule:: functions.imageProcessing.artefactRemoval
    :members:
