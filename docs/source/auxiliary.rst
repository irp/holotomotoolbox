Auxiliary
---------

.. _matlab:

.. automodule:: functions.auxiliary
    :members:

.. toctree::
   :maxdepth: 2
 
   subfolder_gpusupport
