Direct-contrast regime
----------------------

.. _matlab:

.. automodule:: functions.phaseRetrieval.directContrast
    :members:
