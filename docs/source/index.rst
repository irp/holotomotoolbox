.. HoloTomoToolbox documentation master file, created by
   sphinx-quickstart on Tue Mar 19 13:57:17 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

HoloTomoToolbox
===============

Description of the toolbox goes here.

.. toctree::
   :maxdepth: 3
   :caption: Functions

   auxiliary
   fresnelPropagation
   generators
   imageProcessing
   inputOutput
   phaseRetrieval
   plotting
   tomography

.. toctree::
   :maxdepth: 3
   :caption: Examples

   exampleItPhaseRetr


.. toctree::
   :maxdepth: 3
   :caption: References

   zz_references
   

Contact
-------

Indices and tables
------------------

* :ref:`genindex`
* `Module Index`_

.. _Module Index: mat-modindex.html

