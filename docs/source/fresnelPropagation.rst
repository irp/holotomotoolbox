Fresnel Propagation
-------------------

.. _matlab:

.. automodule:: functions.fresnelPropagation
    :members:
