Crop, Pad, Window
-----------------

.. _matlab:

.. automodule:: functions.imageProcessing.cropPadWindow
    :members:
