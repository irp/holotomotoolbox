Phase Retrieval
---------------

.. _matlab:

.. automodule:: functions.phaseRetrieval
    :members:

.. toctree::
   :maxdepth: 2
 
   subfolder_holographic
   subfolder_directContrast
   subfolder_iterative 
   subfolder_utilities
