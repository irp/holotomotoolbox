#!/bin/sh

# MATLAB does not work so well with UTF-8 on Windows

echo "Correcting non-ASCII encoding"
nonascii=$(grep -Pl -n "[^\x00-\x7f]" -r functions)

for f in $nonascii; do
  if [ ${f: -2} != ".m" ]; then
    continue
  fi

  encoding=$(file -b --mime-encoding $f)
  echo "$f: $encoding"
  iconv -f $encoding -t ASCII//TRANSLIT -o $f.ascii $f
done

echo "Correcting DOS line endings..."
dos=$(grep -IUrl  "" functions)

for f in $dos; do
  if [ ${f: -2} != ".m" ]; then
    continue
  fi

  dos2unix $dos
done

