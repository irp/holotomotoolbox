function imNoisy = addPoissonNoise(im, photonsPerPixel)
% ADDPOISSONNOISE adds Poisson noise to a given image.
%
%    ``imNoisy = addPoissonNoise(im, photonsPerPixel)``
% 
% This function adds Poisson noise to a given image assuming that the image is
% normalized such that the value 1 corresponds to the given photon count
% (photonsPerPixel).
%
% Parameters
% ----------
% im : numerical array
%     image to which Poisson noise should be added
% photonsPerPixel : number
%     number of photons per pixel
%
% Returns
% -------
% imNoisy : numerical array
%     resulting image to which Poisson noise was added
%   
%
% Example
% -------
%
% .. code-block:: matlab
%
%     im = phantom(512);
%     
%     subplot(1,2,1)
%     showImage(im)
%     caxis([0,1]);
%     title('Original image')
%     imNoisy = addPoissonNoise(im, 1000);
%     
%     subplot(1,2,2)
%     showImage(imNoisy)
%     caxis([0,1]);
%     title('Noisy image')
    
% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

if nargin < 2
    photonsPerPixel = 10000;
end

imNoisy = zeros(size(im), 'like', im);

% imnoise needs double as input
if ~isa(im, 'double')
  im = double(im);
end

poissonFactor = 1e-12*photonsPerPixel;
imNoisy(:) = (1./poissonFactor) * imnoise(poissonFactor * im,'poisson');

end

