function result = ball(N, settings)
% BALL generates a ball-phantom of magnitude 1 in arbitrary dimensions.
%
%    ``result = ball(N, settings)``
%
% Parameters
% ----------
% N : tuple
%     tuple giving the array-dimensions of the phantom to be created
% settings : structure, optional
%     contains additional settings, see *Other Parameters*
%
% Other Parameters
% ----------------
% center : Default = []
%     Tuple giving the coordinates of the center of the ball. If empty, [0,...,0] is
%     assigned, which corresponds to a centered ball.
% diameter : Default = []
%     Tuple giving the coordinates of the center of the ball. If empty, min(N(:)) is
%     assigned.
% outputClass : Default = 'double'
%     Class of the output-array.
%
% Returns
% -------
% result : numerical array
%     created ball phantom
%
% See also
% --------
% functions.generators.cuboid, functions.generators.gaussian, functions.generators.create3Dphantom
%
% Example
% -------
%
% .. code-block:: matlab
%
%     % Example 1: (default) centered circle that just fits into image
%     phantom = ball([177,228]);
%     figure('name', 'Example 1'); showImage(phantom);
% 
%     % Example 2: shifted circle of fixed diameter, output as an array of logicals
%     settings.diameter = 100;
%     settings.center = [0,22];
%     settings.outputClass = 'logical';
%     phantom = ball([177,228], settings);
%     disp(class(phantom));
%     figure('name', 'Example 2'); showImage(phantom);
% 
%     % Example 3: ball phantom in 3D
%     phantom = ball([177,228,160]);
%     figure('name', 'Example 3'); showOrthoslices(phantom);
%
% See also CUBOID, GAUSSIAN, CREATE3DPHANTOM

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

% Default settings
defaults.center = [];
defaults.diameter = [];
defaults.outputClass = 'double';
if nargin == 0
    result = defaults;
    return
end

% Apply defaults where necessary
if nargin < 2
    settings = struct;
end
settings = completeStruct(settings, defaults);
if isempty(settings.center)
    settings.center = zeros([1,numel(N)]);
end
if isempty(settings.diameter)
    settings.diameter = min(N(:));
end

% Generate phantom
x = cell([numel(N),1]);
[x{:}] = centeredGrid(N);
xNormSq = 0;
for dim = 1:numel(x)
    xNormSq = xNormSq +(x{dim}-settings.center(dim)).^2;
end
result = cast(xNormSq <= (settings.diameter/2).^2, settings.outputClass);

end
