function siemensStar = createSiemensStar(outputSize, settings)
% CREATESIEMENSSTAR creates a Siemens star phantom.
%
%    ``siemensStar = createSiemensStar(outputSize, settings)``
% 
%
% Parameters
% ----------
% outputSize : vector or number
%     Output size of the Siemens star. If less than two dimensions are given, the
%     outputSize will be uniform in both directions (outputSize^2). 
% settings : structure, optional
%     contains additional settings, see *Other Parameters*
%
% Other Parameters 
% ---------------- 
% radius : Default = []
%     Radius of the Siemens star. If [] is given, the radius will be the distance
%     between the center and the edges of the resulting 2D array.
% numRays : Default = 10
%     Number of ray segments.
% phi : Default = 0
%     Phase shift of the restuling Siemens star.
%
% Returns 
% ------- 
% siemensStar : numerical array
%     resulting Siemens star phantom
%
% See also
% --------
% functions.generators.ball
%
% Example
% -------
%
% .. code-block:: matlab
%
%     settings = createSiemensStar;
%     settings.radius = 200;
%     settings.numRays = 20;
% 
%     phantom = createSiemensStar([500 400],settings);
%     
%     showImage(phantom)
%
% See also BALL
    
% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

defaults.radius = [];
defaults.numRays = 10;
defaults.phi = 0;

if nargin == 0
    siemensStar = defaults;
    return
end

if nargin < 2
    settings = struct;
end

settings = completeStruct(settings,defaults);

if numel(outputSize) == 2
    dimX = outputSize(2);
    dimY = outputSize(1);
else
    dimX = outputSize;
    dimY = outputSize;
end

if isempty(settings.radius)
    settings.radius = sqrt(dimX^2 + dimY^2) * 0.5;
end

x = cell([2,1]);
[x{:}] = centeredGrid([dimY dimX]);

condition1 = (sin(atan2(x{1},x{2})*settings.numRays) > 0);
condition2 = (sqrt(x{1}.^2 + x{2}.^2) < settings.radius);

siemensStar =  zeros(dimY,dimX);
siemensStar(condition1 & condition2) = exp(-1i * settings.phi);

end

