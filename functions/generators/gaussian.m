function result = gaussian(N, settings)
% GAUSSIAN generates a gaussian phantom of magnitude 1 in arbitrary dimensions.
%
%    ``result = gaussian(N, settings)``
%
% Parameters
% ----------
% N : tuple
%     tuple giving the array-dimensions of the phantom to be created
% settings : structure, optional
%     contains additional settings, see *Other Parameters*
%
% Other Parameters
% ----------------
% center : Default = []
%     Tuple giving the coordinates of the center of the gaussian. If empty, [0,...,0]
%     is assigned, which corresponds to a centered gaussian.
% sigma : Default = []
%     Tuple giving the standard deviation of the Gaussian. If empty,
%     min(N(:))/4 is assigned.
% outputClass : Default = 'double'
%     Class of the output-array.
%
% Returns
% -------
% result : numerical array
%     created gaussian phantom
%
% See also
% --------
% functions.generators.cuboid, functions.generators.ball
%
% Example
% -------
%
% .. code-block:: matlab
%
%     % Example 1: Gaussian in 2D
%     N = [256,192];
%     settings.center = [0,10];
%     settings.sigma = [10,50];
%     phantom = gaussian(N, settings);
%     figure('name', 'Example 1'); showImage(phantom)
%
%     % Example 2: Gaussian in 3D
%     phantom = gaussian([177,228,160]);
%     figure('name', 'Example 2'); showOrthoslices(phantom);
%
% See also BALL, CUBOID

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

% Default settings
defaults.center = [];
defaults.sigma = [];
if nargin == 0
    result = defaults;
    return
end

% Apply defaults where necessary
if nargin < 2
    settings = struct;
end
settings = completeStruct(settings, defaults);

if isempty(settings.center)
    settings.center = zeros([1,numel(N)]);
end
if isempty(settings.sigma)
    settings.sigma = min(N)/4 * ones([1,numel(N)]);
elseif numel(settings.sigma) == 1
    settings.sigma = settings.sigma * ones([1,numel(N)]);
end

% Generate phantom
x = cell([numel(N),1]);
[x{:}] = centeredGrid(N);
result = 1;
for dim = 1:numel(x)
    result = result .* exp((-1/(2*settings.sigma(dim).^2)) * (x{dim} + settings.center(dim)).^2);
end

end
