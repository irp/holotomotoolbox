function phantom = create3Dphantom(outputSize,settings)
% CREATE3DPHANTOM creates a 3D phantom of specified size.
%
%    ``phantom = create3Dphantom(outputSize,settings)``
% 
% Create a 3D phantom in which a specified number of objects with a specified size
% and geometry are positioned. The positions of these objects can be either
% predefined or random and also the additive strength of each object can be chosen as
% a fixed or random value
%
% Parameters
% ----------
% outputSize : tuple
%     Output size of the 3D object. If less than three dimensions are given, the
%     outputSize will be uniform in all directions (outputSize^3). Note that in case
%     a 2D vector is chosen as input, only the first value of the vector determines
%     the size of the 3D array whereas the second value will be ignored.
% settings : structure, optional
%     contains additional settings, see *Other Parameters*
%
% Other Parameters 
% ---------------- 
% sizeObjects : Default = []
%     Size of the objects which are positioned within the phantom. [] leads to a size
%     of 1/10 of the smallest dimension of the outputSize.
% distrSize : Default = 'fixed'
%     Switch between 'fixed' for fixed size or 'random' with a random size
%     distribution with maximum settings.sizeObjects and minimum
%     (settings.sizeObjects)/2.
% numObjects : Default = 10
%     Number of objects within the 3D volume.
% typeObjects : Default = 'sphere'
%     Type of the objects within the 3D volume. Choices: 'sphere', 'cube', 'cylinder'
%     (with a random orientation along the three main axes) or 'random' which results
%     in a mix of all three.
% positions : Default = []
%     Positions of the object relative to center pixel. [] leads to a random
%     distribution.
% strengthObjects : Default = 1
%     Additive strength of the objects.  
% distrStrength : Default = 'fixed'
%     Switch between 'fixed' for fixed object strength or 'random' with a random
%     strength between settings.strengthObjects and (settings.strengthObjects)/2 for
%     each object.
%
% Returns 
% ------- 
% phantom : numerical array
%     created 3D phantom
%
% See also
% --------
% functions.generators.ball
%
% Example
% -------
%
% .. code-block:: matlab
%
%     settings = create3Dphantom;
%     settings.geometry = 'sphere';
%     settings.numObjects = 30;
%     settings.strengthObjects = 1;
%     settings.distrStrength = 'random';
%     settings.sizeObjects = 50;
%     settings.distrSize = 'random';
% 
%     phantom = create3Dphantom([512 512 200],settings);
%     
%     showImage(phantom(:,:,randi(size(phantom,3))))
%
% See also BALL
    
% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

if (nargin < 2)
    settings = struct;
end

% defaults
defaults.sizeObjects = [];
defaults.distrSize = 'fixed';
defaults.numObjects = 10;
defaults.typeObjects = 'sphere';
defaults.positions = [];
defaults.strengthObjects = 1;
defaults.distrStrength = 'fixed';

% return default settings
if (nargin == 0)
    phantom = defaults;
    return
end

settings = completeStruct(settings, defaults);

if ~isempty(settings.positions) && size(settings.positions,1) < settings.numObjects
    error('Number of objects and specified positions have to be the same')
end

if numel(outputSize) < 3
    outputSize = [outputSize(1) outputSize(1) outputSize(1)];
end

if strcmp(settings.typeObjects,'random')
    types = {'sphere','cylinder','cube'};
    geometry = datasample(types,settings.numObjects);
else
    [geometry{1:settings.numObjects}] = deal(settings.typeObjects);
end

% set size of the objects to 1/10 of the minimum dimension of the output size if no
% other settings were specified
if isempty(settings.sizeObjects)
    settings.sizeObjects = floor(min(outputSize(:))/10);
end

% determine the radius for each of the objects (either random or fixed value)
if strcmp(settings.distrSize,'random')
    radius = (settings.sizeObjects - rand(settings.numObjects,1)*0.5*settings.sizeObjects)/2;
else
    radius = (repmat(settings.sizeObjects,[settings.numObjects 1]))/2;
end


% determine the additive strength for each of the objects (either random or fixed value)
if strcmp(settings.distrStrength,'random')
    delta = settings.strengthObjects - ...
        rand(settings.numObjects,1) * 0.5 * settings.strengthObjects;
else
    delta = repmat(settings.strengthObjects,[settings.numObjects 1]);
end

% determine random positions of objects within the 3D array if no positions
% were given
if isempty(settings.positions)
    settings.positions = floor((-0.5 + rand(settings.numObjects,3)) .* ...
        ((outputSize-1) - 2*max(radius)));
end

% create 3D object
phantom = zeros(outputSize,'single');

x = cell([3,1]);
[x{:}] = centeredGrid(outputSize);

% place the individual objects on their positions in the 3D volume
for objectIdx = 1:settings.numObjects
    switch geometry{objectIdx}
        case 'sphere'
            pixels = ((x{1} - settings.positions(objectIdx,1)).^2 + ...
                (x{2} - settings.positions(objectIdx,2)).^2 + ...
                (x{3} - settings.positions(objectIdx,3)).^2) <= radius(objectIdx)^2;
            
            phantom(pixels) = phantom(pixels) + delta(objectIdx);
        case 'cube'
            pixels = (abs(x{1} - settings.positions(objectIdx,1)) < radius(objectIdx)) & ...
                (abs(x{2} - settings.positions(objectIdx,2)) < radius(objectIdx)) & ...
                (abs(x{3} - settings.positions(objectIdx,3)) < radius(objectIdx));
            
            phantom(pixels) = phantom(pixels) + delta(objectIdx);
        case 'cylinder'
            % determine orientation of the cylinder
            orientation = randi([1 3]);
            index = find((1:3) ~= orientation);
            
            pixels = (((x{index(1)} - settings.positions(objectIdx,index(1))).^2 + ...
                (x{index(2)} - settings.positions(objectIdx,index(2))).^2) <= ...
                radius(objectIdx)^2) & ...
                (abs(x{orientation} - settings.positions(objectIdx,orientation)) < ...
                radius(objectIdx));
            
            phantom(pixels) = phantom(pixels) + delta(objectIdx);
        otherwise
            error('Geometry not known: possible values are sphere, cube or cylinder')
    end
end

end
