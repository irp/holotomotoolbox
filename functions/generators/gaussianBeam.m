function psi = gaussianBeam(dimX,dimY,lambda,propDistance,beamWaist)
% GAUSSIANBEAM generates the wave field of a Gaussian beam at a given propagation
% distance.
%
%    ``psi = gaussianBeam(dimX,dimY,lambda,propDistance,beamWaist)``
% 
% This function generates the complex field amplitude and phase at a plane at
% distance <propDistance> from the waist of the Gaussian beam. For a reference see
% e.g. section 3.1 in :cite:`Teich_1991_Photonics`. 
% Note that in the literature, a forward propagating plane wave is often
% defined as exp(-i*k*z+i*omega*t). Here exp(i*k*z-i*omega*t) is used as a forward
% propagating wave which leads to a switch in signs of the complex terms in the
% gaussian beam field. Note also that the amplitude is not normalized.
%
% Parameters
% ----------
% dimX : number 
%     size of the resulting wave field in x
% dimY : number 
%     size of the resulting wave field in y
% lambda : number
%     wavelength in pixels
% propDistance : number
%     propagation distance at which the Gaussian beam is determined in pixels
% beamWaist
%     1/e-decay half-width of the field amplitude at propDistance = 0 in pixels.
%
% Returns 
% ------- 
% psi : numerical array
%     propagated wave field at distance <propDistance>
%
% Example
% -------
%
% .. code-block:: matlab
%
%     psi = gaussianBeam(512,512,1,100,100);
% 
%     subplot(1,2,1)
%     showImage(abs(psi))
%     title('Amplitude of the propagated wave field')
% 
%     subplot(1,2,2)
%     showImage(angle(psi))
%     title('Phase of the propagated wave field')
    
% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

[Y,X] = centeredGrid([dimY dimX],1,true);
k = 2*pi/lambda;
rayleighLength = pi*beamWaist^2/lambda;

beamWidth = beamWaist*sqrt(1 + (propDistance/rayleighLength)^2);
radiusCurvature = propDistance + rayleighLength^2/propDistance;
phi = atan(propDistance/rayleighLength);

if propDistance == 0
    psi = 1/beamWaist * exp(-(X.^2 + Y.^2)/beamWaist^2);
else
    psi = exp(1i*k*propDistance - 1i*phi)/beamWidth * exp(-(X.^2 + Y.^2)/beamWidth^2 ...
        + 1i*k*(X.^2 + Y.^2)/(2*radiusCurvature));
end

end
