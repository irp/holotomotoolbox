function phantom = getPhantom(phantomName)
% GETPHANTOM loads in the given phantom.
%
%    ``phantom = getPhantom(phantomName)``
%
%
% Parameters
% ----------
% phantomName : string
%     name of the phantom to be loaded in; choose between:
%
%     - 'dicty' 
%     - 'grumpy'
%     - 'irp'
%     - 'kathrine'
%     - 'monk'
%     - 'world'
%
% Returns
% -------
% phantom : numerical array
%     the loaded phantom image
%
% See also
% --------
% functions.generators.getHologram
%
% Example
% -------
%
% .. code-block:: matlab
%
%     showImage(getPhantom('world'))
%
% See also GETHOLOGRAM


% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

% Get absolute path
absolutePath = fileparts(mfilename('fullpath'));

switch lower(phantomName)
    case 'dicty'
        load(fullfile(absolutePath, 'phantoms', 'phantomDicty.mat'));
    case 'grumpy'
        load(fullfile(absolutePath, 'phantoms', 'phantomGrumpy.mat'));
    case 'irp'
        load(fullfile(absolutePath, 'phantoms', 'phantomIRP.mat'));
    case 'kathrine'
        load(fullfile(absolutePath, 'phantoms', 'phantomKathrine.mat'));
    case 'monk'
        load(fullfile(absolutePath, 'phantoms', 'phantomMonk.mat'));
    case 'world'
        load(fullfile(absolutePath, 'phantoms', 'phantomWorld.mat'));
    otherwise
        error('Phantom does not exist!')
end

end

