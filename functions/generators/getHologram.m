function output = getHologram(hologramName)
% GETHOLOGRAM loads in the given hologram data and corresponding scan information.
%
%    ``output = getHologram(hologramName)``
%
%
% Parameters
% ----------
% hologramName : string
%     name of the holograms to be loaded; choose between:
%
%     - 'macrophage' 
%          recorded in the holographic regime with monochromatic illumination and 4
%          propagation distances; includes a support
%     - 'radiodurans'
%          recorded in the holographic regime with monochromatic illumination and 4
%          propagation distances; includes a support
%     - 'spider' 
%          recorded in the direct-contrast regime with polychromatic illumination and
%          a single propagation distance
%     - 'world'
%          recorded in the holographic regime with monochromatic illumination and a
%          single propagation distance; includes a support
%     - 'beads'
%          recorded in the holographic regime with monochromatic illumination and a
%          4 propagation distances
%
% Returns
% -------
% output : structure
%     structure containing the following entries:
%
%     - holograms
%         numerical array containing the measured hologram(s). In the case of multiple holograms,
%         the array is 3D, where the defocus distance varies along the third dimension  
%     - fresnelNumbers 
%         matrix of pixel size-based Fresnel numbers, at which the holograms were acquired: the
%         columns correspond to the different defocus distances. If the array has two rows, the
%         rows contain possibly distinct Fresnel numbers along the  two dimensions of the holograms
%         (astigmatism).
%     - support (optional)
%         if avaialble: numerical array containing a suitable support for the measured sample
%
% See also
% --------
% functions.generators.getPhantom
%
% Example
% -------
%
% .. code-block:: matlab
%
%     macrophage = getHologram('macrophage')
%
%     showImage(macrophage.holograms{1})
%
% See also GETPHANTOM

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

absolutePath = fileparts(mfilename('fullpath'));

switch lower(hologramName)
    case 'macrophage'
        output = load(fullfile(absolutePath,'holograms','hologramsMacrophage.mat'));
    case 'radiodurans'
        output = load(fullfile(absolutePath,'holograms','hologramsRadiodurans.mat'));
    case 'spider'
        output = load(fullfile(absolutePath,'holograms','hologramsSpiderDirectContrast.mat'));
    case 'world'
        output = load(fullfile(absolutePath,'holograms','hologramsWorld.mat'));
    case 'beads'
        output = load(fullfile(absolutePath,'holograms','hologramsBeads.mat'));
    otherwise
        error('Hologram does not exist!')
end
end
