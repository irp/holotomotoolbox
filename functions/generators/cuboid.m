function result = cuboid(N, settings)

% CUBOID generates a cuboid-phantom (box) of magnitude 1 in arbitrary dimensions.
%
%    ``result = cuboid(N, settings)``
%
% Parameters
% ----------
% N : tuple
%     tuple giving the array-dimensions of the phantom to be created
% settings : structure, optional
%     contains additional settings, see *Other Parameters*
%
% Other Parameters
% ----------------
% center : Default = []
%     Tuple giving the coordinates of the center of the cuboid in pixels. If empty, [0,...,0] is
%     assigned, which corresponds to a centered cuboid.
% aspectLength : Default = []
%     Tuple giving the aspect length of the cuboid in pixels. If empty, ceil(N/2) is assigned.
% outputClass : Default = 'double'
%     Class of the output-array.
%
% Returns
% -------
% result : numerical array
%     created cuboid phantom 
%
% See also
% --------
% functions.generators.ball, functions.generators.gaussian
%
% Example
% -------
%
% .. code-block:: matlab
%
%     % Example 1: (default) centered rectangle of aspect length half the image size
%     phantom = cuboid([177,228]);
%     figure('name', 'Example 1'); showImage(phantom);
% 
%     % Example 2: shifted rectangle of fixed diameter, output as an array of logicals
%     settings.aspectLength = [100,42];
%     settings.center = [-27,22];
%     settings.outputClass = 'logical';
%     phantom = cuboid([177,228], settings);
%     disp(class(phantom));
%     figure('name', 'Example 2'); showImage(phantom);
% 
%     % Example 3: cuboid phantom in 3D
%     phantom = cuboid([167,134,190]);
%     figure('name', 'Example 3'); showOrthoslices(phantom);
%
% See also BALL, GAUSSIAN

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

% Default settings
defaults.center = [];
defaults.aspectLength = [];
defaults.outputClass = 'double';
if nargin == 0
    result = defaults;
    return;
end

% Apply defaults where necessary
if nargin < 2
    settings = struct;
end
settings = completeStruct(settings, defaults);
if isempty(settings.center)
    settings.center = zeros([1,numel(N)]);
end
if isempty(settings.aspectLength)
    settings.aspectLength = ceil(N/2);
end
settings.aspectLength = settings.aspectLength .* ones([1,numel(N)]);

% Generate phantom
%
% Create box of ones
result = ones(settings.aspectLength, settings.outputClass);
%
% Pad to have the cuboid embedded in zeros
padAmount = N-settings.aspectLength;
padPre = min(max(ceil(padAmount/2 - settings.center), 0), padAmount);
padPost = padAmount - padPre;
result = padarray(padarray(result, padPre, 0, 'pre'), padPost, 0, 'post');

end
