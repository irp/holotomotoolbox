function hologram = simulateHologram(phaseImage, fresnelNumbers, settings)
% SIMULATEHOLOGRAM simulates a hologram using either a single image 
% for pure phase objects with zero absorption, or a single image and a
% given ratio between phase shift and absorption for a single material
% sample with nonzero absorption, or two images, one modelling the
% projected phase shift and the second image modelling the projected
% absorption.
% 
%    ``hologram = simulateHologram(phaseImage, fresnelNumbers, settings)``
%
% For the simulation a fully coherent plane wave illumination is assumed.
%
% Parameters
% ----------
% phaseImage : numerical array
%     numerical array (2D image) used to simulate the projected phase shift of an
%     object defined as the negative integral over the the phase shifting component
%     delta of the refractive index along the direction of propagation through the
%     object; only 2D arrays with negative values are accepted
% fresnelNumbers : numerical array
%     pixel size-based Fresnel number(s) of the holograms to be simulated: the columns correspond
%     to possibly multiple propagation distances. If the array has more than one row, the rows are
%     interpreted as possibly distinct Fresnel numbers along the the different dimensions of the
%     holograms (astigmatism).
% settings : structure, optional
%     contains additional settings, see *Other Parameters*
%
% Other Parameters
% ----------------
% betaDeltaRatio : Default = 0
%     Fixed ratio between absorption and phase shifts. 
%     The value 0 corresponds to assuming a pure phase object.
% absorptionImage : Default = []
%     Numerical array (2D image) used to simulate the projected
%     absorption of an object defined as the negative integral over the
%     the absorption component beta of the refarctive index along the direction of
%     propagation through the object. Only 2D arrays with negative values
%     are accepted.
%     If left empty, the projected absorption is calculated by settings.betaDeltaRatio * phaseImage.
% simulateLinear : Default = false
%     Simulate using the linear or nonlinear model of x-ray phase-contrast imaging
% propagateSettings : Default = struct
%     Settings used for the Fresnel-propagation. See documentation of function
%     fresnelPropagate for details.
%
% Returns
% -------
% hologram : numerical array
%     simulated hologram(s)
%
% Example
% -------
%
% .. code-block:: matlab
%
%     % Simulate linear and nonlinear holograms for dicty phantom
%     fresnelNumbers = 1e-3;
%     phaseImage = -getPhantom('dicty');
%     settings = simulateHologram;
%     figure('name','Phase image'); showImage(phaseImage);
%
%     hologram = simulateHologram(phaseImage, fresnelNumbers, settings);
%     figure('name','Nonlinearly simulated hologram'); showImage(hologram);
%
%     settings.simulateLinear = true;
%     hologramLin = simulateHologram(phaseImage, fresnelNumbers, settings);
%     figure('name','Linearly simulated hologram'); showImage(hologramLin);
%
%
%     % Simulate hologram from an absorption- and a phase-image
%     fresnelNumbers = 1e-3;
%     %
%     % load a phase image
%     phaseImage = -getPhantom('kathrine');
%     figure('name','Phase image'); showImage(phaseImage);
%     
%     % load an absorption image
%     absorptionImage = -getPhantom('monk');
%     figure('name','Absorption image');showImage(absorptionImage);
%
%     % specify settings
%     settings = simulateHologram;
%     settings.absorptionImage = absorptionImage; 
%
%     % simulate hologram
%     hologram = simulateHologram(phaseImage, fresnelNumbers, settings);
%     figure('name','hologram'); showImage(hologram);


% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

% Default parameters
defaults.betaDeltaRatio = 0;
defaults.absorptionImage = [];
defaults.simulateLinear = false;
defaults.propagateSettings = struct;

% Return default parameters if called without argument
if (nargin == 0)
    hologram = defaults;
    return;
end

% Complete settings with default parameters
if nargin < 3
    settings = struct;
end
settings = completeStruct(settings, defaults);

% Make sure that phaseImage only contains negative values
if  numel(find(phaseImage > 0)) > 0
    warning('Please use only images containing negative values for phaseImage.')
end

% Construct mixed phase- and absorption-image
if numel(settings.absorptionImage) == 0
    phaseAbsImage = (settings.betaDeltaRatio + 1i) * phaseImage;
else
    % Make sure that absorptionImage only contains negative values
    if  numel(find(settings.absorptionImage > 0)) > 0
        warning('Please use only images containing negative values for absorptionImage.')
    end

    phaseAbsImage = 1i * phaseImage + settings.absorptionImage;
end

% Simulate hologram using the linear or nonlinear model of X-ray phase contrast
if settings.simulateLinear
    hologram = 1 + 2*real(fresnelPropagate(phaseAbsImage, fresnelNumbers, settings.propagateSettings));
else
    hologram = abs(fresnelPropagate(exp(phaseAbsImage), fresnelNumbers, settings.propagateSettings)).^2;
end

end
