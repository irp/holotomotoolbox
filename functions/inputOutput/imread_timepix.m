function result = imread_timepix(filename, settings)
% IMREAD_TIMEPIX reads in images acquired by timepix detector.
%
%    ``result = imread_timepix(filename, settings)`` 
%
% Parameters
% ----------
% filename : string
%     full filename (and path) of the \*.raw-file to be read
% settings : structure, optional
%     contains additional settings, see *Other Parameters*
%
% Other Parameters
% ----------------
% gaps : Default = true
%     Should gaps between detector arrays be included in image or cropped? true: gaps
%     are shown, false: pixels within gaps are removed
% interpolate : false
%     Should values of pixels within these gaps be linearly interpolated? true: pixel
%     values are linearly interpolated between the edge values, false: pixels are set
%     to zero
% 
% Returns
% -------
% result : numerical array
%     acquired image 
%

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

defaults.gaps = true;
defaults.interpolate = false;

if (nargin == 0)
    result=defaults;
    return
end

if (nargin < 2)
    settings = struct;
end

settings = completeStruct(settings, defaults);

fid = fopen(filename);
raw = fread(fid, [768 512], 'uint16', 0, 'l');
fclose(fid);

% TODO: mask bad pixels
if settings.gaps == true
    gap_image = zeros(768+2*3,512+4);
    gap_image(1:256,1:256)             = raw(1:256,1:256);
    gap_image(257+3:512+3,1:256)       = raw(257:512,1:256);
    gap_image(513+6:768+6,1:256)       = raw(513:768,1:256);
    gap_image(1:256,257+4:512+4)       = raw(1:256,257:512);
    gap_image(257+3:512+3,257+4:512+4) = raw(257:512,257:512);
    gap_image(513+6:768+6,257+4:512+4) = raw(513:768,257:512);
    if settings.interpolate == true
        gap_image([1:256, 257+3:512+3, 513+6:768+6],257) = 0.8*raw(:,256)+0.2*raw(:,257);
        gap_image([1:256, 257+3:512+3, 513+6:768+6],258) = 0.6*raw(:,256)+0.4*raw(:,257);
        gap_image([1:256, 257+3:512+3, 513+6:768+6],259) = 0.4*raw(:,256)+0.6*raw(:,257);
        gap_image([1:256, 257+3:512+3, 513+6:768+6],260) = 0.2*raw(:,256)+0.8*raw(:,257);
        
        gap_image(257,:) = 0.75*gap_image(256,:)+0.25*gap_image(260,:);
        gap_image(258,:) = 0.50*gap_image(256,:)+0.50*gap_image(260,:);
        gap_image(259,:) = 0.25*gap_image(256,:)+0.75*gap_image(260,:);
        gap_image(516,:) = 0.75*gap_image(515,:)+0.25*gap_image(519,:);
        gap_image(517,:) = 0.50*gap_image(515,:)+0.50*gap_image(519,:);
        gap_image(518,:) = 0.25*gap_image(515,:)+0.75*gap_image(519,:);
    end
else
    gap_image = raw;
end

result = gap_image;

end

