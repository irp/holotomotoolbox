function array = readraw(filename, data_type, array_size)
% READRAW reads a numerical array from a \*.raw-file.
%
%    ``array = readraw(filename, data_type, array_size)``
% 
% Parameters
% ----------
% fileprefix : string
%     full filename (and path) of the \*.raw-file to read from
% data_type : string
%     string specifying the data-type of the numerical data in the \*.raw-file, e.g.,
%     'double', 'single', 'int', 'uint8', ...
% array_size : vector
%     tuple of integers specifying the size of the numerical array to be read
% 
% Returns
% -------
% array : numerical array
%     array read from the \*.raw-file
%
% See also
% --------
% functions.inputOutput.saveraw
%
% Example
% -------
%
% .. code-block:: matlab
%
%     N = [73, 126, 84];
%     data_type = 'single';
%     array = rand(N, data_type);
%     filename = saveraw(array, 'example');
%     array_saved_read = readraw(filename, data_type, N);
%     assert(norm(array(:)-array_saved_read(:)) < 1e-6*norm(array(:)));
%
%
% See also SAVERAW

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

fid = fopen(filename,'r');
array = reshape(fread(fid, prod(array_size), ['*', data_type]), array_size);
fclose(fid);

end


