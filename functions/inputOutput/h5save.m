function h5save(filename, dataset, data)
% H5SAVE is a convenience function to easily save a dataset into a HDF5
% file. It mimics the usability of SAVERAW.
%
% Notes
% ------
% - Compatible data, i.e. same data type and size, will be overwritten 
%   without warning!
% - Changes to data type, size, etc. cannot be made with H5SAVE.
% - Only 'bulk saving', i.e. of full datasets, is supported. To write
%   slices of data, see H5WRITE.
%
%
% Parameters
% ----------
% Filename : 
%   File name and/or path to HDF5 to create or write to. If file does not
%   exists, it will be created.
% Dataset :
%   Path of dataset, i.e. name of dataset, within HDF5 file.
% Data:
%   Data to write.
% 
%
% Example
% -------
% .. code-block:: matlab
%   
%   % simple storing of array
%   data = ones(12, 4);
%   h5save("test.h5", "/data", data);
%   h5info("test.h5", "/data")
%
%   % update stored array (only possible for same data type and size)
%   data = zeros(size(data));
%   h5save("test.h5", "/data", data);
%
%   % read dataset from hdf5
%   data_h5 = h5read("test.h5", "/data");
%
% See also H5WRITE, H5CREATE, SAVERAW

% HoloTomoToolbox
% Copyright (C) 2023  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

    % check if target File and Dataset exists and allocate if not
    if ~h5isdataset(filename, dataset)
       h5create(filename, dataset, size(data), "Datatype", class(data));
    end

    % write (or overwrite, if Data is compatible) data in hdf5 file
    % If incompatilbe data is to be written, an exception is raised.
    h5write(filename, dataset, data);
end


%%% private functions %%%

% quick-n-dirty function to check if dataset exists within a file
function isds = h5isdataset(filename, dataset)
    if ~isfile(filename)
        isds = false;
    else
        try
            info = h5info(filename, dataset);
            isds = true;
        catch
            isds = false;
        end
    end
end