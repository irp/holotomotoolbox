function data = imread_itex(filename)
% IMREAD_ITEX reads in \*.img files saved by Hamamatsu detector software.
%
%    ``data = imread_itex(filename)`` 
%
% This function reads the header of Hamamatsu's \*.img files (ITEX) according to
% documentation.
%
% Parameters
% ----------
% filename : string
%     full filename (and path) of the \*.img-file to be read
% 
% Returns
% -------
% data : numerical array
%     acquired image 
%

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

fid = fopen(filename);

% read magic number
magic = fread(fid, 2, 'int8=>char');

if strcmp(magic,'IM')
    error('No ITEX file!');
end

% read header size
hsize = fread(fid, 1, 'uint16', 0, 'l');

% read some important header fields
% all numbers are little endian
imgheight = fread(fid, 1, 'uint16', 0, 'l');
imgwidth = fread(fid, 1, 'uint16', 0, 'l');
offsetx = fread(fid, 1, 'uint16', 0, 'l');
offsety = fread(fid, 1, 'uint16', 0, 'l');
filetype = fread(fid, 1, 'uint16', 0, 'l');

if filetype ~= 2
    error('Unknown filetype');
end

% skip rest of header (metadata)
fseek(fid, hsize+64, -1);

data = fread(fid, [imgheight imgwidth], 'uint16', 0, 'l');

fclose(fid);
end
