function result = readSpec(file, numScan, numCols)
% READSPEC reads information from spec file.
%
%    ``result = readSpec(file, scan_number, numCols)``
% 
% This function reads data from a specific scan as given in the spec file. The column
% in which the data of interest is given can be specified.
%
% Parameters
% ----------
% file : string
%     full name (and path) of the spec file in which the information of interest is stored
% numScan : number
%     number of the scan of interest
% numCols : number
%     number of the column in which the information of interest is given
%
% Returns 
% ------- 
% result : numerical array
%     data extracted from spec file
%
    
% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

if(nargin < 2)
    % find available scans in specified file
    list_scans(file)
    return
end

if (nargin < 3)
    % this used to set the default: numCols = 1;
    list_scan_variables(file, numScan)
    return
end

lineStart = -1;
lineEnd = -1;

scanString = sprintf('#S %01i',numScan);

fid = fopen(file,'r');

lineNumber = 0;

while ~feof(fid)
    tline = fgetl(fid);
    if (startsWith(tline, scanString))
        break;
    end
    lineNumber = lineNumber + 1;
end

while ~feof(fid)
    tline = fgetl(fid);
    lineNumber = lineNumber + 1;
    if (~startsWith(tline, '#'))
        lineStart = lineNumber;
        break;
    end
end

while ~feof(fid)
    tline = fgetl(fid);
    lineNumber = lineNumber + 1;
    if (isempty(tline) || startsWith(tline, '#'))
        lineEnd = lineNumber - 1;
        break;
    end
end
fclose(fid);

if (lineStart < 0 || lineEnd < 0)
    error('Scan not found')
end

result = dlmread(file,' ',[lineStart numCols-1 lineEnd numCols-1]);

end

function list_scans(file)
    scansFound = {};
    fid = fopen(file,'r');

    scanString = '#S';
    while ~feof(fid)
        tline = fgetl(fid);
        if (startsWith(tline, scanString))
            scansFound = [scansFound, tline]; %#ok<AGROW>
        end
    end
    fclose(fid);
    if numel(scansFound) > 0
        fprintf('scans found in file "%s":\n', file)
        fprintf('  %s\n', scansFound{:});
    else
        fprintf('no scans found\n')
    end
end

function list_scan_variables(file, numScan)
    fid = fopen(file,'r');

    scanString = sprintf('#S %01i',numScan);

    % find scan
    while ~feof(fid)
        tline = fgetl(fid);
        if (startsWith(tline, scanString))
            break
        end
    end

    while ~feof(fid)
        tline = fgetl(fid);
        if (startsWith(tline, '#S'))
            break % no variable found
        elseif (startsWith(tline, '#L'))
            disp('available columns:')
            vars = split(tline);
            for i=2:numel(vars)
                fprintf('%02d %s\n', i-1, vars{i})
            end
            return
        end
    end

    error('no variables found for scan %01i in file "%s"', numScan, file)
end
