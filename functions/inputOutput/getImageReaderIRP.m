function result = getImageReaderIRP(detectorName, settings)
% GETIMAGEREADERIRP creates function handle for reading in data.
%
%    ``result = getImageReaderIRP(detectorName, settings)``
%
% Parameters
% ----------
% detectorName : string
%     name of the detector used for imaging; note that these names are IRP internal
%     and not the official names of the detectors
% settings : structure, optional
%     contains additional settings needed only when using the timepix detector, see
%     *Other Parameters*
%
% Other Parameters
% ----------------
% gaps : Default = true
%     Should gaps between detector arrays be included in image or cropped? true: gaps
%     are shown, false: pixels within gaps are removed
% interpolate : Default = false
%     Should values of pixels within these gaps be linearly interpolated? true: pixel
%     values are linearly interpolated between the edge values, false: pixels are set
%     to zero
%
% Returns
% -------
% result : function handle
%     function handle for reading in data acquired by the given detector and applying
%     necessary transformation so that displayed images always 'look with the x-ray
%     beam'
%

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

switch lower(detectorName)
    %  case {'pilatus','p100','p300','p6m','p1m'}
    %      frame = cbfread(file_name);
    %      image = rot90(frame.data,1);
    %      header = frame.header;
    %      image = flipud(image);
    case 'argos'
        result = @(fileName) (fliplr(single(imread(fileName))));
    case 'talos'
        result = @(fileName) (fliplr(rot90(single(imread(fileName)),-1)));
    case 'pirra'
        % PhotonicScience sCMOS 2kx2k has 2160 x 2048 pixels
        result = @(fileName) (croparray(single(imread(fileName)), [0, 58] , [0, 54]));
    case 'thyia'
        result = @(fileName) (fliplr(single(imread(fileName))));
    case 'iris'
        result = @(fileName) (rot90(single(imread_itex(fileName)),2));
    case 'zyla' % andor zyla
        result = @(fileName) (fliplr(flipud(single(imread(fileName)))));
    case 'pco' % pco2000
        result = @(fileName) (single(imread(fileName)));
    case 'timepix'
        if (nargin > 1)
            result = @(fileName) (rot90(single(imread_timepix(fileName, settings))));
        else
            result = @(fileName) (rot90(single(imread_timepix(fileName)), 3));
        end
    case 'ccd'
        result = @(fileName) (flipud(imread_ccd(fileName)));
    case 'tp' % timepix from sandy, controlled by asiray
        result = @(fileName) (fliplr(rot90(single(imread(fileName)))));
    otherwise
        error(sprintf('detector %s not recognized', detectorName));
end
end
