function result = wwas(path,motor)
% WWAS reads information about acquired image from dump file.
%
%    ``result = wwas(path,motor)``
% 
% Parameters
% ----------
% path : string
%     name and path of the dump file
% motor : string, optional
%     motor of which the position should be read from dump file; if no motor is
%     given, all information given in the dump file is considered
% 
% Returns
% -------
% result : structure array
%     structure array containing information about the acquired image, as, e.g.,
%     motor positions or exposure time
%

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

if (nargin < 1)
    error('Not enough arguments. Usage: wwas(path, motor(optional))');
end

filename = path;

if (~exist(filename,'file'))
    error (['File: ',filename,' does not exist']);
end

fid = fopen(filename,'r');

while ~feof(fid)
        tline = fgetl(fid);
    if (startsWith(tline, '#') || strncmp(tline, ' pandora_gain',1))
        % read header
        tmp = strsplit(tline,' ');
        
        if (isempty(tmp{end}))
            val = '-1';
        else
            val = tmp{end};
        end
        
        % get time and att, ignore the rest
        switch (lower(tmp{end-1}))
            case 'time'
                motors.('time') = str2double(val);
            case 'att'
                motors.('att') = str2double(val);
        end
    else
        % read the content
        tmp = strsplit(tline,' ');    
        motors.(tmp{1}) = str2double(tmp{2});
    end
    
end
fclose(fid);

if nargin == 2
    result = motors.(motor);
else
    result = motors;
end
end
