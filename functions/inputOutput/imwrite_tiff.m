function imwrite_tiff(data,filename, dataType)
%IMWRITE_TIFF saves given data as \*.tif.
%
%    ``imwrite_tiff(data,filename)``
% 
% Parameters
% ----------
% data : 2d numerical array
%     data to save
% filename : string
%     filename of the image to create
%
% See also
% --------
% functions.inputOutput.saveraw
%
% Notes
% -----
% `data` is saved as a single-precision TIFF file.
%
% See also SAVERAW

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

if ~endsWith(filename,'.tif','IgnoreCase',true) && ~endsWith(filename,'.tiff','IgnoreCase',true)
    filename = [filename , '.tif'];
end
if nargin < 3
    dataType = 'single';
end

ttag.ImageLength = size(data,1);
ttag.ImageWidth = size(data,2);
ttag.Photometric = Tiff.Photometric.MinIsBlack;
ttag.PlanarConfiguration = Tiff.PlanarConfiguration.Chunky;
ttag.BitsPerSample = 32;
ttag.SamplesPerPixel = 1;

switch dataType
    case 'single'
        ttag.SampleFormat = Tiff.SampleFormat.IEEEFP;
    case 'uint32'
        ttag.SampleFormat = Tiff.SampleFormat.UInt;
end

ttag.Software = 'MATLAB';

t = Tiff(filename,'w');
t.setTag(ttag);
t.write(cast(data, dataType));
t.close();

end

