function image = imread_ccd(filename)
% IMREAD_CCD reads in \*.spe files saved by WinWiew/32.
%
%    ``data = imread_ccd(filename)``
%
% This function reads the header of WinView/32's \*.spe files (CCD) according to
% documentation.
%
% Parameters
% ----------
% filename : string
%     full filename (and path) of the \*.spe-file to be read
%
% Returns
% -------
% image : numerical array
%     acquired image
%

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

fid = fopen(filename,'r','l');
if fid > 0
    % get datatype and dimensions
    fseek(fid, 42, 'bof');
    xdim = fread(fid, 1, 'uint16');  %   42  actual # of pixels on x axis

    fseek(fid, 108, 'bof');
    datatype = fread(fid, 1, 'uint16');  % 108  experiment datatype
                                         %       0 =   FLOATING POINT
                                         %       1 =   LONG INTEGER
                                         %       2 =   INTEGER
                                         %       3 =   UNSIGNED INTEGER

    fseek(fid, 656, 'bof');
    ydim = fread(fid, 1, 'uint16');  %  656  y dimension of raw data.

    fseek(fid, 1446, 'bof');
    NumFrames = fread(fid, 1, 'uint16');  % 1446  number of frames in file.

    % goto data
    fseek(fid, 4100, 'bof');
    switch datatype
        case 0  % FLOATING POINT (4 bytes / 32 bits)
            image = double(reshape(fread(fid,inf,'float32=>float32'),...
                                   xdim, ydim, NumFrames));
        case 1  % LONG INTEGER (4 bytes / 32 bits)
            image = double(reshape(fread(fid,inf,'int32=>int32'),...
                                   xdim, ydim, NumFrames));
        case 2  % INTEGER (2 bytes / 16 bits)
            image = double(reshape(fread(fid,inf,'int16=>int16'),...
                                   xdim, ydim, NumFrames));
        case 3  % UNSIGNED INTEGER (2 bytes / 16 bits)
            image = double(reshape(fread(fid,inf,'uint16=>uint16'),...
                                   xdim, ydim, NumFrames));
    end
    fclose(fid);

    %permute the X and Y dimensions so that an image looks like in Winview
    image = permute(image,[2,1,3]);

else
    disp(['File not found - ' filename]);
    a = -1;
end%if

end
