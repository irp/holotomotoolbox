function filename = saveraw(array, fileprefix)
% SAVERAW saves a numerical array as a \*.raw-file.
%
%    ``filename = saveraw(array, fileprefix)``
% 
% Parameters
% ----------
% array : numerical array
%     numerical array to be saved
% fileprefix : string
%     first part of the filename (and path) of the output-file to to be created
%     (WITHOUT suffix ".raw'); information on the data-type and size of the array
%     will be appended automatically
% 
% Returns
% -------
% filename : string
%     full filename (and path) of the created output-file (including
%     meta-information)
%
% See also
% --------
% functions.inputOutput.readraw
%
% Example
% -------
%
% .. code-block:: matlab
%
%     N = [73, 126, 84];
%     data_type = 'single';
%     array = rand(N, type);
%     filename = saveraw(array, 'example');
%     array_saved_read = readraw(filename, data_type, N);
%     assert(norm(array(:)-array_saved_read(:)) < 1e-6*norm(array(:)));
% 
% See also READRAW

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

% Check input
if ~isnumeric(array) || ~isreal(array)
    error('Only real-valued numerical arrays can be saved as raw.');
end

% Make complete filename, including size and type of the input array
data_type = class(array);
N = size(array);
filename = [fileprefix, '_type=', data_type, '_size=', size2str(N), '.raw'];

% Write raw
fid = fopen(filename, 'w+');
fwrite(fid, array, data_type);
fclose(fid);

end

function sizeStr = size2str(N)

sizeStr = num2str(N(1));
for dim = 2:numel(N);
    sizeStr = [sizeStr, 'x', num2str(N(dim))];
end

end

