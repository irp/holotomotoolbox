function phase = phaserec_gpm(hologram, fresnelNumber, settings)
%PHASEREC_GPM reconstructs the phase with the "Generalized Paganin Method" for homogeneous objects.
%
%    ``phase = phaserec_gpm(hologram, fresnelNumber, settings)``
%
% Reconstructed the phase in the object plane using the generalized paganin method proposed by Paganin et al. arXiv:2005.03660. It is valid for
% short propagation distances and homogeneous objects.
%
% Parameters
% ----------
% hologram : numerical array
%     hologram to reconstruct from
% fresnelNumber : number
%     pixel size-based Fresnel number
% settings : structure, optional
%     contains additional settings, see *Other Parameters*
%
% Other Parameters
% ----------------
% betaDeltaRatio : Default = 0.01
%     Beta/Delta ratio
% padx : Default = 0
%     Amount by which the hologram is symmetrically (replicate)-padded along the
%     x-dimension before reconstruction
% pady : Default = 0
%     Amount by which the hologram is symmetrically (replicate)-padded along the 
%     y-dimension before reconstruction
%
% Returns
% -------
% phase : numerical array
%     The reconstructed phase image of the same size as the input hologram
%
%
% See also
% --------
% functions.phaseRetrieval.directContrast.phaserec_mba,
% functions.phaseRetrieval.directContrast.phaserec_smo
%
% Example
% -------
%
% .. code-block:: matlab
%
%     out = getHologram('spider');
% 
%     smosettings = phaserec_gpm;
%     smosettings.padx = 100;
%     smosettings.pady = 100;
%     smosettings.betaDeltaRatio = 0.015;
% 
%     projReconstructed = phaserec_gpm(out.holograms, out.fresnelNumbers, smosettings);
% 
%     showImage(projReconstructed);
% 
% 
% See also PHASEREC_MBA, PHASEREC_SMO

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

defaults.padx = 0;
defaults.pady = 0;
defaults.betaDeltaRatio = 0.01;

if (nargin == 0)
    phase = defaults;
    return
end

if (nargin < 3)
    settings = struct;
end

settings = completeStruct(settings, defaults);

if (settings.betaDeltaRatio <= 0)
    error('settings.betaDeltaRatio <= 0 given')
end

if (fresnelNumber <= 0)
    error('fresnelNumber <= 0 given')
end

% optional paddding
hologramPadded = padarray(hologram, [settings.pady settings.padx], 'replicate');

% multplicative factor in Fourier space that correspond to the Laplacian in real space
[xi_y, xi_x] = fftfreq(size(hologramPadded));

% regularization of the Laplacian at zero frequency is determined by Fresnel number and absorption
reg_alpha = 4 * pi * fresnelNumber * settings.betaDeltaRatio;

% apply inverse filter to reconstruct the phase
filterKernel = reg_alpha ./ (reg_alpha - 2 * (cos(xi_x) + cos(xi_y) - 2) );
phase = 1/(2 * settings.betaDeltaRatio) * log(real(ifft2(fft2(hologramPadded).*filterKernel)));

% undo padding 
phase = croparray(phase, [settings.pady, settings.padx]);   
   
end

