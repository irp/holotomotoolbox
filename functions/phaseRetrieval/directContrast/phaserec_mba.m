function phase = phaserec_mba(hologram, fresnelNumber, settings)
%PHASEREC_MBA reconstructs the phase based on the Modified Bronnikov Algorithm.
%
%    ``phase = phaserec_mba(hologram, fresnelNumber, settings)``
%
% Reconstructed the phase in the object plane using the Modified Bronnikov Algorithm
% (MBA) proposed by Groso et al. :cite:`Groso_APL_2006,Groso_OE_2006`. It is valid for short propagation
% distances and pure phase objects.
%
% Parameters
% ----------
% hologram : numerical array
%     hologram to reconstruct from
% fresnelNumber : number
%     pixel size-based Fresnel number
% settings : structure, optional
%     contains additional settings, see *Other Parameters*
%
% Other Parameters
% ----------------
% reg_alpha : Default = 0.01
%     Regularisation Parameter alpha
% padx : Default = 0
%     Amount by which the hologram is symmetrically (replicate)-padded along the
%     x-dimension before reconstruction
% pady : Default = 0
%     Amount by which the hologram is symmetrically (replicate)-padded along the 
%     y-dimension before reconstruction
%
% Returns
% -------
% phase : numerical array
%     The reconstructed phase image of the same size as the input hologram
%
%
% See also
% --------
% functions.phaseRetrieval.directContrast.phaserec_smo,
% functions.phaseRetrieval.directContrast.phaserec_bac
%
% Example
% -------
%
% .. code-block:: matlab
%
%     out = getHologram('spider');
% 
%     mbasettings = phaserec_mba;
%     mbasettings.padx = 100;
%     mbasettings.pady = 100;
%     mbasettings.reg_alpha = 0.04;
% 
%     projReconstructed = phaserec_mba(out.holograms, out.fresnelNumbers, mbasettings);
% 
%     showImage(projReconstructed)
%
% 
% See also PHASEREC_SMO, PHASEREC_BAC

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

defaults.padx = 0;
defaults.pady = 0;
defaults.reg_alpha = 0.01;

if (nargin == 0)
    phase = defaults;
    return
end

if (nargin < 3)
    settings = struct;
end

settings = completeStruct(settings, defaults);

% optional padding
hologramPadded = padarray(hologram,[settings.pady settings.padx],'replicate');

% multplicative factor in Fourier space that correspond to the Laplacian in real space
laplaceKernel = -fftfreqNormSq(size(hologramPadded));

% make (regularized) inverse Laplace filter kernel
if (settings.reg_alpha == 0)
        % avoid division by 0
        laplaceKernel(0,0) = 1;
        filterKernel = 1 ./ (-laplaceKernel);
        % set zero frequency (singularity) to 0
        filterKernel(0,0) = 0;
else
        filterKernel = 1 ./ (-laplaceKernel + settings.reg_alpha);
end


% apply inversion filter kernel to reconstruct the phase
phase = (2 * pi * fresnelNumber) * real(ifft2(fft2(hologramPadded-1).*filterKernel));

% undo padding
phase = croparray(phase, [settings.pady, settings.padx]);   

end

