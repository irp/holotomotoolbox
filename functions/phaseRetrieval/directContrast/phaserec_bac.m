function result = phaserec_bac(im1, settings)
%PHASEREC_BAC applies the Bronnikov Aided Correction algorithm.
%
%    ``result = phaserec_bac(im1, settings)``
%
% Reconstructs the negative logarithm of the intensity distribution in the object
% plane using the Bronnikov Aided Correction (BAC) algorithm proposed by de Witte et
% al. :cite:`Witte_JOSAA_2009`. It is valid for short propagation distances and
% weakly absorbing objects.
%
%
% Parameters
% ----------
% im1 : numerical array
%     hologram to reconstruct from
% settings : structure, optional
%     contains additional settings, see *Other Parameters*
%
% Other Parameters
% ----------------
% reg_alpha : Default = []
%     Regularisation Parameter alpha. If reg_alpha is [], a guess for this parameter
%     is obtained automatically based on the strength of the edges in the image.
% reg_gamma : Default = 1
%     Regularisation Parameter gamma
% padx : Default = 0
%     Amount by which the hologram is symmetrically (replicate)-padded along the
%     x-dimension before reconstruction
% pady : Default = 0
%     Amount by which the hologram is symmetrically (replicate)-padded along the 
%     y-dimension before reconstruction
%
% Returns
% -------
% result : numerical array
%     The phase-corrected image of the same size as the input hologram
%
%
% See also
% --------
% functions.phaseRetrieval.directContrast.phaserec_smo,
% functions.phaseRetrieval.directContrast.phaserec_mba
%
% Example
% -------
%
% .. code-block:: matlab
%
%     out = getHologram('spider');
% 
%     bacsettings = phaserec_bac;
%     bacsettings.padx = 100;
%     bacsettings.pady = 100;
%     bacsettings.reg_alpha = [];
%     bacsettings.reg_gamma = 1;
% 
%     projReconstructed = phaserec_bac(out.holograms, bacsettings);
% 
%     showImage(projReconstructed)
%
% 
% See also PHASEREC_SMO, PHASEREC_MBA

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

defaults.padx = 0;
defaults.pady = 0;
defaults.reg_alpha = [];
defaults.reg_gamma = 1.0;

if (nargin == 0)
    result = defaults;
    return
end

if (nargin < 2)
    settings = struct;
end

settings = completeStruct(settings, defaults);

if (isempty(settings.reg_alpha))
    settings.reg_alpha = guessRegularization(im1);
end

if (settings.reg_alpha < 0)
    error('settings.reg_alpha < 0 given')
end

im1_pad = padarray(im1,[settings.pady settings.padx],'replicate');

laplaceKernel = -fftfreqNormSq(size(im1_pad));

if (settings.reg_alpha > 0)
    filter = -laplaceKernel ./ (-laplaceKernel + settings.reg_alpha);
    tmp = - real(ifft2(fft2(im1_pad-1).*filter));
    
    % undo padding
    C = tmp(settings.pady+1:end-settings.pady, settings.padx+1:end-settings.padx);
else
    C = im1;
end

result = -log(im1 ./ (1 - settings.reg_gamma * C));

end



function result = guessRegularization(im)

edgeThickness = findEdgeThickness(im);

% determine regularization such that 95% of the enhanced edges are covered by the
% high pass filter. 5% of the enhanced edges are said to be due to "real", i.e.
% unenhanced edges and should not be filtered.

EDGE_FRACTION = 0.95;
    
result = (1.0-EDGE_FRACTION) / EDGE_FRACTION * (2*pi/(2*edgeThickness))^2;

end


