function phase = phaserec_smo(hologram, fresnelNumber, settings)
%PHASEREC_SMO reconstructs the phase with Paganin's approach for homogeneous objects.
%
%    ``phase = phaserec_smo(hologram, fresnelNumber, settings)``
%
% Reconstructed the phase in the object plane using the homogeneous object/single
% material object (SMO) approach proposed by Paganin et al. :cite:`Paganin_JoM_2002`. It is valid for
% short propagation distances and homogeneous objects.
%
% Parameters
% ----------
% hologram : numerical array
%     hologram to reconstruct from
% fresnelNumber : numerical array
%     pixel size-based Fresnel numbers, at which the hologram was acquired.
%     If the array has two rows, the rows are interpreted as possibly distinct
%     Fresnel numbers along the two dimensions of the holograms (astigmatism).
% settings : structure, optional
%     contains additional settings, see *Other Parameters*
%
% Other Parameters
% ----------------
% betaDeltaRatio : Default = 0.01
%     Beta/Delta ratio
% padx : Default = 0
%     Amount by which the hologram is symmetrically (replicate)-padded along the
%     x-dimension before reconstruction
% pady : Default = 0
%     Amount by which the hologram is symmetrically (replicate)-padded along the 
%     y-dimension before reconstruction
%
% Returns
% -------
% phase : numerical array
%     The reconstructed phase image of the same size as the input hologram
%
%
% See also
% --------
% functions.phaseRetrieval.directContrast.phaserec_smo,
% functions.phaseRetrieval.directContrast.phaserec_bac
%
% Example
% -------
%
% .. code-block:: matlab
%
%     out = getHologram('spider');
% 
%     smosettings = phaserec_smo;
%     smosettings.padx = 100;
%     smosettings.pady = 100;
%     smosettings.betaDeltaRatio = 0.015;
% 
%     projReconstructed = phaserec_smo(out.holograms, out.fresnelNumbers, smosettings);
% 
%     showImage(projReconstructed);
% 
% 
% See also PHASEREC_MBA, PHASEREC_BAC

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

defaults.padx = 0;
defaults.pady = 0;
defaults.betaDeltaRatio = 0.01;

if (nargin == 0)
    phase = defaults;
    return
end

if (nargin < 3)
    settings = struct;
end

settings = completeStruct(settings, defaults);

if (settings.betaDeltaRatio <= 0)
    error('settings.betaDeltaRatio <= 0 given')
end

numHolos = 1;
if ~isnumeric(fresnelNumber)
    error('fresnelNumber must be a single number, vector or matrix. Cell-arrays are not supported.');
end
if size(fresnelNumber,2) ~= numHolos
    error('size(fresnelNumber,2) must be equal to the number of assigned holograms.');
end
if size(fresnelNumber,1) ~= 1 && size(fresnelNumber,1) ~= 2
    error(['size(fresnelNumber,1) must be either 1 (no astigmatism) or 2', ...
           ' (possibly astigmatic setting).']);
end

% Same Fresnel number along both dimensions of the holograms if fresnelNumber has only one row
fresnelNumber = fresnelNumber .* ones([2,numHolos]);
if (any(fresnelNumber <= 0))
    error('fresnelNumber <= 0 given')
end

% optional paddding
hologramPadded = padarray(hologram, [settings.pady settings.padx], 'replicate');

% multplicative factor in Fourier space that correspond to the Laplacian in real space
[xi_y, xi_x] = fftfreq(size(hologramPadded));
laplaceKernel =  -(xi_y.^2/(fresnelNumber(1,1)) + xi_x.^2/(fresnelNumber(2,1)));

% regularization of the Laplacian at zero frequency is determined by Fresnel number and absorption
reg_alpha = 4 * pi * settings.betaDeltaRatio;

% apply inverse filter to reconstruct the phase
filterKernel = reg_alpha ./ (-laplaceKernel + reg_alpha);
phase = 1/(2 * settings.betaDeltaRatio) * log(real(ifft2(fft2(hologramPadded).*filterKernel)));

% undo padding 
phase = croparray(phase, [settings.pady, settings.padx]);   
   
end

