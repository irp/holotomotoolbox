function [result, stats] = phaserec_nonlinTikhonov(holograms, fresnelNumbers, settings)
% PHASEREC_NONLINTIKHONOV reconstructs the phase based on a fully nonlinear
% phase-contrast model using Tikhonov regularization.
%
%    ``[result, stats] = phaserec_nonlinTikhonov(holograms, fresnelNumbers, settings)``
%
% The method can be seen as a nonlinear generalization phaserec_ctf in the
% sense that both functions will yield exactly the same result in the limit
% of weak phase shifts (linear contrast regime). However, the present method
% may also yield accurate phase-reconstructions outside this regime, thus
% outperforming phaserec_ctf for some data sets.
% The minization of the Tikhonov-functional is performed by a gradient-descent
% method with adaptive (Barzilai-Borwein) stepsizes, using the result of
% phaserec_ctf as an initial guess.
%
%
% Parameters
% ----------
% holograms : cell-array or numerical array
%     hologram(s) to reconstruct from
% fresnelNumbers : numerical array
%     pixel size-based Fresnel numbers, at which the holograms were acquired: the columns
%     correspond to possibly multiple propagation distances (size(fresnelNumbers,2) must
%     match the number of holograms). If the array has two rows, the rows are interpreted
%     as possibly distinct Fresnel numbers along the two dimensions of the holograms (astigmatism).
% settings : structure, optional
%     contains additional settings, see *Other Parameters*
%
% Other Parameters
% ----------------
% lim1 : Default = 1e-3
%     Regularisation Parameter for low frequencies
% lim2 : Default = 1e-1
%     Regularisation Parameter for high frequencies
% betaDeltaRatio : Default = 0
%     Fixed ratio between absorption and phase shifts to be assumed in the
%     reconstruction. The value 0 corresponds to assuming a pure phase object.
% padx : Default = 0
%     Amount by which the holograms are symmetrically (replicate)-padded along the
%     x-dimension before reconstruction
% pady : Default = 0
%     Amount by which the holograms are symmetrically (replicate)-padded along the 
%     y-dimension before reconstruction
% support : Default = []
%     Array of true- and false-values of the same size as the input holograms, specifying
%     an optional support constraint as a binary mask. If support == [], no support 
%     constraint is imposed.
% minPhase : Default = -inf 
%     Lower bound / minimum-constraint for the reconstructed phases. Imposes that all values
%     in result are >= minPhase. If minPhase == -inf, no minimum-constraint is imposed.
% maxPhase : Default = inf
%     Upper bound / maximum-constraint for the reconstructed phases. Imposes that all values
%     in result are <= maxPhase. If maxPhase == inf, no maximum-constraint is imposed.
% useGPUIfPossible : Default = true
%     Run the iterative optimization on GPU if GPU-computations are supported.
%
% Returns
% -------
% result : numerical array
%     The reconstructed phase image of the same size as the input holograms
% stats : struct
%     Structure that contains statistics on the convergence of the gradient-descent
%     iterations performed to reconstruct the phase.
%
%
% See also
% --------
% functions.phaseRetrieval.holographic.phaserec_ctf
%
% Example
% -------
%
% .. code-block:: matlab
%
%     fresnelNumbers = [1.1,0.9,0.8,1.2]*1e-3;
%     phaseTrue = -getPhantom('dicty');
%    
%     simSettings.betaDeltaRatio = 0.01;
%     simSettings.simulateLinear = false;
%     holograms = simulateHologram(phaseTrue, fresnelNumbers, simSettings);
%    
%     settings = simSettings;
%     settings.lim1 = 1e-4;
%     settings.lim2 = 1e-2;
%     
%     phaseRecCTF = phaserec_ctf(holograms, fresnelNumbers, settings);
%     phaseRecNonlin = phaserec_nonlinTikhonov(holograms, fresnelNumbers, settings);
%     
%     subplot(1,3,1)
%     showImage(phaseTrue)
%     title('True phase-image')
%     
%     subplot(1,3,2)
%     showImage(phaseRecCTF)
%     title('Reconstructed phase-image (CTF = linear)');
%     
%     subplot(1,3,3)
%     showImage(phaseRecNonlin)
%     title('Reconstructed phase-image (Nonlinear Tikhonov = nonlinear)');
%     
% 
% See also PHASEREC_CTF

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.


% Default parameters (widely identical to phaserec_ctf)
defaults = phaserec_ctf;
%
% Control parameters for the iterative optimization by projected gradient-descent:
% Only modify if you know what you are doing!
% (see private function projectedGradientDescent for exact meaning of parameters)
defaults.optimization = struct;
defaults.optimization.maxIterations = 100;  % maximum number of iterations
defaults.optimization.tolerance = 3e-3;     % relative tolerance for determining convergence
defaults.optimization.verbose = false;      % display status messages during optimization?
defaults.optimization.initialGuess = [];

% Return default parameters if called without argument
if (nargin == 0)
    result = defaults;
    return
end

% Complete settings with default parameters
if nargin < 3
    settings = struct;
end
settings = completeStruct(settings, defaults);

% If holograms are assigned as cell-arrays, convert to numerical arrays
if iscell(holograms)
    holograms = cat(3, holograms{:});
end

% Check sanity of input
numHolos = size(holograms,3);
if ~isnumeric(fresnelNumbers)
    error('fresnelNumbers must be a single number, vector or matrix. Cell-arrays are not supported.');
end
if size(fresnelNumbers,2) ~= numHolos
    error('size(fresnelNumbers,2) must be equal to the number of assigned holograms.');
end
if size(fresnelNumbers,1) ~= 1 && size(fresnelNumbers,1) ~= 2
    error(['size(fresnelNumbers,1) must be either 1 (no astigmatism) or 2', ...
           ' (possibly astigmatic setting).']);
end

% Same Fresnel number along both dimensions of the holograms if fresnelNumbers has only one row
fresnelNumbers = fresnelNumbers .* ones([2,numHolos]);



% SPECIAL CASE: if settings.betaDeltaRatio is sufficiently high,
% regularization of low frequencies is omitted
if settings.lim1 < 2*numHolos*settings.betaDeltaRatio^2
    settings.lim1 = 0;
end



% Run iterative reconstruction on GPU if desired and possible
useGPU = settings.useGPUIfPossible && checkGPUSupport();
if settings.useGPUIfPossible && ~useGPU
    warning(['Unable to initialize GPU. Performing optimization on CPU instead. ', ...
             'Set settings.useGPUIfPossible = false to suppress this warning.']);
end 



% Optional padding of holograms (and of optional support)
holograms = padarray(gpuArrayIf(holograms, useGPU), [settings.pady settings.padx], 'replicate');
if ~isempty(settings.support)
    settings.support = padarray(gpuArrayIf(settings.support, useGPU), [settings.pady settings.padx]);
end
N = size(holograms(:,:,1));




% ----------------------------------------------------------------------------------------------- %
% --------- Step 1: perform CTF-reconstruction to obtain initial guess (if not assigned) -------- %
% ----------------------------------------------------------------------------------------------- %
if isempty(settings.optimization.initialGuess)
    settingsCTF = settings;
    settingsCTF.padx = 0;            % Holograms are already padded,
    settingsCTF.pady = 0;            % so no more padding necessary
    settingsCTF.useGPUIfPossible = useGPU;
    settingsCTF.optimization = struct;
    settingsCTF.optimization.tolerance = 1e-2;
    initialGuess = gpuArrayIf(phaserec_ctf(holograms, fresnelNumbers, settingsCTF), useGPU);

    % Preliminary nonlinearity-correction in the reconstructed absorption, applied in the low-frequency
    % regime.
    % The idea is the following: the CTF-reconstruction fits the mean value of the phase, phi_0, such
    % that 1-(2*betaDeltaRatio)*phi_0 ~ I_0 where I_0 is the mean of the measured intensities. In the
    % nonlinear model, however, it must hold that exp(-(2*betaDeltaRatio)*phi_0) ~ I_0. The correction
    % below accounts for this difference between CTF- and nonlinear model by adjusting initialGuess
    % accordingly (not only its zero Fourier-frequency (= mean value) but the whole low-frequency
    % part of the image).
    % For strongly absorbing objects, this correction greatly improves convergence of the iterative
    % scheme applied below.
    if settings.betaDeltaRatio > 0
        absorptionImage = (2*settings.betaDeltaRatio) * initialGuess;
        absorptionLowFreq = imgaussfilt(absorptionImage, mean(1./sqrt(2*pi*fresnelNumbers(:))));
        initialGuess = initialGuess +    (log(1 + absorptionLowFreq) - absorptionLowFreq) ...
                                      ./ (2*settings.betaDeltaRatio);
    end
else
    initialGuess = settings.optimization.initialGuess;
end



% ----------------------------------------------------------------------------------------------- %
% ------------------ Step 2: Iteratively improve result using nonlinear model ------------------- %
% ----------------------------------------------------------------------------------------------- %

% Assemble functional to minimize
parOp.gpuFlag = useGPU;
parOp.method = 'fourier';
parOp.betaDeltaRatio = settings.betaDeltaRatio;
%
% Forward operator F that maps a given phase-image onto the corresponding holograms
F = NonlinearPhaseContrastOperator(N, fresnelNumbers, parOp);
%
% Tikhonov functional(f) = || F(f) - holograms ||_2^2 + 2*|| regWeights * fft2(f) ||_2^2,
% where regWeights contains the same Fourier-space regularization weights as used in phaserec_ctf
% (note that "*" denotes composition of Operators/functionals in the following statements)
regWeightsTimes2 = 2*ctfRegWeights(N, mean(fresnelNumbers,2), settings.lim1, settings.lim2, ...
                                   useGPU, 2*numHolos);
TikhonovFunctional =   NormSqFunctional * (F - holograms) ...
                     + NormSqFunctional(@(f) real(ifft2(regWeightsTimes2 .* fft2(f))));



% Assemble projection onto optional support- and min/max-constraints
if settings.minPhase > -inf || settings.maxPhase < inf || ~isempty(settings.support)
    Id = @(f) f;
    if settings.minPhase > -inf
        projMin = @(f) max(f, settings.minPhase);
    else
        projMin = Id;
    end
    if settings.maxPhase < inf
        projMax = @(f) min(f, settings.maxPhase);
    else
        projMax = Id;
    end
    if numel(settings.support)
        projSupport = @(f) settings.support .* f;
    else
        projSupport = Id;
    end

    % Proximal operator of the total constraint functional G
    projection = @(f) projMax(projMin(projSupport(f)));

else
    projection = [];
end


% Perform reconstruction using projected gradient descent method with Barzilai-Borwein stepsizes
optimization = settings.optimization;
optimization.projection = projection;
optimization.initialStepsize = 1/(4*numHolos*(1+settings.betaDeltaRatio.^2) + max(settings.lim1, settings.lim2));
%
% Reference value for the norm of the gradient of TikhonovFunctional to measure convergence of
% the gradient descent setps. If betaDeltaRatio == 0, gradRef is exactly the gradient of
% TikhonovFunctional evaluated for a constant zero phase. For betaDeltaRatio > 0, the gradient
% is computed for zero-mean holograms to avoid overly large reference-gradients due to constant 
% background contributions (for betaDeltaRatio = 0, this is not necessary as the constant part of 
% holograms does not enter in the gradient)
F.evaluate(zeros(size(initialGuess), class(initialGuess)));
if settings.betaDeltaRatio == 0
    gradRef = F.adjoint(holograms);
else
    gradRef = F.adjoint(holograms - mean(mean(holograms,1),2));
end
optimization.resiGradRef = gather(norm(gradRef(:)));    
%
[result, stats] = projectedGradientDescent(TikhonovFunctional, initialGuess, optimization);


% Undo optional padding
result = gather(croparray(result, [settings.pady, settings.padx]));


end

