function result = phaserec_ctf(holograms, fresnelNumbers, settings)
% PHASEREC_CTF reconstructs the phase based on the CTF approach.
%
%    ``result = phaserec_ctf(holograms, fresnelNumbers, settings)``
%
% Reconstructs the phase from one or several holograms for a pure phase- or
% homogeneous object based on the CTF-inversion approach by Peter Cloetens et al.
% :cite:`Cloetens_APL_1999,Zabler_RoSI_2005,Turner_OE_2004`.
%
% Additionally allows to impose support-constraints and/or minimum/maximum values
% for the reconstructed phase. In this case, the reconstruction is computed using
% an iterative optimization method (fast ADMM) described in :cite:`Goldstein_SIAM_2014`.
%
% Parameters
% ----------
% holograms : cell-array or numerical array
%     hologram(s) to reconstruct from
% fresnelNumbers : numerical array
%     pixel size-based Fresnel numbers, at which the holograms were acquired: the columns
%     correspond to possibly multiple propagation distances (size(fresnelNumbers,2) must
%     match the number of holograms). If the array has two rows, the rows are interpreted
%     as possibly distinct Fresnel numbers along the two dimensions of the holograms (astigmatism).
% settings : structure, optional
%     contains additional settings, see *Other Parameters*
%
% Other Parameters
% ----------------
% lim1 : Default = 1e-3
%     Regularisation Parameter for low frequencies
% lim2 : Default = 1e-1
%     Regularisation Parameter for high frequencies
% betaDeltaRatio : Default = 0
%     Fixed ratio between absorption and phase shifts to be assumed in the
%     reconstruction. The value 0 corresponds to assuming a pure phase object.
% padx : Default = 0
%     Amount by which the holograms are symmetrically (replicate)-padded along the
%     x-dimension before reconstruction
% pady : Default = 0
%     Amount by which the holograms are symmetrically (replicate)-padded along the 
%     y-dimension before reconstruction
% support : Default = []
%     Array of true- and false-values of the same size as the input holograms, specifying
%     an optional support constraint as a binary mask. If support == [], no support 
%     constraint is imposed.
% minPhase : Default = -inf 
%     Lower bound / minimum-constraint for the reconstructed phases. Imposes that all values
%     in result are >= minPhase. If minPhase == -inf, no minimum-constraint is imposed.
% maxPhase : Default = inf
%     Upper bound / maximum-constraint for the reconstructed phases. Imposes that all values
%     in result are <= maxPhase. If maxPhase == inf, no maximum-constraint is imposed.
% useGPUIfPossible : Default = true
%     Run the iterative optimization on GPU if GPU-computations are supported (only relevant
%     if support and/or min/max-constraints are imposed)
%
% Returns
% -------
% result : numerical array
%     The reconstructed phase image of the same size as the input holograms
%
%
% See also
% --------
% functions.phaseRetrieval.holographic.phaserec_holotie,
% functions.phaseRetrieval.holographic.phaserec_nonlinTikhonov
% functions.phaseRetrieval.directContrast.phaserec_mba,
% functions.phaseRetrieval.directContrast.phaserec_smo
%
% Example
% -------
%
% .. code-block:: matlab
%
%     fresnelNumbers = [1.1,0.9,0.8,1.2]*1e-3;
%     phaseTrue = -getPhantom('dicty');
%    
%     simSettings.betaDeltaRatio = 0.01;
%     simSettings.simulateLinear = false;
%     holograms = simulateHologram(phaseTrue, fresnelNumbers, simSettings);
%    
%     settings = simSettings;
%     settings.lim1 = 1e-4;
%     settings.lim2 = 1e-2;
%     settings.padx = 512;
%     settings.pady = 512;
%     phaseRec = phaserec_ctf(holograms, fresnelNumbers, settings);
%     
%     subplot(1,2,1)
%     showImage(phaseTrue)
%     title('True phase-image')
%     
%     subplot(1,2,2)
%     showImage(phaseRec)
%     title('Reconstructed phase-image')
%
%
%     % Repeat reconstruction, imposing negativity of the reconstructed phases
%     settings.maxPhase = 0;
%     phaseRecNeg = phaserec_ctf(holograms, fresnelNumbers, settings);
%
%     figure('name', 'CTF-reconstruction with negative-phase-constraint'); 
%     showImage(phaseRecNeg);
%     
% 
% See also PHASEREC_HOLOTIE, PHASEREC_NONLINTIKHONOV

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.


% Default parameters
defaults.lim1 = 1e-3;
defaults.lim2 = 1e-1;
defaults.betaDeltaRatio = 0;
defaults.padx = 0;
defaults.pady = 0;
defaults.support = [];
defaults.minPhase = -inf; 
defaults.maxPhase = inf;
defaults.useGPUIfPossible = true;
%
% Control parameters for the iterative optimization in the case of min-, max- or support-constraints: 
% Only modify if you know what you are doing!
% (see private function fADMM for exact meaning of parameters)
defaults.optimization.maxIterations = 100;      % maximum number of iterations
defaults.optimization.tolerance = 1e-3;         % relative tolerance for determining convergence
defaults.optimization.rho = 1e-2;               % stepsize parameter
defaults.optimization.verbose = false;          % display status messages during optimization?

% Return default parameters if called without argument
if (nargin == 0)
    result = defaults;
    return
end

% Complete settings with default parameters
if nargin < 3
    settings = struct;
end
settings = completeStruct(settings, defaults);

% If holograms are assigned as cell-arrays, convert to numerical arrays
if iscell(holograms)
    holograms = cat(3, holograms{:});
end

% Check sanity of input
numHolos = size(holograms,3);
if ~isnumeric(fresnelNumbers)
    error('fresnelNumbers must be a single number, vector or matrix. Cell-arrays are not supported.');
end
if size(fresnelNumbers,2) ~= numHolos
    error('size(fresnelNumbers,2) must be equal to the number of assigned holograms.');
end
if size(fresnelNumbers,1) ~= 1 && size(fresnelNumbers,1) ~= 2
    error(['size(fresnelNumbers,1) must be either 1 (no astigmatism) or 2', ...
           ' (possibly astigmatic setting).']);
end

% Same Fresnel number along both dimensions of the holograms if fresnelNumbers has only one row
fresnelNumbers = fresnelNumbers .* ones([2,numHolos]);



% SPECIAL CASE: if settings.betaDeltaRatio is sufficiently high,
% regularization of low frequencies is omitted
if settings.lim1 < 2*numHolos*settings.betaDeltaRatio^2
    settings.lim1 = 0;
end



% Determine whether iterative optimization has to be performed: this is the case if min-, max-
% or support-constraints are to be imposed
doIterativeOptimization = (numel(settings.support) > 0 || settings.minPhase > -inf ||  settings.maxPhase < inf);



% Determine whether the GPU is to be used in the reconstruction: this is the case if either the 
% holograms have already been assigned as gpuArray or optionally in the case of iterative optimization
useGPU = isOnGPU(holograms) || (settings.useGPUIfPossible && doIterativeOptimization && checkGPUSupport());
if settings.useGPUIfPossible && doIterativeOptimization && ~useGPU
    warning(['Unable to initialize GPU. Running iterative optimization on CPU instead. ', ...
             'Set settings.useGPUIfPossible = false to suppress this warning.']);
end 



% Optional padding of holograms
holograms = padarray(holograms, [settings.pady settings.padx], 'replicate');
N = size(holograms(:,:,1));


% Assemble CTF-inversion formula:
%
%       result = argmin_f { sum_j   ||2*ctf{j}.* fft2(f) - fft2(holograms{j}-1)||_2^2
%                               + 2*||sqrt(regWeights) .* fft2(f)||_2^2                }
%
%              = ifft2( (sum_j ctf{j} .* fft2(holograms{j}-1)) / (sum_j ctf{j}.^2 + regWeights) )
%
% ctf{j} = sin(xi^2/(4*pi*F(j))) + settings.betaDeltaRatio * cos(xi^2/(4*pi*F(j)));
%
[xi_y, xi_x] = fftfreq(N);
sumCTFHolograms = 0;
sumCTFSq = 0;
for holo_idx = 1:numHolos
    xiNormSqFresnel =   gpuArrayIf(xi_y, useGPU).^2/(4*pi*fresnelNumbers(1,holo_idx)) ...
                      + gpuArrayIf(xi_x, useGPU).^2/(4*pi*fresnelNumbers(2,holo_idx));
    ctf = sin(xiNormSqFresnel);
    if settings.betaDeltaRatio > 0
        ctf = ctf + settings.betaDeltaRatio * cos(xiNormSqFresnel);
    end
    sumCTFHolograms = sumCTFHolograms + ctf .* fft2(gpuArrayIf(holograms(:,:,holo_idx), useGPU));
    sumCTFSq = sumCTFSq + ctf.^2;
end
sumCTFSq = 2*sumCTFSq;
clear 'ctf' 'xiNormSqFresnel';

% Correction for zero-frequency of Fourier transformed data:
% equivalent to subtracting a constant 1 from all input holograms
sumCTFHolograms(1,1) = sumCTFHolograms(1,1) - prod(N) * numHolos * settings.betaDeltaRatio;


% Add regularization weights in Fourier-space that smoothly transition from the value
% lim1 in the low-frequency regime (around the central CTF-minimum) to lim2 at larger 
% larger spatial frequencies beyond the first CTF-maximum 
sumCTFSqReg = sumCTFSq + ctfRegWeights(N, mean(fresnelNumbers,2), settings.lim1, settings.lim2, useGPU);
clear 'sumCTFSq';



% ACTUAL RECONSTRUCTION:
%
% ----------------------------------------------------------------------------------------------- %
% ----- Special case: Minimization with constraints. Required iterative solver (fast ADMM) ------ %
% ----------------------------------------------------------------------------------------------- %
if doIterativeOptimization

    % Assemble proximal map of the regularized CTF-functional
    %
    %       F(f) :=     ||2*ctf{j}.*fft2(f) - fft2(holograms{j}-1)||_2^2 
    %               + 2*||sqrt(regWeights) .* fft2(f)||_2^2 
    %
    proxCTF = @(f, sigma) real(ifft2(    (sumCTFHolograms + fft2((1./sigma)*f)) ...
                                      ./ (sumCTFSqReg + 1./sigma)                   ));

    % Assemble proximal map that implements the optional support- and min/max-constraints
    Id = @(f) f;
    if settings.minPhase > -inf
        proxMin = @(f) max(f, settings.minPhase);
    else
        proxMin = Id;
    end
    if settings.maxPhase < inf
        proxMax = @(f) min(f, settings.maxPhase);
    else
        proxMax = Id;
    end
    if numel(settings.support)
        % If padding is applied to the data, the support has to be padded as well
        support = padarray(gpuArrayIf(settings.support, useGPU), [settings.pady, settings.padx]);
        proxSupport = @(f) support .* f;
    else
        proxSupport = Id;
    end

    % Proximal operator of the total constraint functional G
    proxConstraints = @(f,tau) proxMax(proxMin(proxSupport(f)));

    % Compute minimizer using a fast ADMM algorithm
    [result, ~] = fADMM(proxConstraints, proxCTF, ...
                            zeros(size(sumCTFHolograms), class(sumCTFHolograms)), ...
                            settings.optimization);
  

% ----------------------------------------------------------------------------------------------- %
% ---------- Default case: Minimization without constraints. Can be performed directly ---------- %
% ----------------------------------------------------------------------------------------------- %
else
    result = real(ifft2(sumCTFHolograms ./ sumCTFSqReg));
end


% Undo optional padding
result = croparray(result, [settings.pady, settings.padx]);


% Ensure that result is copied back from GPU if holograms were not already assigned as gpuArray
result = gatherIf(result, ~isOnGPU(holograms));


end

