function [phase,amplitude] = phaserec_holotie(images,fresnelNumbers,settings)
% PHASERE_HOLOTIE reconstructs the phase based on holoTIE algorithm.
%
%    ``[phase,amplitude] = phaserec_holotie(images,fresnelNumbers,settings)``
%
% HoloTIE is a single-step holographic reconstruction based on the Transport of
% Intensity Equation (TIE) :cite:`Krenkel_OE_2013,Krenkel_ACSA_2017`. It is based on directly solving the phase problem with
% the TIE without additional assumptions as a pure phase object or small propagation
% distances. Therefore a differential quotient is needed, which is approximated by a
% difference quotient for which two images recorded in close proximity are needed.
%
% Parameters
% ----------
% images : numerical array or cell-array
%     pair of projections for the phase retrieval
% fresnelNumbers : numerical array
%     pixel size-based Fresnel numbers, at which the holograms were acquired: the columns
%     correspond to possibly multiple propagation distances (size(fresnelNumbers,2) must
%     match the number of holograms). If the array has two rows, the rows are interpreted
%     as possibly distinct Fresnel numbers along the two dimensions of the holograms (astigmatism)
% settings : structure, optional
%     contains additional settings, see *Other Parameters*
%
% Other Parameters
% ----------------
% imageToDivide : Default = 2
%     Projection used for division when solving the TIE.
% reg_alpha : Default = 1e-1
%     Regularisation parameter alpha used for inverse Laplace operator.
% reg_type : {'none', 'mba', 'tikhonov'}, Default = 'mba'
%     Regularisation method.
% padx : Default = 0
%     Amount by which the hologram is symmetrically (replicate)-padded along the
%     x-dimension before reconstruction.
% pady : Default = 0
%     Amount by which the hologram is symmetrically (replicate)-padded along the
%     y-dimension before reconstruction.
% lowfrequencyimage : Default = []
%     If lowfrequencyimage is given, the reconstructed image is replaced by this
%     image at low spatial frequencies.
% combineCutoff : Default = []
%     Frequency cutoff for the low-frequency replacement. [] results in a cutoff at
%     the first maximum of the PCTF.
% combineSigma : Default = 0.01
%     Standard deviation of the error function which defines the transition between
%     the reconstructed image and the given lowfrequencyimage
%
% Returns
% -------
% phase : numerical array
%     The reconstructed phase-image in the object plane
% amplitude : numerical array
%     The reconstructed amplitude-image in the object plane
%
% 
% See also 
% -------- 
% functions.phaseRetrieval.holographic.phaserec_fulltie,
% functions.auxiliary.combineInFourierspace,
% functions.phaseRetrieval.holographic.phaserec_ctf
%
% Example
% -------
%
% .. code-block:: matlab
%
%     out = getHologram('macrophage');
%     usedDistances = [2,3];
%     holograms = out.holograms(:,:,usedDistances);
%     fresnelNumbers = out.fresnelNumbers(:,usedDistances);
%     htiesettings = phaserec_holotie;
% 
%     phase = phaserec_holotie(holograms, fresnelNumbers, htiesettings);
% 
%     showImage(phase);
%
% See also PHASEREC_FULLTIE, COMBINEINFOURIERSPACE,PHASEREC_CTF

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

if (nargin < 3)
    settings = struct;
end

% default settings for solving full TIE
defaults.imageToDivide = 2;
defaults.reg_alpha = 1e-5;
defaults.reg_type = 'mba';
defaults.padx = 0;
defaults.pady = 0;

% default settings fro combining result with given image at low frequencies
defaults.lowfrequencyimage = [];
defaults.combineCutoff = [];
defaults.combineSigma = 0.01;

% return default settings
if (nargin == 0)
    phase = defaults;
    return
end

% Optional Settings
settings = completeStruct(settings,defaults);

%% check for right usage
if iscell(images)
    images = cat(3, images{:});
end
numHolos = size(images,3);

if ~isnumeric(fresnelNumbers)
    error('fresnelNumbers must be a single number, vector or matrix. Cell-arrays are not supported.');
end
if size(fresnelNumbers,2) ~= numHolos
    error('size(fresnelNumbers,2) must be equal to the number of assigned holograms.');
end
if size(fresnelNumbers,1) ~= 1 && size(fresnelNumbers,1) ~= 2
    error(['size(fresnelNumbers,1) must be either 1 (no astigmatism) or 2', ...
           ' (possibly astigmatic setting).']);
end

% Same Fresnel number along both dimensions of the holograms if fresnelNumbers has only one row
fresnelNumbers = fresnelNumbers .* ones([2,numHolos]);

if ((settings.pady  > 0 || settings.padx > 0) && (sum(settings.lowfrequencyimage(:)) ~= 0))
    disp('Warning: Padding seems to introduce edge effects when using combination in Fourier space.');
end


%% solve the TIE in detector plane

% pad intensities to avoid crosstalk and give a better sampling in Fourier space
diffImage = padarray(images(:,:,2)-images(:,:,1), [settings.pady settings.padx],'replicate');
imageToDivide = padarray(images(:,:,settings.imageToDivide),[settings.pady settings.padx],'replicate');

% make (regularized) inverse Laplace filter kernel
[ny, nx] = size(imageToDivide);
laplaceKernel = -fftfreqNormSq([ny, nx]);
switch (settings.reg_type)
    case 'none'
        % avoid division by 0
        laplaceKernel(1,1) = 1;
        invLaplaceFilt = 1 ./ laplaceKernel;
        % set zero frequency (singularity) to 0
        invLaplaceFilt(1,1) = 0;
    case 'mba'
        invLaplaceFilt = -1 ./ (-laplaceKernel + settings.reg_alpha);
    case 'tikhonov'
        invLaplaceFilt = laplaceKernel ./ (laplaceKernel.^2 + settings.reg_alpha^2);
    otherwise
        error(['Invalid reg_method ' settings.reg_method])
        
end

% factors in Fourier space that correspond to the gradient operation in real space
grad_x = 1i * fftfreq(nx);
grad_y = 1i * fftfreq(ny);

filter_x = invLaplaceFilt .* grad_x.';
filter_y = invLaplaceFilt .* grad_y;

% solve the equation stepwise, see e.g. Paganin, Coherent x-ray optics, 2006
imageFilteredX = ifft2(fft2(diffImage) .* filter_x) ./ imageToDivide;
imageFilteredY = ifft2(fft2(diffImage) .* filter_y) ./ imageToDivide;

imageFilteredX = real(ifft2(fft2(imageFilteredX) .* filter_x));
imageFilteredY = real(ifft2(fft2(imageFilteredY) .* filter_y));

% Undo padding
imageFilteredX = croparray(imageFilteredX, [settings.pady,settings.padx]);
imageFilteredY = croparray(imageFilteredY, [settings.pady,settings.padx]);

% determine difference in inverse Fresnel numbers
delta_fresnelx = 1/(1/fresnelNumbers(2,2)-1/fresnelNumbers(2,1));
delta_fresnely = 1/(1/fresnelNumbers(1,2)-1/fresnelNumbers(1,1));

% divergence and last inverse laplacian:
phase = (-2*pi*delta_fresnelx) * imageFilteredX + (-2*pi*delta_fresnely) * imageFilteredY;


% optionally combine reconstructed phase with a low frequency image
if ~isempty(settings.lowfrequencyimage)
    if isempty(settings.combineSigma)
        settings.combineSigma = mean(sqrt(fresnelNumbers/2));
    end
    phase = combineInFourierspace(phase,settings.lowfrequencyimage,settings);
end


% wavefunction in detector plane
wavefield = sqrt(images(:,:,2)) .* exp(1i*phase);

% propagate back into object plane
wavefield = fresnelPropagate(wavefield, -fresnelNumbers(:,2));


% decompose wavefield into the sought phase- and amplitude images
phase = angle(wavefield);
amplitude = abs(wavefield);

end
