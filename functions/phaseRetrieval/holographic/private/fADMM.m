function [result, stats] = fADMM(proxH, proxG, v_hat_start, settings)
% Implements the accelerated variant of the Alternating-Direction Method of Multipliers (ADMM)
% proposed in the article
% 
% Goldstein, T., O'Donoghue, B., Setzer, S., & Baraniuk, R. (2014). 
% Fast alternating direction optimization methods. 
% SIAM Journal on Imaging Sciences, 7(3), 1588-1623.
%
% The algorithm iteratively solves an optimization problem of the form x_res = argmin_x H(x) + G(x)
% with convex functionals H and G, given implementations of the corresponding proximal operators
% proxH(x_0) = argmin_x H(x) + ||x-x_0||_2^2 and proxG(x_0) = argmin_x G(x) + ||x-x_0||_2^2, where
% ||y||_2 denotes the Euclidean 2-norm.

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.


% Complete settings with default parameters
defaults.maxIterations = 1e2;
defaults.eta = 0.999;
defaults.tolerance = 1e-3;
defaults.verbose = false;

if nargin < 4
    settings = struct;
end
settings = completeStruct(settings, defaults);

% Initialize
v_hat = v_hat_start;
v_old = 0;
lambda_hat = 0;
lambda_old = 0;
c_old = inf;
t = 1;

% Iterate
for jj = 1:settings.maxIterations

    % Optionally print status
    if settings.verbose 
        fprintf('[FADMM_Restart] Iteration %i', jj);
    end

    % Perform ADMM-step
    x = proxH(v_hat - lambda_hat, 1./settings.rho);
    v = proxG(x + lambda_hat, 1./settings.rho);
    lambda = lambda_hat + x - v;
    resi_primal = norm(x(:)-v(:));
    resi_dual = norm(v(:)-v_hat(:));
    c = settings.rho * (resi_primal.^2 + resi_dual.^2);

    % Compute relative residuals
    norm_x = norm(x(:));
    norm_v = norm(v(:));
    norm_v_hat = norm(v_hat(:));
    resi_primal = resi_primal ./ max(norm_x, norm_v);
    resi_dual = resi_dual ./ max(norm_v, norm_v_hat);

    % Optionally print residuals
    if settings.verbose
        fprintf(', relative residuals: primal = %.2e, dual = %.2e\n', resi_primal, resi_dual);
    end

    % Check stopping conditions
    if max(resi_primal, resi_dual) < settings.tolerance
        break;
    end

    % Check convergence behavior
    if c < settings.eta*c_old
        % Case "converging": CONTINUE with extrapolation step
        t_new = 0.5*(1 + sqrt(1 + 4*t^2));
        v_hat = v + ((t-1)/t_new) .* (v-v_old);
        lambda_hat = lambda + ((t-1)/t_new) .* (lambda-lambda_old);
        lambda_old = lambda;
        c_old = c;
        v_old = v;
        t = t_new;
    else
        % Case "not converging": RESTART algorithm with initial steplength t = 1
        t = 1;
        v_hat = v_old;
        lambda_hat = lambda_old;
        c = c_old/settings.eta;
    end


end

% Output results
result = x;
stats.num_it = jj;

end
