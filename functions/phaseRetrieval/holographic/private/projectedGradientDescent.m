function [result, stats] = projectedGradientDescent_dev(F, initialGuess, settings)
% Computes a (local) minimum of a functional F by projected/proximal gradient-descent 
% iterations. The implementation uses of Barzilai-Borwein stepsizes, non-monotone linesearch
% and a stopping rule based on the relative residual, as proposed in the article:
% 
% Goldstein, T., Studer, C., & Baraniuk, R. (2014). A field guide to forward-backward 
% splitting with a FASTA implementation. arXiv preprint arXiv:1411.3406.

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.


% Default parameters
defaults.initialStepsize = 1;
defaults.projection = [];
defaults.stepsizeRule = 'bbStepsize';
defaults.resiGradRef = [];
defaults.maxIterations = 100;
defaults.tolerance = 1e-3;
defaults.linesearchDepth = 2;
defaults.linesearchStepsizeDecreaseFactor = 4;
defaults.linesearchMaxTries = 5;
defaults.verbose = false;

% Return default parameters if called without argument
if (nargin == 0)
    result = defaults;
    return;
end

% Complete settings with default parameters
if nargin < 3
settings = struct;
end
settings = completeStruct(settings, defaults);



% Initialize
xhat = initialGuess;
x = applyProjection(settings.projection, xhat);
cache.stepsize = settings.initialStepsize;
%    
% Compute first value and gradient of the objective functional
value = gather(F.evaluate(x));
grad = F.getGradient();
%
% Prepare stats
stats = struct;
stats.functionalValues = [value; nan([settings.maxIterations,1])];
stats.resiGrad = [computeGradientResidual(grad, xhat, x, settings.projection, cache.stepsize); nan([settings.maxIterations,1])];
stats.resiGradRel = [stats.resiGrad(1)/settings.resiGradRef; nan([settings.maxIterations,1])];
stats.isConverged = false;
%
% Optionally print stats
if settings.verbose
    fprintf(['[', mfilename, '] Initialization: rel gradient norm = %.3e, functional value = %.3e\n'], stats.resiGradRel(1), stats.functionalValues(1));
end
%
% Prepare list of reference values for non-monotone linesearch
valuesLinesearch = -inf([settings.linesearchDepth,1]);


% Iterate
for it = 1:settings.maxIterations

    % Compute preliminary stepsize
    [stepsize, cache] = feval(settings.stepsizeRule, F, x, value, grad, cache);

    % Update reference values for non-monotone linesearch: always given by the functional values
    % of the M = settings.linesearchDepth preceding iteratations
    valuesLinesearch = circshift(valuesLinesearch, -1);
    valuesLinesearch(end) = value;

    % Do gradient-descent step, performing line search to avoid excessively large stepsizes
    for linesearchTry = 1:settings.linesearchMaxTries
        xhat = x - stepsize .* grad;
        xNew = applyProjection(settings.projection, xhat);
        valueNew = F.evaluate(xNew);

        % Check non-monotone linesearch condition
        if valueNew <  max(valuesLinesearch) + real(grad(:)' * (xNew(:)-x(:))) + (1/(2*stepsize)) * norm(xNew(:)-x(:))^2
            break;
        end

        % Repeat step with decreased stepsize if linesearch condition is not satisfied
        stepsize = stepsize / settings.linesearchStepsizeDecreaseFactor;
    end
    %
    % Print warning if linesearch was apparently not succesful
    if linesearchTry == settings.linesearchMaxTries
        warning(sprintf(['[', mfilename, '] Maximum number of linesearch tries ', ...
                         'exceeded in iteration %i.'], it));
    end

    % Save new iterate and updated functional value
    x = xNew;
    value = gather(valueNew);
    
    % Compute new gradient
    grad = F.getGradient();

    % Save stats and check stopping conditions
    stats.functionalValues(it+1) = value;
    stats.resiGrad(it+1) = computeGradientResidual(grad, xhat, x, settings.projection, cache.stepsize);
    stats.resiGradRel(it+1) = stats.resiGrad(it+1)/settings.resiGradRef;
    if stats.resiGradRel(it+1) < settings.tolerance
        stats.isConverged = true;
        break;
    end

    % Optionally print stats
    if settings.verbose
        fprintf(['[', mfilename, '] Iteration %03i: rel gradient norm = %.3e, ',       ...
                 'functional value = %.3e, stepsize = %.3e, linesearch tries = %i\n'], ...
                 it, stats.resiGradRel(it+1), stats.functionalValues(it+1), stepsize, linesearchTry);
    end
end

stats.numIt = it;
result = x;

end



function f = applyProjection(projection, f)

    if ~isempty(projection)
        f = projection(f);
    end

end



function resiGrad = computeGradientResidual(grad, xhat, x, projection, stepsize)
    gradProj = grad;
    if ~isempty(projection)
        gradProj = gradProj + (xhat - x) ./ stepsize;
    end
    resiGrad = gather(norm(gradProj(:)));
end



function [stepsize, cache] = bbStepsize(F, x, y, s, cache)

    if ~isfield(cache, 'bb_last_step')
        cache.bb_last_step = 0;
        stepsize = cache.stepsize;
    else
        dx = x - cache.x;
        ds = s - cache.s;
        if cache.bb_last_step == 2 || cache.bb_last_step == 0
            stepsize = norm(ds(:)'*dx(:)) / norm(ds(:)).^2;
            cache.bb_last_step = 1;
        elseif cache.bb_last_step == 1
            stepsize = norm(dx(:))^2 / real(dx(:)'*ds(:));
            cache.bb_last_step = 2;              
        end

        % Use previous stepsize if suggested stepsize is negative
        if stepsize < 0
            stepsize = cache.stepsize;
        end

    end 

    % Update cache   
    cache.x = x;
    cache.s = s;
    cache.stepsize = stepsize;

end



function [stepsize, cache] = constantStepsize(F, x, y, s, cache)

    stepsize = cache.stepsize;
    cache.x = x;
    cache.s = s;

end
