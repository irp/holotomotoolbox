function regWeights = ctfRegWeights(sizeHolo, fresnelNumbers, lim1, lim2, gpuFlag, lim3)
% Computes a regularization weights in Fourier space, suitable for regularized inversion
% of the CTF and similar reconstruction methods. The main idea, originally proposed by
% Peter Cloetens, is to create a mask that smoothly transitions from one (typically low) 
% regularization parameter lim1 at low Fourier frequencies around the central CTF-mimimum
% to a second (typically value) value lim2 that determines the regularization in the
% higher Fourier frequencies beyond the first CTF-maximum.
% See phaserec_ctf, phaserec_nonlinTikhonov and phaserec3D_ctf for typical usage. 

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

if nargin < 5
    gpuFlag = false;
end

ndim = numel(sizeHolo);
xi = cell([ndim,1]);
[xi{:}] = fftfreq(sizeHolo, 1);
XiSqDivFresnel = 0;
for jj = 1:ndim
    xi{jj} = gpuArrayIf(xi{jj}, gpuFlag);
    XiSqDivFresnel = XiSqDivFresnel + xi{jj}.^2 ./ (2*pi^2*fresnelNumbers(jj));
end

sigma = (sqrt(2) - 1) / 2; 
w = 1/2 * erfc((sqrt(XiSqDivFresnel) - 1) / (sqrt(2) * sigma));
regWeights = lim1 .* w + lim2 .* (1 - w);

% Additional regularization level for spatial frequencies beyond the 
% numerical aperture of the optical system. Only relevant for
% deeply holographic data
if nargin == 6
    fresnelNumbersFOV = fresnelNumbers(:) .* sizeHolo(:).^2;
    freqCutoffFOV = pi .* (fresnelNumbersFOV./sizeHolo(:));
    wCutoffFOV = 1;
    XiSqDivCutoffFOV = 0;
    for jj = 1:ndim
        XiSqDivCutoffFOV = XiSqDivCutoffFOV + (xi{jj}./freqCutoffFOV(jj)).^2;
        wCutoffFOV = wCutoffFOV .* (0.5 .* erfc((abs(xi{jj})./freqCutoffFOV(jj)-1)./0.1));
    end
    regWeights = wCutoffFOV .* regWeights + (1-wCutoffFOV) .* lim3;
end

end
