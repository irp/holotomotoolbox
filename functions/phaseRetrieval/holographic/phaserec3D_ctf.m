function volPhase = phaserec3D_ctf(holograms, fresnelNumbers, tomoAngles, settings)
% PHASEREC3D_CTF performs simultaneous phase retrieval and tomographic reconstruction
% from a set of holograms.
%
%    ``volPhase = phaserec3D_ctf(holograms, fresnelNumbers, tomoAngles, settings)``
%
% As detailed in :cite:`Ruhlandt_ActaA_2016,Ruhlandt_Diss_2017`, the idea of the
% method is that reconstructing a tomographic series of holograms by filtered back-projection
% yields a 3D-hologram (at least in the linear contrast regime), from which the underlying
% 3D-object volume can then be recovered by a 3D-CTF-reconstruction.
% Note that in this function uses the ASTRA tomography toolbox (https://www.astra-toolbox.com/)
% for the filtered back-projection operation. Hence, it requires the ASTRA tomography toolbox
% to be installed and added to the Matlab path.
%
%
% Parameters
% ----------
% holograms : 4D-numerical array
%     set of tomographic holograms. The first two dimensions correspond to the lateral
%     image dimensions, the third to variations of the tomographic angle and the
%     fourth to different Fresnel numbers (i.e. defocus distances).
% fresnelNumbers : numerical array
%     pixel size-based Fresnel numbers, at which the holograms were acquired: the columns
%     correspond to possibly multiple propagation distances. If the array has two rows, the
%     rows are interpreted as possibly distinct Fresnel numbers along the two dimensions of
%     the holograms (astigmatism). Must satisfy size(fresnelNumbers,2) == size(holograms,3).
% tomoAngles : vector
%     tomoAngles at which the holograms were acquired.
%     Must satisfy numel(tomoAngles) == size(holograms,3).
% settings : structure, optional
%     contains additional settings, see *Other Parameters*
%
%
% Other Parameters
% ----------------
% betaDeltaRatio : Default = 0
%     Fixed ratio between absorption and phase shifts to be assumed in the
%     reconstruction. The value 0 corresponds to assuming a pure phase object.
% support : Default = []
%     Array of true- and false-values, specifying an optional support constraint for the
%     reconstructed 3D-volume(s) as a binary mask. If empty, no support cconstraint is imposed. 
%     Must satisfy size(support) == [size(holograms,2), size(holograms,2), size(holograms,1)]
% minPhasePerVoxel : Default = -inf 
%     Lower bound / minimum-constraint for the reconstructed phase shifts per voxel. Imposes that
%     all values of volPhase are >= minPhasePerVoxel. If minPhasePerVoxel == -inf, no such
%     constraint is imposed.
% maxPhasePerVoxel : Default = inf 
%     Upper bound / maximum-constraint for the reconstructed phase shifts per voxel. Imposes that
%     all values of volPhase are <= maxPhasePerVoxel. If maxPhasePerVoxel == inf, no such
%     constraint is imposed.
%
%
% Returns
% -------
% volPhase : 3D numerical array
%     3d distribution of the phase shifts per voxel (= wavenumber*delta in suitable units)
%
%
%
%
% See also
% --------
% functions.phaseRetrieval.holographic.phaserec_ctf,
% functions.phaseRetrieval.holographic.phaserec3D_IRP
%
%
% Example
% -------
%
% .. code-block:: matlab
%
%     % create 3D object
%     settingsPhantom.sizeObjects = 40;
%     settingsPhantom.numObjects = 40;
%     settingsPhantom.distrSize = 'random';
%     settingsPhantom.strengthObjects = 0.025;
%     settingsPhantom.distrStrength = 'random';
%     phantom = -create3Dphantom(128,settingsPhantom);
%     
%     settingsPlot.figIdx = figure('name', 'Phantom (orthoslices)');
%     showOrthoslices(phantom, settingsPlot);
%     
%     % create tomographic projections
%     tomoAngles = (0:179);
%     settingsProj = astraParProjection3D;
%     settingsProj.outputSize = size(phantom,2);
%     settingsProj.radonOrientation = false;
%     projections = astraParProjection3D(phantom, tomoAngles, settingsProj);
%     
%     % create holograms
%     fresnelNumbers = [0.03,0.02];
%     holograms = zeros([size(projections,1),size(projections,2),size(fresnelNumbers,2),numel(tomoAngles)], 'single');
%     settingsHologram.betaDeltaRatio = 0.1;
%     for angleIdx = 1:size(projections,3)
%         holograms(:,:,:,angleIdx) = simulateHologram(projections(:,:,angleIdx), fresnelNumbers, settingsHologram); 
%     end
%     
%     
%     % reconstruct 3D object
%     settingsRecon = phaserec3D_ctf;
%     settingsRecon.lim2 = 1e-2;
%     volRecon = phaserec3D_ctf(holograms, fresnelNumbers, tomoAngles, settingsRecon);
%     
%     % plot reconstruction
%     settingsPlot.figIdx = figure('name', 'Reconstructed volume (orthoslices)');
%     showOrthoslices(volRecon, settingsPlot);
%     
%     
%     % Repeat reconstruction, imposing negativity of the reconstructed phase-volume
%     settingsRecon.maxPhasePerVoxel = 0;
%     volReconNeg = phaserec3D_ctf(holograms, fresnelNumbers, tomoAngles, settingsRecon);
%     
%     % plot reconstruction
%     settingsPlot.figIdx = figure('name', 'Reconstructed volume with negative-phase-constraint (orthoslices)');
%     showOrthoslices(volReconNeg, settingsPlot);
%
%
% See also PHASEREC_CTF, PHASEREC3D_IRP

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

if nargin < 4
    settings = struct;
end

% default settings
defaults.lim1 = 1e-3;
defaults.lim2 = 1e-1;
defaults.betaDeltaRatio = 0;
defaults.support = [];
defaults.minPhasePerVoxel = -inf;
defaults.maxPhasePerVoxel = inf;
defaults.optimPar.maxIt = 100;
defaults.optimPar.tol = 1e-2;
defaults.optimPar.rho = 1e-2;
defaults.optimPar.verbose = false;

% return default settings
if (nargin == 0)
    volPhase = defaults;
    return;
end

settings = completeStruct(settings,defaults);


% Check sanity of input
sizeData = size(holograms);
numAngles = numel(tomoAngles);
numDistances = size(fresnelNumbers,2);
if ~isnumeric(holograms)
    error(['Holograms must be a 4-dimensional numerical array of size ', ...
           '[imageSizeY, imageSizeX, numberOfDefocusDistances, numberOfTomographicAngles].']);
end
if size(holograms, 4) ~= numAngles
    error('size(holograms,4) does not match numel(tomoAngles).');
end
if ~isnumeric(fresnelNumbers)
    error('fresnelNumbers must be a single number, vector or matrix. Cell-arrays are not supported.');
end
if size(holograms, 3) ~= numDistances
    error('size(holograms,3) does not match size(fresnelNumbers,2).');
end
if size(fresnelNumbers,1) ~= 1 && size(fresnelNumbers,1) ~= 2
    error(['size(fresnelNumbers,1) must be either 1 (no astigmatism) or 2', ...
           ' (possibly astigmatic setting).']);
end

% Same Fresnel number along both dimensions of the holograms if fresnelNumbers has only one row
fresnelNumbers = fresnelNumbers .* ones([2,numDistances]);



% SPECIAL CASE: if settings.betaDeltaRatio is sufficiently high,
% regularization of low frequencies is omitted
if settings.lim1 < 2*numDistances*settings.betaDeltaRatio^2
    settings.lim1 = 0;
end



% Assemble CTF-inversion formula:
%
%       volPhase = argmin_f { sum_j   ||2*ctf{j}.* FT(f) - FT(holograms{j}-1)||_2^2
%                                 + 2*||sqrt(regWeights) .* FT(f)||_2^2                }
%
%                = iFT( (sum_j ctf{j} .* FT(holograms{j}-1)) / (sum_j ctf{j}.^2 + regWeights) )
%
% ctf{j} = sin(xi^2/(4*pi*fresnel(j))) + settings.betaDeltaRatio * cos(xi^2/(4*pi*fresnel(j)));
%
% Here holograms{j} are understood as 3D-holograms, which are reconstructed from the given
% tomographic sequences of 2D-holograms by filtered back-projection

% Size of the 3D-holograms and of the finally reconstructed 3D-volume
N = [size(holograms,2)*[1,1], size(holograms,1)];

% Required padding of the 2D-holograms along x to avoid artifacts in the reconstructed 3D-holograms
padx = ceil(N(1) * (sqrt(2) - 1) / 2);

sumCTFHolograms = 0;
sumCTFSq = 0;
for idx = 1:numDistances
    % Construct 3D-CTF. The phase- and absorption-CTFs are simply the imaginary- and real parts,
    % respectively, of the Fresnel-propagation kernel exp(-1i*xi.^2/(4*pi*fresnel(j)))
    settingsProp.method = 'fourier';
    fresnelPropFactor = fresnelPropagationKernel(N, ...
        [fresnelNumbers(2,idx)*[1;1]; fresnelNumbers(1,idx)], settingsProp);
    ctf = -imag(fresnelPropFactor);
    if settings.betaDeltaRatio > 0
        ctf = ctf + settings.betaDeltaRatio * real(fresnelPropFactor);
    end
    clear 'fresnelPropFactor';
    sumCTFSq = sumCTFSq + ctf.^2;

    % Reconstruct 3D-hologram by filtered back-projection of the tomographic series of 2D-holograms
    % (need to subtract constant background 1 before tomographic reconstruction)
    settingsFBP.outputSize = N(1);
    holo3D = permute(astraFBP(padarray(squeeze(holograms(:,:,idx,:))-1, [0, padx, 0], ...
                                       'replicate'), tomoAngles, settingsFBP), [2,1,3]   );
    sumCTFHolograms = sumCTFHolograms + ctf .* fftn(double(holo3D)); clear 'ctf'; clear 'holo3D';
end
sumCTFSq = 2*sumCTFSq;


% Assemble regularization weights in Fourier-space that smoothly transitions from the value
% lim1 in the low-frequency regime (around the central CTF-minimum) to lim2 at larger 
% larger spatial frequencies beyond the first CTF-maximum 
meanFresnelNumbers = [mean(fresnelNumbers(2,:))*[1;1]; mean(fresnelNumbers(1,:))];
regWeights = ctfRegWeights(N, meanFresnelNumbers, settings.lim1, settings.lim2);


% Add regularization term to sum of squared CTFs
sumCTFSqReg = sumCTFSq + regWeights; clear 'regWeights'; clear 'sumCTFSq';



% ACTUAL RECONSTRUCTION:
%
% ----------------------------------------------------------------------------------------------- %
% ----- Special case: Minimization with constraints. Required iterative solver (fast ADMM) ------ %
% ----------------------------------------------------------------------------------------------- %
if ~isempty(settings.support) || settings.minPhasePerVoxel > -inf || settings.maxPhasePerVoxel < inf

    % Assemble proximal map of the regularized CTF-functional
    %
    %       F(f) :=     ||2*ctf{j}.*FT(f) - FT(holograms{j}-1)||_2^2 
    %               + 2*||sqrt(regWeights) .* FT(f)||_2^2 
    %
    proxCTF = @(f, sigma) real(ifftn(    (sumCTFHolograms + fftn((1./sigma)*f)) ...
                                      ./ (sumCTFSqReg + 1./sigma)                   ));

    % Assemble proximal map that implements the optional support- and min/max-constraints
    Id = @(f) f;
    if settings.minPhasePerVoxel > -inf
        proxMin = @(f) max(f, settings.minPhasePerVoxel);
    else
        proxMin = Id;
    end
    if settings.maxPhasePerVoxel < inf
        proxMax = @(f) min(f, settings.maxPhasePerVoxel);
    else
        proxMax = Id;
    end
    if ~isempty(settings.support)
        proxSupport = @(f) settings.support .* f;
    else
        proxSupport = Id;
    end

    % Proximal operator of the total constraint functional G
    proxConstraints = @(f,tau) proxMax(proxMin(proxSupport(f)));

    % Compute minimizer using a fast ADMM algorithm
    [volPhase, stats] = fADMM(proxConstraints, proxCTF, ...
                              zeros(size(sumCTFHolograms), class(sumCTFHolograms)), ...
                              settings.optimPar);
  

% ----------------------------------------------------------------------------------------------- %
% ---------- Default case: Minimization without constraints. Can be performed directly ---------- %
% ----------------------------------------------------------------------------------------------- %
else

    volPhase = real(ifftn(sumCTFHolograms ./ sumCTFSqReg));

end


end
