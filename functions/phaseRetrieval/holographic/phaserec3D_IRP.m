function [volPhase, volAbsorp] = phaserec3D_IRP(holograms, fresnelNumbers, tomoAngles, settings)
% PHASEREC3D_IRP performs simultaneous phase retrieval and tomographic reconstruction
% from a set of holograms.
%
%    ``[volPhase, volAbsorp] = phaserec3D_IRP(holograms, fresnelNumbers, tomoAngles, settings)``
%
% Simultaneously performs phase retrieval and tomographic reconstruction from a set
% of holograms recorded at at different tomographic angles and one or several defocus distances
% (Fresnel numbers) using the 'iterative reprojection phase retrieval (IRP)' algorithm proposed 
% by Ruhlandt et al. :cite:`Ruhlandt_Phys.Rev.A_2014`. The present implementation of IRP slightly differs from the 
% description in :cite:`Ruhlandt_Phys.Rev.A_2014`. The main differences are the following:
%
% (1) Instead of the "multiplicative ART" (actually multiplicative SIRT) update proposed in the 
%     article, iterations of the (additive) (S)imultaneous (A)lgebraic (R)econstruction
%     (T)echnique (SIRT) are used to compute a volume-update from projection-updates.
% (2) The present implementation also supports holograms at different
%     Fresnel-numbers.
% (3) Constraints are only imposed on the reconstructed 3D-volume, not on
%     projections.
%
% In principle, the whole algorithm may thus be seen as a combination of 2D phase-retrieval via
% alteranting projections (AP) and tomographic reconstruction via SIRT.
% 
% Note that this function uses the ASTRA tomography toolbox (https://www.astra-toolbox.com/)
% Hence, it requires the ASTRA tomography toolbox to be installed and added to the Matlab path.
%
% Parameters
% ----------
% holograms : 4D-numerical array
%     set of tomographic holograms. The first two dimensions correspond to the lateral
%     image dimensions, the third to variations of the tomographic angle and the
%     fourth to different Fresnel numbers (i.e. defocus distances).
% fresnelNumbers : numerical array
%     pixel size-based Fresnel numbers, at which the holograms were acquired: the columns
%     correspond to possibly multiple propagation distances. If the array has two rows, the
%     rows are interpreted as possibly distinct Fresnel numbers along the two dimensions of
%     the holograms (astigmatism). Must satisfy size(fresnelNumbers,2) == size(holograms,3).
% tomoAngles : vector
%     tomoAngles at which the holograms were acquired.
%     Must satisfy numel(tomoAngles) == size(holograms,3).
% settings : structure, optional
%     contains additional settings, see *Other Parameters*
%
% Other Parameters
% ----------------
% maxIterations : Default = 100
%     Maximum number of outer iterations, comprising (1) a sweep of AP-iterations to compute
%     (updated) tomographic projections, (2) an update of the reconstructed volume by performing
%     some SIRT-iterations using the projections from 1, (3)  
% iterationsSIRT : Default = 2
%     Number of SIRT-iterations performed to update the volume in each step of the IRP-algorithm.
% betaDeltaRatio : Default = []
%     Assumed fixed ratio between the absorption and phase shifts induced by the imaged object.
%     If betaDeltaRatio is 0, the object is assumed to be a pure phase object. If betaDeltaRatio
%     is empty, the absorption reconstructed independently from the phase, otherwise it is given
%     by the reconstructed phase multiplied by betaDeltaRatio.
% initialGuessPhase : Default = []
%     Initial guess for the reconstructed phase shifts per voxel. If empty, the IRP-algorithm is
%     initialized with zero phase shifts. Must satisfy 
%     size(initialGuessPhase) == [size(holograms,2), size(holograms,2), size(holograms,1)].
% initialGuessAbsorp : Default = []
%     Initial guess for the reconstructed absorption shifts per voxel. If empty, the IRP-algorithm
%     is initialized with zero absorption. Must satisfy 
%     size(initialGuessAbsorp) == [size(holograms,2), size(holograms,2), size(holograms,1)].
%     Parameter has no effect if betaDeltaRatio is not empty.
% support : Default = []
%     Array of true- and false-values, specifying an optional support constraint for the
%     reconstructed 3D-volume(s) as a binary mask. If empty, no support cconstraint is imposed. 
%     Must satisfy size(support) == [size(holograms,2), size(holograms,2), size(holograms,1)]
% minPhasePerVoxel : Default = -inf 
%     Lower bound / minimum-constraint for the reconstructed phase shifts per voxel. Imposes that
%     all values of volPhase are >= minPhasePerVoxel. If minPhasePerVoxel == -inf, no such
%     constraint is imposed.
% maxPhasePerVoxel : Default = inf 
%     Upper bound / maximum-constraint for the reconstructed phase shifts per voxel. Imposes that
%     all values of volPhase are <= maxPhasePerVoxel. If maxPhasePerVoxel == inf, no such
%     constraint is imposed.
% minAbsorpPerVoxel : Default = -inf 
%     Lower bound / minimum-constraint for the reconstructed absorption shifts per voxel. Imposes
%     that all values of volAbsorp are >= minAbsorpPerVoxel. If minAbsorpPerVoxel == -inf, no such
%     constraint is imposed. Parameter has no effect if betaDeltaRatio is not empty.
% maxAbsorpPerVoxel : Default = inf 
%     Upper bound / maximum-constraint for the reconstructed absorption shifts per voxel. Imposes
%     that all values of volAbsorp are <= maxAbsorpPerVoxel. If maxAbsorpPerVoxel == -inf, no such
%     constraint is imposed. Parameter has no effect if betaDeltaRatio is not empty.
% verbose : Default = true
%     Display status messages during the reconstruction?
% plotIterates : Default = false
%     Plot intermediate results during the reconstruction?
%
% Returns
% -------
% volPhase : 3D numerical array
%     3d distribution of the phase shifts per voxel (= -wavenumber*delta in suitable units)
% volBeta : 3D numerical array
%     3d distribution of the absorption per voxel (= -wavenumber*beta in suitable units)
%
%
% Example
% -------
%
% .. code-block:: matlab
%
%     % create 3D object
%     settingsPhantom.sizeObjects = 40;
%     settingsPhantom.numObjects = 40;
%     settingsPhantom.distrSize = 'random';
%     settingsPhantom.strengthObjects = 0.025;
%     settingsPhantom.distrStrength = 'random';
%     phantom = -create3Dphantom(128,settingsPhantom);
%     
%     settingsPlot.figIdx = figure('name', 'Phantom (orthoslices)');
%     showOrthoslices(phantom, settingsPlot);
%     
%     % create tomographic projections
%     tomoAngles = (0:179);
%     settingsProj = astraParProjection3D;
%     settingsProj.outputSize = size(phantom,2);
%     settingsProj.radonOrientation = false;
%     projections = astraParProjection3D(phantom, tomoAngles, settingsProj);
%     
%     % create holograms
%     fresnelNumbers = [0.03,0.02];
%     holograms = zeros([size(projections,1),size(projections,2),size(fresnelNumbers,2),numel(tomoAngles)], 'single');
%     settingsHologram.betaDeltaRatio = 0.1;
%     for angleIdx = 1:size(projections,3)
%         holograms(:,:,:,angleIdx) = simulateHologram(projections(:,:,angleIdx), fresnelNumbers, settingsHologram); 
%     end
%     
%     % reconstruct 3D object
%     settingsIRP = phaserec3D_IRP;
%     settingsIRP.betaDeltaRatio = settingsHologram.betaDeltaRatio;
%     settingsIRP.maxPhasePerVoxel = 0;
%     settingsIRP.support = (phantom < 0);
%     settingsIRP.plotIterates = true;
%     settingsIRP.maxIterations = 200;
%
%     vol = phaserec3D_IRP(holograms,fresnelNumbers,tomoAngles,settingsIRP);
%     
%     % plot reconstruction
%     settingsPlot.figIdx = figure('name', 'Reconstructed volume (orthoslices)');
%     showOrthoslices(vol, settingsPlot);
%

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

if nargin < 4
    settings = struct;
end

% default settings
defaults.maxIterations = 100;
defaults.iterationsSIRT = 2;
defaults.betaDeltaRatio = [];
defaults.initialGuessPhase = [];
defaults.initialGuessAbsorp = [];
defaults.support = [];
defaults.minPhasePerVoxel = -inf;
defaults.maxPhasePerVoxel = inf;
defaults.minAbsorpPerVoxel = -inf;
defaults.maxAbsorpPerVoxel = 0;
defaults.verbose = true;
defaults.plotIterates = false;

% return default settings
if (nargin == 0)
    volPhase = defaults;
    volAbsorp = [];
    return;
end

settings = completeStruct(settings,defaults);


% Check sanity of input
sizeData = size(holograms);
numAngles = numel(tomoAngles);
numDistances = size(fresnelNumbers,2);
if ~isnumeric(holograms)
    error(['Holograms must be a 4-dimensional numerical array of size ', ...
           '[imageSizeY, imageSizeX, numberOfDefocusDistances, numberOfTomographicAngles].']);
end
if size(holograms, 4) ~= numAngles
    error('size(holograms,4) does not match numel(tomoAngles).');
end
if ~isnumeric(fresnelNumbers)
    error('fresnelNumbers must be a single number, vector or matrix. Cell-arrays are not supported.');
end
if size(holograms, 3) ~= numDistances
    error('size(holograms,3) does not match size(fresnelNumbers,2).');
end
if size(fresnelNumbers,1) ~= 1 && size(fresnelNumbers,1) ~= 2
    error(['size(fresnelNumbers,1) must be either 1 (no astigmatism) or 2', ...
           ' (possibly astigmatic setting).']);
end

% Same Fresnel number along both dimensions of the holograms if fresnelNumbers has only one row
fresnelNumbers = fresnelNumbers .* ones([2,numDistances]);



%% Create projector that matches a given wave-field in the object plane to the measured
%  Fresnel-magnitudes (= pointwise square-root of the holograms)
settingP.doMErrors = true;
PData = PFresnelMagnitude(sqrt(squeeze(holograms(:,:,:,1))), fresnelNumbers, settingP);



%% Initialize
residual = inf;
if settings.plotIterates
    settingsPlot.figIdx = figure;
end
%
% Initialize volume according to provided initial guess for phase and/or absorption
if ~isempty(settings.initialGuessPhase) 
    vol = settings.initialGuessPhase;
    if isempty(settings.betaDeltaRatio)
        vol = 1i * vol;
        if ~isempty(settings.initialGuessAbsorp)
            vol = vol + settings.initialGuessAbsorp;
        end
    end
elseif ~isempty(settings.initialGuessAbsorp) && isempty(settings.betaDeltaRatio)
    vol = settings.initialGuessAbsorp;
else
    vol = ones([sizeData(2), sizeData(2), sizeData(1)], 'single'); 
end
%
% Settings for the intermediate tomographic reconstructions steps by SIRT-iterations
settingsSIRT.iterations = settings.iterationsSIRT;
settingsSIRT.iradonOrientation = false;
settingsSIRT.outputSize = size(vol,1);
%
% Minimum- and/or maximum-constraints are imposed in the SIRT-iterations
if isempty(settings.betaDeltaRatio)
    settingsSIRT.maxVal = settings.maxAbsorpPerVoxel;
    settingsSIRT.minVal = settings.minAbsorpPerVoxel;
    settingsSIRT.maxImag = settings.maxPhasePerVoxel;
    settingsSIRT.minImag = settings.minPhasePerVoxel;
else
    settingsSIRT.maxVal = settings.maxPhasePerVoxel;
    settingsSIRT.minVal = settings.minPhasePerVoxel;
end
%
% Settings for the reprojection-steps
settingsprojs = astraParProjection3D;
settingsprojs.outputSize = size(vol,1);
settingsprojs.radonOrientation = false;


% Iterate
for iteration = 1:settings.maxIterations

    % Optionally print status
    if settings.verbose
        fprintf('Cycle: %i, SIRT iterations: %i\n', iteration, settingsSIRT.iterations);
    end

    % reprojection step: compute tomographic projections and corresponding wave-field s
    % in the object plane from the current volume
    projs = astraParProjection3D(vol, tomoAngles, settingsprojs);
    if ~isempty(settings.betaDeltaRatio)
        % Return to parametrization projs = i*phase + absorption instead of projs = phase
        projs = (1i + settings.betaDeltaRatio) .* projs;
    end
    wavefieldsObject = exp(projs); 
    clear 'projs';

    % Update wave-fields in detector plane by projecting onto the measured Fresnel-magnitudes
    % (also computing the residuals on-the-fly)
    residualOld = residual;
    residual = 0;
    for indAngle = 1:numAngles
        PData.setMeasurements(sqrt(squeeze(holograms(:,:,:,indAngle))));
        [wavefieldsObject(:,:,indAngle), residualTmp] = PData.eval(wavefieldsObject(:,:,indAngle), iteration);
        residual = residual + residualTmp.^2;
    end
    residual = sqrt(residual / numel(wavefieldsObject));
    
    % Compute phase shifts and absorption from wave-fields: projsUpdate = absorption + 1i*phase
    projsUpdate = log(wavefieldsObject);

    % If fixed betaDeltaRatioRatio is given, project onto this constraint
    % (this is done before updating the volume so that only a real-valued arrays instead 
    % of complex-valued ones have to be processed in the following)
    if ~isempty(settings.betaDeltaRatio)
        projsUpdate =   (imag(projsUpdate) + settings.betaDeltaRatio * real(projsUpdate)) ...
                      ./(1 + settings.betaDeltaRatio.^2);
    end

    % Update volume by performing SIRT-iterations with updated projections
    settingsSIRT.initialGuess = vol;
    vol = astraSIRT(projsUpdate, tomoAngles, settingsSIRT); 
    clear 'projsUpdate';
  
    
    % Impose optional support constraint by setting values outside support to zero
    if ~isempty(settings.support)
        vol = settings.support .* vol;
    end

    % Optionally print residual
    residualDiff = residualOld - residual;
    if settings.verbose
        fprintf('residual: %f (difference to previous iteration: %e)\n', residual, residualDiff);
    end
    
    % Optionally plot intermediate results
    if settings.plotIterates
        showOrthoslices(real(vol), settingsPlot);
        set(gcf, 'name', sprintf(['[', mfilename, '] Iteration %i: ', ...
                                  'reconstructed phase shifts per voxel (orthoslices)'], iteration));
        drawnow;
    end
    
end


% Finalize
if settings.plotIterates
    close(settingsPlot.figIdx);
end
if isempty(settings.betaDeltaRatio)
    volPhase = imag(vol);
    volAbsorp = real(vol);
else
    volPhase = vol;
    if nargout >= 2
        volAbsorp = settings.betaDeltaRatio * volPhase;
    else
        volAbsorp = [];
    end
end


end
