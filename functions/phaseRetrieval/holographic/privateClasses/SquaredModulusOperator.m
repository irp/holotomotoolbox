%%
classdef SquaredModulusOperator < Operator

properties
    x0;
end

methods
    function res = evaluate(obj, x)
        obj.x0 = x;
        res = real(x).^2 + imag(x).^2;     
    end
    function res = derivative(obj, x)
        res = 2 .* real(conj(obj.x0) .* x);    
    end
    function res = adjoint(obj, x)
        res = 2 .* obj.x0 .* real(x);    
    end
end

end
