%%
classdef FresnelPropOperator < LinearOperator

properties
    propagator;
end

methods
    function obj = FresnelPropOperator(sizeIn, fresnelNumbers, settings)
        settings.padVal = 0;
        obj.propagator = FresnelPropagator(sizeIn, fresnelNumbers, settings);
    end

    function res = evaluate(obj, x)
        res = obj.propagator.propagate(x);
    end
    function res = adjoint(obj, x)
        res = obj.propagator.backPropagate(x);
    end
end

end
