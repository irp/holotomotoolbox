%%
classdef OperatorProduct < Operator

properties
    op1;
    op2;
    res1;
    res2;
end


methods
    function obj = OperatorProduct(op1,op2)
        obj.op1 = op1;
        obj.op2 = op2;
    end

    function res = evaluate(obj, x)
        res = obj.op1.evaluate(x) .* obj.op2.evaluate(x);
    end

    function res = derivative(obj, x)
        res = obj.res1 .* obj.op2.derivative(x) + obj.op1.derivative(x) .* obj.res2;
    end

    function res = adjoint(obj, x)
        res = obj.op2.adjoint(conj(obj.res1) .* x) + obj.op1.adjoint(conj(obj.res2) .* x);
    end   
end

end
