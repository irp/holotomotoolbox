%%
classdef OperatorPlusConstant < Operator

properties
    op;
    offset;
end


methods
    function obj = OperatorPlusConstant(op,offset)
        obj.op = op;
        obj.offset = offset;
    end

    function res = evaluate(obj, x)
        res = obj.op.evaluate(x) + obj.offset;
    end

    function res = derivative(obj, x)
        res = obj.op.derivative(x);
    end

    function res = adjoint(obj, x)
        res = obj.op.adjoint(x);
    end   
end

end
