%%
classdef OperatorScaled < Operator

properties
    op;
    scalar;
end


methods
    function obj = OperatorScaled(op, scalar)
        obj.op = op;
        obj.scalar = scalar;
    end

    function res = evaluate(obj, x)
        res = obj.scalar .* obj.op.evaluate(x);
    end

    function res = derivative(obj, x)
        res = obj.scalar .* obj.op.derivative(x);
    end

    function res = adjoint(obj, x)
        res = obj.op.adjoint(conj(obj.scalar) .* x);
    end   
end

end
