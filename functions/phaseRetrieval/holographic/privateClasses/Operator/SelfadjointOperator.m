%%
classdef (Abstract) SelfadjointOperator < LinearOperator

methods
	function res = adjoint(obj, x)
		res = obj.derivative(x);	 
	end
end

end
