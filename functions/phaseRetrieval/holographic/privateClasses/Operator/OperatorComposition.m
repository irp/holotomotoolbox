%%
classdef OperatorComposition < Operator

properties
    op1;
    op2;
end


methods
    function obj = OperatorComposition(op1,op2)
        obj.op1 = op1;
        obj.op2 = op2;
    end

    function res = evaluate(obj, x)
        res = obj.op1.evaluate(obj.op2.evaluate(x));
    end

    function res = derivative(obj, x)
        res = obj.op1.derivative(obj.op2.derivative(x));
    end

    function res = adjoint(obj, x)
        res = obj.op2.adjoint(obj.op1.adjoint(x));
    end   
end

end
