%%
classdef (Abstract) Operator < handle
    
methods

    function res = evaluate(obj, x)
        error('Method ''evaluate'' is not implemented in this or any parent class.');
    end

    function res = derivative(obj, x)
        error('Method ''derivative'' is not implemented in this or any parent class.');
    end

    function res = adjoint(obj, x)
        error('Method ''adjoint'' is not implemented in this or any parent class.');
    end



    function newObj = plus(obj1,obj2)
        if ~isa(obj1, 'Operator')
            newObj = plus(obj2, obj1);
        elseif isnumeric(obj2)
            newObj = OperatorPlusConstant(obj1, obj2);
        elseif isa(obj2, 'Operator')
            newObj = OperatorSum(obj1,obj2);
        else
            error(['Operator ''+'' is not implemented for operands of type ''', class(obj1), ''' and ''', class(obj2), '''.']);
        end
    end

    function newObj = mtimes(obj1,obj2)
        if ~isa(obj1, 'Operator')
            newObj = mtimes(obj2, obj1);
        elseif isnumeric(obj2)
            newObj = mtimes(obj1, MultiplicationOperator(obj2));
        elseif isa(obj2, 'Operator')
            newObj = OperatorComposition(obj1, obj2);
        else
            error(['Operator ''*'' is not implemented for operands of type ''', class(obj1), ''' and ''', class(obj2), '''.']);
        end
    end

    function newObj = times(obj1,obj2)
        if ~isa(obj1, 'Operator')
            newObj = times(obj2, obj1);
        elseif isnumeric(obj2)
            newObj = OperatorScaled(obj1, obj2);
        elseif isa(obj2, 'Operator')
            newObj = OperatorProduct(obj1, obj2);
        else
            error(['Operator ''.*'' is not implemented for operands of type ''', class(op1), ''' and ''', class(op2), '''.']);
        end
    end

    function newObj = minus(obj1,obj2)
        newObj = plus(obj1, (-1) .* obj2);
    end

end

end
