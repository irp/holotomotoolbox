%%
classdef (Abstract) LinearOperator < Operator

methods
    function res = derivative(obj, x)
        res = obj.evaluate(x);     
    end
end

end
