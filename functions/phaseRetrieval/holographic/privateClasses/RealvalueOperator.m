%%
classdef RealvalueOperator < SelfadjointOperator

methods
    function res = evaluate(obj, x)
        res = real(x);  
    end
end

end
