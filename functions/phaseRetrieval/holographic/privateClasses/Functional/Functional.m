%%
classdef (Abstract) Functional < Operator

methods

    function res = getGradient(obj)
        error('Method ''getGradient'' is not implemented in this or any parent class.');
    end

    function res = derivative(obj, x)
        grad = obj.getGradient();
        res = real(grad(:)' * x(:));      
    end

    function res = adjoint(obj, x)
        res = obj.getGradient() .* real(x);
    end



    function newObj = plus(obj1,obj2)
        if ~isa(obj1, 'Operator')
            newObj = plus(obj2, obj1);
        elseif isnumeric(obj2) && isscalar(obj2)
            newObj = FunctionalPlusConstant(obj1, obj2);
        elseif isa(obj2, 'Functional')
            newObj = FunctionalSum(obj1,obj2);
        else
            error(['Operator ''+'' is not implemented for operands of type ''', class(obj1), ''' and ''', class(obj2), '''.']);
        end
    end

    function newObj = mtimes(obj1,obj2)
        if isnumeric(obj2) && isscalar(obj2)
            newObj = mtimes(obj1, MultiplicationOperator(obj2));
        elseif isa(obj2, 'Operator')
            newObj = FunctionalComposition(obj1, obj2);
        else
            error(['Operator ''*'' is not implemented for operands of type ''', class(obj1), ''' and ''', class(obj2), '''.']);
        end
    end

    function newObj = times(obj1,obj2)
        if ~isa(obj1, 'Functional')
            newObj = times(obj2, obj1);
        elseif isnumeric(obj2) && isscalar(obj2)
            newObj = FunctionalScaled(obj1, obj2);
        elseif isa(obj2, 'Functional')
            newObj = FunctionalProduct(obj1, obj2);
        else
            error(['Operator ''.*'' is not implemented for operands of type ''', class(op1), ''' and ''', class(op2), '''.']);
        end
    end

end

end
