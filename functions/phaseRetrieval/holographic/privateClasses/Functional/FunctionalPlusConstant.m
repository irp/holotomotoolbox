%%
classdef FunctionalPlusConstant < Functional

properties
    functional;
    offset;
end


methods
    function obj = FunctionalPlusConstant(functional,offset)
        obj.functional = functional;
        obj.offset = offset;
    end

    function res = evaluate(obj, x)
        res = obj.functional.evaluate(x) + obj.offset;
    end

    function res = getGradient(obj)
        res = obj.functional.getGradient();
    end
end

end
