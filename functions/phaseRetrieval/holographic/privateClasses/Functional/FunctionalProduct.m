%%
classdef FunctionalProduct < Functional

properties
    functional1;
    functional2;
    res1;
    res2;
end


methods
    function obj = FunctionalProduct(functional1,functional2)
        obj.functional1 = functional1;
        obj.functional2 = functional2;
    end

    function res = evaluate(obj, x)
        res = obj.functional1.evaluate(x) .* obj.functional2.evaluate(x);
    end

    function res = getGradient(obj)
        res = obj.res1 .* obj.functional2.getGradient() + obj.functional1.getGradient() .* obj.res2;
    end
end

end
