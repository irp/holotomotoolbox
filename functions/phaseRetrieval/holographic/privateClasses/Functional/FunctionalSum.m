%%
classdef FunctionalSum < Functional

properties
    functional1;
    functional2;
end


methods
    function obj = FunctionalSum(functional1,functional2)
        obj.functional1 = functional1;
        obj.functional2 = functional2;
    end

    function res = evaluate(obj, x)
        res = obj.functional1.evaluate(x) + obj.functional2.evaluate(x);
    end

    function res = getGradient(obj)
        res = obj.functional1.getGradient() + obj.functional2.getGradient();
    end
end

end
