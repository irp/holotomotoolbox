%%
classdef FunctionalComposition < Functional

properties
    functional;
    op;
end


methods
    function obj = FunctionalComposition(functional,op)
        obj.functional = functional;
        obj.op = op;
    end

    function res = evaluate(obj, x)
        res = obj.functional.evaluate(obj.op.evaluate(x));
    end

    function res = getGradient(obj)
        res = obj.op.adjoint(obj.functional.getGradient());
    end
end

end
