%%
classdef FunctionalScaled < Functional

properties
    functional;
    scalar;
end


methods
    function obj = FunctionalScaled(functional, scalar)
        obj.functional = functional;
        obj.scalar = scalar;
    end

    function res = evaluate(obj, x)
        res = obj.scalar .* obj.functional.evaluate(x);
    end

    function res = getGradient(obj)
        res = obj.scalar .* obj.op.getGradient();
    end
end

end
