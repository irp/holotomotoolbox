%%
classdef NonlinearPhaseContrastOperator < Operator

properties
    opCore;
end


methods
    function obj = NonlinearPhaseContrastOperator(sizeIn, fresnelNumbers, settings)
        if nargin < 3
            settings = struct;
        end
        if ~isfield(settings, 'betaDeltaRatio')
            settings.betaDeltaRatio = 0;
        end
        betaDeltaFactor = 1i + settings.betaDeltaRatio;

        obj.opCore =   SquaredModulusOperator() ...
                     * FresnelPropOperator(sizeIn, fresnelNumbers, settings) ...
                     * ExponentialOperator() ...
                     * MultiplicationOperator(betaDeltaFactor) ...
                     * RealvalueOperator();
    end

    function res = evaluate(obj, x)
        res = obj.opCore.evaluate(x);
    end

    function res = derivative(obj, x)
        res = obj.opCore.derivative(x);
    end

    function res = adjoint(obj, x)
        res = obj.opCore.adjoint(x);
    end
end

end
