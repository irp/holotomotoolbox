%%
classdef MultiplicationOperator < LinearOperator

properties
    multiplier;
    multiplier_conj;
end


methods
    function obj = MultiplicationOperator(multiplier)
        obj.multiplier = multiplier;
            if isreal(obj.multiplier)
                obj.multiplier_conj = obj.multiplier;
            else
                obj.multiplier_conj = conj(obj.multiplier);
            end
    end

    function res = evaluate(obj, x)
        res = obj.multiplier .* x;
    end

    function res = adjoint(obj, x)
        res = obj.multiplier_conj .* x;
    end   
end

end
