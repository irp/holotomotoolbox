%%
classdef ExponentialOperator < Operator

properties
    exp_x0;
end

methods
    function res = evaluate(obj, x)
        obj.exp_x0 = exp(x);
        res = obj.exp_x0;
    end
    function res = derivative(obj, x)
        res = obj.exp_x0 .* x;    
    end
    function res = adjoint(obj, x)
        res = conj(obj.exp_x0) .* x;    
    end
end

end
