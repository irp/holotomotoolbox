%%
classdef NormSqFunctional < Functional

properties
    GramianHandle;
    x_Gramian
end

methods
    function obj = NormSqFunctional(Gramian)
        if nargin == 0
            obj.GramianHandle = @(x) x;
        else
            obj.GramianHandle = Gramian;
        end
    end

    function res = evaluate(obj, x)
        obj.x_Gramian = obj.GramianHandle(x);
        res = 0.5*real(obj.x_Gramian(:)' * x(:));
    end

    function res = getGradient(obj)
        res = obj.x_Gramian;
    end   
end

end
