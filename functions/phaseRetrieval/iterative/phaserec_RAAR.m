function [phase, amplitude] = phaserec_RAAR(holograms, fresnelNumbers, settings) 
% PHASEREC_RAAR reconstructs the phase from given holograms using the RAAR-algorithm
% (Relaxed Averaged Alternating Reflections)
%
%    ``[phase, amplitude] = phaserec_RAAR(holograms, fresnelNumbers, settings)``
%
% This function is just a convenience-function that calls phaserec_iterative with
% the algorithm 'RAAR' :cite:`Luke_IP_2005`. For further details, see phaserec_iterative.m .
%
%
% Parameters
% ----------
% holograms : cell-array or numerical array
%     hologram(s) to reconstruct from
% fresnelNumbers : numerical array
%     pixel size-based Fresnel numbers, at which the holograms were acquired: the columns
%     correspond to possibly multiple propagation distances (size(fresnelNumbers,2) must
%     match the number of holograms). If the array has two rows, the rows are interpreted
%     as possibly distinct Fresnel numbers along the two dimensions of the holograms (astigmatism).
% settings : structure, optional
%     contains additional settings, see *Other Parameters*
%
% Other Parameters
% ----------------
% Same parameters as in phaserec_iterative (exception: no parameter algorithm)
%
% Returns
% -------
% phase : numerical array
%     The reconstructed phase image of the same size as the input holograms
% amplitude : numerical array
%     The reconstructed amplitude image of the same size as the input holograms (OPTIONAL: with
%     the default settings minAmplitude = maxAmplitude = 1 (pure phase constraint), no physically 
%     reasonable amplitude image will be recovered) 
%
% See also
% --------
% functions.phaseRetrieval.iterative.phaserec_iterative
%
% Example
% -------
% A Quick start example is given in phaserec_iterative.m
% 
% A tutorial for a basic explanation of the algorithm/projector
% combinations in a single distance setting can be found in:
% :download:`examples/simulateIterativePhaseRetrieval.m <../../examples/simulateIterativePhaseRetrieval.m>` 
% 
% For multiple distance:
% :download:`examples/multiDistancePhaseRetrieval.m <../../examples/multiDistancePhaseRetrieval.m>` 
% 
% See also PHASEREC_ITERATIVE

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.


if nargin == 0
    phase = rmfield(phaserec_iterative(), 'algorithm');
    return;
end

settings.algorithm = 'RAAR';
[phase, amplitude] = phaserec_iterative(holograms, fresnelNumbers, settings); 

end
