classdef PProductSpace < handle
% PPRODUCTSPACE PFRESNELMAGNITUDE is a projector class for iterative phase retrieval.
%
%    ``obj = PProductSpace(constraints, p, opts)``
% 
% Parameters
% ----------
% holograms : numerical array
%     Array of 1-n measurements for phase retrieval. Input is expected (in
%     general) as amplitude.
% FresnelNums : numerical array
%     Array of 1-n Fresnel numbers corresponding to the holograms. If the
%     Fresnelnumber for x and y are different FresnelNums is a nx2 matrix
%     of [fresnelx fresnely].
% guess : numerical array
%     Starting guess for the reconstruction. the size should match the size
%     of the measurements. 
% settings : structure
%     contains additional settings, see *Other Parameters*
%
% Other Parameters
% ----------------
% PM : Default = PFresnelMagnitude;
%     Defines the projector on the measurements. Or equivalent. Depending on the
%     setting different strategies for evaluation are available. For
%     details see in PFresnelMagnitude.m
%
% Returns
% -------
% result : struct
%     Contains the fields:
%     x : result of the reconstruction
%     stats : (optional) error measures obtained for each iteration. With
%     sub-fields: gap, convergenceHistory, errorPM, errorPS
% settings : structure
%     The completed setting struct with the reconstruction parameters.
%
% See also
% --------
% examples.simulateIterativePhaseRetrieval
% functions.phaseRetrieval.iterative.PFresnelMagnitude
% functions.phaseRetrieval.iterative.PPhase
% functions.phaseRetrieval.iterative.PSupport
% functions.phaseRetrieval.iterative.PNothing
% functions.phaseRetrieval.iterative.ProjectorTemplate
%
% Example
% -------
%
% a tutorial can be found in :download:`examples/simulateIterativePhaseRetrieval.m <../../examples/simulateIterativePhaseRetrieval.m>`
% 
% Quick start:
%
% .. code-block:: matlab
%
%     fresnel = 1e-3;
%     sample = modelWaveField('../../../functions/generators/phantoms/phantomDicty.mat',...
%         '../../../functions/generators/phantoms/phantomDicty.mat',...
%         [0 maxPhase], [1 1], [512 512], [2048 2048]);
%     sample = abs(sample) .* exp(1i.*(angle(conj(sample))));
%     
%     propSet = FresnelPropagator.defaultSettings();
%     prop = FresnelPropagator(fresnel, [2048 2048], propSet);
%     hologram = prop.propagate(sample);
%     hologram = abs(hologram);
%     
%     recSet = phaserec_iterative();
%     %recSet.useGPU = 1;
%     guess = complex(ones(size(hologram)));
%     [phaseRec, recSet] = phaserec_iterative(hologram, fresnel, guess, recSet);
%     
%     figure
%     subplot(1,2,1)
%     showImage(angle(cropToCenter(sample, [512 512])))
%     title('True phase-image')
%     
%     subplot(1,2,2)
%     showImage(angle(cropToCenter(phaseRec.x, [512 512])))
%     title('Reconstruction with pure phase constraint')
%     
%     % Repeat reconstruction, imposing negativity of the reconstructed phases
%     recSet.PPhaseOpts.projectOnNegativity = 1;
%     [phaseRecNeg, recSet]= phaserec_iterative(hologram, fresnel, guess, recSet);
%     
%     figure('name', 'AP reconstruction with negative-phase-constraint');
%     showImage(angle(cropToCenter(phaseRecNeg.x, [512 512])));
%
%

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
% Original author: Johannes Hagemann
   
    
    methods
        % constructor
        function obj = PProductSpace()
            
        end %end constructor
        
        % apply the projector and return error measure
        function [x, errorVal] = eval(obj, x, ~)
            
            tmp = mean(x, 3);
            x = repmat(tmp, [1 1 size(x,3)]);     
            errorVal = 0;            
        end

    end
end