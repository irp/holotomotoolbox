%%
classdef Projector < handle
	
	methods
		% evaluation of template-projector is identity map
		function [x, error] = eval(obj, x, ~) 
			error = 0;
		end

		% reflection = 2*projection - identity (formula is independent of the specific projector)
		function [x, xProj, errorProj] = reflect(obj, x, it)
			[xProj, errorProj] = obj.eval(x, it);
			x = 2*xProj - x;
		end
		
		% composition of projectors
		function newObj = mtimes(obj1,obj2)
			if isa(obj1, 'Projector') && isa(obj2, 'Projector')
				newObj = ProjectorComposition(obj1, obj2);
			else
				error(['Operator ''*'' is not implemented for operands of type ''', class(obj1), ''' and ''', class(obj2), '''.']);
			end
		end
	end
	
end
