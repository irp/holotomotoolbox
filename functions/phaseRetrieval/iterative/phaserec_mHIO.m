function [phase, amplitude] = phaserec_mHIO(holograms, fresnelNumbers, settings) 
% PHASEREC_MHIO reconstructs the phase from given holograms using the mHIO
% (modified hybrid-input-output) algorithm.
%
%    ``[phase, amplitude] = phaserec_mHIO(holograms, fresnelNumbers, settings)``
%
% The algorithm has been proposed and is described in detail in :cite:`Giewekemeyer_PRA_2011`.
%
%
% Parameters
% ----------
% holograms : cell-array or numerical array
%     hologram(s) to reconstruct from
% fresnelNumbers : numerical array
%     pixel size-based Fresnel numbers, at which the holograms were acquired: the columns
%     correspond to possibly multiple propagation distances (size(fresnelNumbers,2) must
%     match the number of holograms). If the array has two rows, the rows are interpreted
%     as possibly distinct Fresnel numbers along the two dimensions of the holograms (astigmatism).
% settings : structure, optional
%     contains additional settings, see *Other Parameters*
%
%
% Other Parameters
% ----------------
% Same parameters as in phaserec_iterative (exception: no parameter algorithm) plus
% additional feedback parameters of the modified hybrid-input-output scheme:
%
% beta : Default = 0.2;
%     Feedback-parameter for the amplitude. See :cite:`Giewekemeyer_PRA_2011` for exact meaning.
% gamma : Default = 0.2;
%     Feedback-parameter for the phase. See :cite:`Giewekemeyer_PRA_2011` for exact meaning.
% 
%
% Returns
% -------
% phase : numerical array
%     The reconstructed phase image of the same size as the input holograms
% amplitude : numerical array
%     The reconstructed amplitude image of the same size as the input holograms (OPTIONAL: with
%     the default settings minAmplitude = maxAmplitude = 1 (pure phase constraint), no physically 
%     reasonable amplitude image will be recovered) 
%
%
% See also
% --------
% functions.phaseRetrieval.iterative.phaserec_iterative
% functions.phaseRetrieval.iterative.phaserec_AP
% functions.phaseRetrieval.iterative.phaserec_RAAR
%
%
% Example
% -------
%
%     % Simulate holograms
%     fresnelNumbers = [0.8,0.9]*1e-3;
%     phaseTrue = -getPhantom('dicty');
%     holograms = simulateHologram(phaseTrue, fresnelNumbers);
%     
%     % Reconstruct using modified HIO algorithm (with support- and max-phase-constraints)
%     settings.support = phaseTrue < 0;
%     settings.maxPhase = 0;
%     settings.plotIterates = true;
%     settings.iterations = 200;
%     phaseRec = phaserec_mHIO(holograms, fresnelNumbers, settings);
%     
%     % Plot results
%     subplot(1,2,1);
%     showImage(phaseTrue);
%     title('True phase-image');
%     subplot(1,2,2);
%     showImage(phaseRec);
%     title('Reconstructed phase-image');
%
% 
% See also PHASEREC_ITERATIVE, PHASEREC_AP, PHASEREC_RAAR

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

defaults = rmfield(phaserec_iterative(), 'algorithm');
defaults.beta = 0.2;
defaults.gamma = 0.2;
if nargin == 0
    phase = defaults;
    return;
end

% Complete settings with default parameters
if nargin < 3
    settings = struct;
end
settings = completeStruct(settings, defaults);



% If holograms are assigned as cell-arrays, convert to numerical arrays
if iscell(holograms)
    holograms = cat(3, holograms{:});
end

% Check sanity of input
numHolos = size(holograms,3);
if ~isnumeric(fresnelNumbers)
    error('fresnelNumbers must be a single number, vector or matrix. Cell-arrays are not supported.');
end
if size(fresnelNumbers,2) ~= numHolos
    error('size(fresnelNumbers,2) must be equal to the number of assigned holograms.');
end
if size(fresnelNumbers,1) ~= 1 && size(fresnelNumbers,1) ~= 2
    error(['size(fresnelNumbers,1) must be either 1 (no astigmatism) or 2', ...
           ' (possibly astigmatic setting).']);
end

% Same Fresnel number along both dimensions of the holograms if fresnelNumbers has only one row
fresnelNumbers = fresnelNumbers .* ones([2,numHolos]);    



% Run iterative algorithm on GPU if desired and possible
useGPU = settings.useGPUIfPossible && checkGPUSupport();
if settings.useGPUIfPossible && ~useGPU
    warning(['Unable to initialize GPU. Running phase reconstruction on CPU instead. ', ...
             'Set settings.useGPUIfPossible = false to suppress this warning.']);
end



% Optionally pad holograms and transfer to GPU 
holograms = padarray(gpuArrayIf(holograms, useGPU), [settings.pady, settings.padx], 'replicate');



% Construct projector onto measured holograms
settingsP = struct;
settingsP.useGPU = useGPU;
settingsP.projectionOrder = settings.projectionOrder;
PData = PFresnelMagnitude(sqrt(holograms), fresnelNumbers, settingsP);


% Projector onto minimum- and/or maximum-constraints for the phase
settingsP = struct;
settingsP.minVal = settings.minPhase;
settingsP.maxVal = settings.maxPhase;
PPhaseRange = PMinMax(settingsP);


% Projector onto minimum- and/or maximum-constraints for the amplitude
settingsP = struct;
settingsP.minVal = settings.minAmplitude;
settingsP.maxVal = settings.maxAmplitude;
PAmplitudeRange = PMinMax(settingsP);


% Negative of the binary support mask
notSupport = ~settings.support;


% Initial guess for the reconstructed wave-field (not for the phase itself)
if ~isempty(settings.phaseGuess)
    phase = gpuArrayIf(settings.phaseGuess, useGPU);
else
    phase = gpuArrayIf(zeros([size(holograms,1), size(holograms,2)], class(holograms)), useGPU);
end
amplitude = gpuArrayIf(ones([size(holograms,1), size(holograms,2)], class(holograms)), useGPU);



% Figure for plotting of intermediate reconstructions
if settings.plotIterates
    figRecon = figure;
end




%% ACTUAL RECONSTRUCTION
for iteration = 1:settings.iterations

    % Save amplitude and phase of previous iterate
    amplitudeOld = amplitude;
    phaseOld = phase;
    
    % Project onto hologram data
    x = PData.eval(amplitude .* exp(1i * phase), iteration);

    % Decompose into amplitude and phase;
    amplitude = abs(x);
    phase = angle(x);

    % Amplitude update
    %
    % Update for constant amplitude (= pure phase constraint): feedback everywhere
    % (this is the variant originally proposed by Giewekemeyer et Al. (2011))
    if settings.minAmplitude == settings.maxAmplitude
        amplitude = amplitudeOld - settings.beta .* (amplitude-1);
    %
    % Update for the case of varying amplitudes: usual HIO-update
    else
        amplitude = PAmplitudeRange.eval(amplitude);
        amplitude(notSupport) = amplitudeOld(notSupport) - settings.beta .* (amplitude(notSupport)-1);
    end

    % Phase update
    phase = PPhaseRange.eval(phase);
    phase(notSupport) = phaseOld(notSupport) - settings.gamma * phase(notSupport);
    
    %% optionally plot current iterate
    if settings.plotIterates && mod(iteration, settings.plotInterval) == 0
        figureSilent(figRecon);
        subplot(1,2,1); imagesc(phase); axis 'square'; colorbar; title('phase');
        subplot(1,2,2); imagesc(amplitude); axis 'square'; colorbar; title('amplitude');
        set(figRecon, 'name', sprintf(['[', mfilename, '] Iteration %i: current reconstruction'], iteration));
        drawnow
    end
    
end %iterations



% Clean up
if settings.plotIterates
    close(figRecon);
end


end
