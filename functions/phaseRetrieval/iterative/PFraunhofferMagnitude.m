%%
classdef PFraunhofferMagnitude < Projector
% PFraunhofferMagnitude is a projector class that matches a given image to Fresnel-amplitudes
% (square-root of measured holograms) by applying a magnitude-constraint in the Fresnel-domain.
%
%    ``obj = PFresnelMagnitude(measurements, fresnelNumbers, settings)``
%
% The projector supports both the case of a single hologram as well as several holograms
% at different Fresnel-numbers. In the latter case, different variants are available
% of how the information from the different holograms is combined into one projection.
%
%
% Parameters
% ----------
% measurements : numerical array 
%     contains the amplitudes of the Fresnel-measurements, i.e. the pointwise square-root of
%     measured holograms. In the case of multiple holograms, the array must be 3D, where
%     the measurements where the Fresnel number varies along the third array-dimension.
% fresnelNumbers : numerical array
%     the Fresnel number(s) corresponding to the different holograms.
% settings : struct
%     contains settings, see *Other Parameters*
%
%
% Other Parameters
% ----------------
% projectionOrder : Default = 'averaged'
%     Determines how the projections are computed in the case of multiple measurements for
%     different Fresnel-numbers. For a single measurement, all options lead to the same behavior:
%
%     'averaged'
%          The output-image imOut is computed as the average of the projections onto the different
%          measurements, i.e. if P_1, P_2, ..., P_n denote the corresponding projectors, then
%          imOut = (P_1(imIn) + P_2(imIn) + ... + P_n(imIn))
%     'sequential' 
%          The input-image imIn is projected onto the measurements on after the other, i.e. if 
%          P_1, P_2, ..., P_n denote the projectors for the single measurements, the composition
%		   is evaluated: imOut = P_n(... P_2(P_1(imIn))... )
%     'cyclic' 
%          The input-image imIn is projected only onto a single measurement in each 
%          projector-evaluation, but the target-measurement is cyclically varied, i.e. if the
%          projector evaluated the i-th time, then imOut = P_{mod(i-1,n)+1}(imIn)
%
% applicationMask : Default = []
%     array of true/false values (size = size(measurements)) that allows to specify for which
%     entries of measurements the magnitude-constraint is applied. This allows to account for possible
%     outliers in the hologram-data by setting the corresponding entries of applicationMask to false.
%     If empty, the magnitude-constraint is applied for all entries of measurements.
% useGPU : Default = false
%     Determines whether the projector-evaluations are computed on the GPU.
% doMErrors: Default = false
%     Determines whether the residuals, i.e. the norm of the difference between the measurements
%     and the Fresnel-magnitudes of the input images, are computed at each projector-evaluation.
%
% Example
% -------
%

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.


properties (SetAccess = 'private')
	prop	% Fresnel-propagator object
	settings
	measurements
    absx
    xNew
end
	
methods (Static)
	
	function defaults = defaultSettings()
		defaults.applicationMask = [];
		defaults.useGPU = true;
		defaults.doMErrors = false;
		defaults.zeroThreshold = 1e-10;
        defaults.plotIterates = false;
        defaults.plotInterval = 50;
	end
		
end	 % end static methods
	
	
methods

	% constructor
	function obj = PFraunhofferMagnitude(measurements, settings)
		
		% Complete settings with defaults
		if nargin < 2
			settings = struct;
		end
		obj.settings = completeStruct(settings, obj.defaultSettings());
		
		obj.measurements = gpuArrayIf(measurements, obj.settings.useGPU);
		
		sizeIm = [size(measurements,1), size(measurements,2)];

		if isempty(obj.settings.applicationMask)
			% Empty array has to be replicated to avoid errors in evalSequential and evalCyclic 
			obj.settings.applicationMask = repmat(obj.settings.applicationMask, [1,1]);
		else
			obj.settings.applicationMask = gpuArrayIf(obj.settings.applicationMask, obj.settings.useGPU);
		end
					
	end %end constructor
		
		
	% apply the projector and return error measure
    function [x, residual] = eval(obj, x, currentIteration)
        
        xOld = x;
        
        % Fraunhoffer
        x = fftnc(exp(1i*x));
        
        % apply magnitude-constraint to Fresnel-propagated array (avoiding to divide by zero)
        obj.absx = abs(x);
        
        % optionally calculate residual
        if obj.settings.doMErrors && currentIteration > 1
            residual = gather(norm(obj.absx(:) - obj.measurements(:), 2));
        else
            residual = inf;
        end
%             psi = np.exp(1j*x)
%     X = ft2(psi)/2048. 
%     X = dp*X/(abs(X)+(X.real==0)*(X.imag==0)) 
%     psi2 = ift2(X)*2048.
%    diff = psi2/psi - 1;
%     #diff = diff*(psi!=0) + psi2*(psi==0)
%     #return x - 1j*diff
%      return x - 1j*(diff - diff**2/2.)
        
        nonzeroIdx = (obj.absx >= obj.settings.zeroThreshold);
        if all(nonzeroIdx(:))
            obj.xNew = (obj.measurements./obj.absx) .* x;
%              obj.xNew = (obj.measurements .* x) ./ (obj.absx);
        else
            obj.xNew = obj.measurements;
%             obj.xNew(nonzeroIdx) = (obj.measurements(nonzeroIdx)./obj.absx(nonzeroIdx)) .* x(nonzeroIdx);
            obj.xNew(nonzeroIdx) = (obj.measurements(nonzeroIdx) .* x(nonzeroIdx)) ./ (obj.absx(nonzeroIdx));
        end
        
        % only use those values of xNew for which mask is true
        if isempty(obj.settings.applicationMask)
            x = obj.xNew;
        else
            x(obj.settings.applicationMask) = obj.xNew(obj.settings.applicationMask);
        end
        if  obj.settings.plotIterates && mod(currentIteration, obj.settings.plotInterval) == 0
            figureSilent(10100)
            %                 subplot(1,3,1)
            imagesc(log10(obj.absx(:,:,1)))
            %                 subplot(1,3,2)
            %                 imagesc(abs(obj.xNew(:,:,1)))
            %                 subplot(1,3,3)
            %                 imagesc(abs(obj.xNew(:,:,1)) - obj.absx(:,:,1))
            title(['iteration ' num2str(currentIteration)])
            drawnow
        end
        % back-propagation
        x = ifftnc(x);
 
%         sum(isnan(x(:)))
%         sum(isinf(x(:)))
        %     diff = psi2/psi - 1
%     #diff = diff*(psi!=0) + psi2*(psi==0)
%     #return x - 1j*diff
%     return x - 1j*(diff - diff**2/2.)
%         diff = (x./exp(1i*xOld)) - 1;
%         x = xOld - 1i* (diff - 0.5.*diff.^2);
%         sum(isnan(x(:)))
%         sum(isinf(x(:)))  
        
    end %end eval
    
    function setMeasurements(obj, newMeasurements)
        if (   ~strcmpi(class(obj.measurements), class(newMeasurements)) ...
                || any(size(obj.measurements) ~= size(newMeasurements))         )
            error('New measurements must be of the same size and class as the original ones.');
        end
        obj.measurements = gpuArrayIf(newMeasurements, obj.settings.useGPU);
    end
    
end % methods
	
end % class
