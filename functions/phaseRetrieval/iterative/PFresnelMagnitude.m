%%
classdef PFresnelMagnitude < Projector
% PFRESNELMAGNITUDE is a projector class that matches a given image to Fresnel-amplitudes
% (square-root of measured holograms) by applying a magnitude-constraint in the Fresnel-domain.
%
%    ``obj = PFresnelMagnitude(measurements, fresnelNumbers, settings)``
%
% The projector supports both the case of a single hologram as well as several holograms
% at different Fresnel-numbers. In the latter case, different variants are available
% of how the information from the different holograms is combined into one projection.
%
%
% Parameters
% ----------
% measurements : numerical array
%     contains the amplitudes of the Fresnel-measurements, i.e. the pointwise square-root of
%     measured holograms. In the case of multiple holograms, the array must be 3D, where
%     the Fresnel number varies along the third array-dimension.
% fresnelNumbers : numerical array
%     pixel size-based Fresnel numbers, at which the holograms were acquired: the columns
%     correspond to possibly multiple propagation distances (size(fresnelNumbers,2) must
%     match the number of holograms). If the array has two rows, the rows are interpreted
%     as possibly distinct Fresnel numbers along the two dimensions of the holograms (astigmatism).
% settings : struct
%     contains settings, see *Other Parameters*
%
%
% Other Parameters
% ----------------
% projectionOrder : Default = 'averaged'
%     Determines how the projections are computed in the case of multiple measurements for
%     different Fresnel-numbers. For a single measurement, all options lead to the same behavior:
%
%     'averaged'
%          The output-image imOut is computed as the average of the projections onto the different
%          measurements, i.e. if P_1, P_2, ..., P_n denote the corresponding projectors, then
%          imOut = (P_1(imIn) + P_2(imIn) + ... + P_n(imIn))
%     'sequential'
%          The input-image imIn is projected onto the measurements on after the other, i.e. if
%          P_1, P_2, ..., P_n denote the projectors for the single measurements, the composition
%		   is evaluated: imOut = P_n(... P_2(P_1(imIn))... )
%     'cyclic'
%          The input-image imIn is projected only onto a single measurement in each
%          projector-evaluation, but the target-measurement is cyclically varied, i.e. if the
%          projector evaluated the i-th time, then imOut = P_{mod(i-1,n)+1}(imIn)
%
% applicationMask : Default = []
%     array of true/false values (size = size(measurements)) that allows to specify for which
%     entries of measurements the magnitude-constraint is applied. This allows to account for possible
%     outliers in the hologram-data by setting the corresponding entries of applicationMask to false.
%     If empty, the magnitude-constraint is applied for all entries of measurements.
% useGPU : Default = false
%     Determines whether the projector-evaluations are computed on the GPU.
% doMErrors: Default = false
%     Determines whether the residuals, i.e. the norm of the difference between the measurements
%     and the Fresnel-magnitudes of the input images, are computed at each projector-evaluation.
% representation : Default = angleAmp
%     Choses how the wave field is represented. Options are angleAmp
%     conventioal representation in amplitude and phase or refractive which
%     uses the representation of the refractive index n = δ + iβ.
% singlePrecision : Default = true
%      Carries out computations in single or double precision.
%
% Example
% -------
%

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

    
    properties (SetAccess = 'private')
        prop	% Fresnel-propagator object
        settings
        measurements
%         absx
        xNew
        numMeasurements
    end
    
    properties (GetAccess = 'public')
        alignmentShift
    end
    
    methods (Static)
        
        function defaults = defaultSettings()
            defaults.projectionOrder = 'averaged';
            defaults.representation = 'angleAmp';
            defaults.applicationMask = [];
            defaults.useGPU = false;
            defaults.singlePrecision = true;
            defaults.doMErrors = false;
            defaults.zeroThreshold = 1e-10;
            defaults.plotIterates = false;
            defaults.plotInterval = 50;
            %% more extras
            defaults.doEnsemble = false;
            defaults.shifts = [];
            defaults.rho = [];
            %% alignment settings
            defaults.checkAlignment = false;
            defaults.settingsAlignment = alignImages;
        end
        
    end	 % end static methods
    
    
    methods
        
        % constructor
        function obj = PFresnelMagnitude(measurements, fresnelNumbers, settings)
            
            % Complete settings with defaults
            if nargin < 3
                settings = struct;
            end
            obj.settings = completeStruct(settings, obj.defaultSettings());
            
            obj.measurements = gpuArrayIf(measurements, obj.settings.useGPU);
            obj.numMeasurements = size(obj.measurements,3);
            sizeIm = [size(measurements,1), size(measurements,2)];
            
            % Check compatibility of input measurements and Fresnel numbers
            if ~isnumeric(fresnelNumbers)
                error('fresnelNumbers must be a single number, vector or matrix. Cell-arrays are not supported.');
            end
            if size(fresnelNumbers,2) ~= obj.numMeasurements
                error('size(fresnelNumbers,2) must be equal to the number of assigned holograms size(measurements,3).');
            end
            if size(fresnelNumbers,1) ~= 1 && size(fresnelNumbers,1) ~= 2
                error(['size(fresnelNumbers,1) must be either 1 (no astigmatism) or 2', ...
                    ' (possibly astigmatic setting).']);
            end
            
            if isempty(obj.settings.applicationMask)
                % Empty array has to be replicated to avoid errors in evalSequential and evalCyclic
                obj.settings.applicationMask = repmat(obj.settings.applicationMask, [1,1,obj.numMeasurements]);
            else
                obj.settings.applicationMask = gpuArrayIf(obj.settings.applicationMask, obj.settings.useGPU);
            end
            
            % set up propagators
            propSet = FresnelPropagator.defaultSettings();
            propSet.method = 'chirpLimited';
            propSet.gpuFlag = obj.settings.useGPU;
            propSet.singlePrecision = obj.settings.singlePrecision;
            
            % construct Fresnel-propagator(s): one joint propagator in the averaged case (for
            % performance-reasons) and separate ones for each Fresnel-number in the case of
            % 'sequential' or 'cyclic' as projectionOrder
            if strcmpi(obj.settings.projectionOrder, 'averaged')
                obj.prop = FresnelPropagator(sizeIm, fresnelNumbers, propSet);
            else
                for ii = 1:obj.numMeasurements
                    obj.prop{ii} = FresnelPropagator(sizeIm, fresnelNumbers(:,ii), propSet);
                end
            end
            
        end %end constructor
        
        %%
        % apply the projector and return error measure
        function [x, residual] = eval(obj, x, currentIteration)
            
            switch obj.settings.projectionOrder
                case 'averaged'
                    evalAveraged();
                    
                case 'sequential'
                    evalSequential();
                    
                case 'cyclic'
                    evalCyclic();
                    
                case 'averagedRefractive'
                    evalAveragedRefractive();

                case 'sequentialRefractive'
                    evalSequentialRefractive();
                    
                case 'cyclicRefractive'
                    evalCyclicRefractive();
                    
                otherwise
                    error(['Undefined projection order ' obj.settings.projectionOrder])
            end
            
        %%
            function [x, residual] = evalBase(x, M, prop, mask)

                % Fresnel-propagation
                x = prop.propagate(x);

                % apply magnitude-constraint to Fresnel-propagated array (avoiding to divide by zero)
                absx = abs(x);

                % optionally calculate residual
                if obj.settings.doMErrors
                    residual = gather(norm(absx(:) - M(:), 2));
                else
                    residual = Inf;
                end

                nonzeroIdx = (absx >= obj.settings.zeroThreshold);

                if all(nonzeroIdx(:))
                    if strcmp(obj.settings.representation, 'refractive')
                        obj.xNew = x .* (1-(M./absx));
                    else
                        obj.xNew = (M./absx) .* x;
                    end
                else
                    obj.xNew = M;
                    obj.xNew(nonzeroIdx) = (M(nonzeroIdx)./absx(nonzeroIdx)) .* x(nonzeroIdx);
                end

                % only use those values of xNew for which mask is true
                if isempty(mask)
                    x = obj.xNew;
                else
                    if strcmp(obj.settings.representation, 'refractive')
                        x = obj.xNew .* mask + x.* (1 - mask);
                    else
                        x(mask) = obj.xNew(mask);
                    end
                end

                % Fresnel-back-propagation
                x = prop.backPropagate(x);

                if  obj.settings.plotIterates && mod(currentIteration, obj.settings.plotInterval) == 0
                    figureSilent(10100)
                    subplot(2,2,1)
                    imagesc(absx(:,:,1))
                    title('absx')
                    subplot(2,2,2)
                    imagesc(absx - M)
                    title('absx - M')
                    subplot(2,2,3)
                    imagesc(abs(obj.xNew(:,:,1)))
                    title(num2str(currentIteration))
                    drawnow
                end

            end
            
            function evalAveraged()
                [x, residual] = evalBase(x, obj.measurements, obj.prop, obj.settings.applicationMask);
                x = (1./obj.numMeasurements) * x;
            end
            
            function evalAveragedRefractive()
                xOld = exp(1i*x); %convert to amp/pha for propagation
                xNew = xOld;
                xNew(:) = 0;
                maxO = max(abs(xOld(:)).^2);
                for ii = 1:obj.numMeasurements
                    [tmp, residual] = evalBase(xOld, obj.measurements(:, :, ii), obj.prop{ii}, ...
                        obj.settings.applicationMask(:, :, ii) );
                     xNew = xNew  + tmp;
                end
                % x_{i+1} = x_{i} - Δx ! its - the positive gradient
                 x = x - ((1./obj.numMeasurements) * -1i*(conj(xOld)) ./ maxO .* xNew);                
            end
            
            function evalCyclicRefractive()
                xOld = exp(1i*x);
                maxO = max(abs(xOld(:)).^2);
                ii = mod(currentIteration, obj.numMeasurements)+1;
                [tmp, residual] = evalBase(xOld, obj.measurements(:,:,ii), obj.prop{ii}, ...
                    obj.settings.applicationMask(:, :, ii));
                 x = x + 1i*(conj(xOld) ./ maxO .* tmp);
            end

            function evalSequentialRefractive()
                for ii = 1:obj.numMeasurements
                    xOld = exp(1i*x); %convert to amp/pha for propagation
                    maxO = max(abs(xOld(:)).^2);

                    [tmp, residual] = evalBase(xOld, obj.measurements(:, :, ii), obj.prop{ii}, ...
                        obj.settings.applicationMask(:, :, ii) );

                    % x_{i+1} = x_{i} - Δx ! its - the positive gradient
                    x = x + 1i*(conj(xOld) ./ maxO .* tmp);
                end
            end
            
            function evalSequential()
                for ii = 1:obj.numMeasurements
                    [x, residual] = evalBase(x, obj.measurements(:, :, ii), obj.prop{ii}, ...
                        obj.settings.applicationMask(:, :, ii) );
                end
            end
            
            
            function evalCyclic()
                ii = mod(currentIteration, obj.numMeasurements)+1;
                [x, residual] = evalBase(x, obj.measurements(:,:,ii), obj.prop{ii}, ...
                    obj.settings.applicationMask(:, :, ii)        );
            end
            
        end
        
        
        function setMeasurements(obj, newMeasurements)
            if (   ~strcmpi(class(obj.measurements), class(newMeasurements)) ...
                    || any(size(obj.measurements) ~= size(newMeasurements))         )
                error('New measurements must be of the same size and class as the original ones.');
            end
            obj.measurements = gpuArrayIf(newMeasurements, obj.settings.useGPU);
        end
        
        function checkAlignment(obj, x)
            for MIdx = 1:size(obj.measurements, 3)
                % Fresnel-propagation
                absx = abs(obj.prop{MIdx}.propagate(x));
                
                [imAligned, shift, rotAngleDegree, imFilt] = ...
                    alignImages(gather(obj.measurements(:, :, MIdx)), ...
                    gather(absx), obj.settings.settingsAlignment);
                
                obj.alignmentShift(:,MIdx) = shift';
                obj.measurements(:, :, MIdx) = gpuArrayIf(imAligned, obj.settings.useGPU);
            end
        end    
    end % methods

    
end % class
