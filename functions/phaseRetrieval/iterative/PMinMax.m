%%
classdef PMinMax < Projector
% PMINMAX is a projector class that allows to impose minimum- and/or maximum constraints
% on the values of arrays.
%
%    ``obj = PMinMax(settings)``
%
% For complex-valued arrays, minimum- and/or maximum values may be imposed independently
% in real- and imaginary part
%
%
% Parameters
% ----------
% settings : struct
%     contains settings for this projector, see *Other Parameters*
%
%
% Other Parameters
% ----------------
% minVal : Default = -inf;
%     Lower bound / minimum-constraint for the array-values (applied in the real part for
%     complex-valued arrays).
% maxVal : Default = inf;
%     Upper bound / maximum-constraint for the array-values (applied in the real part for
%     complex-valued arrays).
% minImag : Default = -inf;
%     Lower bound / minimum-constraint for the imaginary part of the array-values. Without
%     effect if real-valued arrays are projected.
% maxImag : Default = inf;
%     Upper bound / maximum-constraint for the imaginary part of the array-values. Without
%     effect if real-valued arrays are projected.
%
%
% Example
% -------
%
%
% See also
% --------
%
    
% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

properties (SetAccess = 'private')
	settings
	isTrivial
end


methods (Static)
	function defaults = defaultSettings()
		% settings defaults
		defaults.minVal = -inf;
		defaults.maxVal = inf;
		defaults.minImag = -inf;
		defaults.maxImag = inf;
	end
end	 % end static methods


methods

	% constructor
	function obj = PMinMax(settings)
		obj.settings = completeStruct(settings, obj.defaultSettings());
		if obj.settings.minVal > obj.settings.maxVal
			error('minVal must be <= maxVal.');
		end
		if obj.settings.minImag > obj.settings.maxImag
			error('minImag must be <= maxImag.');
		end
		% Flag that is true if projector does nothing (allows to save computation time)
		obj.isTrivial = (   obj.settings.minVal == -inf && obj.settings.maxVal == -inf ...
		                 && obj.settings.minImag == -inf && obj.settings.maxImag == -inf  );
	end %end constructor


	% apply the projector and return residual measure
	function [x, residual] = eval(obj, x, ~)

		% This projector does not compute any residuals
		residual = 0;

		% Special case: trivial constants
		if obj.isTrivial
			return;
		end

		% Impose minimum and/or maximum-constraints as required
		if isreal(x)
			if obj.settings.minVal > -inf
				x = max(x, obj.settings.minVal);
			end
			if obj.settings.maxVal < inf
				x = min(x, obj.settings.maxVal);
			end
		else
			realX = real(x);
			if obj.settings.minVal > -inf
				realX = max(realX, obj.settings.minVal);
			end
			if obj.settings.maxVal < inf
				realX = min(realX, obj.settings.maxVal);
			end

			imagX = imag(x);
			if obj.settings.minImag > -inf
				imagX = max(imagX, obj.settings.minImag);
			end
			if obj.settings.maxImag < inf
				imagX = min(imagX, obj.settings.maxImag);
			end

			x = realX + 1i * imagX;
		end

	end

end % methods

end


