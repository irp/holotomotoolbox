%%
classdef PMinMaxPhase < Projector
% PMINMAXPHASE is a projector class that allows to impose minimum- and/or maximum constraints
% on the phase of complex-valued arrays.
%
%    ``obj = PMinMaxPhase(settings)``
%
%
% Parameters
% ----------
% settings : struct
%     contains settings for this projector, see *Other Parameters*
%
%
% Other Parameters
% ----------------
% maxPhase : Default = inf;
%     Allows to enforce a maximum value of the phases. For example, assigning the
%     value 0 imposes negative phases. This is very powerful for samples with a
%     minimum phaseshift larger than -pi. For samples with stronger
%     phaseshift this constraint induces artifacts. In this case the
%     unwrap option has to be enabled as well for correct results.
% minPhase : Default = -inf;
%     Allows to enforce a minimum value for the phases.
% unwrapPhases : Default = false;
%     unwrap phases - can be necessary to correctly apply minPhase and/or maxPhase,
%     otherwise has not effect.
%
%
% Example
% -------
%
%
% See also
% --------
%
    
% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

properties (SetAccess = 'private')
	settings
end


methods (Static)
	function defaults = defaultSettings()
		% settings defaults
		defaults.maxPhase = inf;
		defaults.minPhase = -inf;
		defaults.unwrapPhases = false;
	end
end	 % end static methods


methods
	% constructor
	function obj = PMinMaxPhase(settings)
		obj.settings = completeStruct(settings, obj.defaultSettings());
		if obj.settings.minPhase > obj.settings.maxPhase
			error('minPhase must be <= maxPhase.');
		end
	end %end constructor


	% apply the projector and return residual measure
	function [x, residual] = eval(obj, x, ~)

		% This projector does not compute any residuals
		residual = Inf;

		% If no minimum- or maximum-constraints are imposed, evaluation is trivial
		if obj.settings.maxPhase == inf && obj.settings.minPhase == -inf
			return;
		end

		% Compute phase of complex-valued input
		phase = angle(x);
	   
		% Optional phase-unwrapping
		if obj.settings.unwrapPhases
			phase = unwrap(phase);
		end
			
		% Apply optional min-/max-constraints to phase			
		if obj.settings.maxPhase < inf
			phase = min(phase, obj.settings.maxPhase);
		end
		if obj.settings.minPhase > -inf
			phase = max(phase, obj.settings.minPhase);
		end

		% Combine with processed phases with amplitude of input
		x = abs(x) .* exp(1i .* phase);
	end

end % methods

end


