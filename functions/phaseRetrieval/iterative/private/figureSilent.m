function figureSilent(h)
% changes the current figure without given the focus to this window
% 
% Author: Martin Krenkel

	if ishandle(h)
		set(0,'CurrentFigure',h)
	else
		figure(h)
	end
end

