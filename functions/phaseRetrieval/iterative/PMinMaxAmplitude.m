%%
classdef PMinMaxAmplitude < Projector
% PMINMAXAMPLITUDE is a projector class that allows to impose minimum- and/or maximum constraints
% on the amplitude of complex-valued arrays.
%
%    ``obj = PMinMaxAmplitude(settings)``
%
%
% Parameters
% ----------
% settings : struct
%     contains settings for this projector, see *Other Parameters*
%
%
% Other Parameters
% ----------------
% maxAmplitude : Default = inf;
%     Allows to enforce a maximum value of the amplitudes. For example, assigning the
%     value 1 corresponds to enforcing physically meaningful values of the absorption
% minAmplitude : Default = 0;
%     Allows to enforce a minimum value for the amplitudes. Negative values are not meaningful.
%
%
% Example
% -------
%
%
% See also
% --------
%
    
% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

properties (SetAccess = 'private')
	settings
end


methods (Static)
	function defaults = defaultSettings()
		% settings defaults
		defaults.maxAmplitude = inf;
		defaults.minAmplitude = 0;
		defaults.zeroThreshold = 1e-10;
	end
end	 % end static methods


methods

	% constructor
	function obj = PMinMaxAmplitude(settings)
		obj.settings = completeStruct(settings, obj.defaultSettings());
		if obj.settings.minAmplitude > obj.settings.maxAmplitude
			error('minAmplitude must be <= maxAmplitude.');
		end
	end %end constructor


	% apply the projector and return residual measure
	function [x, residual] = eval(obj, x, ~)

		% This projector does not compute any residuals
		residual = Inf;

		% Special case 1: no minimum- or maximum-constraints are imposed, evaluation is trivial
		if obj.settings.maxAmplitude == inf && obj.settings.minAmplitude <= 0
			return;
		end

		% Compute amplitude of input
		amplitude = abs(x);

		% Special case 2: maximum and minimum coincide. Set all amplitudes to this value
		if obj.settings.maxAmplitude == obj.settings.minAmplitude
			targetAmplitude = obj.settings.minAmplitude;
		end

		% General case: apply non-trivial minimum and/or maximum constraints to amplitude
		targetAmplitude = amplitude;
		if obj.settings.maxAmplitude < inf
			targetAmplitude = min(targetAmplitude, obj.settings.maxAmplitude);
		end
		if obj.settings.minAmplitude > 0
			targetAmplitude = max(targetAmplitude, obj.settings.minAmplitude);
		end
				
		% Set amplitudes to targetAmplitude, taking care of possible values close to zero
		nonzeroIdx = (amplitude >= obj.settings.zeroThreshold);
		if all(nonzeroIdx(:))
			x = (targetAmplitude ./ amplitude) .* x;
		else
			x(nonzeroIdx) = (targetAmplitude(nonzeroIdx) ./ amplitude(nonzeroIdx)) .* x(nonzeroIdx);
			x(~nonzeroIdx) = targetAmplitude(~nonzeroIdx);	% Zero phase is assigned where x is close to 0
		end

	end

end % methods

end


