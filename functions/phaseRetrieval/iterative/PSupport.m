classdef PSupport < Projector
% PSUPPORT is a projector class for iterative phase retrieval.
%
%    ``obj = PSupport(support, valueOutsideSupport)``
%
% This projector applies a support constraint in the sample plane. 
%
%
% Parameters
% ----------
% support : boolean array
%     contains a field 'measurements' with the amplitudes of the measurement
% valueOutsideSupport : number, Default = 1
%     the complement of the support is set to this number
%
%
% See also
% --------
% PPhase
 
% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
% Original author: Johannes Hagemann
    
    properties (SetAccess = 'private')
        supp
        valueOutsideSupp
    end


    methods
        function obj = PSupport(support, valueOutsideSupport)
            obj.supp = support;
            if nargin == 2
                obj.valueOutsideSupp = valueOutsideSupport;
            else
                obj.valueOutsideSupp = 1;
            end
        end % constructor


        function [x, error] = eval(obj, x, ~)
            x = x .* obj.supp + obj.valueOutsideSupp .* (1 - obj.supp);
            error = Inf;
        end
    end % methods
end

