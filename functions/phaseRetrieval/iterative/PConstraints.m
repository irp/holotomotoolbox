classdef PConstraints < Projector
% PROJECTORTEMPLATE is the base to start your implementation of a new fancy
% projector
%
%    ``obj = ProjectorTemplate(constraints, p, opts)``
% 
% Parameters
% ----------
% Don't forget to comment what you did!
%
% Other Parameters
% ----------------
% 
%
% Returns
% -------
%

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

properties (SetAccess = 'private')
	settings
    pha
    amp
end


methods (Static)
    function defaults = defaultSettings()
        % options defaults
        
        % we can choose if we want to keep the amplitudes of the wave
        % field. setting this to 0 is basically the assumption of a
        % non-absorbing object
        defaults.keepAmplitude = 1;
        defaults.upperAmplitude = 1;
        defaults.lowerAmplitude = 0;
        
        % negativity is a powerful constraint for weak objects. but if
        % the phase shift of the object should get to strong,
        % additional measures have to be taken. see unwrapping further
        % down.
        defaults.projectOnNegativity = 1;
        
        % FWHM in px of a gaussian to smooth the current amplitudes
        % suggested value: 2 px
        defaults.smoothAmplitudesBy = 0;
        
        % FWHM in px of a gaussian to smooth the current phases. This
        % can be dangerous if we have phase jumps.
        % suggested value: 2 px
        defaults.smoothPhasesBy = 0;
        
        % unwrap phases - can be necessary to apply other constraints,
        % e.g. negativity. Extremly expensive
        defaults.unwrapPhases = 0;
        
        defaults.verbose = 0;
        
        defaults.plotInterval = 10;
        
        
    end
end	 % end static methods

methods
        % constructor
        function obj = PConstraints(settings)
            obj.settings = completeStruct(settings, obj.defaultSettings());
        end %end constructor
        
        % apply the projector and return error measure
        function [x, errorVal] = eval(obj, x, itNum)
            errorVal = inf;
              % this should hopefully be the correct order to apply the
            % options. first we apply the constraints, smoothing is done at
            % last to iron out the other effects
            obj.pha = angle(x);
            
            if obj.settings.unwrapPhases
                
                obj.pha = (gpuArray(eval_unwrap(gather(cropToCenter(obj.pha, [2048 2048])),...
                    gather(cropToCenter(abs(x), [2048 2048])))));
                obj.pha = padToSize(obj.pha, [2048 2048]*2);
                
            end
            
            if obj.settings.projectOnNegativity
%                 if p.use_GPU
%                     obj.pha = arrayfun(@(x)(min(x, 0) ), obj.pha);
%                 else
                    obj.pha(obj.pha > 0) = 0;
%                 end
            end
            
            % smoothing the phases can be tricky. if wrapping occurs (even
            % on unwrapped phases) the smoothing can obscure the phase
            % shift and the reconstruction will fail.
            if  obj.settings.smoothPhasesBy	 > 0
                obj.pha = (imgaussfilt((obj.pha), obj.settings.smoothPhasesBy/2.35));
            end
            
            
            if(obj.settings.keepAmplitude)
                obj.amp = abs(x);
                
                % General case: apply non-trivial minimum and/or maximum constraints to amplitude
                if obj.settings.upperAmplitude < inf
                    obj.amp = min(obj.amp, obj.settings.upperAmplitude);
                end
                if obj.settings.lowerAmplitude > 0
                    obj.amp = max(obj.amp, obj.settings.lowerAmplitude);
                end
                                
                
                if  obj.settings.smoothAmplitudesBy > 0
                    obj.amp = (imgaussfilt(abs(obj.amp), obj.settings.smoothAmplitudesBy/2.35));
                end
                
                x(:,:,end) = obj.amp .* exp(1i * obj.pha);
            else
                x(:,:,end) = exp(1i .* obj.pha);
            end

            
            if obj.settings.verbose && mod(itNum, obj.settings.plotInterval) == 1
                figureSilent(1003)
                imagesc(obj.pha); 
                title(['phases iteration ' num2str(itNum)])
                drawnow
            end
            
            
            
            error = 0; % gap is evaluated outside
        end

    end
end