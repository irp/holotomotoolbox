function [phase, amplitude] = phaserec_iterative(holograms, fresnelNumbers, settings) 
% PHASEREC_ITERATIVE reconstructs the phase with an iterative projection algorithm.
%
%    ``[phase, amplitude] = phaserec_iterative(holograms, fresnelNumbers, settings)``
%
% TODO: detailed description
%
%
% Parameters
% ----------
% holograms : cell-array or numerical array
%     hologram(s) to reconstruct from
% fresnelNumbers : numerical array
%     pixel size-based Fresnel numbers, at which the holograms were acquired: the columns
%     correspond to possibly multiple propagation distances (size(fresnelNumbers,2) must
%     match the number of holograms). If the array has two rows, the rows are interpreted
%     as possibly distinct Fresnel numbers along the two dimensions of the holograms (astigmatism).
% settings : structure, optional
%     contains additional settings, see *Other Parameters*
%
%
% Other Parameters
% ----------------
% algorithm : Default = AP;
%     Defines the alternating projection-type algorithm to be used. {AP, RAAR, DRAP}
% iterations : Default = 100
%     Number of iterations performed by the algorithm.
% phaseGuess : Default = []
%     Initial guess for the reconstructed phase. Must be of the same size as
%     the holograms. If empty, the reconstruction is initialized with zeros.
% padx : Default = 0
%     Amount by which the holograms are symmetrically (replicate)-padded along the
%     x-dimension before reconstruction
% pady : Default = 0
%     Amount by which the holograms are symmetrically (replicate)-padded along the 
%     y-dimension before reconstruction
% projectionOrder : Default = 'averaged'
%     Determines how projections are computed in the case of multiple holograms.
%     Options = {'averaged', 'sequential', 'cyclic'}. For one hologram, the choice has no effect.
%     See documentation of PFresnelMagnitude for details.
% support : Default = []
%     Array of true- and false-values of the same size as the input holograms, specifying
%     an optional support constraint as a binary mask. If support == [], no support 
%     constraint is imposed.
% minPhase : Default = -inf 
%     Lower bound / minimum-constraint for the reconstructed phases. Imposes that all values
%     in phase are >= minPhase. If minPhase == -inf, no minimum-constraint is imposed.
% maxPhase : Default = inf
%     Upper bound / maximum-constraint for the reconstructed phases. Imposes that all values
%     in phase are <= maxPhase. If maxPhase == inf, no maximum-constraint is imposed.
% minAmplitude : Default = 1
%     Lower bound / minimum-constraint for the reconstructed amplitudes. Imposes that all values
%     in amplitude are >= minAmplitude. If minAmplitude <= 0, no minimum-constraint is imposed.
%     The default case minAmplitude = maxAmplitude corresponds to imposing a pure phase image
%     with constant amplitude = 1.
% maxAmplitude : Default = 1
%     Upper bound / maximum-constraint for the reconstructed amplitudes. Imposes that all values
%     in amplitude are <= maxAmplitude. If maxAmplitude == inf, no maximum-constraint is imposed.
%     The default case minAmplitude = maxAmplitude corresponds to imposing a pure phase image
%     with constant amplitude = 1.
% useGPUIfPossible : Default = true
%     Run the iterative optimization on GPU if GPU-computations are supported (only relevant
%     if support and/or min/max-constraints are imposed.
% plotIterates : Default = false
%     Plot intermediate reconstructions while the algorithm is running?
% plotInterval : Default = 10
%     If plotIterates is true, intermediate reconstructions are plotted every plotInterval-th 
%     iteration is plotted. If plotIterates is false, this parameter has no effect.
%
%
% Returns
% -------
% phase : numerical array
%     The reconstructed phase image of the same size as the input holograms
% amplitude : numerical array
%     The reconstructed amplitude image of the same size as the input holograms (OPTIONAL: with
%     the default settings minAmplitude = maxAmplitude = 1 (pure phase constraint), no physically 
%     reasonable amplitude image will be recovered) 
%
%
%
% See also
% --------
% functions.phaseRetrieval.holographic.phaserec_ctf
% functions.phaseRetrieval.holographic.phaserec_nonlinTikhonov
%
%
% Example
% -------
%
% .. code-block:: matlab
%
%     fresnelNumbers = [1.1,0.9,0.8,1.2]*1e-3;
%     phaseTrue = -getPhantom('dicty');
%     
%     holograms = simulateHologram(phaseTrue, fresnelNumbers);
%     
%     % CTF-reconstruction to obtain suitable initial guess
%     settingsCTF.lim1 = 1e-4;
%     settingsCTF.lim2 = 1e-2;
%     phaseRecCTF = phaserec_ctf(holograms, fresnelNumbers, settingsCTF);
%         
%     % Improve using iterative projection algorithm (RAAR)
%     settings.algorithm = 'RAAR';
%     settings.phaseGuess = phaseRecCTF;
%     settings.plotIterates = true;
%     phaseRecIterative = phaserec_iterative(holograms, fresnelNumbers, settings);
%     
%     subplot(1,3,1)
%     showImage(phaseTrue)
%     title('True phase-image')
%     
%     subplot(1,3,2)
%     showImage(phaseRecCTF)
%     title('Reconstructed phase-image (CTF)')
%
%     subplot(1,3,3)
%     showImage(phaseRecIterative)
%     title('Reconstructed phase-image (iterative)')
%
%     
% 
% See also PHASEREC_CTF, PHASEREC_NONLINTIKHONOV

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.


% Default parameters
defaults.algorithm = 'AP';
defaults.iterations = 100;
defaults.phaseGuess = [];
defaults.padx = 0;
defaults.pady = 0;
defaults.minPhase = -inf; 
defaults.maxPhase = inf;
defaults.minAmplitude = 1; 
defaults.maxAmplitude = 1;
defaults.support = [];
defaults.projectionOrder = 'averaged';
defaults.useGPUIfPossible = true;
defaults.plotIterates = false;
defaults.plotInterval = 10;

% Return default parameters if called without argument
if (nargin == 0)
    phase = defaults;
    return
end

% Complete settings with default parameters
if nargin < 3
    settings = struct;
end
settings = completeStruct(settings, defaults);
settings = completeStruct(settings, projectionAlgorithm.defaultSettings());


% If holograms are assigned as cell-arrays, convert to numerical arrays
if iscell(holograms)
    holograms = cat(3, holograms{:});
end

% Check sanity of input
numHolos = size(holograms,3);
if ~isnumeric(fresnelNumbers)
    error('fresnelNumbers must be a single number, vector or matrix. Cell-arrays are not supported.');
end
if size(fresnelNumbers,2) ~= numHolos
    error('size(fresnelNumbers,2) must be equal to the number of assigned holograms.');
end
if size(fresnelNumbers,1) ~= 1 && size(fresnelNumbers,1) ~= 2
    error(['size(fresnelNumbers,1) must be either 1 (no astigmatism) or 2', ...
           ' (possibly astigmatic setting).']);
end

% Same Fresnel number along both dimensions of the holograms if fresnelNumbers has only one row
fresnelNumbers = fresnelNumbers .* ones([2,numHolos]);    



% Run iterative algorithm on GPU if desired and possible
useGPU = settings.useGPUIfPossible && checkGPUSupport();
if settings.useGPUIfPossible && ~useGPU
    warning(['Unable to initialize GPU. Running phase reconstruction on CPU instead. ', ...
             'Set settings.useGPUIfPossible = false to suppress this warning.']);
end



% Optionally pad holograms and transfer to GPU 
holograms = padarray(gpuArrayIf(holograms, useGPU), [settings.pady, settings.padx], 'replicate');



% Construct projector onto measured holograms
settingsP = struct;
settingsP.useGPU = useGPU;
settingsP.projectionOrder = settings.projectionOrder;
PData = PFresnelMagnitude(sqrt(holograms), fresnelNumbers, settingsP);



% Construct projector onto constraints
%
% Optional constraints on the minimum and/or maximum amplitude (dummy projector if no bounds are given)
if settings.minAmplitude > 0 || settings.maxAmplitude < inf
    settingsP = struct;
    settingsP.minAmplitude = settings.minAmplitude;
    settingsP.maxAmplitude = settings.maxAmplitude;
    PAmplitudeConstr = PMinMaxAmplitude(settingsP);
else
    PAmplitudeConstr = Projector();
end
%
% Optional constraints on the minimum and/or maximum phase (dummy projector if no bounds are given)
if settings.minPhase > -inf || settings.maxPhase < inf
    settingsP = struct;
    settingsP.minPhase = settings.minPhase;
    settingsP.maxPhase = settings.maxPhase;
    PPhaseConstr = PMinMaxPhase(settingsP);
else
    PPhaseConstr = Projector();
end
%
% Optional support constraint (dummy projector if no such constraint is imposed)
if ~isempty(settings.support)
    support = padarray(gpuArrayIf(settings.support, useGPU), [settings.pady, settings.padx]);
    PSupportConstr = PSupport(support, 1);
else
    PSupportConstr = Projector();
end
%
% Combined projector on all constraints = composition of the different projectors
PConstr = PPhaseConstr * PAmplitudeConstr * PSupportConstr;



% Initial guess for the reconstructed wave-field (not for the phase itself)
if ~isempty(settings.phaseGuess)
    wavefield = exp(1i * gpuArrayIf(settings.phaseGuess, useGPU));
else
    wavefield = gpuArrayIf(ones([size(holograms,1), size(holograms,2)], class(holograms)), useGPU);
end



% Reconstruct wave-field in object plane by running alternating projection-type algorithm
projAlg = projectionAlgorithm(PData, PConstr, wavefield, settings);
result = projAlg.execute(settings.iterations);
wavefield = result.x;


% Undo optional padding
wavefield = croparray(wavefield, [settings.pady, settings.padx]);


% Compute phase and amplitude from reconstructed wavefield
phase = gather(angle(wavefield));
if nargout == 2
    amplitude = gather(abs(wavefield));
end


end %function
