classdef projectionAlgorithm < handle
% function [result, settings] = projectionAlgorithm(PM, PS, guess, settings)
% PROJECTIONALGORITHM provides general implementations of projection algorithm for
% phase retrieval.
%
%    ``[result, settings] = projectionAlgorithm(PM, PS, guess, settings)``
%
% This implementation provides the algorithms: AP, RAAR, DRAP.
% The algorithms can be customized
%
%
%
% Parameters
% ----------
% PM : projector object, Default = PFresnelMagnitude;
%     Defines the projector on the measurements. Or equivalent. Depending on the
%     setting different strategies for evaluation are available. For
%     details see in PFresnelMagnitude.m
% PS : projector object, Default = PPhase;
%     Defines the projector/constraints in the sample plane.
% guess : numerical array
%     Starting guess for the reconstruction. the size should match the size
%     of the measurements.
% settings : structure
%     contains additional settings, see *Other Parameters*
%
% Other Parameters
% ----------------
% algorithm : Default = AP;
%     Defines the algorithm to be used. {AP, RAAR, DRAP, HIO, NAG}
% supp : numerical array, optional
%     Defines the support. This array is only necessary if some version of
%     the support projector is applied.
% useGPU : Default = false;
%     Run computations on GPU?
% progressBar : Default = 0;
%     this slows computation immensely, use if a single iteration
%     takes quite long
% iterations : Default = 150
%     Number of iterations to do
% doMErrors : Default = 0
%     errors with respect to measurement or equivalent
% doSErrors : Default = 0
%     errors with respect to S constraint
% doGap : Default = 0
%     abs[P\ :sub:`S`\ (x\ :sub:`n`\ ) - P\ :sub:`M`\ (x\ :sub:`n`\ )]
% doConvergenceHistory : Default = 0;
%     abs[x\ :sub:`n`\ - x\ :sub:`n-1`\ ]
% abortThreshold = -7.2;
%      If the convergence history is recorded we can try to abort the
%      algorithm earlier when a certain smoothness of the preceeding
%      iterations is reached.
% abortAverageIterations = 100;
%      Determines the number of iterations which are used to calculate the
%      smoothness criterium for abort.
% plotIterates : Default = 0;
%     show current iterates
% plotInterval : Default = 10;
%     interval in which the current state is updated
% roiSize: Default = [];
%     Size of the preview for the current state. If this value is not set,
%     the whole array is shown.
% b0 : Default = 0.99
%     Only used with RAAR. Starting value of relaxation parameter.
% bM : Default = 0.75
%     Only used with RAAR. Final value of relaxation parameter.
% bS : Default = 20
%     Only used with RAAR. Iteration number when relaxation strategy is
%     switched.
% b : Default = 0.7
%     Only used with DRAP. Feedback strength.
% eta : Default = 1.5
%     Momentum parameter
% gamma : Default = 0.9
%     Momentum parameter
% doRefractive : Default = false
%     
%
% Returns
% -------
% result : struct
%     Contains the fields:
%     x : result of the reconstruction
%     stats : (optional) error measures obtained for each iteration. With
%     sub-fields: gap, convergenceHistory, errorPM, errorPS
% settings : structure
%     The completed setting struct with the reconstruction parameters.
%
% See also
% --------
% functions.phaseRetrieval.iterative.PFresnelMagnitude
% functions.phaseRetrieval.iterative.PPhase
% functions.phaseRetrieval.iterative.PSupport
%
% Example
% -------
%
% .. code-block:: matlab
%
%     fresnel = 1e-3;
%     sample = modelWaveField('dicty',...
%        'dicty', [0 maxPhase], [1 1], [512 512], [2048 2048]);
%     sample = abs(sample) .* exp(1i.*(angle(conj(sample))));
%
%     propSet = FresnelPropagator.defaultSettings();
%     prop = FresnelPropagator(fresnel, [2048 2048], propSet);
%     hologram = prop.propagate(sample);
%     hologram = abs(hologram);
%
%     recSet = projectionAlgorithm.defaultSettings();
%     %recSet.useGPU = 1;
%     guess = complex(ones(size(hologram)));
%     projAlg = projectionAlgorithm(PM, PS, guess, recSet)
%     [phaseRec, recSet] = phaserec_iterative(hologram, fresnel, guess, recSet);
%
%     figure
%     subplot(1,2,1)
%     showImage(angle(cropToCenter(sample, [512 512])))
%     title('True phase-image')
%
%     subplot(1,2,2)
%     showImage((cropToCenter(phaseRec, [512 512])))
%     title('Reconstruction with pure phase constraint')
%
%
%

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>

properties
    settings
    stats
    PM
    PS
    % algorithm update functions
    update
    % algorithm variables
    currIteration = 1
    converged
    iterationsStart
    iterationsEnd
    x
    xOld
    % momentum variables
    x_v
    x_grad

end

methods (Static)
    function defaults = defaultSettings()
        %% Default parameters
        % General
        % Run computations on GPU?
        defaults.useGPU = false;

        % algorithm parameters
        defaults.iterations = 100;
        defaults.algorithm = 'AP';

        %set the RAAR parameters if necessary
        defaults.b0 = 0.75;
        defaults.bM = 0.99;
        defaults.bS = 20;

        %set the HIO/DRAP parameter if necessary
        defaults.b = 0.7;


        % tracking errors
        defaults.doMErrors = 0; 			% errors with respect to measurement or equivalent
        defaults.doSErrors = 0; 			% errors with respect to PS constraint
        defaults.doGap = 0; 				%|P_S(x) - P_M(x)|/N
        defaults.doConvergenceHistory = 0; 	%|x_n - x_(n-1)|

        % early abort criteria
        % for convergence
        defaults.abortThreshold = -7.2;
        % and the respective projectors
        defaults.abortThresholdPM = -7.2;
        defaults.abortThresholdPS = -7.2;
        defaults.abortAverageIterations = 100;

        % show current iterate
        defaults.plotIterates = 0;
        defaults.plotInterval = 10;
        defaults.roiSize = [];

        % momentum options
        clear addMomentum %clear remains... we have to clear here due to some persistent variables
        defaults.addMomentum = 0;
        defaults.accelerationStartAt = 100;
        defaults.accelerationStopAt = 100; %stops x iterations with acc. before end
        defaults.accelerationFrequency = 5;
        defaults.eta = 1.5;
        defaults.gamma = 0.9;
        defaults.doRefractive = false;
        %%
    end
end

methods
    % constructor

    function obj = projectionAlgorithm(PM, PS, guess, settings)

        obj.PM = PM;
        obj.PS = PS;


        % Complete settings with default parameters
        if nargin < 3
            obj.settings = struct;
        end
        obj.settings = completeStruct(settings,...
                                      projectionAlgorithm.defaultSettings());

        %set the ROI size
        if   ~isfield(settings, 'roiSize') | isempty(settings.roiSize)
            obj.settings.roiSize = size(guess);
        end

                %% error measures
        obj.stats.convergenceHistory = [];
        obj.stats.gap = [];
        % initialize the errors just above the abortion threshold
        obj.stats.errorPM = [];
        obj.stats.errorPS = [];
        
        
        % choose algorithm
        switch upper(settings.algorithm)
            case 'AP'
                obj.update = @obj.updateAP;
            case 'RAAR'
                obj.update = @obj.updateRAAR;
            case 'DRAP'
                obj.update = @obj.updateDRAP;
            case 'HIO'
                obj.update = @obj.updateHIO;
            case 'NAG'
                obj.update = @obj.updateNAG;
                obj.settings.addMomentum = 0;
            otherwise
                warning(['Undefined algorithm:' settings.algorithm])
                return
        end
        fprintf('Using algorithm: %s\n', settings.algorithm)

        % initialize reconstruction variable
        obj.x = gpuArrayIf(guess, settings.useGPU);
        obj.xOld = obj.x;
    end %constructor

    function result = execute(obj, iterations, guess)

        obj.converged = false;
        
        %% error measures
        obj.stats.convergenceHistory = [obj.stats.convergenceHistory; zeros(iterations, 1)];
        obj.stats.gap = [obj.stats.gap; zeros(iterations, 1)];
        % initialize the errors just above the abortion threshold
        obj.stats.errorPM = [obj.stats.errorPM; ones(iterations, 1) * Inf];
        obj.stats.errorPS = [obj.stats.errorPS; ones(iterations, 1) * Inf];
        

        if nargin < 2
            iterations = obj.settings.iterations;
        end

        if nargin > 2
            obj.x = guess;
        end

        while ~obj.converged && obj.currIteration<iterations
%             if sum(isnan(obj.x(:))>0
%                 obj.x = guess;
%             end

            obj.iterate();
        end

        %% pack everything up and finish
        % PM
        obj.x = obj.PM.eval(obj.x, 0);
        result.x = gather(obj.x);
        result.stats = obj.stats;
    end

    %% Iterate
    function iterate(obj)
        ii = obj.currIteration;
        obj.currIteration = obj.currIteration + 1;

        % Save current iterate
        obj.xOld = obj.x;

        % Update the iterate according to specified algorithm (AP, HIO, RAAR, ...)
        [obj.x, xPM, obj.stats] = obj.update(obj.x, obj.PM, obj.PS, obj.settings, ii, obj.stats);

        if obj.settings.doConvergenceHistory && ii > 1
            %calculation of convergence: convergence = |(x_n) - (x_n)|_2
            obj.stats.convergenceHistory(ii) = gather(norm(obj.x(:) - obj.xOld(:), 'fro'));
            if  obj.settings.plotIterates && mod(ii, obj.settings.plotInterval) == 0
                figureSilent(9998);
                clf
                plot(log10(obj.stats.convergenceHistory));
                title(['Convergence iteration ' num2str(ii)])
                ax = gca;
                ax.XScale = 'log';

                figureSilent(9995);
                imagesc(abs(obj.x-obj.xOld)); zoom(2);
                title(['Difference to previous iteration ' num2str(ii)])
            end
        end

        %% add Momentum
        if obj.settings.addMomentum
             obj.addMomentum(ii);
        end

        %% error measurements
        if obj.settings.doGap
            %calculation of gap: gap = |PM(x_n) - PS(x_n)|_2
            obj.stats.gap(ii) = gather(norm(xPM(:) -...
                reshape(obj.PS.eval(obj.xOld, ii),[numel(obj.xOld), 1]), 2));
            %         obj.stats.gap(ii) = obj.stats.gap(ii) ./ numel(obj.xPM);
        end

        %% test if we can finish earlier
        if obj.settings.doConvergenceHistory && ii > 10*obj.settings.abortAverageIterations

            abortCondition = movmean(abs(diff(movmean(...
                medfilt1(obj.stats.convergenceHistory(ii-10*obj.settings.abortAverageIterations+1:ii)),...
                obj.settings.abortAverageIterations, 'Endpoints','discard'))),...
                50, 'Endpoints','discard');

            %                 abortCondition = movmean(medfilt1(abs(diff(movmean(obj.stats.convergenceHistory(ii-10*obj.settings.abortAverageIterations+1:ii), 50,'Endpoints','discard'))), 5),obj.settings.abortAverageIterations, 'Endpoints','discard' );

            decideAbortCondition = log10(abortCondition(end)) < obj.settings.abortThreshold;

            % check for abortion criteria. Using either the convergence of
            % iterates or the error measures from projectors
            if any([gather(decideAbortCondition), ...
                    obj.stats.errorPS(ii) < 10^obj.settings.abortThresholdPS, ...
                    obj.stats.errorPM(ii) < 10^obj.settings.abortThresholdPM])

                fprintf('algorithm converged after %i iterations. skipping the remaining iterations.\n', ii);
                fprintf('Who triggered(true/false): Convergence %i, PM %i, PS %i.\n',...
                    gather(decideAbortCondition),...
                    obj.stats.errorPM(ii) < 10^obj.settings.abortThresholdPM,...
                    obj.stats.errorPS(ii) < 10^obj.settings.abortThresholdPS);

                obj.converged = true;
                return
            end
        end

        %% optionally plot current iterate
        if obj.settings.plotIterates && mod(ii, obj.settings.plotInterval) == 0
            if obj.settings.doRefractive
                figureSilent(9999);
                imagesc(cropToCenter(real(obj.x), obj.settings.roiSize)); colorbar;
                title(['iteration ' num2str(ii)])
                
                figureSilent(10000);
                imagesc(cropToCenter(imag(obj.x), obj.settings.roiSize)); colorbar;
                title(['iteration ' num2str(ii)])
                drawnow
            else
                figureSilent(9999);
                imagesc(cropToCenter(angle(obj.x), obj.settings.roiSize)); colorbar;
                title(['iteration ' num2str(ii)])
                
                figureSilent(10000);
                imagesc(cropToCenter(abs(obj.x), obj.settings.roiSize)); colorbar;
                title(['iteration ' num2str(ii)])
                drawnow
            end
        end

        if obj.settings.doMErrors && obj.settings.plotIterates && mod(ii, obj.settings.plotInterval) == 0
            figureSilent(9997);
            plot(log10(obj.stats.errorPM))
            ax = gca;
            ax.XScale = 'log';
            title(['Magnitude error, iteration ' num2str(ii)])
            drawnow
        end

        if obj.settings.doGap && obj.settings.plotIterates && mod(ii, obj.settings.plotInterval) == 0
            figureSilent(9996);
            plot(log10(obj.stats.gap))
            ax = gca;
            ax.XScale = 'log';
            title(['Gap, iteration ' num2str(ii)])
            drawnow
        end


    end %iterate

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% nested functions in iterate
    %% addMomentum
    function addMomentum(obj, itNum )


        if isempty(obj.x_v)
            obj.x_v = gpuArrayIf(zeros(size(obj.x)), obj.settings.useGPU);
            obj.x_grad = gpuArrayIf(zeros(size(obj.x)), obj.settings.useGPU);
        end

        if itNum == (obj.settings.accelerationStartAt - obj.settings.accelerationFrequency)+1
            obj.x_grad = obj.x;
            fprintf('adding momentum\n');
        end

        if itNum < obj.settings.accelerationStartAt || ...
                mod(itNum, obj.settings.accelerationFrequency) == 1
            return
        end

        % evaluate function
        [obj.x_grad,~,~] = obj.update(obj.x - obj.settings.gamma*obj.x_v, obj.PM, obj.PS, obj.settings, itNum, obj.stats);

        % calculate gradient
        obj.x_grad = (obj.x - obj.settings.gamma*obj.x_v) - obj.x_grad;

        %     figureSilent(30)
        %     imagesc(real(x_grad))
        %     drawnow

        obj.x_v = obj.settings.gamma * obj.x_v + obj.settings.eta*obj.x_grad;

        % for the next time we calculate the gradient
        %     x_grad = x;

        obj.x = obj.x - obj.x_v;



    end


    %% implementations of the update steps for the different algorithms
    %% AP = Alternating Projections: x_{n+1} = PS(PM(x_n))
    function [xNew, xPM, stats] = updateAP(obj, x, PM, PS, settings, itNum, stats)
        % Project on M
        [xPM, obj.stats.errorPM(itNum)] = PM.eval(x, itNum);

        % Project on S
        [xNew, obj.stats.errorPS(itNum)] = PS.eval(xPM, itNum);
    end


    %% RAAR: Relaxed Averaged Alternating Reflections: x_{n+1} = (b/2) * (RS(RM(x_n)) + x_n) + (1-b) * PM(x_n)
    function [xNew, xPM, stats] = updateRAAR(obj, x, PM, PS, settings, itNum, stats)
        % relaxation parameter for current iteration
        b = exp(-(itNum./settings.bS)^3)*settings.b0 + (1 - exp(-(itNum/settings.bS)^3))*settings.bM;

        % Reflect on M
        [xNew, xPM, obj.stats.errorPM(itNum)] = PM.reflect(x, itNum);

        % Reflect on S
        [xNew, ~, obj.stats.errorPS(itNum)] = PS.reflect(xNew, itNum);

        % Complete update
        xNew = (b/2) .* (xNew + x) + (1-b) .* xPM;
    end % RAAR


    %% DRAP = Dougles-Rachford Alternating Projections: x_{n+1} = PS((1 + b) * PM(x_n) - b * x_n) - b * (PC(x_n) - x_n)
    function [xNew, xPM, stats] = updateDRAP(obj, x, PM, PS, settings, itNum, stats)
        % Project on M
        [xPM, obj.stats.errorPM(itNum)] = PM.eval(x, itNum);

        [xNew, obj.stats.errorPS(itNum)] = PS.eval((1 + settings.b) * xPM - settings.b * x, itNum);
        xNew = xNew - settings.b * (xPM - x);
    end %DRAP


    %% HIO = Hybrid Input-Output: x_{n+1} = (1/2) * (RS(RM(x) + (beta-1)*PM(x)) + x + (1-beta) * PM(x))
    function [xNew, xPM, stats] = updateHIO(obj, x, PM, PS, settings, itNum, stats)
        % Project on M
        [xPM, obj.stats.errorPM(itNum)] = PM.eval(x, itNum);

        % RM(x) + (beta-1)*PM(x)
        xNew = (1+settings.b)*xPM - x;

        % RS(RM(x) + (beta-1)*PM(x))
        [xNew, ~, obj.stats.errorPS(itNum)] = PS.reflect(xNew, itNum);

        % 0.5 * (RS(RM(x) + (beta-1)*PM(x)) + x + (1-beta) * PM(x))
        xNew =  0.5 * (xNew + x + (1-settings.b) * xPM);
    end %HIO


    %% NAG = Nesterov Accelerated Gradient update
    function [xNew, xPM, stats] = updateNAG(obj, x, PM, PS, settings, itNum, stats)

        if isempty(obj.x_v)
            obj.x_v = x;
            obj.x_grad = obj.x_v;
        end

        % evaluate function
        % https://jlmelville.github.io/mize/nesterov.html#nag
        [xPM,~,~] = obj.updateAP(obj.x_v, PM, PS, settings, itNum, obj.stats);
%         xPM = xPM - obj.x_v;
%         obj.x_v = obj.settings.eta * obj.x_v + (xPM);
%         xNew = x - xPM - obj.settings.eta * obj.x_v;
        xNew = obj.x_v + obj.settings.eta .* (xPM - obj.x_v);

%          xNew = PS.eval(xNew, itNum);

        obj.x_v = xNew + obj.settings.gamma.*(xNew - x);

    end %NAG

end % methods function
end % end classdef
