%%
classdef ProjectorComposition < Projector
% Class that implements composition (sequential application) of projectors.

    properties (SetAccess = 'private')
        proj1
		proj2
    end    

    
    methods

        % constructor
        function obj = ProjectorComposition(proj1, proj2)
            obj.proj1 = proj1;
			obj.proj2 = proj2;
        end %end constructor

		function [result, error] = eval(obj, x, itNum) 
			[result, err1] = obj.proj1.eval(x, itNum);
            [result, err2] = obj.proj2.eval(result, itNum);
            % The default value for errors should be Inf, so we avoid
            % accidential triggering of the abortion automatic.
            error = min(err1, err2);
		end

	end
        
end
