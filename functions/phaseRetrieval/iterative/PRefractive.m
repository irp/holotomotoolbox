classdef PRefractive < Projector
    % PROJECTORTEMPLATE is the base to start your implementation of a new fancy
    % projector
    %
    %    ``obj = ProjectorTemplate(constraints, p, opts)``
    %
    % Parameters
    % ----------
    % Don't forget to comment what you did!VE
    %
    % Other Parameters
    % ----------------
    %
    %
    % Returns
    % -------
    %
    
    % HoloTomoToolbox
    % Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
    %
    % This program is free software: you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation, either version 3 of the License, or
    % (at your option) any later version.
    %
    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.
    %
    % You should have received a copy of the GNU General Public License
    % along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
    properties (SetAccess = 'private')
        settings
        sl2d1 
%         realX
%         imagX
    end
    
    
    methods (Static)
        function defaults = defaultSettings()
            % settings defaults
            defaults.maxReal = inf;
            defaults.minReal = 0;
            defaults.maxImag = inf;
            defaults.minImag = 0;
            
            % reflections can be achieved via negative factors!
            defaults.ImagNegativeScaleFactor = 0.0;
            defaults.fwhmReal = 0;
            defaults.fwhmImag = 0;
            defaults.deltaOverBeta = [];
            defaults.periodicBoundaries = false;
            defaults.useGPU = false;
        end
    end	 % end static methods
    
    methods
        % constructor
        function obj = PRefractive(settings)
            obj.settings = completeStruct(settings, obj.defaultSettings());
            
            if(0)
                obj.sl2d1 = SLgetShearletSystem2D(true, obj.settings.roiSize(1), obj.settings.roiSize(2),4,[0 0 1 1]);
            end
            
        end %end constructor
        
        % apply the projector and return error measure
        function [x, errorVal] = eval(obj, x, itNum)
            errorVal = Inf;
            % for phase shift
            realX = real(x);
            imagX = imag(x);
            
            if obj.settings.minReal > -inf
                if abs(obj.settings.ImagNegativeScaleFactor) > 1e-4
                    realX(realX < obj.settings.minReal) = ...
                        -1*obj.settings.ImagNegativeScaleFactor * realX(realX < obj.settings.minReal);
                else
                    realX = max(realX, obj.settings.minReal);
                end
                
            end
            
            if (obj.settings.maxReal) < inf
                
                if abs(obj.settings.ImagNegativeScaleFactor) > 1e-4
                    realX(realX > obj.settings.maxReal) = ...
                        obj.settings.ImagNegativeScaleFactor * realX(realX > obj.settings.maxReal);
                else
                    realX = min(realX, obj.settings.maxReal);
                end
                
                removeSet.neighborhood = 3;
%                 realX = removeOutliers(realX, removeSet);
                
            end

            %smoothness
            if obj.settings.fwhmReal > 1e-4
                realX = imgaussfilt(realX, obj.settings.fwhmReal/2.35);
            end
            
            % for absorption
            if ~isempty(obj.settings.deltaOverBeta)
                imagX = -realX*2/obj.settings.deltaOverBeta;
            else
                                
                if abs(obj.settings.ImagNegativeScaleFactor) > 1e-4
                    idx = imagX < obj.settings.minImag;
                    imagX(idx) = ...
                        1*obj.settings.ImagNegativeScaleFactor * imagX(idx);
                else
                    imagX = max(imagX, obj.settings.minImag);
                end
                   
                
                if abs(obj.settings.ImagNegativeScaleFactor) > 1e-4
                    idx = imagX > obj.settings.maxImag;
                    imagX(idx) = ...
                        -1*obj.settings.ImagNegativeScaleFactor * imagX(idx);
                    
                else
                    imagX = min(imagX, obj.settings.maxImag);
                end
                
%                 removeSet.neighborhood = 3;
%                  imagX = removeOutliers(imagX, removeSet);

                
            end
            
            if obj.settings.fwhmImag > 1e-4
%                 imagX = medfilt2(imagX, [5 5]);

                 imagX = imgaussfilt(imagX, obj.settings.fwhmImag/2.35);
            end
            
            % apply periodic boundaries.
            
            if obj.settings.periodicBoundaries
                %%
%                 realX = padToSize(cropToCenter(realX, obj.settings.roiSize), obj.settings.recSize);
%                 imagX = padToSize(cropToCenter(imagX, obj.settings.roiSize), obj.settings.recSize);
                fadeSet = fadeoutImage;
                fadeSet.method = 'ellipse';
                fadeSet.ellipseSize = [0.75 0.6] .* obj.settings.roiSize./obj.settings.recSize;
                fadeSet.numSegments = [];
                fadeSet.transitionLength = 20;
                fadeSet.numSegments = 100;
%                 fadeSet.fadeToVal = 0;
%                 realX = cropToCenter(realX, obj.settings.roiSize);
%                 imagX = cropToCenter(imagX, obj.settings.roiSize);
                
                realX = fadeoutImage(realX, fadeSet);
                imagX = fadeoutImage(imagX, fadeSet);
            end
            
            x = realX + 1i*imagX;
            
            if(0) %shearalet denoising
                thresholdingFactor = [0 2.5 2.5 2.5 3.8].*0.5;
                x = cropToCenter(x, obj.settings.roiSize);
                coeffssl2d1 = SLsheardec2D(x, obj.sl2d1);
                %apply hard thresholding on shearlet coefficients
                for j = 1:obj.sl2d1.nShearlets
                    idx = obj.sl2d1.shearletIdxs(j,:);
                    coeffssl2d1(:,:,j) = coeffssl2d1(:,:,j).*(abs(coeffssl2d1(:,:,j)) >= thresholdingFactor(idx(2)+1)*obj.sl2d1.RMS(j));
                end
                
                %compute reconstruction
                x = SLshearrec2D(coeffssl2d1,obj.sl2d1);
                x = padToSize(x, obj.settings.recSize);
            end
            
            
            
                       
        end % eval
    end %methods
end %classdef
