classdef ProjectorTemplate < Projector
% PROJECTORTEMPLATE is the base to start your implementation of a new fancy
% projector
%
%    ``obj = ProjectorTemplate(constraints, p, opts)``
% 
% Parameters
% ----------
% Don't forget to comment what you did!
%
% Other Parameters
% ----------------
% 
%
% Returns
% -------
%

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

properties (SetAccess = 'private')
	settings
end


methods (Static)
	function defaults = defaultSettings()
		% settings defaults
        defaults.dummy = 0;
	end
end	 % end static methods
    
methods
        % constructor
        function obj = ProjectorTemplate()
            obj.settings = completeStruct(settings, obj.defaultSettings());
        end %end constructor
        
        % apply the projector and return error measure
        function [x, errorVal] = eval(obj, x, itNum)
     
        end

    end
end