
classdef PSupportAdaption < Projector
% PSUPPORTADAPTION is the base to start your implementation of a new fancy
% projector
%
%    ``obj = PSupportAdaption(constraints, p, opts)``
% 
% Parameters
% ----------
% Don't forget to comment what you did!
%
% Other Parameters
% ----------------
% 
%
% Returns
% -------
%

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

properties (SetAccess = 'private')
	settings
    dilateElement
    erodeElement
    oldSupp 
    errors
end

properties (SetAccess = 'public')
    supp
end

methods (Static)
    function defaults = defaultSettings()
        defaults.adaptionInterval = 5;
        defaults.firstAdaption = 10;
        defaults.supportThreshold = -0.1;
        defaults.erodeValue = 60;
        defaults.dilateValue = 70;
        defaults.fillHoles = 1; % fill up any holes there are inside the support.
        defaults.valueOutsideSupp = 1;
        defaults.plotIterates = 1;
        defaults.plotInterval = 5;
        defaults.doRefractive = false;
        % automatic abort if support does not change anymore
        defaults.doError = false;
        defaults.lookBackIterations = 10;
        
    end
end	 % end static methods
    
methods
        % constructor
        function obj = PSupportAdaption(support, settings)
            obj.settings = completeStruct(settings, obj.defaultSettings());
            obj.supp = support;
            
            obj.dilateElement = strel('disk',obj.settings.dilateValue);
            obj.erodeElement = strel('disk',obj.settings.erodeValue);
            
        end %end constructor
        
        % apply the projector and return error measure
        function [x, errorVal] = eval(obj, x, itNum)
            % We have to initialize the variable here, so we have to return
            % something even if we dont do the adaption;
            errorVal = Inf;
            
            if itNum > obj.settings.firstAdaption && mod(itNum, obj.settings.adaptionInterval) == 0
                
                if obj.settings.doError
                    if isempty(obj.errors)
                        obj.errors = zeros(obj.settings.lookBackIterations, 1);
                    end
                    obj.oldSupp = obj.supp;
                end
                    
                if obj.settings.doRefractive
                    obj.supp = (abs(real(x))) > obj.settings.supportThreshold;
                else
                    obj.supp = abs(angle(x)) > obj.settings.supportThreshold;
                end
                
                
                obj.supp = imerode(obj.supp, obj.erodeElement);
                obj.supp = imdilate(obj.supp, obj.dilateElement);
               
               
                
                if obj.settings.fillHoles
                    obj.supp = imfill((obj.supp), 'holes');
                end
                
                if obj.settings.doError
                   lookBackIdx = mod(floor(itNum/obj.settings.adaptionInterval), obj.settings.lookBackIterations) + 1;
                   
                   obj.errors(lookBackIdx) = gather(norm(obj.supp-obj.oldSupp, 'fro'));
                   errorVal = mean(obj.errors);
                   
                end
                
            if obj.settings.plotIterates 
                figureSilent(1002)
                imagesc(obj.supp); 
                title(['current support threshold ' num2str(obj.settings.supportThreshold)]) 
            end
                
            end
            
            x = x .* obj.supp + obj.settings.valueOutsideSupp .* (1 - obj.supp);
            

        end

    end
end