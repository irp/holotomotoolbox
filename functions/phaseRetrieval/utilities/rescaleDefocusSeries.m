function [imAligned,fresnelNumbersRescaled,imFiltered] = rescaleDefocusSeries(images,fresnelNumbers,magnifications,settings)
% RESCALEDEFOCUSSERIES transforms set of images in cone geometry to parallel-beam
% geometry.
%
%    ``[imAligned,fresnelNumbersRescaled,imFiltered] = rescaleDefocusSeries(images,fresnelNumbers,magnifications,settings)``
% 
% To account for a varying magnification in a defocus scan obtained in divergent
% geometry, all images are scaled to the effective pixel size of the projection with
% the highest magnification. Subsequently, the images are either shifted by a
% specified amount, aligned to each other in Fourier space or by manual
% identification of a feature of interest in all images to identify overlapping
% regions. By subsequently cutting all projections accordingly, projection images
% with the same field of view and effective pixel size but varying Fresnel numbers
% can be obtained, well suited for the application of multi-distance phase-retrieval
% approaches.
%
% Parameters
% ----------
% images : numerical array or cell-array
%     set of projections to be aligned
% fresnelNumbers : numerical array
%     pixel size-based Fresnel numbers, at which the holograms were acquired: the columns
%     correspond to the different defocus distances (hence, size(fresnelNumbers,2) must
%     match the number of holograms). If the array has two rows, the rows are interpreted
%     as possibly distinct Fresnel numbers along the two dimensions of the holograms (astigmatism).
% magnifications : numerical
%     geometrical magnifications, at which the holograms were acquired: the columns
%     correspond to the different defocus distances (hence, size(magnifications,2) must
%     match the number of holograms). If the array has two rows, the rows are interpreted
%     as possibly distinct magnifications along the two dimensions of the holograms (astigmatism).
% settings : structure, optional
%     contains additional settings, see *Other Parameters*
%
% Other Parameters 
% ---------------- 
% alignMethod : Default = 'dftreg'
%     Method for aligning the images. 'dftreg' uses dftregistration, 'manualFeature' uses
%     a manually selected feature of interest.
% registrationMode : Default = 'shift'
%     Determines whether alignment is performed w.r.t. shifts (case: 'shift'), rotation
%     (case: 'rotation') or both (case: 'shiftRotation')
% registrationAccuracy : Default = 10
%     Accuracy-factor for the image-registration. Shifts and/or rotations between the images
%     are detected to an accuracy of 1/registrationAccuracy lengths of a pixel.
% alignReconstructedImages : Default = false
%     Should alignment be performed for single-distance CTF reconstructed
%     projections? 
% noCropOutput : Default = false
%     Output the full holograms after rescaling without cropping. 
% ctfsettings : Default = []
%     Settings for the CTF reconstruction, if alignReconstructedImages is true. If it is
%     empty, the settings betaDeltaRatio = 1/100, lim1 = 0.01 and lim2 = 0.1 are
%     chosen.
% sigmaHighpass : Default = 0
%     Standard deviation of the Gaussian function used for highpass filtering. If
%     sigmaHighpass is 0, no filtering will be performed.
% sigmaLowpass : Default = 0
%     Standard deviation of the Gaussian function used for lowpass filtering. If
%     sigmaLowpass is 0, no filtering will be performed.
% medfilt : Default = false
%     Should median filtering be applied prior to the alignment? 
% cutLeft : Default = 0
%     Amount of cutting at the left edge in pixels.
% cutRight : Default = 0
%     Amount of cutting at the right edge in pixels.
% cutTop : Default = 0
%     Amount of cutting at the top edge in pixels.
% cutBottom : Default = 0
%     Amount of cutting at the bottom edge in pixels.
% givestatus : Default = false
%     Should status of the alignment progress be printed in Matlab terminal? 
%
% Returns 
% ------- 
% imAligned : numerical array
%     numerical array containing the scaled, aligned and cropped projections
% fresnelNumbersRescaled : numerical array
%     array of rescaled Fresnel numbers
% imFiltered : numerical array 
%     exemplary image for which all image processing steps prior to the alignment were applied
%
% See also
% --------
% functions.imageProcessing.alignment.alignImages
%
% Example
% -------
%
% .. code-block:: matlab
%
%     projs = zeros(512,512,4);
%     magnifications = [1,2,3,4];
%     fresnelNumbers = 0.01./magnifications; 
%     shifts = rand([2,4])*10;
%     for ii = 1:4
%         projs(:,:,ii) = shiftImage(magnifyImage(phantom(512), magnifications(:,ii)), shifts(:,ii));
%     end
% 
%     settings = rescaleDefocusSeries;
%     settings.alignMethod = 'dftreg';
%     settings.sigmaHighpass = 50;
%     settings.medfilt = true;
% 
%     [projsAligned, fresnelNumbersRescaled] = rescaleDefocusSeries(projs,fresnelNumbers,magnifications,settings);
%
%     figure(1)
%     for jj = 1:4
%         subplot(2,2,jj); showImage(projs(:,:,jj)); title(['Original image ', num2str(jj)]);
%     end
%
%     figure(2)
%     for jj = 1:4
%         subplot(2,2,jj); showImage(projsAligned(:,:,jj)); title(['Rescaled image ', num2str(jj)]);
%     end
%
%
% See also ALIGNIMAGES
    
% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

if (nargin < 4)
    settings = struct;
end

% default settings for function alignImages
defaults.sigmaHighpass = 0;        
defaults.sigmaLowpass = 0;         
defaults.medfilt = false;              
defaults.cutLeft = 0;             
defaults.cutRight = 0;            
defaults.cutTop = 0;              
defaults.cutBottom = 0;           
defaults.alignMethod = 'dftreg';   
defaults.registrationMode = 'shift';            
defaults.registrationAccuracy = 10;            
defaults.noCropOutput = false;  


defaults.givestatus = 0;
defaults.alignReconstructedImages = false;
defaults.ctfsettings = [];

% return default settings
if (nargin == 0)
    imAligned = defaults;
    return
end

settings = completeStruct(settings,defaults);

% default CTF settings
if isempty(settings.ctfsettings)
    settings.ctfsettings = phaserec_ctf;
    settings.ctfsettings.betaDeltaRatio = 1/100;
    settings.ctfsettings.lim1 = 0.01;
    settings.ctfsettings.lim2 = 0.1;
end


%% Verification of correct usage
if  iscell(images)
    images = cat(3, images{:});
end
numHolos = size(images,3);
%
% If only one hologram is assigned, then output = input
if numHolos<2
    imAligned = images;
    fresnelNumbersRescaled = fresnelNumbers;
    imFiltered = [];
    return
end
%
% Check that Fresnel numbers are assigned in the right format
if ~isnumeric(fresnelNumbers)
    error('fresnelNumbers must be a single number, vector or matrix. Cell-arrays are not supported.');
end
if length(fresnelNumbers) ~= numHolos
    error('size(fresnelNumbers,2) must be equal to the number of assigned holograms.');
end
if size(fresnelNumbers,1) ~= 1 && size(fresnelNumbers,1) ~= 2
    error(['size(fresnelNumbers,1) must be either 1 (no astigmatism) or 2', ...
           ' (possibly astigmatic setting).']);
end
%
% Same Fresnel number along both dimensions of the holograms if fresnelNumbers has only one row
fresnelNumbers = fresnelNumbers .* ones([2,numHolos]);
%
% Check that magnifications are assigned in the right format
if ~isnumeric(magnifications)
    error('magnifications must be a single number, vector or matrix. Cell-arrays are not supported.');
end
if size(magnifications,2) ~= numHolos
    error('size(magnifications,2) must be equal to the number of assigned holograms.');
end
if size(magnifications,1) ~= 1 && size(magnifications,1) ~= 2
    error(['size(magnifications,1) must be either 1 (no astigmatism) or 2', ...
           ' (possibly astigmatic setting).']);
end
%
% Same magnifications along both dimensions of the holograms if magnifications has only one row
magnifications = magnifications .* ones([2,numHolos]);



%% Perform the rescaling and alignment
% initialize
imAligned = zeros([size(images,1),size(images,2),numHolos], class(images));

% determine hologram with maximum magnification
[~,idxMaxMagnification] = max(magnifications(1,:));
maxMagnification = magnifications(:,idxMaxMagnification);

% direction of defocus series
if idxMaxMagnification == numHolos
    direction = -1; % from smallest to largest magnification
elseif idxMaxMagnification == 1
    direction = 1;  % from largest to smallest magnification
else
    error('The magnifications must be either monotonically increasing or decreasing.');
end

% rescale Fresnel numbers
fresnelNumbersRescaled = fresnelNumbers .* (magnifications ./ maxMagnification).^2;

% determine scaling factors with respect to image with largest magnification
scalings = maxMagnification ./ magnifications;

if (settings.noCropOutput)
    [imAligned, imFiltered] = rescaleNoCrop();
    return
end

if (settings.givestatus)
    disp (['Aligning image 1 / ' num2str(numHolos)])
end

% identify feature for manually determining shifts between images
featureCoords = zeros([numHolos,2]);
if strcmp(settings.alignMethod,'manualFeature')
    showImage(images(:,:,idxMaxMagnification))
    colormap gray
    % get coordinates (y, x)
    featureCoords(idxMaxMagnification,:) = fliplr(ginput(1));
end

% image with largest magnification is reference --> no alignment needed
imAligned(:,:,idxMaxMagnification) = images(:,:,idxMaxMagnification);

% all other images are aligned to the predecessor
shift = 0; number = 1;
for imageIdx = idxMaxMagnification+direction:direction:idxMaxMagnification+direction*(numHolos-1)
    
    if (settings.givestatus)
        number = number+1;
        disp (['Aligning image ' num2str(number) ' / ' num2str(numHolos)])
    end
    
    % scale all images to the pixelsize of the one with the largest
    % magnification
    imScaled = magnifyImage(images(:,:,imageIdx), scalings(:,imageIdx));  
    
    % determine reference image
    imReference = imAligned(:,:,imageIdx-direction);
    
    % optional CTF reconstruction to better align images to each other as
    % holograms might look too different
    if settings.alignReconstructedImages
        imReference = phaserec_ctf(imReference, fresnelNumbersRescaled(:,imageIdx-direction), settings.ctfsettings);
        imScaled = phaserec_ctf(imScaled, fresnelNumbersRescaled(:,imageIdx), settings.ctfsettings);
    end
           
    % identify feature for manually determining shifts between images
    if strcmp(settings.alignMethod,'manualFeature')
        imagesc(imScaled)
        colormap gray
        axis equal tight off
        % get coordinates (y, x)
        featureCoords(imageIdx,:) = fliplr(ginput(1)) - ...
            [(size(imScaled,1)-size(images(:,:,idxMaxMagnification),1))/2 ...
            (size(imScaled,2)-size(images(:,:,idxMaxMagnification),2))/2];
        shift_diff = featureCoords(imageIdx-direction,:) - featureCoords(imageIdx,:);
        shift = shift+shift_diff;
        
        imFiltered = imScaled;
    else
        % align the cropped image to the reference using alignImages and dftregistration
        [~, shifts, rotAngleDegree, imFiltered] = alignImages(imScaled, imReference ,settings);
    end
    
    % Apply scaling and registered shifts/rotations to original image (performed in a single step
    % to minimize the degradation of image-quality due to interpolation)
    imAligned(:,:,imageIdx) = shiftRotateMagnifyImage(images(:,:,imageIdx), ...
                                    shifts, rotAngleDegree, scalings(:,imageIdx));
end


function [imAligned,imFiltered] = rescaleNoCrop()
% for the no crop option it makes sense to do everything the other way
% round. starting at the smallest magnification but the largest scaling.
% since this is what determines the output.

[maxScaling, maxScalingIdx] = max(scalings(1,:));

% image with largest scaling is reference --> no alignment needed
 imReference = imresize(images(:,:,maxScalingIdx), scalings(1,maxScalingIdx), 'lanczos3');  
 outputSize = size(imReference);
 
 imAligned = zeros([outputSize numHolos]);
 imAligned(:,:,maxScalingIdx) = imReference;

 
% all other images are aligned to the predecessor
shift = 0; number = 1;
direction = direction*-1;
for imageIdx = maxScalingIdx+direction:direction:maxScalingIdx+direction*(numHolos-1)
    
    if (settings.givestatus)
        number = number+1;
        disp (['Aligning image ' num2str(number) ' / ' num2str(numHolos)])
end
    
    % scale all images to the pixelsize of the one with the largest
    % magnification
%     imScaled = magnifyImage(images(:,:,imageIdx), scalings(:,imageIdx));  
    imScaled = imresize(images(:,:,imageIdx), scalings(1,imageIdx), 'lanczos3');  
    imScaled = padToSize(imScaled, outputSize, 0);
        
    % optional CTF reconstruction to better align images to each other as
    % holograms might look too different
    if settings.alignReconstructedImages
        imScaled = phaserec_ctf(imScaled, fresnelNumbersRescaled(:,imageIdx), settings.ctfsettings);
    end
           
    % identify feature for manually determining shifts between images
    if strcmp(settings.alignMethod,'manualFeature')
        imagesc(imScaled)
        colormap gray
        axis equal tight off
        % get coordinates (y, x)
        featureCoords(imageIdx,:) = fliplr(ginput(1)) - ...
            [(size(imScaled,1)-size(images(:,:,idxMaxMagnification),1))/2 ...
            (size(imScaled,2)-size(images(:,:,idxMaxMagnification),2))/2];
        shift_diff = featureCoords(imageIdx-direction,:) - featureCoords(imageIdx,:);
        shift = shift+shift_diff;
        
        imFiltered = imScaled;
    else
        % align the cropped image to the reference using alignImages and dftregistration
        [~, shifts, rotAngleDegree, imFiltered] = alignImages(imScaled, imReference ,settings);
        shifts
        rotAngleDegree
    end
    imScaled = padToSize(imScaled, ceil(outputSize*1.1), 0);
    imScaled = circshiftSubPixel(imScaled, shifts);
    imScaled = cropToCenter(imScaled, outputSize);
    imAligned(:,:, imageIdx) = imScaled;
                           
end

end % end rescaleNoCrop 

end % end RescaleDefocusSeries


