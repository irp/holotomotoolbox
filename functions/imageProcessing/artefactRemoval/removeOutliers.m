function correctedImage = removeOutliers( originalImage, settings)
% REMOVEOUTLIERS removes pixels with extreme values from an image.
%
%    ``correctedImage = removeOutliers(originalImage, settings)``
% 
% Parameters
% ----------
% originalImage : numerical 2d-array
%     image to be corrected
% settings : structure, optional
%     contains additional settings, see *Other Parameters*
%
% Other Parameters 
% ---------------- 
% neighbourhood : Default = 5 
%     Kernel size for the median filter.
% threshold : Default = 2
%     Threshold for the detection of outliers.
%
% Returns 
% ------- 
% imCorrected : numerical array
%     corrected image
%
% Example
% -------
%
% .. code-block:: matlab
%
%     image = phantom(100)  ;
%     image(29,40) = 100000 ;
%     correctedImage = removeOutliers(image) ;
%     figure(1)
%     imagesc(image); axis equal tight; axis off ;
%     title('image with hot pixel') ;
%     figure(2)
%     imagesc(correctedImage); axis equal tight; axis off ;
%     title('image without hot pixel') ;
%
    
% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

% default parameters
defaults.neighborhood = 5 ;
defaults.threshold = 2 ;     
if nargin == 0
    correctedImage = defaults;
    return
end
if nargin < 2
    settings = struct;
end
settings = completeStruct(settings, defaults);

% handle NaN
originalImage(isnan(originalImage)) = inf;

% remove hot and cold pixels

% filtered image
neighbourhood = [settings.neighborhood settings.neighborhood];
if ~isOnGPU(originalImage)
    filteredImage = medfilt2(originalImage, neighbourhood, 'symmetric');
else
    filteredImage = medfilt2(originalImage, neighbourhood);
end
    
% difference between original and filtered image
differenceImage = originalImage - filteredImage;

% standard deviation of gray values in difference-image
differenceImageFinite = differenceImage(~isinf(differenceImage));
stdGrayVal = std(differenceImageFinite(:));

% find pixels to correct
pixelsToCorrect = (abs(differenceImage) > settings.threshold * stdGrayVal);

% replace faulty pixels
correctedImage = originalImage;
correctedImage(pixelsToCorrect) = filteredImage(pixelsToCorrect);

end
