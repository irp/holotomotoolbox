function diffMatrix = getForwardDiffMatrix(arraySize, diffDim)
% GETFORWARDDIFFMATRIX creates a sparse matrix that implements forward-
% differences on arrays of a given size along a given dimension. 
%
%    ``diffMatrix = getForwardDiffMatrix(arraySize, diffDim)``
%
% Example
% -------
%
% .. code-block:: matlab
%
%    arraySize = [11,17,23,9];
%    arr = rand(arraySize);
%    diffMatrix = getForwardDiffMatrix(arraySize, 3);
%
%    diff1 = arr(:,:,2:end,:)-arr(:,:,1:end-1,:);
%    diff2 = reshape(diffMatrix * arr(:), arraySize);
%
%    innerValuesAreEqual = (diff1 == diff2(:,:,1:end-1,:));
%    assert(all(innerValuesAreEqual(:)));
%    boundaryValuesAreZero = (diff2(:,:,end,:) == 0);
%    assert(all(boundaryValuesAreZero(:)));
%
% HoloTomoToolbox
% Copyright (C) 2022  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

ndim = numel(arraySize);
n = prod(arraySize);

% Index offset between two adjacent points along the diff-dimension
diffOffset = prod(arraySize(1:diffDim-1));   

% True for all boundary points, i.e. such points that do not have a (forward-)neighbor
boundaryPoints = (mod(floor((diffOffset:n-1+diffOffset).' / diffOffset), arraySize(diffDim)) == 0);

% Build sparse matrixrix
idx = (1:n).';
idx(boundaryPoints) = [];
ROWS = [idx, idx];
COLS = [idx, idx+diffOffset];
one = ones([numel(idx),1]);
VALS = [-one, one];
diffMatrix = sparse(ROWS, COLS, VALS, n, n);

end
