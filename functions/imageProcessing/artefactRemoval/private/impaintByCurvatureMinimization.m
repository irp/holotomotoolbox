function imCompleted = impaintByCurvatureMinimization(im, impaintingRegion)
% IMPAINTBYCURVATUREMINIMIZATION impaints missing values in an image
% such that the total curvature (measured as the squared 2-norm of the
% second derivatives at all pixels) of the imCompleteding image is minimal.
%
%    ``imCompleted = impaintByCurvatureMinimization(im, impaintingRegion)``
%
% HoloTomoToolbox
% Copyright (C) 2022  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

N = size(im);
if any(size(impaintingRegion) ~= N)
    error('Input image and impaintingRegion must be images of the same size.');
end
n = prod(N);
nImpaint = sum(impaintingRegion(:));

dx = getForwardDiffMatrix(N, 2);
dy = getForwardDiffMatrix(N, 1);
hessianOperator = [dx'*dx; dy'*dx; dx'*dy; dy'*dy];

embeddingMatrix = sparse(find(impaintingRegion(:)), (1:nImpaint).', ones([nImpaint,1]), n, nImpaint);
hessianOperatorInImpaintingRegion = hessianOperator * embeddingMatrix;
imMasked = (~impaintingRegion) .* im;

impaintingValues = -((hessianOperatorInImpaintingRegion' * hessianOperatorInImpaintingRegion) ...
     \ (hessianOperatorInImpaintingRegion' * (hessianOperator * imMasked(:))));

imCompleted = im;
imCompleted(impaintingRegion) = impaintingValues;

end
