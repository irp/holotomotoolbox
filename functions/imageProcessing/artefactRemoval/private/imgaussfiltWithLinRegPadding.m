function imFiltered = imgaussfiltWithLinRegPadding(im, sigma, filtDims)
% IMGAUSSFILTWITHLINREGPADDING is a variant of imgaussfilt, where the
% padding values at the image boundaries are determined based on a
% linear regression of the pixel-values close to the boundaries. In other
% words, padding is achieved by linear extrapolation.
%
%    ``imFiltered = imgaussfiltWithLinRegPadding(im, sigma, filtDims)``
%
% HoloTomoToolbox
% Copyright (C) 2022  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

if nargin < 3
    filtDims = find(size(im) ~= 1 & (1:ndims(im)) < 3);
end
sigma = sigma(:).' .* ones([1,numel(filtDims)]);
padBy = ceil(3*sigma);
sizeKernel = 1 + (6*ceil(sigma));

imFiltered = im;
padSettings.polynomialDegree = 1;

% 2D-filtering is achieved by subsequently applying 1D-filters
for jj = 1:numel(filtDims)
    permuteDims = (1:ndims(im));
    permuteDims([1,filtDims(jj)]) = [filtDims(jj),1];
    padSettings.regressionWeights = exp(-((0.5:ceil(3*sigma(jj))).').^2 / (2*sigma(jj)^2));
    imPaddedReshaped = padarrayByRegression(permute(imFiltered, permuteDims), padBy(jj), 'both', padSettings);
    filterKernel = padToSize(fspecial('gaussian', [sizeKernel(jj),1], sigma(jj)), [size(imPaddedReshaped,1),1], 0);
    imFiltered = ipermute(croparray(ifft(fft(ifftshift(filterKernel)) .* fft(imPaddedReshaped)), padBy(jj), padBy(jj)), permuteDims);
    if isreal(im)
        imFiltered = real(imFiltered);
    end
end

end

