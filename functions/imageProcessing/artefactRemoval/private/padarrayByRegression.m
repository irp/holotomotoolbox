function arrPadded = padarrayByRegression(arr, padBy, padWhere, settings)
% PADARRAYBYREGRESSION is a variant of padarray (with the same standard
% parameters), where the padding values are determined based on a polynomial
% regression of the array-values near the boundary.
%
%    ``arrPadded = padarrayByRegression(arr, padBy, padWhere, settings)``
%
% HoloTomoToolbox
% Copyright (C) 2022  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

defaults.polynomialDegree = 1;
defaults.regressionWeights = [];
if nargin == 0
    arrPadded = defaults;
    return;
end
if nargin < 4
    settings = struct;
end
settings = completeStruct(settings, defaults);
if nargin < 3
    padWhere = 'both';
end

arrPadded = arr;
for dim = 1:numel(padBy)
    if padBy(dim) > 0
        if iscell(settings.regressionWeights)
            arrPadded = padarrayByRegression1D(arrPadded, padBy(dim), padWhere, dim, settings.regressionWeights{dim}, settings.polynomialDegree);
        else
            arrPadded = padarrayByRegression1D(arrPadded, padBy(dim), padWhere, dim, settings.regressionWeights, settings.polynomialDegree);
        end
    end
end

end



function arrPadded = padarrayByRegression1D(arr, padBy, padWhere, padDim, regressionWeights, polynomialDegree)

if nargin < 6
    polynomialDegree = 1;
end
if isempty(regressionWeights)
    regressionWeights = ones([padBy,1]);
end

if nargin >= 4 && padDim ~= 1
    permute_dims = (1:ndims(arr));
    permute_dims([1,padDim]) = [padDim,1];
    arrPadded = ipermute(padarrayByRegression1D(permute(arr, permute_dims), ...
        padBy, padWhere, 1, regressionWeights, polynomialDegree), permute_dims);
    return;
end

N = size(arr);

% Ensure that regression-weights-array is of suitable size
n = max(polynomialDegree+1, min(numel(regressionWeights), N(1)));
regressionWeights = padToSize(croparray(regressionWeights(:), 0, max(0, numel(regressionWeights)-n)), [n,1], 'replicate');
%
x = linspace(-1,1,n).';
dx = x(2)-x(1);
coeffs_to_value_matrix = x.^(0:polynomialDegree);
fit_matrix = coeffs_to_value_matrix' * diag(regressionWeights(:)) * coeffs_to_value_matrix;
fit_matrix = inv(fit_matrix) * coeffs_to_value_matrix' * diag(regressionWeights(:));
x_pre = (-(1+padBy*dx):dx:-(1+dx)).';
extrapolation_matrix_pre = (x_pre.^(0:polynomialDegree)) * fit_matrix;
extrapolation_matrix_post = flipud(fliplr(extrapolation_matrix_pre));

arr = reshape(arr, [N(1), prod(N(2:end))]);
if strcmpi(padWhere, 'both')
    arrPadded = [extrapolation_matrix_pre * arr(1:n,:); arr; extrapolation_matrix_post * arr(end-(n-1):end,:)];
elseif strcmpi(padWhere, 'pre')
    arrPadded = [extrapolation_matrix_pre * arr(1:n,:); arr];
elseif strcmpi(padWhere, 'post')
    arrPadded = [arr; extrapolation_matrix_post * arr(end-(n-1):end,:)];
else
    error('Invalid value for parameter padWhere');
end
arrPadded = reshape(arrPadded, [size(arrPadded,1), N(2:end)]);

end
