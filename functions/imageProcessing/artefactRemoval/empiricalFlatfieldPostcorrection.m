function [holoCorrected,flatfieldErrorEstimate] = empiricalFlatfieldPostcorrection(holo, support, settings)
% EMPIRICALFLATFIELDPOSTCORRECTION clears slowly varying artifacts from a hologram,
% that arise due to imperfect flat-field correction.
%
%    ``[holoCorrected,flatfieldErrorEstimate] = empiricalFlatfieldPostcorrection(holo, support, settings)``
%
% The function is only applicable in the following scenario:
% (a) The hologram has been acquired with a slowly-varying(!) beam profile,
%     as achieved in synchrotron imaging with a waveguide.
% (b) The imaged object has a compact support, i.e. outside some bounded subdomain
%     of the field-of-view only fine diffraction fringes are visible,
%     but no bulk "shadow" of the object.
%
% The correction algorithm works as follows:
% (1) Outside the given support, the flatfield-error in the hologram is estimated
%     by simple lowpass-filtering (this is reasonable since only high-frequency fringes
%     are present in this image-region according to assumption (b))
% (2) Within the support, the flatfield-error is estimated by image-impainting:
%     the values in the support are determined such that the total "error-image"
%     combined with the values outside the support is "as smooth as possible".
%     Mathematically this is implemented by minimizing the 2-norm of of the
%     (finite-difference) Hessian of the error-image.
% (3) In order to avoid a misestimate in the mean-value of the hologram, the
%     correction is adjusted by a scalar factor to match the expected amount
%     of total absorption and/or scattering by the imaged object.
%
% Parameters
% ----------
% holo : numerical 2d-array
%     The hologram to be corrected. NOTE: must already be flatfield-corrected,
%     so that only slight background variations remain resulting from a
%     possible mismatch between the beam profiles in the hologram and the flatfield.
% support : numerical 2d-array
%     Boolean mask that loosely(!) encloses the object support in the field-of-view:
%     as a rule of thumb, no imprint of the imaged object except for fine diffraction
%     fringes must be visible where the array-values are true.
%     The array must be of the same size as holo. 
%
% Other Parameters 
% ----------------
% expectedTransmissionFraction : Default = 0.99
%     Normalization factor for the output hologram, that accounts for the expected
%     amount of absorption and scattering by the object (step (3) in the algorithm
%     description). The output hologram is rescaled such that
%     mean(holoCorrected(:)) == settings.expectedTransmissionFraction
% sigmaLowpass : Default = 100
%     Standard deviation of the Gaussian lowpass-filter that is applied to estimate
%     the flatfield-error outside the support (step (1) in the algorithm description)
%
% Returns 
% ------- 
% holoCorrected : numerical 2d-array
%    the corrected hologram
% flatfieldErrorEstimate : numerical 2d-array
%    the estimated residual flatfield-error in the hologram: it always holds that
%    holo == holoCorrected .* flatfieldErrorEstimate
%
% Example
% -------
%
% .. code-block:: matlab
%
%     % Load hologram from data-base
%     A = getHologram('radiodurans');
%     holoOriginal = A.holograms(:,:,1);
%     N = size(holoOriginal);
%
%     % Simulate slowly varying flat-field errors with 5% root-mean-square-deviation
%     % (This is necessary as the loaded hologram is almost free of such errors)
%     flatfieldErrorsTrue = imgaussfilt(rand(N)-0.5, 250);
%     flatfieldErrorsTrue = 1 + (0.05 * sqrt(prod(N)) / norm(flatfieldErrorsTrue(:))) * flatfieldErrorsTrue;
%     holoWithFlatfieldErrors = flatfieldErrorsTrue .* holoOriginal;
%
%     figure('name', 'simulated flatfield-errors'); showImage(flatfieldErrorsTrue);
%     figure('name', 'hologram with flat-field errors'); showImage(holoWithFlatfieldErrors);
%
%     % Coarse support estimate: determine binary mask that roughly encloses the image
%     % region where the object is located as well as all surrounding thick fringes
%     [x1,x2] = centeredGrid(N);
%     support = x1.^2 + x2.^2 <= 600.^2;
%
%     % Verify estimated support
%     figure('name', 'hologram with highlighted support');  showImage(holoOriginal + support);
%
%     % Estimate flatfield-errors and corrected hologram
%     settings = struct;
%     % Object is almost non-absorbing and scattering is negligible
%     settings.expectedTransmissionFraction = 1;
%     [holoCorrected, flatfieldErrorsEstimated] ...
%         = empiricalFlatfieldPostcorrection(holoWithFlatfieldErrors, support, settings);
%
%     figure('name', 'corrected hologram'); showImage(holoCorrected);
%     figure('name', 'estimated flatfield-errors'); showImage(flatfieldErrorsEstimated);
%
%
% HoloTomoToolbox
% Copyright (C) 2022  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

% Parameter handling
defaults.expectedTransmissionFraction = 0.99;
defaults.sigmaLowpass = 100;
if nargin == 0
    holoCorrected = defaults;
    flatfieldErrorEstimate = [];
    return;
end
if nargin < 3
    settings = struct;
end
settings = completeStruct(settings, defaults);

% Lowpass-filtering of holograms
% (with padding by linear regression for more natural behavior near the image boundaries)
holoFiltered = imgaussfiltWithLinRegPadding(holo, settings.sigmaLowpass);

% As the hologram is strongly lowpass-filtered, we can work on downsampled
% data to decrease the computational costs
%
% Choose binning-factor such that images of at most 256x256 pixels have to be processed
% (unless settings.sigmaLowpass is chosen unusually small, which is not recommended)
binningFactor = max(1, min(0.5*min(settings.sigmaLowpass(:)), sqrt(numel(holo)/256.^2)));
%
holoFilteredBinned = imresize(holoFiltered, 1/binningFactor);
supportBinned = imresize(support, 1/binningFactor, 'nearest');

% Core algorithm:
% - assume that the lowpass-filtered hologram outside the support shows only
% variations due to flatfield-correction-errors, not due to the object.
% - impaint the missing region within the object support so that the
% curvature (i.e. second derivative) of the combined image is minimial
flatfieldErrorEstimateBinned = impaintByCurvatureMinimization(double(holoFilteredBinned), supportBinned);

% Output single array if input was in single precision
if isa(holo, 'single')
    flatfieldErrorEstimateBinned = single(flatfieldErrorEstimateBinned);
end

% Apply flatfield-correction-error estimate to correct the hologram
flatfieldErrorEstimate = imresize(flatfieldErrorEstimateBinned, size(holo));
holoCorrected = holo ./ flatfieldErrorEstimate;

% Assure that the mean over the corrected hologram matches the expected
% total scattering and absorption fractions
correctionFactor = settings.expectedTransmissionFraction / mean(holoCorrected(:));
holoCorrected = correctionFactor * holoCorrected;
flatfieldErrorEstimate = flatfieldErrorEstimate / correctionFactor;

end
