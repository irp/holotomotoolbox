function [imCorr, corr2D] = removeStripes(imToCorr,settings)
% REMOVESTRIPES removes horizontal and vertical stripes from an image.
%
%    ``[imCorr, corr2D] = removeStripes(imToCorr,settings)``
% 
% Remove horizontal/vertical stripes from an image by averaging over a specified
% number of lines at the top/bottom or left/right edge of the image and linearly
% interpolating in between. If only one range is specified it is taken as the
% correction for the entire image.
%
% Parameters
% ----------
% imToCorr : numerical array
%     image from which stripes are removed
% settings : structure, optional
%     contains additional settings, see *Other Parameters*
%
% Other Parameters 
% ---------------- 
% rangeTop : Default = []
%     Range at the top of the image to average. [] results in the top most 4%
% rangeBottom : Default = []
%     Range at the bottom of the image to average. [] results in the bottom most 4%
% rangeLeft : Default = []
%     Range at the left edge of the image to average. [] results in the left most 4%
% rangeRight : Default = []
%     Range at the right edge of the image to average. [] results in the right most 4%
% windowSize : Default = 5
%     Window size for smoothing the profile via a moving mean.
% direction : Default = 'vertical'
%     Direction of the stripes to be removed. Chose between  'vertical', 'horizontal'
%     or 'both'.
% method : Default = 'multiplicative'
%     Method for removing the stripes. Choose 'multiplicative' for dividing the image by the
%     correction matrix or 'additive' for subtracting the correction matrix from the image.
%
% Returns 
% ------- 
% imCorr : numerical array
%     corrected image
% corr2D : numerical array
%     correction matrix used for correcting the corrupted image
%
%
% Example
% -------
%
% .. code-block:: matlab
%
%     image = phantom(512);
%     gaussSettings = gaussian;
%     gaussSettings.sigma = 10;
%     stripe = repmat(gaussian(512,gaussSettings),[512 1]);
%     image = image + 0.6*stripe + 0.5*stripe';
%     settings = removeStripes;
%     settings.rangeTop = 1:20;
%     settings.rangeBottom = size(image,1)-20+1:size(image,1);
%     settings.rangeLeft = 1:20;
%     settings.rangeRight = size(image,2)-20+1:size(image,2);
%     settings.direction = 'both';
%     settings.method = 'additive';
%     [imageCorrected,corr2D] = removeStripes(image,settings);
% 
%     subplot(1,3,1)
%     showImage(image)
%     subplot(1,3,2)
%     showImage(imageCorrected)
%     subplot(1,3,3)
%     showImage(corr2D)
    
% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

if nargin<2
    settings = struct;
end

defaults.rangeTop = [];   
defaults.rangeBottom = [];     
defaults.rangeLeft = [];  
defaults.rangeRight = [];     
defaults.windowSize = 5;     
defaults.direction = 'vertical';      
defaults.method = 'multiplicative';   

% return default values
if (nargin == 0)
    imCorr = defaults;
    return
end

% complete settings with default values
settings = completeStruct(settings,defaults);

% set the bottom range to the bottom most 4% pixels for settings.rangeBottom = [] 
if isempty(settings.rangeBottom)
    settings.rangeBottom = size(imToCorr,1)-ceil(size(imToCorr,1)*0.04)+1:size(imToCorr,1);
end

% set the right range to the right most 4%  pixels for settings.rangeRight = [] 
if isempty(settings.rangeRight)
    settings.rangeRight = size(imToCorr,2)-ceil(size(imToCorr,2)*0.04)+1:size(imToCorr,2);
end

% set the bottom range to the bottom most 4% pixels for settings.rangeBottom = [] 
if isempty(settings.rangeTop)
    settings.rangeTop = 1:ceil(size(imToCorr,1)*0.04);
end

% set the right range to the right most 4%  pixels for settings.rangeRight = [] 
if isempty(settings.rangeLeft)
    settings.rangeLeft = 1:ceil(size(imToCorr,2)*0.04);
end

% create correction matrix
switch settings.direction
    case 'vertical'
        corr2D = removeStripesVertical(imToCorr, settings.rangeTop, settings.rangeBottom, settings.windowSize);
        
    case 'horizontal'
        corr2D = removeStripesVertical(imToCorr.', settings.rangeLeft, settings.rangeRight, settings.windowSize);
        corr2D = corr2D.';
        
    case 'both'
        % create correction matrix for horizontal and vertical
        % direction independently
        corr2D_vert = removeStripesVertical(imToCorr, settings.rangeTop, settings.rangeBottom, settings.windowSize);
        corr2D_horz = removeStripesVertical(imToCorr.', settings.rangeLeft, settings.rangeRight, settings.windowSize);
        
        % create the combined correction matrix by averaging
        corr2D = (corr2D_horz.' + corr2D_vert);
end

switch settings.method
    case 'multiplicative'
        % remove stripes by dividing by the correction matrix
        corr2D = corr2D - mean(corr2D(:)) + 1 ; 
        imCorr = imToCorr ./ corr2D ;
    case 'additive'
        % remove stripes by subtracting the correction matrix
        corr2D = corr2D - mean(corr2D(:));
        imCorr = imToCorr - corr2D;
end

end




function corr2D = removeStripesVertical(im, rangeTop, rangeBottom, windowSize) 
    % average over the specified ranges at the top and bottom of the
    % image
    profTop = mean(im(rangeTop,:),1);
    profTop = movmean(profTop, windowSize);
    
    profBottom = mean(im(rangeBottom,:),1);
    profBottom = movmean(profBottom, windowSize);
    
    lambda = reshape(linspace(0,1,size(im,1)), [size(im,1), 1] );
    corr2D = (1-lambda) * profTop + lambda * profBottom;
end

