function imTransformed = shiftRotateMagnifyImage(im, shifts, rotAngleDegree, magnify, settings)
% SHIFTROTATEMAGNIFYIMAGE applies shifts and/or rotations and/or magnification to a given image.
%
%    ``imTransformed = shiftRotateMagnifyImage(im, shifts, rotAngleDegree, magnify, settings)``
%
% Does so in a single operation, such that the loss of image-resolution due to interpolation
% is reduced compared to applying magnification, rotations and shifts subsequently.
% The beneficial effect of this can be observed in the provided code-example.
% IMPORTANT: the geometrical image-transformations are applied in the following order: 
% (1) magnification
% (2) rotation
% (3) shift
%
% Parameters
% ----------
% im : numerical 2d-array
%     image to be shifted and/or rotated
% shifts : 2-tuple of floats
%     shifts to be applied to the image (in units of pixel-lengths) 
% rotAngleDegree : float
%     angle of the rotation to be applied to the image (in degrees)
% magnify : float or 2-tuple of floats
%     geometrical magnification of the image. If a tuple is assigned, the values are interpreted
%     as possibly different magnification applied along the different dimensions.
%
% Other Parameters 
% ---------------- 
% padVal : Default = 'replicate'
%     Determines the type of the padding that is applied to the input image prior to
%     shifting and rotating. Admissible choices are the same as in the
%     MATLAB-function padarray.
% interpMethod : Default = 'linear'
%     Interpolation-method used in the image-transformation. Admissible choices are
%     the same as in the MATLAB-function interp2.
% invertTransform : Default = false
%     Determines whether to invert the assigned geometrical transform before application.
%     This allows to magnify-rotate-shift back and forth without manually computing
%     the inverse transformation (see code-example)
%
% Returns 
% ------- 
% imTransformed : numerical 2d-array
%    the scaled and/or shifted and/or rotated image
%
% See also 
% ------- 
% functions.imageProcessing.alignment.shiftRotateImage
% functions.imageProcessing.alignment.shiftImage
% functions.imageProcessing.alignment.rotateImage
% functions.imageProcessing.alignment.magnifyImage
%
% Example
% -------
%
% .. code-block:: matlab
%
%     n = 64;
%     rotAngleDegree = 17.4;
%     shifts = [-9.7, 8.3];
%     magnify = [1.08,0.91];
%     im = padarray(ones(n/2), [n/4,n/4]); 
%     im(n/8+1:n/4,n/4+1:3*n/8) = 1;
%     figure('name', 'Original image'); showImage(im);
%     
%     imTransformed1 = shiftRotateMagnifyImage(im, shifts, rotAngleDegree, magnify);
%     figure('name', 'All-at-once magnified, rotated and shifted image'); showImage(imTransformed1);
%     
%     imTransformed2 = shiftImage(rotateImage(magnifyImage(im, magnify), rotAngleDegree), shifts);
%     figure('name', 'Sequentially magnified, rotated and shifted image'); showImage(imTransformed2);
%
%     settings.invertTransform = true;
%     imTransformedBack = shiftRotateImage(imTransformed1, shifts, rotAngleDegree, settings);
%     figure('name', 'Back and forth shift-rotated-magnified image'); showImage(imTransformedBack);
%
%
% See also SHIFTROTATEIMAGE, SHIFTIMAGE, ROTATEIMAGE, MAGNIFYIMAGE
    
% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

% Default settings
defaults.padVal = 'replicate';
defaults.interpMethod = 'linear';
defaults.invertTransform = false;
if (nargin == 0)
    imTransformed = defaults;
return;
end
if nargin < 5
    settings = struct;
end
settings = completeStruct(settings, defaults);


% Transform a Cartesian coordinate grid by applying inverse shifts, rotation
% and magnification in reverse order. 
% Applying the original (non-inverted) transforms in the native order
% magnify->rotate->shift to the coordinate grid corresponds to applying the inverse
% geometrical transformation to the image.
N = size(im);
[Y,X] = centeredGrid(N);
if settings.invertTransform
    [X,Y] = magnifyGrid(X,Y,magnify);
    [X,Y] = rotateGrid(X,Y,rotAngleDegree);
    [X,Y] = shiftGrid(X,Y,shifts);
else
    [X,Y] = shiftGrid(X,Y,-shifts);
    [X,Y] = rotateGrid(X,Y,-rotAngleDegree);
    [X,Y] = magnifyGrid(X,Y,1./magnify);
end


% Pad image according to the minimum/maximum values of the transformed coordinates
padPre = max(0, [ceil(-(N(1)-1)/2-min(Y(:))), ceil(-(N(2)-1)/2-min(X(:)))]);
padPost = max(0, [ceil(max(Y(:))-(N(1)-1)/2), ceil(max(X(:))-(N(2)-1)/2)]);
im = padarray(padarray(im, double(padPre), settings.padVal, 'pre'), double(padPost), settings.padVal, 'post');


% Perform image-transformation by interpolating the padded image on the transformed grid
imTransformed = interp2(im, X + (0.5*(N(2)+1) + padPre(2)), Y + (0.5*(N(1)+1) + padPre(1)), ... 
                  settings.interpMethod);

end




function [X,Y] = shiftGrid(X,Y,shifts)
    if any(shifts)
        Y = Y + shifts(1);
        X = X + shifts(2);
    end
end

function [X,Y] = rotateGrid(X,Y,rotAngleDegree)
    if rotAngleDegree
        YTmp = cosd(rotAngleDegree) * Y - sind(rotAngleDegree) * X;
        X = sind(rotAngleDegree) * Y + cosd(rotAngleDegree) * X;
        Y = YTmp;
    end
end

function [X,Y] = magnifyGrid(X,Y,magnify)
    if any(magnify ~= 1)
        magnify = magnify(:).' .* ones([1,2]);
        Y = magnify(1) .* Y;
        X = magnify(2) .* X;
    end
end
