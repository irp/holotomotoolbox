function imTransformed = magnifyImage(im, magnify, settings)
% MAGNIFYIMAGE applies geometrical magnification to a given image.
%
%    ``imTransformed = magnifyImage(im, magnify, settings)``
%
% Other than the MATLAB-function imresize, this function automatically 
% applies a suitable padding if required and leaves the image-size unchanged.
%
% Parameters
% ----------
% im : numerical 2d-array
%     image to be magnified
% magnify : float or 2-tuple of floats
%     geometrical magnification of the image. If a tuple is assigned, the values are interpreted
%     as possibly different magnification applied along the different dimensions.
%
% Other Parameters 
% ---------------- 
% padVal : Default = 'replicate'
%     Determines the type of the padding that is applied to the input image prior to
%     shifting and rotating. Admissible choices are the same as in the
%     MATLAB-function padarray.
% interpMethod : Default = 'linear'
%     Interpolation-method used in the image-transformation. Admissible choices are
%     the same as in the MATLAB-function interp2.
%
% Returns 
% ------- 
% imTransformed : numerical 2d-array
%    the magnified image
%
% See also 
% ------- 
% functions.imageProcessing.alignment.registerImages
% functions.imageProcessing.alignment.shiftRotateMagnifyImage
% functions.imageProcessing.alignment.shiftRotateImage
% functions.imageProcessing.alignment.shiftImage
% functions.imageProcessing.alignment.rotateImage
%
% Example
% -------
%
% .. code-block:: matlab
%
%     im = rand(100);
%     magnify = [1.3,0.8];
%     imMagnified = magnifyImage(im, magnify);     
%
%     figure('name', 'Original image'); showImage(im);
%     figure('name', 'Magnified image'); showImage(imMagnified);
%
%
% See also REGISTERIMAGES, SHIFTROTATEMAGNIFYIMAGE, SHIFTROTATEIMAGE, SHIFTIMAGE, ROTATEIMAGE
    
% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

if nargin < 3
    settings = struct;
end
settings.invertTransform = false;
imTransformed = shiftRotateMagnifyImage(im, [], [], magnify, settings);

end

