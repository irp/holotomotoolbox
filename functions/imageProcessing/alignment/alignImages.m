function [imAligned,shifts,rotAngleDegree,imToAlignFiltered] = alignImages(imToAlign,imReference,settings)
% ALIGNIMAGES aligns two images to each other.
%
%    ``[imAligned,shifts,rotAngleDegree,imToAlignFiltered] = alignImages(imToAlign,imReference,settings)``
% 
% Aligns two images to each other (imToAlign is aligned to imReference). The shift and/or rotation
% angle needed to do so is calculated via dftregistration. To improve the alignment of the images, 
% additional highpass and lowpass as well as median filtering can be applied. 
%
% Parameters
% ----------
% imToAlign : numerical array
%     image which is aligned with respect to the reference
% imReference : numerical array
%     reference image for the alignment
% settings : structure, optional
%     contains additional settings, see *Other Parameters*
%
% Other Parameters 
% ---------------- 
% registrationMode : Default = 'shift'
%     Determines whether alignment is performed w.r.t. shifts (case: 'shift'), rotation
%     (case: 'rotation') or both (case: 'shiftRotation')
% registrationAccuracy : Default = 10
%     Accuracy-factor for the image-registration. Shifts and/or rotations between the images
%     are detected to an accuracy of 1/registrationAccuracy lengths of a pixel.
% sigmaHighpass : Default = 0
%     Standard deviation of the Gaussian function used for highpass filtering. If
%     sigmaHighpass is 0, no filtering will be performed.
% sigmaLowpass : Default = 0
%     Standard deviation of the Gaussian function used for lowpass filtering. If
%     sigmaLowpass is 0, no filtering will be performed.
% medfilt : Default = false
%     Should median filtering be applied prior to the alignment?
% cutLeft : Default = 0
%     Amount of cutting at the left edge in pixels.
% cutRight : Default = 0
%     Amount of cutting at the right edge in pixels.
% cutTop : Default = 0
%     Amount of cutting at the top edge in pixels.
% cutBottom : Default = 0
%     Amount of cutting at the bottom edge in pixels.
%
% Returns 
% ------- 
% imAligned : numerical array
%     aligned image
% shifts : 2-tuple
%     shifts of the image with respect to the reference along different dimensions
% rotAngleDegree : float
%     registered rotation angle between the images (in degrees)
% imToAlignFiltered : numerical array
%     image for which all image processing steps prior to the alignment were applied
%
% See also
% --------
% functions.phaseRetrieval.utilities.rescaleDefocusSeries, functions.imageProcessing.alignment.registerImages, 
% functions.imageProcessing.alignment.shiftRotateImage
%
% Example
% -------
%
% .. code-block:: matlab
%
%     imToAlign = shiftImage(phantom(512),[17.3 -30.5]);
%     imReference = phantom(512);
%     settings = alignImages;
%     settings.sigmaLowpass = 5;
% 
%     [imAligned,shifts,rotAngleDegree,imFiltered] = alignImages(imToAlign,imReference,settings);
% 
%     subplot(2,2,1); showImage(imToAlign); title('Image to be aligned');
%     subplot(2,2,2); showImage(imFiltered); title('Filtered image used in the registration');
%     subplot(2,2,3); showImage(imAligned); title('Aligned image');
%     subplot(2,2,4); showImage(imReference); title('Reference image');
%
% See also RESCALEDEFOCUSSERIES, REGISTERIMAGES, SHIFTROTATEIMAGE
    
% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

% if no settings are passed, take the default values
if (nargin < 3)
    settings = struct;
end

% default settings
defaults.sigmaHighpass = 0;        
defaults.sigmaLowpass = 0;       
defaults.medfilt = false;             
defaults.cutLeft = 0;             
defaults.cutRight = 0;             
defaults.cutTop = 0;               
defaults.cutBottom = 0;     
defaults.registrationMode = 'shift';                   
defaults.registrationAccuracy = 10;             


% return default settings
if (nargin == 0)
    imAligned = defaults;
    shifts = [];
    imToAlignFiltered = [];
    return
end

settings = completeStruct(settings,defaults);

%% Check that images have the same size
if ndims(imToAlign) ~= ndims(imReference) || any(size(imToAlign) ~= size(imReference))
    error ('Image sizes do not match, canceling aligment');
end


%% Perform the alignment
imReferenceFiltered = applyFilters(imReference, settings);
imToAlignFiltered = applyFilters(imToAlign, settings);

[shifts, rotAngleDegree] = registerImages(imReferenceFiltered, imToAlignFiltered, settings);
         
imAligned = shiftRotateImage(imToAlign, shifts, rotAngleDegree);


%% Filtering function
function imFiltered = applyFilters(im, settings)
    imFiltered = im;

    % optional lowpass filtering of the images
    if (settings.sigmaLowpass > 0)
        imFiltered = imgaussfilt(imFiltered, settings.sigmaLowpass);
    end

    % optional highpass filtering of the images
    if (settings.sigmaHighpass > 0)
        imFiltered = imFiltered - imgaussfilt(imFiltered, settings.sigmaHighpass);
    end

    % optional median filtering
    if (settings.medfilt)
        imFiltered = medfilt2(imFiltered,'symmetric');
    end

    % optionally crop image
    imFiltered = croparray(imFiltered, [settings.cutTop,settings.cutLeft], ...
                                       [settings.cutBottom,settings.cutRight] );
end

end


