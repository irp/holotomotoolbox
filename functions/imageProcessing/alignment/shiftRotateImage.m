function imTransformed = shiftRotateImage(im, shifts, rotAngleDegree, settings)
% SHIFTROTATEIMAGE applies shifts and/or rotations to a given image.
%
%    ``imTransformed = shiftRotateImage(im, shifts, rotAngleDegree, settings)``
%
% Does so in a single operation, such that the loss of image-resolution
% due to interpolation is reduced compared to applying shifts and rotations
% subsequently. The beneficial effect of this can be observed in the provided
% code-example.
% IMPORTANT: the geometrical image-transformations are applied in the following order: 
% (1) rotation
% (2) shift
%
% Parameters
% ----------
% im : numerical 2d-array
%     image to be shifted and/or rotated
% shifts : 2-tuple of floats
%     shifts to be applied to the image (in units of pixel-lengths) 
% rotAngleDegree : float
%     angle of the rotation to be applied to the image (in degrees)
%
% Other Parameters 
% ---------------- 
% padVal : Default = 'replicate'
%     Determines the type of the padding that is applied to the input image prior to
%     shifting and rotating. Admissible choices are the same as in the
%     MATLAB-function padarray.
% interpMethod : Default = 'linear'
%     Interpolation-method used in the image-transformation. Admissible choices are
%     the same as in the MATLAB-function interp2.
% invertTransform : Default = false
%     Determines whether to invert the assigned shift-rotation before application.
%     This allows to shift-rotate back and forth without manually computing
%     the inverse shifts and rotation (see code-example)
%
% Returns 
% ------- 
% imTransformed : numerical 2d-array
%    the shifted and/or rotated image
%
% See also 
% ------- 
% functions.imageProcessing.alignment.shiftRotateMagnifyImage
% functions.imageProcessing.alignment.shiftImage
% functions.imageProcessing.alignment.rotateImage
% functions.imageProcessing.alignment.magnifyImage
%
% Example
% -------
%
% .. code-block:: matlab
%
%     n = 64;
%     rotAngleDegree = 17.4;
%     shifts = [-9.7, 8.3];
%     im = padarray(ones(n/2), [n/4,n/4]); 
%     im(n/8+1:n/4,n/4+1:3*n/8) = 1;
%     figure('name', 'Original image'); showImage(im);
%     
%     imTransformed1 = shiftRotateImage(im, shifts, rotAngleDegree);
%     figure('name', 'One-step shift-rotated image'); showImage(imTransformed1);
%     
%     imTransformed2 = shiftImage(rotateImage(im, rotAngleDegree), shifts);
%     figure('name', 'Two-step shift-rotated image'); showImage(imTransformed2);
%
%     settings.invertTransform = true;
%     imTransformedBack = shiftRotateImage(imTransformed1, shifts, rotAngleDegree, settings);
%     figure('name', 'Back and forth shift-rotated image'); showImage(imTransformedBack);
%
%
% See also SHIFTROTATEMAGNIFYIMAGE, SHIFTIMAGE, ROTATEIMAGE, MAGNIFYIMAGE
    
% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

if nargin == 0
    imTransformed = shiftRotateMagnifyImage;
    return
end
if nargin < 4
    settings = struct;
end
imTransformed = shiftRotateMagnifyImage(im, shifts, rotAngleDegree, [], settings);

end

