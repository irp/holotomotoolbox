function imTransformed = shiftImage(im, shifts, settings)
% SHIFTIMAGE applies geometrical shifts to a given image.
%
%    ``imTransformed = shiftImage(im, shifts, settings)``
%
% Other than the MATLAB-function circshift (or subpixel-accuracy versions of it), this function
% does not assume periodic boundary conditions but applies a suitable padding before shifting.
%
% Parameters
% ----------
% im : numerical 2d-array
%     image to be shifted
% shifts : 2-tuple of floats
%     shifts to be applied to the image (in units of pixel-lengths) 
%
% Other Parameters 
% ---------------- 
% padVal : Default = 'replicate'
%     Determines the type of the padding that is applied to the input image prior to
%     shifting and rotating. Admissible choices are the same as in the
%     MATLAB-function padarray.
% interpMethod : Default = 'linear'
%     Interpolation-method used in the image-transformation. Admissible choices are
%     the same as in the MATLAB-function interp2.
%
% Returns 
% ------- 
% imTransformed : numerical 2d-array
%    the shifted image
%
% See also 
% ------- 
% functions.imageProcessing.alignment.shiftRotateMagnifyImage
% functions.imageProcessing.alignment.shiftRotateImage
% functions.imageProcessing.alignment.rotateImage
% functions.imageProcessing.alignment.magnifyImage
%
% Example
% -------
%
% .. code-block:: matlab
%
%     im = rand(100);
%     shifts = [14.7,-19.3];
%     imShifted = shiftImage(im, shifts);     
%
%     figure('name', 'Original image'); showImage(im);
%     figure('name', 'Shifted image'); showImage(imShifted);
%
%
% See also SHIFTROTATEMAGNIFYIMAGE, SHIFTROTATEIMAGE, ROTATEIMAGE, MAGNIFYIMAGE
    
% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

if nargin == 0
    imTransformed = rmfield(shiftRotateMagnifyImage, 'invertTransform');
    return
end
if nargin < 3
    settings = struct;
end
settings.invertTransform = false;
imTransformed = shiftRotateMagnifyImage(im, shifts, [], [], settings);

end

