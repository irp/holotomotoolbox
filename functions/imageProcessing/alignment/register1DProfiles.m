function shiftval = register1DProfiles(profToAlign,profReference,settings)
% ALIGNPROFILES registers two 1D signals and returns the shift.
%
%    ``shiftval = register1DProfiles(profToAlign,profReference,settings)``
% 
%
% Parameters
% ----------
% profToAlign : vector
%     profile which is aligned with respect to a given reference
% profReference : vector 
%     reference profile for the alignment
% settings : structure, optional
%     contains additional settings, see *Other Parameters*
%
% Other Parameters
% ----------------
% registrationAccuracy : Default = 100
%     Accuracy-factor for the image-registration. Shifts between the 1D profiles
%     are detected to an accuracy of 1/registrationAccuracy lengths of a pixel.
%
% Returns
% -------
% shiftval : number
%     determined shift value between the two profiles
%
%
% Example
% -------
%
% .. code-block:: matlab
%
%     profReference = rand([100,1]);
%     profShifted = circshift(profReference,[20 0]);
% 
%     settings = register1DProfiles;
%     settings.registrationAccuracy = 100;
% 
%     shiftval = register1DProfiles(profShifted, profReference, settings);
%     plot(profReference)
%     hold on
%     plot(circshift(profShifted,[shiftval 0]))
%     hold off
    
% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

if nargin < 3
    settings = struct;
end

defaults.registrationAccuracy = 100;

if (nargin == 0)
    shiftval = defaults;
    return
end

settings = completeStruct(settings, defaults);

profToAlign = profToAlign(:);
profReference = profReference(:);

out = dftregistration(fft(profReference),fft(profToAlign),settings.registrationAccuracy);
shiftval = out(3);

end

