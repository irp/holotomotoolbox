function [shiftsCombined, rotAngleCombined] = combineShiftRotations(shifts1, shifts2, rotAngle1, rotAngle2)
% COMBINESHIFTROTATIONS applies computes a geometrical image-transformation (rotation+shift)
% that combines two subsequent shift-rotations
%
%    ``shiftsCombined, rotAngleCombined] = combineShiftRotations(shifts1, shifts2, rotAngle1, rotAngle2)``
%
% The benefit of the combined transformation is that the loss of image-resolution due to
% interpolation is reduced compared to the naive approach of applying the two shift-rotations
% to an image one after the other.
% The beneficial effect of this can be observed in the provided code-example.
% IMPORTANT: the geometrical image-transformations are applied in the following order: 
% (1) rotation
% (2) shift
%
% Parameters
% ----------
% shifts1 : 2-tuple of floats
%     shifts to be applied to the image in the first transformation (in units of pixel-lengths)
% shifts2 : 2-tuple of floats
%     shifts to be applied to the image in the second transformation (in units of pixel-lengths)
% rotAngle1 : float
%     angle of the rotation to be applied to the image in the first transformation (in degrees)
% rotAngle2 : float
%     angle of the rotation to be applied to the image in the second transformation (in degrees)
%
% Returns 
% ------- 
% shiftsCombined : 2-tuple of floats
%     shifts to be applied in the combined image-transformation (in units of pixel-lengths)
% rotAngleCombined : float
%     angle of the rotation to be applied in the combined image-transformation (in degrees)
%
% See also 
% ------- 
% functions.imageProcessing.alignment.shiftRotateImage
%
% Example
% -------
%
% .. code-block:: matlab
%
%     n = 64;
%     rotAngle1 = 17.4;
%     shifts1 = [-9.7, 8.3];
%     rotAngle2 = -10.9;
%     shifts2 = [6.4, -3.7];
%     im = padarray(ones(n/2), [n/4,n/4]); 
%     im(n/8+1:n/4,n/4+1:3*n/8) = 1;
%
%     im_twostep = shiftRotateImage(shiftRotateImage(im, shifts1, rotAngle1), shifts2, rotAngle2);
%     figure('name', 'Two-step transformed image'); showImage(im_twostep);
%     
%     [shiftsCombined, rotAngleCombined] = combineShiftRotations(shifts1, shifts2, rotAngle1, rotAngle2);
%     im_combined = shiftRotateImage(im, shiftsCombined, rotAngleCombined);
%     figure('name', 'Combined-step transformed image'); showImage(im_combined);
%     
%     figure('name', 'Difference image'); showImage(im_combined-im_twostep);
%
%
% See also SHIFTROTATEIMAGE
    
% HoloTomoToolbox
% Copyright (C) 2022 Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

c = cosd(rotAngle2);
s = sind(rotAngle2);
rotMatrix2 = [[c,-s]; [s,c]];
shiftsCombined = reshape(rotMatrix2 * shifts1(:), size(shifts1)) + shifts2;
rotAngleCombined = rotAngle1 + rotAngle2;

end
