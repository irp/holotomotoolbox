function [shifts, rotAngleDegree] = registerImages(imRef, imMoving, settings)
% REGISTERIMAGES registers shifts and/or rotations of an image w.r.t. a reference-image.
%
%    ``[shifts, rotAngleDegree] = registerImages(imRef, imMoving, settings)``
%
% The implementation is based on dftregistration, which is applied either to the images
% themselves (to register shifts) or to images transformed to polar coordinates (to register
% rotations). For a full registration of shifts and rotations, both registration-operations
% are performed iteratively until convergence is reached.
%
% Parameters
% ----------
% imRef : numerical 2d-array
%     reference image
% imMoving : numerical 2d-array
%     image to be registered, which is ideally just a shifted and rotated version of imRef 
% settings : structure, optional
%     contains additional settings, see *Other Parameters*
%
% Other Parameters 
% ---------------- 
% registrationMode : Default = 'shiftRotation'
%     Determines whether image-registration is performed with respect to only shifts 
%     (case: 'shift'), only rotation (case: 'rotation') or both (case: 'shiftRotation')
% registrationAccuracy : Default = 10
%     Accuracy-factor for the image-registration. Shifts and/or rotations between the images
%     are detected to an accuracy of 1/registrationAccuracy lengths of a pixel.
% maxIterations : Default = 5
%     Maximum number of alternating registrations in shifts and rotation-angle that
%     are performed before stopping the algorithm.
%
% Returns 
% ------- 
% shifts : 2-tuple of floats
%     registered shifts in units of pixel-lengths
% rotAngleDegree : float
%	  registered rotation angle in degrees
%
% See also 
% ------- 
% functions.contrib.dftregistration, 
% functions.imageProcessing.alignment.shiftRotateImage, 
% functions.imageProcessing.alignment.alignImages
%
% Example
% -------
%
% .. code-block:: matlab
%
%     rotAngleDegreeTrue = 3.7;
%     shiftsTrue = [-14.4,23.7];
%     imRef = padarray(ones(1024), [512,512]); imRef(257:512,513:768) = 1;
%     settings.invertTransform = true;
%     imMoving = shiftRotateImage(imRef, shiftsTrue, rotAngleDegreeTrue, settings);
%     
%     figure('name', 'Reference image'); imagesc(imRef); colorbar;
%     figure('name', 'Image to be registered'); imagesc(imMoving); colorbar;
%     
%     [shiftsReg, rotAngleDegreeReg] = registerImages(imRef, imMoving);
%     
%     fprintf('True shifts are [%d, %d].\n', shiftsTrue(1), shiftsTrue(2));
%     fprintf('Registered shifts are [%d, %d].\n\n', shiftsReg(1), shiftsReg(2));
%     fprintf('True rotation-angle is %d degrees.\n', rotAngleDegreeTrue);
%     fprintf('Registered rotation-angle is %d degrees.\n', rotAngleDegreeReg);
%
%     imAligned = shiftRotateImage(imMoving, shiftsReg, rotAngleDegreeReg);
%     figure('name', 'Aligned image'); imagesc(imAligned); colorbar;
%     figure('name', 'Difference between aligned- and reference-image'); 
%     imagesc(imAligned-imRef); colorbar;
%
%
% See also DFTREGISTRATION, SHIFTROTATEIMAGE, ALIGNIMAGES
    
% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

% Default settings
defaults.registrationMode = 'shiftRotation';
defaults.registrationAccuracy = 10;
defaults.maxIterations = 5;
if (nargin == 0)
    shifts = defaults;
return;
end
if nargin < 3
    settings = struct;
end
settings = completeStruct(settings, defaults);


% Ensure that input images are of double-precision. Otherwise FFT-based alignment may be inaccurate
imRef = double(imRef);
imMoving = double(imMoving);


% Perform image-registration according to specified mode
switch lower(settings.registrationMode)

    % Case 1: Register only shifts between the images. This can be performed in a single step
    %         via dftregistration.
    case 'shift'
        shifts = registerShifts(imRef, imMoving, settings);
        rotAngleDegree = 0;


    % Case 2: Register only a rotation between the images. This can be performed in a single step
    %         by applying dftregistration to the images transformed to polar coordinates
    case 'rotation'
        shifts = [0,0];
        rotAngleDegree = registerRotation(imRef, imMoving, settings);


    % Case 3: Register both shifts and rotations between the images. This is done by alternatingly
    %         registering with respect to EITHER shifts or rotation until convergence of the
    %         motion-parameters is reached.
    case 'shiftrotation'

        % Initialize
        converged = false;
        shifts = [0,0];
        rotAngleDegree = 0;

        % Weighting factor that translates rotation angles in degrees to mean pixel-movements
        rotAngleWeightFactor = (pi/180) * (min(size(imRef))/3);

        % Iterate
        for iteration = 1:settings.maxIterations

            % Apply previously detected rotation and register shifts
            shiftsOld = shifts;
            shifts = registerShifts(imRef, rotateImage(imMoving, rotAngleDegree), settings);

            % Check convergence condition
            shiftsIncrement = norm(shiftsOld(:)-shifts(:));
            if shiftsIncrement < 1./settings.registrationAccuracy
                converged = true;
                break;
            end

            % Apply previously detected shifts and register rotation
            rotAngleDegreeOld = rotAngleDegree;
            rotAngleDegree = registerRotation(imRef, shiftImage(imMoving, shifts), settings);

            % Check convergence condition
            rotAngleIncrement = rotAngleWeightFactor * abs(rotAngleDegree - rotAngleDegreeOld);
            if rotAngleIncrement < 1./settings.registrationAccuracy
                converged = true;
                break;
            end
            
        end

        % Print convergence-warning if the maximum number of iterations has been reached
        if ~converged
            warning(['Alternating registration of shifts and rotation-angle has not converged. '...
                     'Result may be inaccurate.']);
        end


    % Throw error if no valid mode has been assigned.
    otherwise
        error(['Invalid image-registration mode assigned in settings.registrationMode. ', ...  
               'Admissible choices are ''shift'', ''rotation'' and ''shiftRotation''.']);
end

end




function shifts = registerShifts(imRef, imMoving, settings)

% Register images using dftregistration
out = dftregistration(fft2(imRef), fft2(imMoving), settings.registrationAccuracy);
shifts = out(3:4);

end



function rotAngleDegree = registerRotation(imRef, imMoving, settings)

% Convert images to polar coordinates
[imRefPolar, Rho] = imageToPolarCoords(imRef);
imMovingPolar = imageToPolarCoords(imMoving);

% Apply weighting according to size of polar pixels
imRefPolar = sqrt(Rho) .* imRefPolar;
imMovingPolar = sqrt(Rho) .* imMovingPolar;

% Register shifts between images on polar grid
shifts = registerShifts(imRefPolar, imMovingPolar, settings);

% Shift component along angular dimension is the sought shift (up to rescaling)
rotAngleDegree = -(360/size(imRefPolar,2)) * shifts(2);

end

