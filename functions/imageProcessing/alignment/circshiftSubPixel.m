function shiftedImage = circshiftSubPixel(imageToShift, shift)
% CIRCSHIFTSUBPIXEL performs circular shift with sub-pixel accuarcy.
%
%    ``shiftedImage = circshiftSubPixel(imageToShift, shift)``
%
% Performs circular shift with sub-pixel accuracy using linear interpolation in real
% space.
%
% Parameters
% ----------
% imageToShift : numerical array
%     image to be shifted
% shift : numerical array
%     array in which the shift values for each dimensions are given
%
% Returns
% -------
% shiftedImage : numerical array
%     shifted image
%
% Example
% -------
%
% .. code-block:: matlab
%
%     image = phantom(512);
%     shiftedImage = circshiftSubPixel(image, [10.5 73.1]);
%     subplot(1,2,1); showImage(image)
%     subplot(1,2,2); showImage(shiftedImage)

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

integerShiftX = floor(shift(2));
integerShiftY = floor(shift(1));
weightShiftX = shift(2)-integerShiftX;
weightShiftY = shift(1)-integerShiftY;

shiftedImage = circshift(imageToShift,[integerShiftY,integerShiftX]).*(1-weightShiftY) ...
    + circshift(imageToShift,[integerShiftY+1,integerShiftX]).*weightShiftY;
shiftedImage = shiftedImage.*(1-weightShiftX) + circshift(shiftedImage,[0,1]).*weightShiftX;

end
