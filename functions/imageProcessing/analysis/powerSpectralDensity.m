function psdVals = powerSpectralDensity(array, betaWindow)
% POWERSPECTRALDENSITY computes the power-spectral-density (PSD) of a given array.
%
%    ``psdVals = powerSpectralDensity(array, beta_window)``
%
% Note that prior to the Fourier transform, the array is weighted with a Kaiser-Bessel window
% in order to avoid truncation errors.
%
% Parameters
% ----------
% array : numerical array
%     array of which the PSD is computed
% betaWindow : number, optional
%     non-negative number that determines the shape of the Kaiser-Bessel-window;
%     values <= 0 omit the windowing-operation; default = 8
%
% Returns
% -------
% psdVals : numerical array
%     array containing the computed PSD
% 
% Example
% -------
%
% .. code-block:: matlab
%
%     % PSD of a disk (should yield an Airy-disk)
%     N = [128,128];
%     r_disk = 10;
%     im = fspecial('disk', r_disk);
%     im = padarray(padarray(im, ceil((N-size(im))/2), 0, 'pre'), floor((N-size(im))/2), 0, 'post');
%     figure('name', 'image'); showImage(im)
%     figure('name', 'Log(PSD(image))'); showImage(log(powerSpectralDensity(im)))

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

if (nargin < 2)
    betaWindow = 8;
end

% Optionally weight array with Kaiser-Bessel-window to avoid truncation errors
if betaWindow > 0
    array = array .* kaiserBesselWindow(size(array), betaWindow);
end

% Compute power spectral density
psdVals = fftshift(abs(fftn(array)).^2);

end
