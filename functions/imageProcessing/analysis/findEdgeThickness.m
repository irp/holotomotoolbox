function edge = findEdgeThickness(im)
% FINDEDGETHICKNESS determines the thickness of edges within the given image.
%
%    ``edge = findEdgeThickness(im)``
%
%
% Parameters
% ----------
% im : numerical 2d-array
%     image of which the edge thickness is determined
%
% Returns 
% ------- 
% edge : float
%    determined edge thickness
%
% See also 
% ------- 
% functions.phaseRetrieval.directContrast.phaserec_bac
%
% Example
% -------
%
% .. code-block:: matlab
%
%     findEdgeThickness(phantom(256))
%
% See also PHASEREC_BAC
    
% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

% constants
GAUSSFILTER_SIGMA = 1.0;

absoluteGradient = abs(removeOutliers(imgradient( ...
    imgaussfilt(im,GAUSSFILTER_SIGMA)))) ;

meangrad = mean(absoluteGradient(:));
stdgrad = std(absoluteGradient(:));

tmp_th = absoluteGradient > (meangrad + 2 * stdgrad) ;

se = strel('disk',1);
tmp_th = imerode(tmp_th, se);
tmp_th = imdilate(tmp_th, se) ;

area = sum(tmp_th(:) );

grad_tmp_th = imgradient(tmp_th) ;
grad_tmp_th = grad_tmp_th > 0 ;
grad_tmp_th = grad_tmp_th + tmp_th ;

grad_tmp_th = (grad_tmp_th == 2) ;

length = sum(grad_tmp_th(:) ) / 2.0;

edge = area / length + 2; % seems that edges are underestimated due to the procedure above-> therefore add 2

end
