function centerOfMass = centerOfMass(image)
% CENTEROFMASS determines the center of mass of the given image.
%
%    ``centerOfMass = centerOfMass(image)``
%
% Parameters
% ----------
% image : numerical array
%     n-dimensional array of which the center of mass is determined
%
% Returns 
% ------- 
% centerOfMass : numerical array
%     n-dimensional center of mass
%
%
% Example
% -------
%
% .. code-block:: matlab
% 
%     image = phantom(512);
% 
%     centerOfMass = centerOfMass(image);
% 
% 
%     settings.diameter = 30;
%     settings.center = [0,22,-17];
%     image3D = ball([177,228,160], settings);
% 
%     COM_expected = 0.5*([177,228,160]+1) + [0,22,-17]
%     COM_computed = centerOfMass(image3D)
% 

    
% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

N = size(image);
ndim = numel(N);

centerOfMass = zeros(1,ndim);

for iDim = 1:ndim
    notDims = find((1:ndim) ~= iDim);
    imageSummed = image;
    for notDim = notDims
        imageSummed = sum(imageSummed, notDim);
    end
    centerOfMass(iDim) = ((1:N(iDim)) * imageSummed(:)) / sum(imageSummed);
end

centerOfMass = centerOfMass(size(image)~=1);

end
