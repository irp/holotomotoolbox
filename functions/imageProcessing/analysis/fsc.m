function [fscValues, frequencies, thresHalfBit, thresFullBit] = fsc(im1, im2, settings)
% FSC computes the Fourier Shell Correlation between two 2D or 3D arrays.
%
%     ``[fscValues, frequencies, thresHalfBit, thresFullBit] = fsc(im1, im2, settings)``
%
% This function comoutes the Fourier Shell Correlation (FSC) between two given 2D or
% 3D images. In the 2D-setting this is also known as the Fourier Ring Correlation
% (FRC). By comparing the FSC between two indepedent image-reconstructions from two
% different data sets to the 1/2- or 1-bit-threshold curve, the achieved resolution
% can be estimated :cite:`Heel_JSB_2005`.
%
% Parameters
% ----------
% im1 : numerical 2d- or 3d-array
%     first image (2d or 3d)
% im2 : numerical 2d- or 3d-array
%     second image of the same size as im1
% settings : structure, optional
%     contains additional settings, see *Other Parameters*
%
% Other Parameters
% ----------------
% beta : Default = 8
%     See Parameter of the Kaiser-Bessel window that is applied to the images prior to computing
%     the fsc/frc in order to suppress periodicity-effects of the FFTs, see documentation
%     of function kaiserBesselWindow. If beta = 0, no window-function is applied.
%
% Returns
% -------
% fscValues : vector of floats
%     the computed values of the FSC/FRC
% frequencies : vector of floats
%     the Fourier-frequencies corresponding to the values in fscValues
% thresHalfBit : vector of floats
%     values of the 1/2-bit-threshold-curve at the points given in frequencies
% thresFullBit : vector of floats
%     values of the 1-bit-threshold-curve at the points given in frequencies
%
%
% Example
% -------
%
% .. code-block:: matlab
%
%     % Example 1: FRC of two images, where one is blurred version of the other
%     im1 = padarray(rand(1024), [512,512]);
%     im2 = imfilter(im1, fspecial('gaussian', [12,12], 2));
%     [frcValues, freq, halfBit, fullBit] = fsc(im1, im2);
%
%     figure('name', 'First image'); imagesc(im1); colorbar;
%     figure('name', 'Second image'); imagesc(im2); colorbar;
%     figure;
%     hold on;
%     plot(freq, frcValues, 'DisplayName', 'Fourier-ring-correlation');
%     plot(freq, halfBit, 'DisplayName', '1/2 bit threshold-curve');
%     plot(freq, fullBit, 'DisplayName', '1 bit threshold-curve');
%     hold off;
%     legend show;
%
%     % Example 2: FSC of two 3d-arrays, where one is shifted version of the other
%     arr1 = rand([256,256,256]);
%     arr2 = circshift(arr1, [3,-2,1]);
%     settings.beta = 0;      % Do not suppress periodicity by window-function
%     [fscValues, freq, halfBit, fullBit] = fsc(arr1, arr2, settings);
%
%     figure;
%     hold on;
%     plot(freq, fscValues, 'DisplayName', 'Fourier-ring-correlation');
%     plot(freq, halfBit, 'DisplayName', '1/2 bit threshold-curve');
%     plot(freq, fullBit, 'DisplayName', '1 bit threshold-curve');
%     hold off;
%     legend show;


% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.


% Default parameters
defaults.beta = 8;

% Return default parameters if called without argument
if (nargin == 0)
    fsc = defaults;
    return
end

% Complete settings with default parameters
if nargin < 3
    settings = struct;
end
settings = completeStruct(settings, defaults);



% Determine image-dimensions
N = size(im1);
ndim = numel(N);


% Compute Fourier transforms
%
% Default case: apply Kaiser-Bessel window prior to Fourier-transforming
if settings.beta > 0
    window = kaiserBesselWindow(N, settings.beta);
    im1_fft = fftn(im1 .* window);
    im2_fft = fftn(im2 .* window);
    clear 'window';
    
    % Special case beta == 0: no Kaiser-bessel window applied
else
    im1_fft = fftn(im1);
    im2_fft = fftn(im2);
end


% Compute norm of the wave vector for all points in Fourier space
xi = sqrt(fftfreqNormSq(N));

% Round values to integers for distribution to different Fourier shells
shells = round(min(N)/(2*pi) * xi)+1;

% Number of Fourier frequencies in each cell
n_xi = accumarray(shells(:), ones([numel(shells),1]));
num_xi = numel(n_xi);

% Compute correlation on shells
fscValues =    real( accumarray(shells(:), im1_fft(:) .* conj(im2_fft(:)), [num_xi, 1]) ) ...
    ./ sqrt(   accumarray(shells(:), abs(im1_fft(:)).^2, [num_xi, 1]) ...
    .* accumarray(shells(:), abs(im2_fft(:)).^2, [num_xi, 1])     );

% Restrict to values on shells that are fully contained within the image
fscValues = fscValues(1:ceil((min(N)+1)/2));
n_xi = n_xi(1:ceil((min(N)+1)/2));


% Frequency vector
frequencies = (pi/numel(n_xi))*(0:numel(n_xi)-1)';

% 1/2-bit threshold curve
thresHalfBit = ( 0.2071 + 1.9102 ./ n_xi.^(1/(ndim-1))  ) ./ ( 1.2071 + 0.9102 ./ n_xi.^(1/(ndim-1)));

% 1-bit threshold curve
thresFullBit = ( 0.5 + 2.4142 ./ n_xi.^(1/(ndim-1))  ) ./ ( 1.5 + 1.4142 ./ n_xi.^(1/(ndim-1)));

end

