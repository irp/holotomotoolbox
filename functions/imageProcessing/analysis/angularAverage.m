function [averages, radii] = angularAverage(im)
% ANGULARAVERAGE computes the average of a 2D- or 3D-image over concentric
% spherical shells around the image-centre.
%
%     ``[averages, radii] = angularAverage(im)``
%
% The averaging is performed using bilinear interpolation. The centre of the image
% is assumed at the point floor(size(im)/2)+1, as is the case for shifted FFTs.
% 
%
% Parameters
% ----------
% im : numerical 2d- or 3d-array
%     image of which the angular average is to be computed
%     
%
% Returns 
% ------- 
% averages : vector of floats of length ceil(min(size(im))/2)
%     the computed angular averages 
% radii : vector of floats of the same length as averages
%     the radii of the spherical shells over which the averages were computed 
%
% Example
% -------
%
% .. code-block:: matlab
%
%     N = [1024,1024];
%     fresnel = 1e-2;
%     xi2 = fftfreqNormSq(N, 1);
%     ctf = fftshift(sin(xi2/(4*pi*fresnel)));
%     [ctfAveraged, radii] = angularAverage(ctf);
%     
%     figure('name', 'image'); imagesc(ctf); colorbar;
%     figure('name', 'angular average'); plot(radii, ctfAveraged);
%
%
    
% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

% Image dimensions
N = size(im);
ndim = numel(N);

% Image centre
centre = floor(N/2)+1;

% Compute distance of pixels to center
r = 0;
for jj = 1:ndim
    r = r + reshape( ((1:N(jj)) - centre(jj)).^2, [ones([1,jj-1]), N(jj), ones([1,ndim-jj])] );
end
r = sqrt(r);

% Each point is contained in between two concentric spherical shells:
% Compute index of these shells and the distance from the larger of the two shells.
lower_shell_idx = round(r+0.5-1e-10);
upper_shell_idx = lower_shell_idx + 1;
dist_to_upper_shell = lower_shell_idx - r;
lower_shell_idx = max(lower_shell_idx, 1);

% Number of pixels corresponding to the different radial shells
n_shells = accumarray(upper_shell_idx(:), (1-dist_to_upper_shell(:)));
n_shells = n_shells + accumarray(lower_shell_idx(:), dist_to_upper_shell(:), size(n_shells));

% Compute average values over all shells
averages = (  accumarray(lower_shell_idx(:), dist_to_upper_shell(:) .* im(:),...
                        size(n_shells)) ...
            + accumarray(upper_shell_idx(:), (1-dist_to_upper_shell(:)) .* im(:),...
                        size(n_shells))) ./ n_shells;
    
% Restrict to values on shells that are fully contained within the image
num_shells = ceil(min(N)/2);
averages = averages(1:num_shells);

% Corresponding radial coordinates
radii = (0:num_shells-1).';

end

