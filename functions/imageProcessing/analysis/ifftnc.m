function result = ifftnc(image)
% IFFTNC performs a centered inverse Fourier transform.
%
% Parameters
% ----------
% image : numerical array
%     input data
%
% Returns
% -------
% result : numerical array
%     centered inverse Fourier transform of the input
%
% See also
% --------
% functions.imageProcessing.analysis.fftnc, functions.auxiliary.fftfreq
%
% Notes
% -----
% This function should only be used in combination with fftnc when transforming a
% centered radial symmetric image. In functions working with Fourier transformed
% images, the normal ifft should be used and filters should be created using fftfreq.
%
% Example
% -------
%
% .. code-block:: matlab
%
%     image = ifftnc(phantom(512));
%
%     showImage(abs(image))
%
%
% See also FFTNC, FFTFREQ

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

result = sqrt(numel(image)) * fftshift(ifftn(ifftshift(image)));

end
