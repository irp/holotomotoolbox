function [ranges, cropSize, cropImg] = cropTool(region, imageOriginal)
%cropTool is an interactive and command-based function to crop an image to arbitrary sizes.
%
% Parameters
% ----------
% region : matlab imrect object or 4-tupil
%     cropRegion, if given manually of format: [ymin ymax xmin xmax].
%     Otherwise an imrect object.
%
% Returns
% -------
% ranges : numerical array
%     cropping region
% cropImg : numerical array
%     cropped image
%
%
% Example
% -------
%
% .. code-block:: matlab
%
%     image = phantom(512);
%     showImage(image)
%     autoContrast
%

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

% get the current figure
currentFig = gca;

if isempty(currentFig)
    error('No figure window open');
end
% define crop region
if (nargin<1)
    region = imrect;
    wait(region);
end

if isa(region, 'imrect') % region is a imrect
    imageOriginal = getimage(currentFig);
    
    % create mask
    % matlabs position format:[xmin ymin width height]
    dimMask = round(region.getPosition);
    cropSize = [dimMask(4) dimMask(3)];
    
    %range format: [ymin ymax xmin xmax]
    ranges = [dimMask(2), dimMask(2)+dimMask(4)-1, dimMask(1), dimMask(1)+dimMask(3)-1];
    
    % only consider part of the image which lies within specified region
    cropImg = imageOriginal(dimMask(2):dimMask(2)+dimMask(4)-1,dimMask(1):dimMask(1)+dimMask(3)-1);
    
    region.delete
    
else
    % region are pixel coordinates ranges
    ranges = ...
        imageOriginal(region(1):region(2), region(3):region(4), :);
end

end

