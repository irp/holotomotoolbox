function imPadded = padFadeout(im, sizePadded, settings)
% PADFADEOUT smoothly pads an input image to a given output size.
%
%    ``imPadded = padFadeout(im, sizePadded, settings)``
%
% Does so by smoothly transitioning from the image's boundary-values to a constant padding value.
% This should be done to avoid sharp edges between the original image and the padding
% area since these edges often introduce artifacts when the image is Fresnel-propagated
% (or if related operations, such as CTF-phase-reconstruction for example, are performed).
% Similarly, jumps of the pixel-values between opposite image-boundaries may lead to
% Fresnel-propagation-artifacts due to periodic boundary-conditions that are implicitly
% assumed in FFTs. This function performs padding such that both types of jumps in the
% padded image are prevented.
%
%
% Parameters
% ----------
% im : numerical array
%      image to be padded
% sizePadded : tuple
%      size of the padded output image ([height width])
% settings : structure, optional
%      contains additional settings, see *Other Parameters*
%
% Other Parameters
% ----------------
% padVal : Default = []
%      padding value to which the image is faded out. If empty, padVal = mean(im(:)) is assigned.
% transitionLength: Default = []
%      length of the transition to the constant padding-value in pixels. If a 2-tuple is assigned, the
%      two values are interpreted as (possibly different) lengths along along the different 
%      image-dimensions. If empty, transitionLength = ceil((sizePadded-size(im))/2) is used,
%      so that the transition-region fills the whole padding area between the original boundaries
%      of the original image and the padded one.
% fadeoutParallel : Default = false
%      Determines whether, the replicated boundary-values of the image are also faded out parallel
%      to the image edge, by increasingly smoothing the replicated rows and columns the further
%      away the padding proceeds from the edges of the original image. This option results in a 
%      visually 'cleaner' fade-out, but is also computationally more costly.
%       
%
% Returns
% -------
% imPadded : numerical array
%     padded array
%
% Example
% -------
%
% .. code-block:: matlab
%
%     im = rand(128);
%     settings = padFadeout();
%     %settings.fadeoutParallel = true;
%     imPadded = padFadeout(im, 2*size(im), settings);
% 
%     figure(1);
%     showImage(im)
%     figure(2);
%     showImage(imPadded);
%

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

defaults.padVal = [];
defaults.transitionLength = [];
defaults.fadeoutParallel = false;

% return default settings
if (nargin == 0)
    imPadded = defaults;
    return;
end

% complete setttings with defaults
if nargin < 3
    settings = struct;
end
settings = completeStruct(settings,defaults);



% Determine padding amounts
sizeIn = size(im);
padPre = ceil( (sizePadded - sizeIn(1:2)) / 2 );      % Number of lower and
padPost = floor( (sizePadded - sizeIn(1:2)) / 2 );  % upper pixels to be padded

% Padding-value is taken as mean of the image if not assigned
if isempty(settings.padVal)
    settings.padVal = mean(im(:));
end

% Apply defaults for the transition length
if isempty(settings.transitionLength)
    % Default: Transition spreads over total pad-region 
    settings.transitionLength = max(1,padPre);        
elseif numel(settings.transitionLength)
    % Same transition length along both dimensions if scalar is assigned
    settings.transitionLength = settings.transitionLength * [1,1];
end



% Step 1: Construct raw padded image
%
% Variant 1.1: replicate padding with additional fade-out parallel to the image-edges
%            by repetitively smoothing the replicated boundary-values of the image
if settings.fadeoutParallel
    imPadded = padarray(padarray(im, padPre, settings.padVal, 'pre'), padPost, settings.padVal, 'post');

    imPadRegion{1} = imPadded;
    imPadRegion{2} = imPadded.';
    for dim = [1,2]
        otherDim = 3-dim;
        imTmp = fft(imPadRegion{dim});
        kernel = fft([1/3; 1/3; zeros([sizePadded(dim)-3, 1]); 1/3]);
        for jj = padPre(otherDim):-1:1
            imTmp(:,jj) = kernel .* imTmp(:,jj+1);
        end
        for jj = padPost(otherDim):-1:1
            imTmp(:,end-jj+1) = kernel .* imTmp(:,end-jj);
        end
        imTmp = ifft(imTmp);
        if isreal(im)
            imTmp = real(imTmp);
        end
        imPadRegion{dim} = imTmp;
    end

    imPadded = imPadRegion{1} + imPadRegion{2}.' - imPadded;
%
%
% Variant 1.2 (default case): no fade-out parallel to the edges. Raw padding is simple 
% replicate-operation 
else
    imPadded = padarray(padarray(im, padPre, 'replicate', 'pre'), padPost, 'replicate', 'post');
end



% Step 2: Fadeout perpendicular to the image boundaries 
%
% Construct mask that is 1 within the region covered by the original
% image and decays to zero as the distance to the image-boundaries increases.
% The length of the transition from 1 to 0 is defined by transitionLength
[Y,X] = centeredGrid(sizePadded);
X = min((pi/settings.transitionLength(2)) .* max(abs(X) - sizeIn(2)/2, 0), pi);
Y = min((pi/settings.transitionLength(1)) .* max(abs(Y) - sizeIn(1)/2, 0), pi);
transitionMask = ( 0.25 * ( 1 + cos(X) ) ) .* ( 1 + cos(Y) );

% Superimpose constant pad-value and replicate-padded image with the transition mask
imPadded = transitionMask .* imPadded + (1-transitionMask) .* settings.padVal;


end


