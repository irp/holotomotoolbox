function result = cropToCenterSubPixel(im, newSize)
% CROPTOCENTERSUBPIXEL returns the central part of a 2d input array with sub-pixel precision.
%
%    ``result = cropToCenterSubPixel(im,newSize)``
%
% Parameters
% ----------
% im : numerical array
%     input image
% newSize : tuple
%     size of the central part as tuple [height width] to which the input
%     is cropped
%
% Returns
% -------
% result : numerical array
%     central part of im of size newSize
%
% Example
% -------
%
% .. code-block:: matlab
%
%     image = phantom(100);
%     newSize = [20 51];
%     croppedImage = cropToCenterSubPixel(image, newSize);
%     showImage(croppedImage)

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

assert( newSize(1)<=size(im,1) , 'output height is larger than in input.')
assert( newSize(2)<=size(im,2) , 'output width is larger than in input.')

rawOffset = (size(im) - newSize) / 2 + 1;
offset = ceil(rawOffset);
shiftDist = offset - rawOffset;

if any(shiftDist)
  im = shiftImage(im, -shiftDist);
end

offsetEnd = offset + newSize - 1;
result = im( offset(1):offsetEnd(1), offset(2):offsetEnd(2) ); 

end
