function result = replaceCenterSubPixel(largeArray, smallArray)
% REPLACECENTERSUBPIXEL replaces the center of a large array by a given smaller array.
%
%    ``result = replaceCenterSubPixel(largeArray, smallArray)``
%
% This operation is performed with sub-pixel precision.
%
% Parameters
% ----------
% largeArray : numerical array
%     array if which the center is replaced
% smallArray : numerical array
%     array which is placed in the center of largeArray
%
% Returns
% -------
% result : numerical array
%     resulting array in which the center of largeArray is replaced by smallArray
%
% Example
% -------
%
% .. code-block:: matlab
%
%     largeArray = phantom(100);
%     smallArray = rand([20 51]);
%     newArray = replaceCenterSubPixel(largeArray, smallArray);
%     showImage(newArray)

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

largeSize = size(largeArray);
smallSize = size(smallArray);

assert( largeSize(1) >= smallSize(1) , 'center height is larger than total.')
assert( largeSize(2) >= smallSize(2) , 'center width is larger than total.')

rawOffset = (largeSize - smallSize) / 2 + 1;
offset = floor(rawOffset);

shiftDist = rawOffset - offset;
innerSize = smallSize + ceil(shiftDist);
offsetEnd = offset + innerSize - 1;

if any(shiftDist)
    innerArray = zeros(innerSize, 'like', largeArray);
    innerMask = zeros(innerSize, 'like', largeArray);
    
    innerArray(1:smallSize(1),1:smallSize(2)) = smallArray;
    innerMask(1:smallSize(1),1:smallSize(2)) = 1;
    
    shiftSettings = struct;
    shiftSettings.padVal = 0;
    
    innerArray = shiftImage(innerArray, shiftDist, shiftSettings);
    innerMask = shiftImage(innerMask, shiftDist, shiftSettings);
    innerArray = innerArray ...
               + (1-innerMask) .* largeArray(offset(1):offsetEnd(1), offset(2):offsetEnd(2)) ;
else
    innerArray = smallArray;
end

result = largeArray;
result(offset(1):offsetEnd(1), offset(2):offsetEnd(2)) = innerArray;

end
