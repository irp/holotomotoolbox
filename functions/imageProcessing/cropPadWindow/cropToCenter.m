function out = cropToCenter(in, newSize)
% CROPTOCENTER returns the central part of a 2d or 3d input array.
%
%    ``out = cropToCenter(in, newSize)``
%
% This function simply trims down the input image and leaves the coordinate
% grid untouched. If a resampling is required use cropToCenterSubPixel, to
% interpolate the output (over 1px difference).
% 
%
% Parameters
% ----------
% in : numerical array
%     input image of size [M N Z]
% newSize : tuple
%     size of the centered part as tuple [height width] to which the input
%     is cropped; if Z > 1, the cropping is repeated for all entries of the 3rd
%     dimension
%
% Returns
% -------
% result : numerical array
%     central part of im of size newSize
%
% Example
% -------
%
% .. code-block:: matlab
%
%     image = phantom(100); 
%     newSize = [20 51]; 
%     croppedImage = cropToCenter(image, newSize); 
%     showImage(croppedImage)

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

sizeIn = size(in);
sizeIn = sizeIn(1:numel(newSize));
assert( all(newSize <= sizeIn), ...
    ['output size [', sprintf('%d,',newSize), '] is larger than input size [', sprintf('%d,',size(in)), '] in least one dimension.'] );

% note: if M or N is even and m or n odd, then the (M/2)+1 element is the
% center, the same holds for N.
% this is the location of the 0th order in a FFT of an image with even
% number of pixels.
cropPre = max(ceil((size(in) - newSize) / 2), 0);
cropPost = max(size(in) - newSize - cropPre, 0);
out = croparray(in, cropPre, cropPost);

end
