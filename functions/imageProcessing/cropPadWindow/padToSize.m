function result = padToSize(im, outputSize, padval)
% PADTOSIZE pads array to given size.
%
%    ``result = padToSize(image, outputSize, mode)``
%
% Pads an array to a specified size by , e.g., adding zeros or replicating the edge
% values. The method of how the image is padded can be specified via mode.
%
% Parameters
% ----------
% image : numerical array
%     image which is padded
% outputSize : vector
%     2-element vector which specifies the output size of the padded image. If the
%     output size is smaller than the input size, the image will be cropped.
% padval : string or number, optional, Default = 'replicate'
%     Specifies the method of how the image is padded, e.g, a constant number,
%     'replicate', 'symmetric'. These are the standard methods of Matlab's padarray
%     function. An additional mode is 'fadeOut' which fades out the outmost
%     vales of the image across the pad-area. The 'zero' pads with 0's to the given size.
%
% Returns
% -------
% result : numerical array
%     padded image
%
% See also
% --------
% functions.imageProcessing.cropPadWindow.fadeOut
%
% Example
% -------
%
% .. code-block:: matlab
%
%     image = phantom(256);
%     imagePadded = padToSize(image,[500 300],'replicate');
% 
%     showImage(imagePadded)
% 
% See also FADEOUT

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

if nargin < 3
    padval = 'replicate';
end

imageSize = size(im);

% This ensures correct sized results for some cases
padsize = floor((outputSize - imageSize)/2);
padsize2 = outputSize - (imageSize + 2 * padsize);


% LL: wieso cropt diese Funktion?
if any(padsize < 0)
    imageSize = min(imageSize, outputSize);
    result = cropToCenter(im, imageSize);
    return;
end

if strcmpi(padval, 'fadeOut')
    result = padFadeout(im, outputSize);
    return;
end

if ~strcmp(padval, 'zero')
    result = padarray(im, padsize, padval, 'both');
else
    result = padarray(im, padsize, 0, 'both');
end
if any(padsize2 > 0)
    if ~strcmp(padval, 'zero')
        result = padarray(result, padsize2, padval, 'pre');
    else
        result = padarray(result, padsize2, 0, 'pre');
    end
    
end

end
