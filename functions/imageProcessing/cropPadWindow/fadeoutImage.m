function [imFaded window]= fadeoutImage(im, settings)
% FADEOUTIMAGE smoothly fades out an image towards its boundaries 
%
%    ``imFaded = fadeoutImage(im, settings)``
%
% The function offers 3 variants for this task:
%
% - 'cosine' 
%    The input image is faded out to a constant value at the boundaries using a
%    cosine-shaped transition value.
% - 'ellipse' 
%    The fade out is performed by applying an ellipse-shaped mask, where the
%    value(s), to which the image is faded out, are determined from the mean value of
%    the pixels near the boundary of the ellipse. The number of averaging segments
%    can be chosen to account for a strongly varying value distribution on the edge.
%    This method offers more flexibility than variant 1, but is slightly less
%    computationally efficient.
% - 'rectangle' 
%    The fade out is performed by applying an rectangle-shaped mask. Same
%    behavior as the 'ellipse'-case.
%
% Parameters
% ----------
% im : numerical array
%      image to be processed
% settings : structure, optional
%      contains additional settings, see *Other Parameters*
%
% Other Parameters
% ----------------
% method : Default = 'cosine'
%      choose one of the two variants {'cosine', 'ellipse'}
% fadeToVal : Default = []
%      Constant value to which the image is faded out. If empty, suitable values are computed
%      automatically from the input image.
% transitionLength: Default = []
%      length of the transition to constant padding-value in pixels for the cosine-method. 
%      If empty, transitionLength = ceil(mean(size(im))/8) is assigned.
% windowShift: Default = [0 0]
%      Amount of pixels [heigt width] of which the fading window is shifted
%      with respect to the center of the input image.
% ellipseSize: Default = [0.8,0.8]
%      relative size of the ellipsoidal mask (half-axes along the different dimensions) used
%      to fade out the image in the ellipse-method. 
%      Without effect if method == 'cosine'
% numSegments: Default = 1
%      number of angular segments, in which the image is faded out to different values.
%      Without effect if fadeToVal is non-empty or method == 'cosine'
% angularOffsetSegments: Default = 0
%      Angular offset/rotation of the segments in radian.
%      
%
% Returns
% -------
% imFaded : numerical array
%     padded array
%
% Example
% -------
%
% .. code-block:: matlab
%
%     % test defaults - cosine padding
%     im = rand(128);
%     settings = fadeoutImage();
%     %settings.method = 'ellipse';
%     imFaded = fadeoutImage(im, settings);
% 
%     figure(1);
%     showImage(im)
%     figure(2);
%     showImage(imFaded);
%

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modiy
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) asizeIm(1) later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

% default parameters
defaults.method = 'cosine';
defaults.fadeToVal = [];
defaults.transitionLength = [];
defaults.ellipseSize = [0.8,0.8];
defaults.numSegments = 1;
defaults.angularOffsetSegments = 0;
defaults.windowShift = [0 0];

% return default settings
if (nargin == 0)
    imFaded = defaults;
    return;
end

% complete setttings with defaults
if nargin < 2
    settings = struct;
end
settings = completeStruct(settings,defaults);

% Transition length is taken as 1/8 of the image's aspect length if not assigned
if isempty(settings.transitionLength)
    settings.transitionLength = ceil(mean([size(im,1), size(im,2)])/8);
end

% Perform padding with chosen method
switch settings.method
    case 'cosine'
        [imFaded, window] = fadeoutImageCosine(im, settings);
    case 'ellipse'
        [imFaded, window]= fadeoutImageEllipse(im, settings);
    case 'rectangle'
        [imFaded, window]= fadeoutImageEllipse(im, settings);
    otherwise
        error('Invalid value of settings.method. Admissible choices are ''cosine'' and ''ellipse''');
end

end




function [imFaded, transitionMask] = fadeoutImageCosine(im, settings)

% Target value of the fadeout is taken as mean value of the image if not assigned
if isempty(settings.fadeToVal)
    settings.fadeToVal = mean(im(:));
end

% Size of the image stack
sizeIm = size(im);

% Construct mask that is 1 in the interior of the images and that
% decays like a cosine to 0 in the transition region at the boundaries.
[Y,X] = centeredGrid(sizeIm);

% shift center of the fading window
Y = Y + settings.windowShift(1);
X = X + settings.windowShift(2);


X = min((pi/settings.transitionLength) .* max(abs(X) - (sizeIm(2)/2 - settings.transitionLength), 0), pi);
Y = min((pi/settings.transitionLength) .* max(abs(Y) - (sizeIm(1)/2 - settings.transitionLength), 0), pi);
transitionMask = ( 0.25 * ( 1 + cos(X) ) ) .* ( 1 + cos(Y) );

% Superimpose constant and input images weighted with the transition mask.
imFaded = transitionMask .* im + (settings.fadeToVal * (1-transitionMask)) .* ones(sizeIm);

end



function [imFaded transitionMask] = fadeoutImageEllipse(im, settings)
sizeIm = size(im);
        useGPU = isOnGPU(im);
switch settings.method
    case 'ellipse'
        [Y,X] = centeredGrid(sizeIm, 1, true);
        
        % shift center of the fading window
        Y = Y - settings.windowShift(1);
        X = X - settings.windowShift(2);
        
        %radii in x and y in units of pixels
        ry = (settings.ellipseSize(1)*sizeIm(1)-2)/2;
        rx = (settings.ellipseSize(2)*sizeIm(2)-2)/2;
        
        % Indices of the image pixels lying within a centered ellipse
        idxEllipse = X.^2./rx.^2 + Y.^2./ry.^2 < 1;
        
        % extract pixels on the boundary of the ellipse to calculate mean values for fadeout
        rxInner = rx - settings.transitionLength;
        ryInner = ry - settings.transitionLength;
        idxBoundary = xor(idxEllipse, X.^2./rxInner.^2 + Y.^2./ryInner.^2 < 1);
        
        transitionMask = double(idxEllipse);
    case 'rectangle'
        %radii in x and y in units of pixels
        ry = ceil(settings.ellipseSize(1)*sizeIm(1)-2);
        rx = ceil(settings.ellipseSize(2)*sizeIm(2)-2);
        
        
        % Indices of the image pixels lying within a centered rectangle
        idxRectangle = ones([ry rx]);
        idxRectangle = padToSize(idxRectangle, sizeIm, 'zero');
        idxRectangle = circshift(idxRectangle, settings.windowShift);
        
        % extract pixels on the boundary of the ellipse to calculate mean values for fadeout
        rxInner = rx - settings.transitionLength;
        ryInner = ry - settings.transitionLength;
        idxInnerRectangle = ones([ryInner rxInner]);
        idxInnerRectangle = padToSize(idxInnerRectangle, sizeIm, 'zero');
        idxInnerRectangle = circshift(idxInnerRectangle, settings.windowShift);
        
        
        idxBoundary = xor(idxRectangle, idxInnerRectangle);
        transitionMask = double(idxRectangle);
end



% Determine value(s) to which the image is faded out
%
% Case 0: Constant fadeout-value has been assigned
if ~isempty(settings.fadeToVal)
    fadeToVals = settings.fadeToVal;
%
% Case 1: Compute fadeout-values in multiple angular segments
elseif settings.numSegments > 1
    %% Determine angular segments of the image
    [Y,X] = centeredGrid(sizeIm, 1, true);
        
    % shift center of the fading window
    Y = Y - settings.windowShift(1);
    X = X - settings.windowShift(2);
    
    [theta, rho] = cart2pol(X, Y);
    theta = mod(theta + settings.angularOffsetSegments, 2*pi);
    segmentIndices = gpuArrayIf(min(floor((settings.numSegments/(2*pi)) * theta) + 1, settings.numSegments), useGPU);
    imFilt = imgaussfilt(im, 10/2.35);
    % Compute target values for the fade out in each angular segment
    fadeToVals = gpuArrayIf(zeros(sizeIm), useGPU);
    idxBoundary = gpuArrayIf(idxBoundary, useGPU);
    for segment = 1:settings.numSegments
        inSegment = (segment == segmentIndices);
        fadeToVals(inSegment) = mean(imFilt(idxBoundary & inSegment));
    end

    % Smear out the transition between the individular segments
    fadeToVals = imgaussfilt(fadeToVals, settings.transitionLength/2.35);
%
% Case 2: Compute one fadeout-value globally
else
    % only 1 segment
    fadeToVals =  mean(im(idxBoundary));
end

% Mask for the transition between the original image and the fadeout values: smeared-out ellipse

if settings.transitionLength > eps
    transitionMask = imgaussfilt(transitionMask, settings.transitionLength/2.35);
end

% convex combination of transition maks
imFaded = im .* transitionMask + fadeToVals.*(1-transitionMask);
imFaded(isnan(imFaded)) = 1;
imFaded(isinf(imFaded)) = 1;

end

