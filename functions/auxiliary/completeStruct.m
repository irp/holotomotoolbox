function sNew = completeStruct(s, sRef)
% COMPLETESTRUCT compares parameters specified in a structure s to a reference 
% structure sRef and returns a parameter structure sNew in which missing entries
% in s are taken from sRef, i.e. completes the structure s with sRef.
%
%     ``sNew = completeStruct(s, sRef)``
% 
%
% Parameters
% ----------
% s : structure
%     Structure to be completed
% sRef : structure
%     Structure containing the reference values used to complete s
%
%
% Returns 
% ------- 
% sNew : structure
%     The completed structure
%
% Example
% -------
%
% .. code-block:: matlab
%
%     s.a = 1;
%     s.b = 2;
%     sRef.b = 1000;
%     sRef.c = 3; 
%     sNew = completeStruct(s, sRef);
%     disp(sNew);
%
%
    
% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

sNew = s;
fnames =  fieldnames(sRef);
for jj = 1:length(fnames)
    fname = fnames{jj};
    x = getfield(sRef, fname);
    if ~isfield(sNew, fname)
        sNew = setfield(sNew, fname, x);
    else if isstruct(x)    % recursively apply the completion to sub-structs
        sNew = setfield(sNew, fname, completeStruct(getfield(sNew, fname), x));
    end
end

end
