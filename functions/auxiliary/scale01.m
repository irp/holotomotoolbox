function scaledImage = scale01(image)
% SCALE01 scales the given image to values between 0 and 1.
%
%    ``scaledImage = scale01(image)``
%
% Parameters
% ----------
% image : numerical array
%     image to be scaled
%
% Returns 
% ------- 
% scaledImage : numerical array
%     scaled image with intensity between 0 and 1
%
%
% Example
% -------
%
% .. code-block:: matlab
%
%     image = gradient(phantom(512));
% 
%     subplot(1,2,1)
%     showImage(image)
%     title('original image')
% 
%     subplot(1,2,2)
%     showImage(scale01(image))
%     title('scaled image')
%
    
% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

scaledImage = image-min(image(:));
scaledImage = scaledImage/max(scaledImage(:));

end

