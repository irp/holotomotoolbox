function arraySlice = getSlice(array, sliceIdx, dim)
% GETSLICE is a convenience function to get slices of N-dimensional arrays.
%
%    ``arraySlice = getSlice(array, sliceIdx, dim)``
%
%
% Parameters
% ----------
% array : numerical array
%      array to be sliced
% sliceIdx : integer
%     non-negative integer or tuple of such, giving the index/indices of the slice(s)
%     to be extracted
% dim : integer
%     array-dimension along which the slice(s) are to be extracted
%
% Returns
% -------
% arraySlice : numerical array
%    extracted slice(s) of the input array
%
% Example
% -------
%
% .. code-block:: matlab
%
%     % Get slice of a 4-dimensional array along third dimension
%     array = rand([23,32,17,19]);
%     sliceIdx = 11;
%     dim = 3;
%     arraySlice = getSlice(array, sliceIdx, dim);
%     array_slice_ref = array(:,:,sliceIdx,:);
%     assert(norm(arraySlice(:)-array_slice_ref(:))/norm(array_slice_ref(:)) < 1e-12);


% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

ndim = ndims(array);
c1 = repmat({':'}, [dim-1,1]);
c2 = repmat({':'}, [ndim-dim,1]);
arraySlice = array(c1{:}, sliceIdx, c2{:});

end
