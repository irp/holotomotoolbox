function array = gatherIf(array, condition)
% GATHERIF copies a gpuArray back to host under a given condition.
%
%    ``array = gatherIf(array, condition)``
%
% Parameters
% ----------
% array : numerical array
%   array to be copied back to host
% condition  
%   condition under which the array is copied back to host
%
% Returns
% -------
% array : numerical array
%    input array copied back to CPU
%

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

if condition
    array = gather(array);
end

end
