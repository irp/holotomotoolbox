function arrayIsOnGPU = isOnGPU(array)
% ISONGPU checks whether a given object is a gpuArray.
%
%    ``arrayIsOnGPU = isOnGPU(array)``
%
% Parameters
% ----------
% array : numerical array
%   array to be checked whether it is a GPU array
%
% Returns
% -------
% arrayIsOnGPU : boolean
%    true if object is a GPU array
%

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

arrayIsOnGPU = strcmp(class(array), 'gpuArray');

end
