function gpuIsAvailable = checkGPUSupport()
% CHECKGPUSUPPORT checks availability of GPU support.
%
%    ``gpuIsAvailable = checkGPUSupport()``
%
% Returns
% -------
% gpuIsAvailable : boolean
%    true if GPU is available, false otherwise
%

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

try
    gpuDevice();
    gpuIsAvailable = true;
catch ME
    gpuIsAvailable = false;
end

end
