function XiSq = fftfreqNormSq(N, dx)
% FFTFREQNORMSQ creates squared norm of the FFT-frequency vector.
%
%    ``XiSq = fftfreqNormSq(N, dx)``
%
%
% Parameters
% ----------
% N : ndim-tuple
%     tuple of grid dimensions
% dx : ndim-tuple, optional
%     tuple of spacings in the different grid dimensions; default = ones(size(N))
%
% Returns
% -------
% XiSq : numerical array
%    array of size N that contains the squared norm of the FFT-frequency vector
%
% Example
% -------
%
% .. code-block:: matlab
%
%     % Squared norm of FFT-frequencies on 2D-grid of size [256,384];
%     XiSq = fftfreqNormSq([256,384]);
%     showImage(fftshift(XiSq))

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.


% Default: Standard grid spacing assigned by function fftfreq.
if nargin < 2
    dx = ones(size(N));
end

xi = cell(numel(N),1);
[xi{:}] = fftfreq(N, dx);
XiSq = xi{1}.^2;
for dim = 2:numel(xi)
    XiSq = XiSq + xi{dim}.^2;
end

end
