function [] = initOctave()
% INITOCTAVE load additional packages required to run the toolbox in OCTAVE.
%
%    ``[] = initOctave()``
%
% When called from MATLAB, this function raises an error.
%
%
% Parameters
% ----------
%
% Returns 
% ------- 
%
    
% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

if isOctave
    % Load additional (dummy-)functions that are needed but not natively implemented in OCTAVE
    addpath(fullfile(fileparts(mfilename('fullpath')), '..', '..', 'octave'));

    % Load image-processing toolbox
    pkg load image;
else
    error(['This function performs the required initialization to run the toolbox in OCTAVE ', ...
           'and is not meant to be called from within MATLAB.']);
end

end
