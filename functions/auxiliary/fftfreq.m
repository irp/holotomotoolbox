function varargout = fftfreq(N, dx)
% FFTFREQ creates a grid of Fourier-frequencies corresponding to the sampling points
% of an n-dimensional FFT of given size.
%
%     ``varargout = fftfreq(N, dx)``
% 
%
% Parameters
% ----------
% N : Tuple of non-negative integers
%     Size of the Fourier-grid.
% dx : float or tuple of floats, optional
%     Underyling spacing in real-space, possibly different along each grid-dimension. 
%     Default = ones(size(N)).
%
%
% Returns 
% ------- 
% varargout : tuple of numerical arrays
%     numel(N) vectors of length N(1), N(2),... that span the Fourier-grid 
%
% See also
% --------
% functions.auxiliary.centeredGrid
%
% Example
% -------
%
% .. code-block:: matlab
%
%     [xi1,xi2,xi3] = fftfreq([7,5,12]);
%     disp(xi1);
%     disp(xi2);
%     disp(xi3);
%
%
% See also CENTEREDGRID
    
% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

% Default: unit spacing
if nargin < 2
    dx = 1;
end
dx = dx(:).' .* ones([1,numel(N)]);
ndim = numel(N);

% Grid in Fourier space
xi = cell(1, ndim);
for jj = 1:ndim
    xi{jj} = ifftshift( ( -floor(0.5*N(jj)) : floor(0.5*(N(jj)-1)) ).' * ( 2*pi / (N(jj)*dx(jj)) ) );
end

if ndim > 1
    for jj = 1:ndim
        xi{jj} = reshape( xi{jj}, [ones([1,jj-1]), N(jj), ones([1,ndim-jj])] );
    end
end


% Unpack cell-array
varargout = xi;

end
