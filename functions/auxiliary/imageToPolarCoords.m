function [imPolar, R, Phi] = imageToPolarCoords(im, settings)
% IMAGETOPOLARCOORDS transforms an image into polar coordinates.
%
%     ``[imPolar, R, Phi] = imageToPolarCoords(im, settings)``
% 
%
% Parameters
% ----------
% im : numerical 2d-array
%     image to be transformed to polar coordinates
% settings : structure, optional
%     contains additional settings, see *Other Parameters*
%
% Other Parameters 
% ---------------- 
% numAngles : Default = []
%     Number of sampling points along the azimuthal dimension of the transformed image.
%     If numAngles == [], a suitable number is determined based on the size of the input image.
% numRadii : Default = []
%     Number of sampling points along the radial dimension of the transformed image.
%     If numAngles == [], a suitable number is determined based on the size of the input image.
% interpMethod : interpMethod = 'linear'
%     The interpolation method used in the image-transformation. The admissible choices are
%     the same as for the MATLAB-functions interp1, interp2, etc.
%
% Returns 
% ------- 
% imPolar : numerical 2d-array
%     the image in polar coordinates  
% R : numerical 2d-array
%     grid of radial coordinates corresponding to the pixels in imPolar
% Phi : numerical 2d-array
%     grid of azimuthal coordinates corresponding to the pixels in imPolar
%
% Example
% -------
%
% .. code-block:: matlab
%
%     N = [2048,2048];
%     im = padarray(ones(N/2), N/4);
%     settings.diameter = N(1)/4;
%     im = im + ball(N, settings);
%
%     figure('name', 'Original image'); imagesc(im); colorbar; xlabel('x'); ylabel('y');
%     [imPolar, R, Phi] = imageToPolarCoords(im);
%     figure('name', 'Image on polar coordinates'); pcolor(R, Phi, imPolar); shading 'flat';
%     colorbar; xlabel('radius'); ylabel('angle'); xlim([0,max(R(:))]); ylim([0,2*pi]);
%
%
    
% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

% Default settings
defaults.numAngles = [];
defaults.numRadii = [];
defaults.interpMethod = 'linear';

if (nargin == 0)
    imPolar = defaults;
    return
end

if (nargin < 2)
    settings = struct;
end
settings = completeStruct(settings, defaults);

% Image size
N = size(im);

% If radial and/or angular resolutions are not assigned, choose automatically
if isempty(settings.numRadii)
    numRadii = 0.5*min(N);
end
if isempty(settings.numAngles)
    numAngles = 2*numRadii;
end

% Construct polar-equispaced coordinate grid
r_max = 0.5*(min(N)-1);
[R,Phi] = ndgrid((0:r_max./(numRadii-1):r_max), (0:numAngles-1)*(2*pi/numAngles));
X_polar = R.*cos(Phi);
Y_polar = R.*sin(Phi);

% Interpolate onto polar coordinate grid (midpoint of original Cartesian grid is at (N+1)/2)
imPolar = interp2(im, X_polar + 0.5*(N(2)+1), Y_polar + 0.5*(N(1)+1), settings.interpMethod);

end
