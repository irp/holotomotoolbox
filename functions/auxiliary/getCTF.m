function [CTF,Q] = getCTF(fresnelnumber,numPoints)
% GETCTF calculates the phase contrast transfer function for the given Fresnel number(s).
%
%    ``[CTF,Q] = getCTF(fresnelnumber,numPoints)``
%
% Parameters
% ----------
% fresnelnumber : number or vector
%      pixel size-based Fresnel numbers(s)
% numPoints : integer
%      size of the output vector; if no value is given, the output vector has size
%      512
%      
% Returns
% -------
% CTF : vector
%     contains the phase contrast transfer function for the given Fresnel numbers
% Q : vector
%     contains the image frequencies (scaling from 0 to 0.5)
%
% See also
% --------
% functions.phaseRetrieval.holographic.phaserec_ctf
%
% Example
% -------
%
% .. code-block:: matlab
%
%     [PCTF,Q] = getCTF(0.1,512);
%     plot(Q,PCTF)
%
% See also PHASEREC_CTF

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

if nargin < 2
    numPoints = 512;
end
if iscell(fresnelnumber)
    fresnelnumber = cell2mat(fresnelnumber);
end

Q = linspace(0,0.5,numPoints);

CTF = zeros(size(Q));
for indFresnel = 1:numel(fresnelnumber)
    CTF = CTF + sin(pi/fresnelnumber(indFresnel).*Q.^2);
end

if nargout == 0
    plot(Q,CTF)
end
end

