function N_FFTfriendly = makeSizeFFTfriendly(N)
% MAKESIZEFFTFRIENDLY provides array size that can be efficiently transformed using
% ffts.
%
%    ``N_FFTfriendly = makeSizeFFTfriendly(N)``
%
% Does so by computing the smallest sizes N_FFTfriendly(jj) >= N(jj) such that
% the prime factorisation of N_FFTfriendly(jj) contains no number larger than 7.
%
%
% Parameters
% ----------
% N : ndim-tuple
%      array size to be made fft friendly
%
% Returns
% -------
% N_FFTfriendly : ndim-tuple
%    (padded) fft-friendly array size
%
% Example
% -------
%
% .. code-block:: matlab
%
%     N = [2053, 1031]; % these sizes are primes
%     N_FFTfriendly = makeSizeFFTfriendly(N);
%     disp(N_FFTfriendly);
%
%     im = rand(N);
%     tic; fft2(im); dt1 = toc;
%     fprintf('Computation time for 2D-FFT with original size was %d seconds.\n', dt1);
%
%     imFFTFriendly = rand(N_FFTfriendly);
%     tic; fft2(imFFTFriendly); dt2 = toc;
%     fprintf('Computation time for 2D-FFT with FFT-friendly size was %d seconds.\n', dt2);
%     

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

N_FFTfriendly = N;
for jj = 1:numel(N)
    n = N(jj);
    while(max(factor(n)) > 7)
        n = n + 1;
    end
    N_FFTfriendly(jj) = n;
end

end
