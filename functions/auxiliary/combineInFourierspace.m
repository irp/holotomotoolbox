function combinedImage = combineInFourierspace(highFrequencyImage,lowFrequencyImage,settings)
% COMBINEINFOURIERSPACE combines two images in Fourier space.
%
%    ``combinedImage = combineInFourierspace(highFrequencyImage,lowFrequencyImage,settings)``
%
% Combines two images in fourierspace by taken the high frequencies from the first
% input and the low frequencies from the second input. The transition between the two
% images is carried out via an error function with a specified cutoff frequency and
% standard deviation.
%
% Parameters
% ----------
% highFrequencyImage : numerical array
%     image from which the high frequencies are taken
% lowFrequencyImage : numerical array
%      image from which the low frequencies are taken
% settings : structure, optional
%     contains additional settings, see *Other Parameters*
%
% Other Parameters
% ----------------
% combineCutoff : Default = 0.1
%     Frequency cutoff for the low-frequency replacement.
% combineSigma : Default = 0.01
%     Standard deviation of the error function which defines the transition between
%     the reconstructed image and the given lowfrequencyimage
%
% Returns
% -------
% combinedImage : numerical array
%     combined image
%
% See also
% --------
% functions.phaseRetrieval.holographic.phaserec_holotie
%
% Example
% -------
%
% .. code-block:: matlab
%
%     image = phantom(512);
%     lowFrequencyImage = imgaussfilt(image,10);
%     highFrequencyImage = image - imgaussfilt(image,100);
%
%     settings=combineInFourierspace;
%     settings.combineCutoff = 0.2;
%     settings.combineSigma = 0.2;
%
%     combinedImage = combineInFourierspace(highFrequencyImage,...
%         lowFrequencyImage,settings);
%     showImage(combinedImage)
%
% See also PHASEREC_HOLOTIE

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

% default settings
defaults.combineCutoff = 0.1;
defaults.combineSigma = 0.01;
if (nargin == 0)
    combinedImage = defaults;
    return
end
if (nargin < 3)
    settings = struct;
end
settings = completeStruct(settings, defaults);


if (size(highFrequencyImage)~=size(lowFrequencyImage))
    error('Size of the images does not match');
end

N = size(highFrequencyImage);
[kyField, kxField] = fftfreq(N,[2*pi 2*pi]);
freqNorm = sqrt(kxField.^2 + kyField.^2);

weight = erfc((freqNorm - settings.combineCutoff)./settings.combineSigma);
weight = weight / max(weight(:));

% Combine the images
fftHighFrec = fft2(highFrequencyImage);
fftLowFrec = fft2(lowFrequencyImage);

% result
combinedImage = ifft2((1-weight).*fftHighFrec+weight.*fftLowFrec);

end

