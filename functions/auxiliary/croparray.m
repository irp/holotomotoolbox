function arrayCropped = croparray(array, cropPre, cropPost)
% CROPARRAY crops a given numerical array by a specified amount of rows/columns
% at the beginning and/or end of the array. 
%
%     ``arrayCropped = croparray(array, cropPre, cropPost)``
% 
%
% Parameters
% ----------
% array : numerical array
%     Array to be cropped
% cropPre : tuple of non-negative integers
%     Amount rows/columns/etc to crop at the beginning of the array along the different dimensions
% cropPost : tuple of non-negative integers, optional
%     Amount rows/columns/etc to crop at the end of the array along the different dimensions.
%     If not assigned, cropPost = cropPre (--> symmetric cropping) is assumed.
%
%
% Returns 
% ------- 
% arrayCropped : numerical array
%     The cropped array. size(arrayCropped) = size(array)-cropPre-cropPost.
%
% Example
% -------
%
% .. code-block:: matlab
%
%     array = rand([23,32,17,19]);
%     padPre = [7,3,14,10];
%     padPost = [13,8,7,5];
%     arrayPadded = padarray(padarray(array, padPre, 0, 'pre'), padPost, 0, 'post');
%     arrayPaddedCropped = croparray(arrayPadded, padPre, padPost);
%     norm(array(:)-arrayPaddedCropped(:))/norm(array(:))
%
%
    
% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

% Default: symmetric cropping
if nargin < 3
    cropPost = cropPre;
end

N = size(array);
ndim = numel(N);

% If cropPre and/or cropPost have less entries than the number
% of dimensions of array, fill with zeros
cropPre = [cropPre(:).', zeros([1,ndim-numel(cropPre)])];
cropPost = [cropPost(:).', zeros([1,ndim-numel(cropPost)])];

% Crop array dimension by dimension
arrayCropped = array;
for dim = 1:ndim
    if cropPre(dim) > 0 || cropPost(dim) > 0
        c1 = repmat({':'}, [dim-1,1]);
        c2 = repmat({':'}, [ndim-dim,1]);
        arrayCropped = arrayCropped(c1{:}, cropPre(dim)+1:N(dim)-cropPost(dim), c2{:});
    end
end

end
