function varargout = centeredGrid(N, dx, computeMeshgrid)
% CENTEREDGRID creates a centered coordinate-grid of given size.
%
%     ``varargout = centeredGrid(N, dx, computeMeshgrid)``
% 
%
% Parameters
% ----------
% N : Tuple of non-negative integers
%     Size of the grid to be created.
% dx : float or tuple of floats, optional
%     Grid spacing, possibly different along each grid-dimension. Default = ones(size(N)).
% computeMeshgrid : tuple of non-negative integers, optional
%     Return grid as meshgrid (case: true) or merely as coordinate vectors
%     for the different dimensions? Default = false.
%
%
% Returns 
% ------- 
% varargout : tuple of numerical arrays
%     numel(N) vectors of length N(1), N(2), ... that span the grid
%     OR, if computeMeshgrid == true, the meshgrid corresponding to these vectors
%
% See also
% --------
% functions.auxiliary.fftfreq
%
% Example
% -------
%
% .. code-block:: matlab
%
%     % Example 1: 3D-grid-vectors for dimensions 17,15,12 with spacing 0.4
%     [x1,x2,x3] = centeredGrid([17,15,12], 0.4);
%     disp(x1);
%     disp(x2);
%     disp(x3);
%     
%     % Example 2: 2D-meshgrid of size [13,8] with unit-spacing
%     [x1,x2] = centeredGrid([13,8], 1, true);
%     disp(x1);
%     disp(x2);
%
%
% See also FFTFREQ
    
% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

% Default: unit spacing
if nargin < 2
    dx = 1;
end
dx = dx(:).' .* ones([1,numel(N)]);
ndim = numel(N);

% Grid in Fourier space
for jj = 1:ndim
    x{jj} = ( -(0.5*(N(jj)-1)) : (0.5*(N(jj)-1)) ) * dx(jj);
end

% Optionally assemble ndgrid (occupies more memory!)
if nargin == 3 && computeMeshgrid
    [x{:}] = ndgrid(x{:});
elseif ndim > 1
    for jj = 1:ndim
        x{jj} = reshape( x{jj}, [ones([1,jj-1]), N(jj), ones([1,ndim-jj])] );
    end
end

% Unpack cell-array
varargout = x;

end
