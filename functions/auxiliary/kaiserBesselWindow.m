function window = kaiserBesselWindow(N, beta)
% KAISERBESSELWINDOW computes a Kaiser-Bessel window for an array of given size.
%
%    ``window = kaiserBesselWindow(N, beta)``
%
% By multiplying an array with the window prior to computing the FFT of it, periodicity
% effects of the discrete Fourier transform are suppressed. Accordingly, the Kaiser
% Bessel window improves the approximation of the continuous Fourier transform by
% the discrete one.
%
% Parameters
% ----------
% N : tuple
%     size of Kaiser-Bessel window
% beta : number, optional
%     non-negative number that determines the shape of the window; default = 8
%
% Returns
% -------
% window : numerical array
%     computed Kaiser-Bessel window
% 
% Example
% -------
%
% .. code-block:: matlab
%
%     beta = 8;
%     window_size = [117,288];
%     showImage(kaiserBesselWindow(window_size, beta))

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

if nargin < 2
    beta = 8;
end

x_norm_sq = fftshift(fftfreqNormSq(N, pi));
window = (1/besseli(0,beta)) * real(besseli(0, beta*sqrt(1 - x_norm_sq)));
window(x_norm_sq >= 1) = 0;

end
