function res = FresnelKernelB1(x, tNF)

erfi_factor = (0.5 + 0.5*1i) * sqrt(tNF);
res =   (0.5*1i) .* ((2*x) .* erfi(erfi_factor .* x) - (x-1) .* erfi(erfi_factor .* (x-1)) - (x+1) .* erfi(erfi_factor .* (x+1)) ) ...
      - (0.5*(1+1i) ./ sqrt(pi*tNF)) .* exp((1i*tNF/2) .* x.^2) .* (2 - (2*exp(1i*tNF/2)) .* cos(tNF * x));

end
