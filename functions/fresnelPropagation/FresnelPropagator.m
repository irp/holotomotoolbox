%%
classdef FresnelPropagator
% FRESNELPROPAGATOR is class implementation Fresnel-propagation and back-propagation of images
%
%    ``prop = FresnelPropagator(sizeIn, fresnelnumbers, settings)``
%
% Note that this propagator does not only work for 2D-images, but implements Fresnel-propagation
% of arrrays in arbitrary dimensions.
% Member functions of this class are described below.
% 
%
% Parameters
% ----------
% sizeIn : vector
%     Size of the images to be Fresnel-propagated
% fresnelnumbers : numerical array
%     pixel size-based Fresnel number(s): the columns correspond to possibly multiple propagation
%     distances. If the array has more than one row, the rows are interpreted as possibly distinct
%     Fresnel numbers along the the different dimensions of the input images (astigmatism).
% settings : structure, optional
%     contains additional settings, see *Other Parameters*
%
%
% Other Parameters
% ----------------
% sizePad : Default = []
%     Size to which the images are padded before Fresnel-propagating to avoid periodicity-artifacts.
%     All entries of sizePad must be >= those in sizeIn. If empty, sizePad = sizeIn is assigned.
% sizeOut : Default = []
%     Output size of the propagated images. If larger than sizePad, the image is cropped after
%     Fresnel-propagation. All entries of sizeOut must be <= those in sizePad. 
%     If empty, sizeOut = sizePad is assigned.
% method : Default = 'fourier'
%     Determines the method (= underlying discretization approach) used to Fresnel-propagate images.
%     Admissible choices: {'fourier', 'chirp', 'chirpLimited', 'pixel2pixel'} (details are given 
%     in the documentation of fresnelPropagationKernel)
% padVal : Default = 0
%     Number or string that determines how the images are optionally padded. Same admissible
%     choices as for the MATLAB-function padarray.
% singlePrecision : Default = false
%     If true, Fresnel-propagation is computed in single-precision arithmetics.
%     If false (default), computations are performed in double precision.
% gpuFlag : Default = false
%     If true, the Fresnel-propagation by the constructed object is performed on GPU.
%     
%
% Returns
% -------
% Prop : class-object
%     The initialized Fresnel-Propagator object 
%
%
% See also
% --------
% functions.fresnelPropagation.fresnelPropagate
% functions.fresnelPropagation.fresnelPropagationKernel
%
%
% Example
% -------
%
% .. code-block:: matlab
%
%     fresnelNumbers = 2e-4;
%     im = getPhantom('dicty');
%     figure('name', 'image'); imagesc(im); colorbar;
%     
%     prop = FresnelPropagator(size(im), fresnelNumbers);
%     imProp = prop.propagate(im);
%     figure('name', 'Propagated image (absolute value)'); showImage(abs(imProp));
%     
%     imPropBackProp = prop.backPropagate(imProp);
%     figure('name', 'Propagated and back-propagated image'); showImage(real(imPropBackProp));
%     
% 
% See also FRESNELPROPAGATE, FRESNELPROPAGATIONKERNEL

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

    
properties (SetAccess = 'private')
    
    settings;
    sizeIn;
    fresnelNumbers
    numDistances;
    numDims;
    padPre;
    padPost;
    cropPre;
    cropPost;
    propKernel;
    
end % end properties


methods

    %% Constructor
    function obj = FresnelPropagator(sizeIn, fresnelNumbers, settings)

        if nargin < 3
            settings = struct;
        end
        obj.settings = completeStruct(settings, FresnelPropagator.defaultSettings());
        
        obj.sizeIn = sizeIn;
        obj.fresnelNumbers = fresnelNumbers;
        obj.numDims = numel(obj.sizeIn);
        obj.numDistances = size(fresnelNumbers,2);
        

        % Check consistency of input-parameters
        %
        % Input-, output- and padded array sizes
        if isempty(obj.settings.sizePad)
            obj.settings.sizePad = sizeIn;
        elseif numel(obj.settings.sizePad) ~= numel(sizeIn) || any(obj.settings.sizePad < sizeIn)
            error('settings.sizePad must be of the same length as sizeIn and settings.sizePad >= sizeIn.');
        end
        if isempty(obj.settings.sizeOut)
            obj.settings.sizeOut = obj.settings.sizePad;
        elseif numel(obj.settings.sizeOut) ~= numel(sizeIn) || any(obj.settings.sizeOut > obj.settings.sizePad)
            error('settings.sizeOut must be of the same length as sizeIn and settings.sizeOut <= settings.sizePad.');
        end
        %
        % Check sanity of Fresnel numbers
        if size(obj.fresnelNumbers,1) > 1 && size(obj.fresnelNumbers,1) ~= numel(sizeIn)
            error(['size(fresnelNumbers,1) must be either 1 (no astigmatism) or equal', ...
                      ' to the number of dimensions numel(sizeIn) (possibly astigmatic setting).']);
        end
        %
        % Check whether GPU-computations are possible
        if obj.settings.gpuFlag && ~checkGPUSupport();
            warning(['Unable to initialize GPU. Fresnel-propagation will be performed on CPU instead. ', ...
                'Set settings.gpuFlag = false to suppress this warning.']);
            obj.settings.gpuFlag = false;
        end


        % Ensure that Fresnel-propagation will be performed in the correct precision (single/double)
        % by casting Fresnel numbers to single/double
        if obj.settings.singlePrecision
            obj.fresnelNumbers = single(obj.fresnelNumbers);
        else
            obj.fresnelNumbers = double(obj.fresnelNumbers);
        end
        
        
        % Determine padding- and cropping-amounts (and whether to pad/crop at all)
        obj.padPre = ceil((obj.settings.sizePad-obj.sizeIn)/2);
        obj.padPost = floor((obj.settings.sizePad-obj.sizeIn)/2);
        obj.cropPre = ceil((obj.settings.sizePad-obj.settings.sizeOut)/2);
        obj.cropPost = floor((obj.settings.sizePad-obj.settings.sizeOut)/2);


        % Construct propagation kernel to be applied in Fourier-space
        for idxFresnel = 1:obj.numDistances
            obj.propKernel{idxFresnel} = fresnelPropagationKernel(obj.settings.sizePad, obj.fresnelNumbers(:,idxFresnel), obj.settings);
        end
        obj.propKernel = cat(obj.numDims+1, obj.propKernel{:});
        
    end %end constructor
    
    


    function imProp = propagate(obj, im)
    % PROPAGATE Fresnel-propagates a given image according to the internal parameters 
    % (Fresnel-numbers, padding, etc) of the Fresnel-propagator object.
    % 
    %    ``imProp = propagate(im)``

        % Transfer to GPU if required
        imProp = gpuArrayIf(im, obj.settings.gpuFlag && ~isOnGPU(im));
        
        % Optional padding
        if any(obj.settings.sizePad ~= obj.sizeIn)
            imProp = padarray(padarray(imProp, obj.padPre, obj.settings.padVal, 'pre'), ...
                              obj.padPost, obj.settings.padVal, 'post');
        end
        
        % Propagation step
        imProp = obj.iFT(obj.propKernel .* fftn(imProp));

        
        % Optional cropping
        if any(obj.settings.sizePad ~= obj.settings.sizeOut)
            imProp = croparray(imProp, obj.cropPre, obj.cropPost);
        end
        
        % Transfer back from GPU to host if required
        imProp = gatherIf(imProp, obj.settings.gpuFlag && ~isOnGPU(im));
    end
    
    
    function imBack = backPropagate(obj, im)
    % BACKPROPAGATE Fresnel-back-propagates a given image, i.e. applies the inverse
    % operation to propagate. In the case of Fresnel-propagation to more than one
    % plane, the result is the sum of the back-propagated images from each of the planes. 
    % 
    %    ``imBack = backPropagate(im)``

        % Transfer to GPU if required
        imBack = gpuArrayIf(im, obj.settings.gpuFlag && ~isOnGPU(im));
        
        
        % Padding from size obj.settings.sizeOut to obj.settings.sizePad
        if any(obj.settings.sizePad ~= obj.settings.sizeOut)
            imBack = padarray(padarray(imBack, obj.cropPre, obj.settings.padVal, 'pre'), ...
                              obj.cropPost, obj.settings.padVal, 'post');
        end 

        % Back-propagation step (plus summation in the case of multiple Fresnel-numbers)
        imBack = conj(obj.propKernel) .* obj.FT(imBack);
        if obj.numDistances > 1
            imBack = sum(imBack, obj.numDims+1);
        end
        imBack = ifftn(imBack);
        
        % Cropping from size obj.sizePad to obj.sizeIn
        if any(obj.settings.sizePad ~= obj.sizeIn)
            imBack = croparray(imBack, obj.padPre, obj.padPost);
        end
        
        % Transfer back from GPU to host if required
        imBack = gatherIf(imBack, obj.settings.gpuFlag && ~isOnGPU(im));
    end

    
end % end methods


methods (Static)
    
    function defaults = defaultSettings()
    % DEFAULTSETTINGS returns the default-values for the optional settings-struct
    % of the Fresnel-propagator
    % 
    %    ``defaults = FresnelPropagator.defaultSettings()``

        defaults.method = 'fourier';
        defaults.sizePad = [];
        defaults.sizeOut = [];
        defaults.padVal = 0;
        defaults.fresnely = [];
        defaults.fresnelz = [];
        defaults.singlePrecision = false;
        defaults.gpuFlag = false;
    end
    
end % end static methods


methods (Access = 'private')

    function im = FT(obj, im)
        if obj.numDistances == 1
            im = fftn(im);
        else
            % Ugly workaround to perform fftn only along the first obj.numDims dimensions
            for jj = 1:size(im,obj.numDims+1)
                fftDims = repmat({':'}, [obj.numDims,1]);
                im(fftDims{:}, jj) = fftn(im(fftDims{:}, jj));
            end
        end
    end

    function im = iFT(obj, im)
        if obj.numDistances == 1
            im = ifftn(im);
        else
            % Ugly workaround to perform ifftn only along the first obj.numDims dimensions
            for jj = 1:size(im,obj.numDims+1)
                fftDims = repmat({':'}, [obj.numDims,1]);
                im(fftDims{:}, jj) = ifftn(im(fftDims{:}, jj));
            end
        end
    end

end % end private methods
    

end
