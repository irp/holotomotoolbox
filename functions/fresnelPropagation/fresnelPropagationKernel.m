function kernel = fresnelPropagationKernel(N, fresnelNumbers, settings)
% FRESNELPROPAGATIONKERNEL generates a Fourier space kernel for Fresnel propagation.
%
%    ``kernel = fresnelPropagationKernel(N, fresnelNumbers, method)``
%
% Generates a Fourier multiplier for Fresnel propagation to the given Fresnel number(s),
% i.e. an array kernel such that the Fresnel-propagated version psiProp of a wave-field
% psi can be computed as psiProp = ifftn(kernel .* fftn(psi)). The function supports different
% discretization variants of the Fresnel-propagator, see parameter documentation below.
% 
%
%
% Parameters
% ----------
% N : vector
%     Size of the kernel to be constructed (size of the images to be Fresnel-propagated)
% fresnelNumbers : vector
%     pixel size-based Fresnel number(s) along the different image-dimensions. If only one
%     Fresnel number is assigned, it is assumed to be the same along all dimensions
% settings : structure, optional
%     contains additional settings, see *Other Parameters*
%
% Other Parameters
% ----------------
% method : Default = 'fourier'
%     Determines the type of the constructed propagation-kernel:
%
%     'fourier'
%          The standard variant where the unitary Fourier-multiplier, exp(-1i*xi^2/(4*pi*F))
%          (F: Fresnel-number), of the continuous Fresnel-propagator is discretized.
%          Also called the "transfer function approach", see :cite:`Voelz_AO_2009`.
%     'chirp' 
%          Here, the real space convolution-kernel of the continuous Fresnel-propagator,
%          ~exp(1i*pi*F*x^2) (up to a multiplicative constant), is discretized.
%          Also called the "impulse response approach", see :cite:`Voelz_AO_2009`.
%     'chirpLimited' 
%          Same as chirp, but with an additional modification that limits
%          the scattering angle of image-features to physically correct range
%          defined by Abbe's criterion. In particular, this suppresses the artifacts
%          that typically arise for method 'chirp' at larger Fresnel-numbers.
%          This variant has been proposed in Aike Ruhlandt's PhD-thesis :cite:`Ruhlandt_Diss_2017`.
%     'pixel2pixel' 
%          Implements analytically exact Fresnel-propagation, if both the image
%          and its propagated version are thought of as piecewise constant functions
%          on square pixels of equal size. In practice, this approach leads to similar
%          results as 'chirpLimited'.
% gpuFlag : Default = false
%     If true, the propagation-kernels are constructed directly as gpuArrays. 
%     
%     
%
% Returns
% -------
% kernel : numerical array
%     The constructed propagation-kernel
%
% See also
% --------
% functions.fresnelPropagation.fresnelPropagator
%
%
% Example
% -------
%
% .. code-block:: matlab
%
%     fresnelNumbers = [8e-4, 7e-4];
%     im = getPhantom('dicty');
%     N = size(im);
%     figure('name', 'image'); imagesc(im); colorbar;
%     
%     kernels = {'fourier', 'chirp', 'chirpLimited', 'pixel2pixel'};
%     
%     for jj = 1:numel(kernels)
%         settings.method = kernels{jj};
%         kernel = fresnelPropagationKernel(N, fresnelNumbers, settings);
%         imProp = ifft2(kernel .* fft2(im));
%         figure('name', ['Propagated image using ', kernels{jj} '-kernel (absolute value)']);
%         showImage(abs(imProp)); colorbar;
%     end
%     
% 
% See also FRESNELPROPAGATOR

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.


% Complete settings with default parameters
defaults.method = 'fourier';
defaults.gpuFlag = false;

if nargin < 3
settings = struct;
end
settings = completeStruct(settings, defaults);


% If only one Fresnel-number is assigned, it is assumed to be the same
% along all dimensions
fresnelNumbers = fresnelNumbers(:) .* ones([numel(N),1]);

switch settings.method
    case 'fourier'
        kernel = fresnelPropagationKernel_fourier(N, fresnelNumbers, settings.gpuFlag);
    case 'chirp'
        kernel = fresnelPropagationKernel_chirp(N, fresnelNumbers, settings.gpuFlag);
    case 'chirpLimited'
        kernel = fresnelPropagationKernel_chirpLimited(N, fresnelNumbers, settings.gpuFlag);
    case 'pixel2pixel'
        kernel = fresnelPropagationKernel_pixel2pixel(N, fresnelNumbers, settings.gpuFlag);
    otherwise
        error(['Invalid value of the argument method. Admissible choices: ', ...
               '''fourier'', ''chirp'', ''chirpLimited'' and ''pixel2pixel''.'  ]);
end % switch

end % function




% method: fourier
% This is what people also call the transfer function approach
function kernel = fresnelPropagationKernel_fourier(N, fresnelNumbers, gpuFlag)

ndim = numel(N);
xi = cell([ndim,1]);
[xi{:}] = fftfreq(N, 1);
kernel = 1;
for dim = 1:ndim
    xi{dim} = gpuArrayIf(xi{dim}, gpuFlag);
    kernel = kernel .* exp((-1i/(4*pi*fresnelNumbers(dim))) * xi{dim}.^2);
end

end



% method: chirp
function kernel = fresnelPropagationKernel_chirp(N, fresnelNumbers, gpuFlag)

ndim = numel(N);
x = cell([ndim,1]);
[x{:}] = fftfreq(N, 2*pi./N);    % fft-shifted grid with spacing 1
kernel = prod(sqrt(abs(fresnelNumbers)) .* exp((-1i*pi/4) .* sign(fresnelNumbers)));
for dim = 1:ndim
    x{dim} = gpuArrayIf(x{dim}, gpuFlag);
    kernel = kernel .* fft(exp((1i*pi*fresnelNumbers(dim)) * x{dim}.^2), [], dim);
end

end



% method: chirpLimited
function kernel = fresnelPropagationKernel_chirpLimited(N, fresnelNumbers, gpuFlag)

ndim = numel(N);
x = cell([ndim,1]);
[x{:}] = fftfreq(N, 2*pi./N);    % fft-shifted grid with spacing 1
kernel = prod(sqrt(abs(fresnelNumbers)) .* exp((-1i*pi/4) .* sign(fresnelNumbers)));
for dim = 1:ndim
    x{dim} = gpuArrayIf(x{dim}, gpuFlag);
    kernel = kernel .* exp((1i*pi*fresnelNumbers(dim)) *  x{dim}.^2);
end

% Multiply by obliquity factor as proposed in Aike Ruhlandts's PhD-thesis
thetaDivThetaMax = 0;
for dim = 1:ndim
    thetaDivThetaMax = thetaDivThetaMax + (2*fresnelNumbers(dim)*x{dim}).^2;
end
obliquityFactor = cos((pi/2) * min(thetaDivThetaMax, 1)); clear 'thetaDivThetaMax';
kernel = kernel .* obliquityFactor; clear 'obliquityFactor';

kernel = fftn(kernel);

end



% method: pixel2pixel
function kernel = fresnelPropagationKernel_pixel2pixel(N, fresnelNumbers, gpuFlag)

ndim = numel(N);
x = cell([ndim,1]);
[x{:}] = fftfreq(N, 2*pi./N);    % fft-shifted grid with spacing 1
kernel = 1;
for dim = 1:ndim
    x{dim} = gpuArrayIf(x{dim}, gpuFlag);
    kernel = kernel .* fft(FresnelKernelB1(x{dim}, 2*pi*fresnelNumbers(dim)), [], dim);
end

end
