function imProp = fresnelPropagate(im, fresnelNumbers, settings)
% FRESNELPROPAGATE applies Fresnel-propagation to a given image (1D, 2D or 3D).
%
%    ``imProp = fresnelPropagate(im, fresnelNumbers, settings)``
%
% This function is a convenience-function, which - by default - automatically chooses
% a suitable propagation-method and determines the required padding to minimize
% propagation artifacts. Basically, it is a wrapper for the class FresnelPropagator,
% which is able to determine some settings automatically. 
% Note: When a large number of images have to be Fresnel-propagated, it is preferable
% (performance-wise) to first construct a FresnelPropagator-object prop and then to apply
% propagation by using the method prop.propagate.
%
%
% Parameters
% ----------
% im : numerical array
%     image to be propagated
% fresnelNumbers : numerical array 
%     pixel size-based Fresnel number(s): the columns correspond to possibly multiple propagation
%     distances. If the array has more than one row, the rows are interpreted as possibly distinct
%     Fresnel numbers along the the different dimensions of the image im (astigmatism).
% settings : struct
%     contains additional settings, see *Other Parameters*
%
% Other Parameters
% ----------------
% method : Default = 'auto'
%     The propagation-method to be used. Same admissible choices as in FresnelPropagator and
%     fresnelPropagationKernel. Additionally, 'auto' can be assigned, in which case the method
%     'fourier' is chosen for large Fresnel-numbers (max(size(im) .* fresnelNumbers(:)) > 1) and
%     'chirpLimited' for small Fresnel-numbers (max(size(im) .* fresnelNumbers(:)) <= 1).
% sizePad : Default = 'auto'
%     The padded size of the image. If empty, no padding is applied. If 'auto', the padded size
%     is chosen automatically, such that no propagation artifacts occur.
% sizeOut : Default = []
%     The size of the propagated output-image. If empty, sizeOut = size(im) is used.
% padVal : Default = 'replicate'
%     Number or string that determines how the image is padded. Same admissible choices as for the
%     MATLAB-function padarray.
% gpuFlag : Default = false
%     If true, Fresnel-propagation is performed on the GPU, otherwise on CPU.
% verbose : Default = false
%     If true, the chosen method and/or padding is printed as a status message in the 'auto'-mode. 
%     Otherwise, no status messages are printed.
%     
%
% Returns
% -------
% imProp : numerical array
%     The Fresnel-propagated image
%
%
% See also
% --------
% functions.fresnelPropagation.FresnelPropagator
%
%
% Example
% -------
%
% .. code-block:: matlab
%
%     fresnelNumber = 1e-3;
%     im = getPhantom('dicty');
%     im = im(end/2-337:end/2+519,end/2-413:end/2+385);
%     figure('name', 'image'); showImage(im);
%     
%     imProp = fresnelPropagate(im, fresnelNumber);
%     figure('name', 'Propagated image (absolute value)'); showImage(abs(imProp));
%     
% 
% See also FRESNELPROPAGATOR

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

% Complete settings with default parameters
defaults.method = 'auto';
defaults.sizePad = 'auto';
defaults.sizeOut = [];
defaults.padVal = 'replicate';
defaults.gpuFlag = false;
defaults.verbose = false;


if nargin < 3
settings = struct;
end
settings = completeStruct(settings, defaults);


% Output size is same as input size by default
sizeIn = size(im);
if isempty(settings.sizeOut)
    settings.sizeOut = sizeIn;
end


% Check sanity of input
if size(fresnelNumbers,1) > 1 && size(fresnelNumbers,1) ~= numel(size(im))
    error(['size(fresnelNumbers,1) must be either 1 (no astigmatism) or equal', ...
           ' to the number of dimensions numel(size(im)) (possibly astigmatic setting).']);
end


% Automatically choose suitable propagation-method if required
if strcmpi(settings.method, 'auto')
    if max(sizeIn .* abs(fresnelNumbers(:))) >= 1
        settings.method = 'fourier';
    else
        settings.method = 'chirpLimited';
    end

    % Optionally print chosen method 
    if settings.verbose 
        fprintf(['[', mfilename, '] Automatically choosing propagation method ''%s''\n'], settings.method);
    end
end


% Automatically choose suitable padded size if required
if strcmpi(settings.sizePad, 'auto')
    settings.sizePad = sizeIn + max(2*ceil(0.5./abs(fresnelNumbers.')), [], 1);
    settings.sizePad = max(settings.sizePad, settings.sizeOut);

    % For convolution-based methods, padded size need not be greater
    % than input- plus output-size
    if ~strcmpi(settings.method, 'fourier')
        settings.sizePad = min(settings.sizePad, sizeIn + settings.sizeOut);
    end

    % Ensure a padded size that allows for efficient computation of ffts
    settings.sizePad = makeSizeFFTfriendly(settings.sizePad);

    % Optionally print chosen padded size 
    if settings.verbose
        fprintf(['[', mfilename, '] Automatically choosing padded size as %s.\n'], ['[',num2str(settings.sizePad), ']']);
    end

    % Raise warning and allow to abort if the automatically chosen padding is excessively large, 
    % which may occur for method == 'fourier'
    if (   strcmpi(settings.method, 'fourier')                     ...
        && any(settings.sizePad > 1.2*(sizeIn + settings.sizeOut)) ... 
        && prod(settings.sizePad) > 4096^2                            )
        warning(sprintf(['Automatically chosen padded size %s would be excessively large ', ...
                         '(hint: choosing other method than ''fourier'' prevents excessive padding).'], ...
                         ['[',num2str(settings.sizePad), ']']));
        while true
            yn = input('Abort Fresnel-propagation? [y/n]: ', 's');
            if strcmpi(yn,'yes') || strcmpi(yn,'y')
                imProp = [];
                return;
            elseif strcmpi(yn,'no') || strcmpi(yn,'n')
                break;
            end
        end
    end

end


% PERFORM PROPAGATION
prop = FresnelPropagator(sizeIn, fresnelNumbers, rmfield(settings, 'verbose'));
imProp = prop.propagate(im);


end

