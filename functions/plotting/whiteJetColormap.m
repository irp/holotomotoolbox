function cmap = whiteJetColormap(lengthColormap)
% WHITEJETCOLORMAP returns a colormap based on the standard jet colormap
% but starts with white instead of dark blue. 
%
%    ``cmap = whiteJetColormap(lengthColormap)``
%
% This colormap is well suited to plot signals spanning many orders of magnitude.
% 
% Parameters
% ----------
% lengthColormap : integer
%  size of the colormap; if no value is given, the size of the current colormap is
%  adopted
%
% Returns
% -------
% cmap : numerical array
%  resulting colormap in which the lowest values are displayed white
%
% Example
% -------
%
% .. code-block:: matlab
%
%     showImage(phantom(512))
%     colormap(whiteJetColormap) 
% 
    
% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

if nargin < 1, lengthColormap = size(get(gcf,'colormap'),1); end

n = max(2,ceil(lengthColormap/8));
mask = linspace(1, 0, n).';

cmap = colormap(jet(lengthColormap));
cmap(1:n,1:2) = mask + cmap(1:n,1:2);
cmap(1:n,3) =  1;

end
