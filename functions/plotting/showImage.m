function showImage(image,figureIndex)
% SHOWIMAGE displays image in current figure.
%
%    ``showImage(image)``
% 
% This function is a wrapper function to automatically display a figure with tight
% axes, colormap 'gray' and an automatic caxis based on the minimum and maximum
% values of the median filtered image.
%
% Parameters
% ----------
% image : numerical array
%     image to be displayed
% figureIndex : number (optional)
%     index of the figure in which image should be displayed
%
% See also
% --------
% functions.plotting.showStack, functions.plotting.splitFigure
%
% Example
% -------
%
% .. code-block:: matlab
%
%     showImage(phantom(512),3)
%
% See also SHOWSTACK, SPLITFIGURE
    
% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

if (nargin == 2)
    figure(figureIndex);
end

image = squeeze(image);
h = imagesc(image);
axis equal tight off; colorbar;

% determine reasonable range of color axis
if islogical(image)
    caxis([0,1]);
else
    image = medfilt2(image,'symmetric');
    minVal = min(image(:));
    maxVal = max(image(:));
    caxis([minVal maxVal]);
end

end
