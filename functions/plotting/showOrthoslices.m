function fig = showOrthoslices(obj, settings)
% SHOWORTHOSLICES plots 2D orthoslices of a 3D-array along the different coordinate
% planes.
%
%    ``fig = showOrthoslices(obj, settings)``
%
% Parameters
% ----------
% obj : numerical 3d-array
%     3D-array to be plotted
% settings : structure, optional
%     contains additional settings, see *Other Parameters*
%
% Other Parameters 
% ---------------- 
% figIdx : Default = [] 
%     Optional index of the figure where the orthoslices are to be plotted. If empty,
%     the current figure is used, i.e. figIdx = gcf is assigned
% cmap : Default = 'bone'
%     Colormap to be used in the plot
% caxis : Default = []
%     Range of the color axis in the plots. If empty, caxis = [min(obj(:)),
%     max(obj(:))] is used.
% sliceIndices : Default = []
%     Indices of the slices to be plotted along the different dimensions. If empty,
%     sliceIndices = ceil(size(obj)/2) (--> central slices) is used
%
% Returns 
% ------- 
% fig : integer
%     index of the created or updated figure in which the orthoslices are plotted
%
% Example
% -------
%
% .. code-block:: matlab
%
%     obj = gaussian([177,228,160]);
%     settings = showOrthoslices
%     settings.cmap = flipud(bone);
%     fig = showOrthoslices(obj, settings);
%
    
% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

defaults.figIdx = [];
defaults.cmap = 'bone';
defaults.caxis = [];
defaults.sliceIndices = [];

if nargin == 0
    fig = defaults;
    return;
end

if nargin < 2
    settings = struct;
end

settings = completeStruct(settings,defaults);

% Complete parameters
if isempty(settings.figIdx)
    fig = gcf;
else
    fig = figure(settings.figIdx);
    
end
clf;
%
% Plot central slices if no slice indices given
if isempty(settings.sliceIndices)
    settings.sliceIndices = ceil(size(obj)/2);
end
for sliceDim = 1:3
    slices{sliceDim} = squeeze(getSlice(real(obj), settings.sliceIndices(sliceDim), sliceDim));
end
%
% Automatically determine color-axis if not assigned
if isempty(settings.caxis)
    if islogical(obj)
        settings.caxis = [0,1];
    else
        sliceValues = [slices{1}(:); slices{2}(:); slices{3}(:)];
        settings.caxis = [min(sliceValues), max(sliceValues)];
    end
end


% Plot 2D-slices along the three different coordinate planes
for sliceDim = 3:-1:1
    s = subplot('position', [0.02+(sliceDim-1)*0.32, 0.04, 0.29, 0.9]);

    imagesc(slices{sliceDim});
    caxis(settings.caxis);
    colormap(settings.cmap);
    axis 'equal' 'tight';
    colorbar;

    plotDims = setdiff((1:3), sliceDim);
    title(s, sprintf('x%i-x%i-slice at x%i = %i', plotDims(1), plotDims(2), sliceDim, settings.sliceIndices(sliceDim)));
end

end
