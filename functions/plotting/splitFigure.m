function result = splitFigure(image1, image2, settings)
% SPLITFIGURE shows two images side by side.
%
%    ``result = splitFigure(image1, image2, settings)``
% 
% This function creates a figure in which image1 and image2 are shown side by side by
% splitting the figure and plotting image1 at the left/upper position and image2 at
% the right/lower position. The share of each image within the figure can be
% adjusted.
%
% Parameters
% ----------
% image1 : numerical array
%     image to be plotted on the left/top
% image2 : numerical array
%     image to be plotted on the right/bottom
% settings : structure, optional
%     contains additional settings, see *Other Parameters*
%
% Other Parameters 
% ---------------- 
% orientation : Default = 'horizontal'
%     Orientation of the cut through the images. 'horizontal' splits figure along
%     horizontal line, 'vertical' splits figure along vertical line.
% shareImage1 : Default = 0.5
%     Share of image1 which is shown in figure. Range: 0 (only figure2) to 1 (only
%     figure1)
% lineWidth : Default = 1
%     Linewidth of line which divides both images.
% lineColor : Default = 'r'
%     Color of dividing line.
%  
% See also
% --------
% functions.plotting.showImage
%
% Example
% -------
%
% .. code-block:: matlab
%
%     settings = splitFigure;
%     settings.orientation = 'vertical';
%     settings.shareImage1 = 0.4;
%     settings.lineColor = 'r';
%     settings.lineWidth = 2;
% 
%     image1 = phantom(512);
%     image2 = 1-phantom(512);
% 
%     splitFigure(image1,image2,settings)
%
% See also SHOWIMAGE
    
% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

if nargin < 3
    settings = struct;
end

% defaults
defaults.orientation = 'horizontal';   
defaults.shareImage1 = 0.5;             
defaults.lineWidth = 1;                
defaults.lineColor = 'r';               

if (nargin == 0)
    result = defaults;
    return
end

settings = completeStruct(settings, defaults);

if(strcmp(settings.orientation, 'horizontal'))
    image1 = image1';
    image2 = image2';
end

[M,N] = size(image1);

if(size(image1) ~= size(image2))
    error('Images have to be equal in size')
end

newImage = zeros(M,N);

newImage(:, 1:floor(N*settings.shareImage1)) = image1(:, 1:floor(N * settings.shareImage1));
newImage(:, ceil(N * settings.shareImage1):end) = image2(:, ceil(N * settings.shareImage1):end);

if(strcmp(settings.orientation, 'horizontal'))
    showImage(newImage')
else
    showImage(newImage)
end

figureAxes = axis;
posLineX = (figureAxes(2)-1) * settings.shareImage1;
posLineY = (figureAxes(4)) * settings.shareImage1;

if(strcmp(settings.orientation, 'horizontal'))
    line([0 M], [posLineY posLineY],'LineWidth',settings.lineWidth,'Color',settings.lineColor);
else
    line([posLineX posLineX], [0 M],'LineWidth',settings.lineWidth,'Color',settings.lineColor);
end

end
