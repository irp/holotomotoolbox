function result = addSupportLine(support,settings)
% ADDSUPPORTLINE adds a line along the edges of a given support in the current
% figure.
%
%    ``addSupportLine(support,format)``
% 
%
% Parameters
% ----------
% support : logical array
%     support along which a line is plotted
% settings : structure, optional
%     contains additional settings, see *Other Parameters*
%
% Other Parameters
% ----------------
% color : Default = 'r'
%     Color of the added line.
% style : Default = '- -'
%     Drawing style of the added line.
% lineWidth : Default = 1
%     Width of the added line.
%
% See also
% --------
% functions.plotting.showImage
%
% Example
% -------
%
% .. code-block:: matlab
%
%     load('../macrophage.mat')
%     showImage(holograms{1})
%     addSupportLine(support)
%
% See also SHOWIMAGE
    
% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

% default settings
defaults.color = 'r';
defaults.style = '--';
defaults.lineWidth = 1;
if nargin == 0
    result = defaults;
    return
end
if nargin < 2
   settings = struct; 
end
settings = completeStruct(settings, defaults);

% determine bounding lines of support mask
bound = bwboundaries(support);

% plot boundaries
hold on;
for idx = 1:numel(bound)
    plot(bound{idx}(:,2),bound{idx}(:,1), settings.style, 'Color', settings.color, ...
        'LineWidth', settings.lineWidth)
end
hold off;

end

