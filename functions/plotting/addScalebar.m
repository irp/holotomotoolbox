function scalebar = addScalebar(scalebarLength, pixelsize, settings)
% ADDSCALEBAR adds a scalebar of specified length in the current figure.
%
%    ``scalebar = addScalebar(scalebarLength, pixelsize, settings)``
% 
%
% Parameters
% ----------
% scalebarLength : number
%     length of scale bar in same unit as pixel size
% pixelsize : number
%     pixel size in the current figure
% settings : structure, optional
%     contains additional settings, see *Other Parameters*
%
% Other Parameters 
% ---------------- 
% posX : Default = 0.95
%      x coordinate of the scale bar in percent of the image dimensions
% posY : Default = 0.95
%      y coordinate of the scale bar in percent of the image dimensions
% scalebarColor : Default = 'k'
%     Color of the scale bar. Use Matlab standard color strings.
% scalebarWidth : Default = 4
%     Width of the scale bar in pixels.
% scalebarText : Default = []
%     Label for scale bar. Set scalebarText to '' to turn it off. If it is set to [],
%     the label is '<scalebarLength> micron'.
% fontSize : Default = 12
%     Font size of the scalebar label.
% offset: Default = 18
%     Offset between scalebar and label. Default value equals 1.5 times the
%     FontSize
%
% Returns
% -------
% scalebar : number
%     length of scalebar in pixels
%   
% Example
% -------
%
% .. code-block:: matlab
%
%     showImage(phantom(512))
% 
%     settings = addScalebar;
%     settings.posX = 0.95;
%     settings.posY = 0.95;
%     settings.scalebarColor = 'w';
%     settings.scalebarWidth = 8;
%     settings.fontSize = 16;
% 
%     addScalebar(70,1, settings)
% 
    
% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

if nargin < 3
    settings = struct;
end

defaults.posX = 0.95;
defaults.posY = 0.95;
defaults.scalebarColor = 'k';
defaults.scalebarWidth = 4;
defaults.scalebarText = [];
defaults.fontSize = 12;
defaults.offset = 18;

if (nargin == 0)
    scalebar = defaults;
    return
end

settings = completeStruct(settings,defaults);

if isempty(settings.scalebarText)
    settings.scalebarText = [num2str(scalebarLength), ' micron'];
end

% determine scalebar length in pixels
scalebar = round(scalebarLength/pixelsize);

% determine position of scalebar
figureAxes = axis;
posScalebarX = figureAxes(2) * settings.posX;
posScalebarY = figureAxes(4) * settings.posY;

% add scalebar and label to the figure
line([posScalebarX posScalebarX-scalebar],[posScalebarY posScalebarY],'LineWidth',...
    settings.scalebarWidth,'Color',settings.scalebarColor);


text(posScalebarX - 0.5*scalebar, posScalebarY - settings.offset - settings.fontSize, settings.scalebarText,...
    'Color',settings.scalebarColor , 'FontSize',settings.fontSize,...
    'HorizontalAlignment', 'center')

end

