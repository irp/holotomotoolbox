function result = showStack(stack,settings)
% SHOWSTACK displays a stack of images along a specified dimension.
%
%    ``result = showStack(stack,settings)``
% 
%
% Parameters
% ----------
% stack : numerical array
%     stack of images to be displayed
% settings : structure, optional
%     contains additional settings, see *Other Parameters*
%
% Other Parameters 
% ---------------- 
% dimension : Default = 3
%     Dimension along which the stack is displayed.
% pause : Default = 0
%     Pause between frames in seconds.
% showColorbar : Default = true
%     Show the colorbar?
% colormap : Default = 'gray'
%     Colormap to be used. In case flipped colormaps should be used, make sure to not
%     use quotation marks, e.g. flipud(gray)
% axisEqualTight : Default = true
%     Should axis be equal tight (true) or normal tight (false)?
% showAxis : Default = true
%     Show axes in the frames?
% caxis : Default = -1
%     -1: fixed to minimum and maximum values of image stack, 0: not fixed, [min
%     max]: fixed to specified range
% step : Default = 10
%     Step size between frames.
% range : Default = []
%     Range of frames to be shown. An empty range results in the entire stack.
% roiX : Default = []
%     Region of interest to be shown in x. An empty range results in the entire range
%     in x.
% roiY : Default = []
%     Region of interest to be shown in y. An empty range results in the entire range
%     in y.
% title : Default = []
%     Title which is added before the current number of the frame is given.
% savePath : Default = []
%     Path to where the images should be stored. If no path is given, the images are
%     not stored. Images will be stored as png with name
%     'settings.savePath_framenumber.png'.
%
% See also
% --------
% functions.plotting.showImage
%
% Example
% -------
%
% .. code-block:: matlab
%
%     stack = create3Dphantom(512);
% 
%     settings = showStack;
%     settings.step = 20;
%     settings.pause = 0.1;
%     settings.dimension = 2;
% 
%     showStack(stack,settings)
%
% See also SHOWIMAGE
    
% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License

% default settings
defaults.dimension = 3;       
defaults.pause = 0;              
defaults.showColorbar = true;           
defaults.colormap = 'gray';    
defaults.axisEqualTight = true;  
defaults.showAxis = true;        
defaults.caxis = -1;             
defaults.step = 10;                
defaults.range = [];             
defaults.roiX = [];               
defaults.roiY = [];                
defaults.title = [];             
defaults.savePath = [];       

% return default settings
if (nargin == 0)
    result = defaults;
    return
end

% no settings passed
if (nargin < 2)
    settings = struct;
end

% complete settings with defaults
settings = completeStruct(settings,defaults);

% convert stack to array
if iscell(stack)
    if size(stack,1) > size(stack,2)
        stack = stack';
    end
    stack = cat(ndims(stack{1})+1, stack{:});
end
%%
%get dimensions
tmp=size(stack);
numImages = tmp(settings.dimension);
tmp(settings.dimension) = [];
Ny = tmp(1);
Nx = tmp(2);

%set frame range
if isempty(settings.range)
    frameRange = 1:settings.step:numImages;
else
    frameRange = settings.range(1):settings.step:settings.range(2);
end

%set ROI
if isempty(settings.roiY)
    settings.roiY = [1 Ny];
end

if isempty(settings.roiX)
    settings.roiX = [1 Nx];
end

%set caxis to min and max of image stack
if settings.caxis == -1
    minv = min(stack(:));
    maxv = max(stack(:));
end


for k_frame = frameRange    

    % Slice current image from 3d-array
    currentFrame = squeeze(getSlice(stack, k_frame, settings.dimension));

    % Optionally restrict to roi
    currentFrame = currentFrame(settings.roiY(1):settings.roiY(end),...
        settings.roiX(1):settings.roiX(end));
     
    % rotate image to right orientation
    if settings.dimension~=3
        currentFrame=flipud(rot90(currentFrame));
    end
    
    %show frame
    imagesc(currentFrame)
    
    %caxis
    if settings.caxis == -1
        caxis([minv maxv])
    end
    
    if size(settings.caxis,2) == 2
        caxis(settings.caxis)
    end
    
    
    %set colorbar
    if settings.showColorbar
        colorbar;
    end
    
    %set colormap
    colormap(settings.colormap);
    
    %set axis
    if settings.axisEqualTight
        axis equal tight on
    else
        axis normal tight on
    end
    
    if ~settings.showAxis
        axis off
    end
    
    %set title
    if isempty(settings.title)
        stringTitle = sprintf('%04d / %04d',k_frame,numImages);
    else
        stringTitle = sprintf('%s, %04d / %04d',settings.title, k_frame,numImages);
    end
    title(stringTitle)

    
    %draw it
    drawnow;
  
    %optionally save image
    if ~isempty(settings.savePath)
        print (sprintf('%s_%04d.png',settings.savePath,k_frame),'-dpng');
    end
    
    %pause
    pause(settings.pause)

end

end


