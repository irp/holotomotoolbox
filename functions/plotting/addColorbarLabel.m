function settings = addColorbarLabel(label, settings)
% ADDCOLORBARLABEL adds specified colorbar label to current figure.
%
%    ``addColorbarLabel(label, settings)``
%
% This function is applied to the current figure. It adds a label on top of the
% colorbar and removes ticks too close to the edges of the colorbar. NOTE: Since
% addColorbarLabel changes the ticks on the colorbar, one should first adjust the
% caxis and after that add the label.
% 
%
% Parameters
% ----------
% label : string
%     label to be put on the colorbar can contain latex code.
% settings : structure, optional
%     contains additional settings, see *Other Parameters*
%
% Other Parameters 
% ---------------- 
% FontSize : Default = get(0,'defaultAxesFontSize')
%     Font size of the colorbar label.
% position : Default = FontSize*1.5
%      Defines an offset of the label to the colorbar in pt.
% TickFontSize : Default = get(0,'defaultAxesFontSize')
%       Size of the labels on the ticks of the colorbar.
% HorizontalAlignment : Default = get(0,'defaultAxesFontSize')
%       Alignment of label wrt to the colorbar. Values = {left, right,
%       center}
% Interpreter : Default = 'latex'
%       Chose text interpreter {latex, tex, none}.
% cbHandle: Default = []
%       Handle to colorbar to modify.

% 
% Example
% -------
%
% .. code-block:: matlab
%
%     %% this will simply add a label with standard values
%     showImage(phantom(512))
%     addColorbarLabel('Intensity');
%     
%     %% for all options     
%     label_options = addColorbarLabel;
%     label_options.HorizontalAlignment = 'center' 
%     showImage(phantom(512))
%     addColorbarLabel('Intensity', label_options);

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%                   
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

defaults.FontSize = get(0,'defaultAxesFontSize');
defaults.TickFontSize = defaults.FontSize;
defaults.position = defaults.FontSize*1.5; % measred in pt
defaults.HorizontalAlignment = 'right';
defaults.Interpreter = 'latex';
defaults.cbHandle = [];
defaults.cutOff = 0.2;

if (nargin == 0)
    settings = defaults;
    return
end

if (nargin < 2)
    settings = defaults;
end

settings = completeStruct(settings,defaults);

if isempty(settings.cbHandle)
    cb = findall(gcf,'type','ColorBar');
    if isempty(cb)
        cb = colorbar;
    end
else
    cb = settings.cbHandle;
end
cb.Units = 'points';
cb.Color = [0 0 0];
cb.FontSize = settings.TickFontSize;
cb.Label.String = label;
cb.Label.HorizontalAlignment = settings.HorizontalAlignment;
cb.Label.Rotation = 0;
cb.Label.Units = 'points';
cb.Label.Interpreter = settings.Interpreter;
cb.Label.FontSize = settings.FontSize;
% Matlab does not use the coordinates with respect to the whole figure, but
% always with respect to the parent of an object.
% In this case the parent of the Label is the colorbar thus the new label
% position is calculated wrt to the position of the colorbar.
cb.Label.Position = [cb.Position(3) cb.Position(4)+settings.position];

TicksDelta = cb.Ticks(2) - cb.Ticks(1);
% remove ticks too close to edges
if  cb.Ticks(1) - cb.Limits(1) < settings.cutOff * TicksDelta
    cb.Ticks = cb.Ticks(2:end);
end

if cb.Limits(2) - cb.Ticks(end) < settings.cutOff * TicksDelta
    cb.Ticks = cb.Ticks(1:end-1); 
end
settings = cb;
end
