function autoCaxis = autoContrast(region)
%AUTOCONTRAST automatically optimizes the caxis.
%
% Parameters
% ----------
% region : region
%     Region for which the contrast is optimized. If no input is given, the region is
%     selected manually by drawing a rectangle (imrect) in the current figure.
%
% Returns
% -------
% autoCaxis : numerical array
%     optimized values for the caxis
%
%
% Example
% -------
%
% .. code-block:: matlab
%
%     image = phantom(512);
%     showImage(image)
%     autoContrast
%

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

% get the current figure
currentFig = gca;

if isempty(currentFig)
    error('No figure window open');
end
% define region to adjust contrast to
if (nargin<1)
    region = imrect;
    wait(region);
end

imageOriginal = getimage(currentFig);

% create mask
dimMask = round(region.getPosition);

% only consider part of the image which lies within specified region
imageRegion = imageOriginal(dimMask(2):dimMask(2)+dimMask(4)-1,dimMask(1):dimMask(1)+dimMask(3)-1);

% apply median and averaging filter to remove outliers from the image which
% would otherwise lead to incorrect minimum and maximum values
imageRegion = medfilt2(imageRegion);
imageRegion = movmean(imageRegion,3);
imageRegion = imageRegion(3:end-2,3:end-2);

% set caxis to minimum and maximum values within the specified region
cmin = min(imageRegion(:)) + eps;
cmax = max(imageRegion(:)) - eps;
caxis([cmin cmax]);
autoCaxis = caxis;
region.delete

end

