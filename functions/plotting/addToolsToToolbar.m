function addToolsToToolbar()
% ADDTOOLSTOTOOLBAR adds an button in the toolbar to easily access
% the autoContrast and cropTool function.
%
%    ``addToolsToToolbar()``
%
% This function alters the behavior of all figure windows after invocation.
% It alters the default function which is called by matlab upon creation of
% a new figure. Afterwards new figures have a new tool represented by a
% light bulb to adjust the contrast of an image.
%
% Returns
% -------
% This function changes the behavior of all figure created after
% invocation.
%
% See also
% --------
% autoContrast
% cropTool
%
% Example
% -------
%
% .. code-block:: matlab
%
%     addToolsToToolbar()
%     figure; 
%     imagesc(rand(128))

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%

fig_cmd = ...
    ['addToolbarExplorationButtons(gcf);'...
    'AutoContrastBtn = uipushtool(uitoolbar(gcf),''TooltipString'',''Auto Contrast Tool'', ''ClickedCallback'',''eval(''''autoContrast'''')'');'...
    '[img,map] = imread(fullfile(matlabroot,''toolbox'',''matlab'',''icons'',''demoicon.gif''));'...
    'AutoContrastBtn.CData = ind2rgb(img,map); '];

fig_cmd = ...
    [fig_cmd ...
    'CropToolBtn = uipushtool(''TooltipString'',',...
    '''Cropping Tool'', ''ClickedCallback'',''eval(''''[cropRegion, cropSize, cropImg] = cropTool;'''')'');'...
    '[img,map] = imread(fullfile(matlabroot,''toolbox'',''matlab'',''icons'',''tool_rectangle.gif''));'...
    'CropToolBtn.CData = ind2rgb(img,map); '];

set(groot,'defaultFigureCreateFcn',fig_cmd)

end
