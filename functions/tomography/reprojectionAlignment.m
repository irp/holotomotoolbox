function [shifts, rotAngles, projsAligned] = reprojectionAlignment(projs, tomoAngles, settings)
% REPROJECTIONALIGNMENT aims to align (parallel-beam) tomographic projections by a
% reprojection-approach
%
%    ``[shifts, rotAngles, projsAligned] = reprojectionAlignment(projs, tomoAngles, settings)``
%
% The algorithm works iteratively, where each iteration is composed of two steps:
% (1) Reconstruct a 3D-object from pre-aligned projections by filtered back-projection
% (2) Simulate projections from the reconstructed 3D-object
% (3) Improve the current alignment by registering the measured with the simulated projections
% NOTE: To improve numerical performance, this scheme is (by default) performed on binned,
% i.e. downsampled versions, of the input projection data.
%
% Parameters
% ----------
% projs : numerical array
%     misaligned tomographic projections (with the same axis-ordering as expected by astraFBP)
% tomoAngles : vector
%     tomographic angles at which the projections were recorded
% settings : structure, optional
%     contains additional settings, see *Other Parameters*
%
% Other Parameters
% ----------------
% binningFactor : Default = 4
%     Factor by which the input projections are downsampled along each spatial dimension
%     before running the actual reprojection-algorithm.
%     NOTE: Binning is done to reduce the computational effort of the method, while
%     taking advantage of the fact that image-registration can be achieved to sub-pixel
%     accuracy. The latter implies that alignment shifts- and/or rotations, that are
%     determined upon the basis of binned projections, may still be sufficiently accurate
%     for the original high-resolution data set.
% registrationMode : Default = 'shift'
%     Type of the alignment that is performed on the projections. Options are the same
%     as in the function registerImages.
% registrationAccuracy : Default = 20
%     Accuracy-factor for the alignment. Shifts and/or rotations of the projections
%     are detected to an accuracy of at most 1/registrationAccuracy lengths of a pixel.
%     NOTE: a larger value increases the theoretical accuracy, but also the compuational
%     effort
% sigmaLowpass : Default = 10
%     Standard-deviation of a gaussian low-pass filter that is applied to the projections
%     before image-registration. If the value is <= 0, no filtering will be performed.
% sigmaHighpass : Default = 10
%     Standard-deviation of a gaussian low-pass filter that is applied to the projections
%     before image-registration. If the value is <= 0, no filtering will be performed.
% objSupport : Default = []
%     Optional boolean mask (size [size(projs,1),size(projs,2),size(projs,2)]) that restricts
%     the support of the reconstructed 3D-object (i.e. all voxel values where objSupport==false
%     are set to 0) before the reprojection-step (2).
%     If the array is empty (the default case []), no such support-constraint is applied.
%     NOTE: Imposing support-constraints often improves convergence of reprojection alignment.
% minObjDensity : Default = -inf
%     Minimum allowed voxel value in the reconstructed 3D-objects. Reconstructed voxel values
%     < minObjDensity are set to this value before the reprojection-step (2).
%     NOTE: Imposing min/max-constraints often improves convergence of reprojection alignment.
% maxObjDensity : Default = inf
%     Maximum allowed voxel value in the reconstructed 3D-objects. Reconstructed voxel values
%     > maxObjDensity are set to this value before the reprojection-step (2).
%     NOTE: Imposing min/max-constraints often improves convergence of reprojection alignment.
% shiftsGuess : Default = 0
%     Starting value for the iteratively computed alignment-shifts. Must be a float
%     (i.e. the same value for all projections) or an array of size [numel(tomoAngles),2].
% rotAnglesGuess : Default = 0
%     Starting value for the iteratively computed alignment-rotations. Must be a float
%     (i.e. the same value for all projections) or a vector of lenght numel(tomoAngles).
% verbose : Default = true
%     Should status messages be displayed on screen during the reprojection-alignment?
% plotIterates : Default = true
%     Should plots of the intermediate object-reconstructions and alignment-parameters
%     be displayed on screen during the reprojection-alignment?
% maxIterations : Default = 20
%     Maximum number of iterations.
% shiftTolerance : Default = 0.1
%     Tolerance in the computed shifts used as a stopping criterion for the iterations:
%     the iterations are stopped when the shifts changed by <= shiftTolerance 
%     AND the rotations by <= rotAngleTolerance from one iteration to the next FOR ALL
%     projections.
% rotAngleTolerance : Default = 0.1
%     Tolerance in the computed rotations used as a stopping criterion for the iterations.
%       
%
% Returns
% -------
% shifts : numerical array
%     Array of size [numel(tomoAngles),2] containing the computed shifts that
%     need to be applied to projs to compensate the misalignment.
%     First column: shifts parallel to the tomographic axis, i.e. along dimension 1 of projs
%     Second column: shifts normal to the tomographic axis, i.e. along dimension 2 of projs
% rotAngles : numerical array, optional
%     Vector of size numel(tomoAngles) containing the computed rotations that
%     need to be applied to projs to compensate the misalignment.
%     setting.registrationMode needs to be set to 'shiftRotation' or 'rotation' for this to be nonzero.
% projsAligned: numerical array, optional
%     Array containing the aligned tomographic projections, obtained by shifting
%     and/or rotating the images in projs according to shifts and rotAngles
%
% See also
% --------
% functions.imageProcessing.alignment.registerImages, 
% functions.imageProcessing.alignment.alignImages, 
% functions.tomography.astraWrappers.astraParProjection3D, 
% functions.tomography.astraWrappers.astraFBP,
% functions.tomography.alignRotaxis,
% functions.tomography.checkRotaxis
%
% Example
% -------
%
% .. code-block:: matlab
%
%     % Create phantom
%     phantomSettings.sizeObjects = 128;
%     phantomSettings.numObjects = 100;
%     phantomSettings.distrSize = 'random';
%     phantomSettings.distrStrength = 'random';
%     vol = create3Dphantom(512, phantomSettings);
%     
%     % Compute tomographic projections
%     nt = 300;
%     tomoAngles = (1:nt).' * (180/nt);
%     projs_true = astraParProjection3D(vol, tomoAngles);
%     projs_noisy = projs_true + 10*(2*rand(size(projs_true))-1);
%     
%     % Simulate alignment errors
%     vertical_shifts_true = linspace(-5,5,nt).' + (2*rand([nt,1])-1);
%     horizontal_shifts_true = 4*sind(8*tomoAngles) + (2*rand([nt,1])-1);
%     shifts_true = [vertical_shifts_true, horizontal_shifts_true];
%     projs_misaligned = zeros(size(projs_true), class(projs_true));
%     for jj = 1:nt
%         projs_misaligned(:,:,jj) = shiftImage(projs_noisy(:,:,jj), -shifts_true(jj,:));
%     end
%     
%     % RUN REPROJECTION ALIGNMENT
%     settings = struct;
%     settings.minObjDensity = 0;     % object constraint: enforce positivity of the reconstructed voxel values
%     settings.binningFactor = 4;     % perform alignment on 4-times downsampled data (this is the default)
%     [shifts_recon, rotAngles_recon, projs_aligned] = reprojectionAlignment(projs_misaligned, tomoAngles, settings);
%     %
%     % Refine alignment with reduced binning factor, i.e. with higher resolution
%     % in the projection data and the reconstructed objects
%     % (Note that this is computationally more expensive than the prevous run)
%     settings.binningFactor = 2;
%     settings.shiftsGuess = shifts_recon;    % set previously computed alignment as initial guess
%     [shifts_recon, rotAngles_recon, projs_aligned] = reprojectionAlignment(projs_misaligned, tomoAngles, settings);
%     
%     % Compare object reconstructions
%     figure('name', 'True object'); showOrthoslices(vol);
%     figure('name', 'Reconstruction from perfectly aligned projections'); showOrthoslices(astraFBP(projs_noisy, tomoAngles));
%     figure('name', 'Reconstruction from misaligned projections'); showOrthoslices(astraFBP(projs_misaligned, tomoAngles));
%     figure('name', 'Reconstruction from reprojection-aligned projections'); showOrthoslices(astraFBP(projs_aligned, tomoAngles));
%     
%     % Compare reconstructed alignment parameters to true shifts
%     figure('name', 'Alignment shifts');
%     plot((1:nt), vertical_shifts_true, '--b', (1:nt), horizontal_shifts_true, '--r', ...
%         (1:nt), shifts_recon(:,1), '-b', (1:nt), shifts_recon(:,2), '-r');
%     legend({'true: along tomo-axis', 'true: normal to tomo-axis', 'reconstructed: along axis', 'reconstructed: normal'});
%     xlabel('Projection number');
%     ylabel('Shift [px]');
%
%
% See also REGISTERIMAGES, ALIGNIMAGES, ASTRAFBP, ASTRAPARPROJECTION3D, ALIGNROTAXIS, CHECKROTAXIS

% HoloTomoToolbox
% Copyright (C) 2022  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

% default settings
defaults.binningFactor = 4;
defaults.registrationMode = 'shift';
defaults.registrationAccuracy = 20;
defaults.sigmaLowpass = 10;
defaults.sigmaHighpass = 50;
defaults.objSupport = [];
defaults.minObjDensity = -inf;
defaults.maxObjDensity = inf;
defaults.shiftsGuess = 0;
defaults.rotAnglesGuess = 0;
defaults.verbose = true;
defaults.plotIterates = true;
defaults.maxIterations = 20;
defaults.shiftTolerance = 0.1;
defaults.rotAngleTolerance = 0.1;
%
% return default settings if not argument is passed
if (nargin == 0)
    shifts = defaults;
    rotAngles = [];
    projsAligned = [];
    return
end
%
if (nargin < 3)
    settings = struct;
end
settings = completeStruct(settings, defaults);


% Configure filters that are applied to projections before image-registration
if settings.sigmaLowpass <= 0;
    lowpassFilt = @(im) im;
else
    lowpassFilt = @(im) imgaussfilt(im, settings.sigmaLowpass/settings.binningFactor);
end
if settings.sigmaHighpass <= 0;
    highpassFilt = @(im) im;
else
    highpassFilt = @(im) im - imgaussfilt(im, settings.sigmaHighpass/settings.binningFactor);
end
filterProjection = @(proj) highpassFilt(lowpassFilt(fadeoutImage(proj)));


% Pre-compute binned projections
numProjs = size(projs,3);
if settings.binningFactor > 1
    projsBinned = imresize(projs, 1/settings.binningFactor);
else
    projsBinned = projs;
end


% Configure optional constraints on the 3d-object
if isempty(settings.objSupport)
    applySupport = @(vol3d) vol3d;
else
    support = imresize3(settings.objSupport, 1/settings.binningFactor);
    applySupport = @(vol3d) support .* vol3d;
end
if settings.minObjDensity > -inf
    % Object-density is in units of 1/voxel and thus must be rescaled by binnning factor
    applyMin = @(vol3d) max(settings.minObjDensity*settings.binningFactor, vol3d);
else
    applyMin = @(vol3d) vol3d;
end
if settings.maxObjDensity < inf
    % Object-density is in units of 1/voxel and thus must be rescaled by binnning factor
    applyMax = @(vol3d) min(settings.maxObjDensity*settings.binningFactor, vol3d);
else
    applyMax = @(vol3d) vol3d;
end
applyConstraints = @(vol3d) applyMax(applyMin(applySupport(vol3d)));


% Define functions to remove shift-components that correspond to a global shift of the 3D-object
c = cosd(tomoAngles);
c = c./norm(c(:));
s = sind(tomoAngles);
s = s - c(:)' * s(:);
s = s./norm(s(:));
removeGlobalHorizontalShift = @(s_horz) s_horz - (c(:)'*s_horz(:)).*reshape(c,size(s_horz)) - (s(:)'*s_horz(:)).*reshape(s,size(s_horz));
removeGlobalVerticalShift = @(s_vert) s_vert - mean(s_vert(:));
removeGlobalShifts = @(shifts) [removeGlobalVerticalShift(shifts(:,1)), removeGlobalHorizontalShift(shifts(:,2))];
removeGlobalShiftsRelative = @(shifts) settings.shiftsGuess/settings.binningFactor + removeGlobalShifts(shifts - settings.shiftsGuess/settings.binningFactor);


% Settings for tomo-reconstruction and reprojection
projsWidth = size(projsBinned,2);
sinoPadding = ceil(0.5*(sqrt(2)-1)*projsWidth) + 1;
settingsTomoRec.outputSize = projsWidth;
settingsReproj.outputSize = projsWidth;


% MAIN ALGORITHM
%
% Initialize
shifts = settings.shiftsGuess .* ones([size(projs,3),2]) / settings.binningFactor;
rotAngles = settings.rotAnglesGuess(:) .* ones([size(projs,3),1]);
figObj = 42;
figAlignment = 43;
converged = false;
%
% Iterate
for it = 1:settings.maxIterations
    % Apply current guess for shifts and rotation to measured (binned) projections
    for jj = numProjs:-1:1
        projsAligned(:,:,jj) = shiftRotateImage(projsBinned(:,:,jj), shifts(jj,:), rotAngles(jj));
    end
    %
    % Perform tomographic reconstruction from projections under current alignment
    obj = astraFBP(padarray(projsAligned, [0,sinoPadding,0], 'replicate'), tomoAngles, settingsTomoRec);
    obj = applyConstraints(obj);
    %
    % Optionally plot reconstructed object
    iteration_string = sprintf('[%s] Iteration %d: ', mfilename, it);
    if settings.plotIterates
        figure(figObj); showOrthoslices(obj); set(gcf, 'name', [iteration_string, 'reconstructed object']); drawnow; pause(1);
    end
    %
    % Simulate tomographic projections from reconstructed 3D-object
    reprojs = astraParProjection3D(obj, tomoAngles, settingsReproj);
    %
    % Register measured projections with simulated reprojections
    shiftsLast = shifts;
    rotAnglesLast = rotAngles;
    for jj = 1:numProjs
        [d_shift, d_rotAngle] = registerImages(filterProjection(reprojs(:,:,jj)), filterProjection(projsAligned(:,:,jj)), settings);
        [shifts(jj,:), rotAngles(jj)] = combineShiftRotations(shifts(jj,:), d_shift, rotAngles(jj), d_rotAngle);
    end
    %
    % Remove shift-components that correspond to a global shift of the 3D-object
    shifts = removeGlobalShiftsRelative(shifts);
    %
    % Compute changes in the registered shifts and rotations
    maxShiftIncr = norm(sqrt(sum((shifts-shiftsLast).^2, 2)), inf);
    maxRotAngleIncr = norm(rotAngles(:)-rotAnglesLast(:), inf);
    %
    % Optionally print status message
    fprintIf(settings.verbose, [iteration_string, 'max. shift-increment = %.2f pixels, max. rotation-increment = %.2f degrees\n'], maxShiftIncr, maxRotAngleIncr);
    %
    % Optionally plot computed alignment transforms
    if settings.plotIterates
        figure(figAlignment); clf; plot(tomoAngles, shifts(:,1), tomoAngles, shifts(:,2), tomoAngles, rotAngles);
        set(gcf, 'name', [iteration_string, 'alignment transforms']);
        legend({'Vertical shifts', 'Horizontal shifts', 'Rotations'});
        xlabel('projection number'); 
        ylabel('shift [px] / rotation [deg]'); 
        drawnow; pause(1);
    end
    %
    % Check stopping criterion
    if maxShiftIncr <= settings.shiftTolerance && maxRotAngleIncr <= settings.rotAngleTolerance
        converged = true;
        fprintIf(settings.verbose, ['[%s] Alignment has converged to the prescribed tolerance after %d iterations.\n'], mfilename, it);
        break;
    end
end

% Raise warning if stopping criterion was not met, i.e. iterations did not converge
if ~converged
    warning('[%s] Alignment did not converge within %d iterations. Result may be inaccurate.', mfilename, settings.maxIterations);
end


% Finalize
shifts = settings.binningFactor * shifts;
if nargout == 3
    projsAligned = zeros(size(projs), class(projs));
    for jj = 1:numProjs
        projsAligned(:,:,jj) = shiftRotateImage(projs(:,:,jj), shifts(jj,:), rotAngles(jj));
    end
end
if settings.plotIterates
    close(figObj);
    close(figAlignment);
end

end
