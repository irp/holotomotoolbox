function result = checkRotaxis(proj0deg,proj180deg,settings)
% CHECKROTAXIS checks the alignment of the rotation axis.
%
%    ``result = checkRotaxis(proj0deg,proj180deg,settings)``
%
% Compares two images which were recorded at an angular difference of 180 deg. One of
% the images is flipped around the vertical axis. If the rotation axis is in the
% center of the detector, both images should cancel each other out. In the case of a
% misaligned rotation axis, the images have to be shifted against each other by the
% distance of the actual rotation axis to the center of the detector.
%
% Parameters
% ----------
% proj0deg : numerical array
%     tomographic projection acquired at 0 degree
% proj180deg : numerical array
% settings : structure, optional
%     contains additional settings, see *Other Parameters*
%
% Other Parameters
% ----------------
% shiftx : Default = 0
%     Horizontal shift needed so that the images cancel each other out.
% shifty : Default = 0
%     Vertical shift needed so that the images cancel each other out (this should be
%     zero as otherwise the sample moved in vertical direction during the acquisition).
% tiltAngle : Default = 0
%     Tilt angle of the detector with respect to the rotation axis.
% crop : Default = 0
%     Crops the specified number of pixels from the edges of the images.
%
% Returns
% -------
% result : numerical array
%     difference between the image recorded at 0 degree and the image at 180 degree
%     (vertically flipped) after applying the corrections specified in settings
%
% Example
% -------
%
% .. code-block:: matlab
%
%     vol = create3Dphantom(512);
%     tomoAngles = [0 180];
%     projs = radon3Darray(vol, tomoAngles);
%     shiftRotaxis = 5;
%     projsShifted = circshift(projs, [0 shiftRotaxis]);
% 
%     figure(1)
%     showImage(projsShifted(:,:,1) - fliplr(projsShifted(:,:,2)))
% 
%     settings = checkRotaxis;
%     settings.shiftx = -shiftRotaxis;
% 
%     figure(2)
%     showImage(checkRotaxis(projsShifted(:,:,1),projsShifted(:,:,2),settings))
%
%

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

if nargin < 3
    settings = struct;
end

defaults.shiftx = 0;
defaults.shifty = 0;
defaults.tiltAngle = 0;
defaults.crop = 0;

if (nargin == 0)
    result = defaults;
    return
end

settings = completeStruct(settings, defaults);

% Shift and/or rotate the projections
proj0deg = shiftRotateImage(proj0deg, [0 settings.shiftx], settings.tiltAngle);
proj180deg = shiftRotateImage(proj180deg, [settings.shifty settings.shiftx], settings.tiltAngle);


% show the difference between the first projection and the second
% projection (vertically flipped)
result = croparray(proj0deg - fliplr(proj180deg), settings.crop*[1,1]);

end

