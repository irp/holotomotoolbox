function sinoCorrected = ringremove(sino,settings)
% RINGREMOVE removes ring artifacts in tomographic reconstructions.
%
%    ``sinoCorrected = ringremove(sino,settings)``
%
% Removes ring artifacts in tomographic reconstructions by reducing the amount of
% horizontal lines in the given sinogram. Ring removal is either based on the method
% proposed by Ketcham et al. :cite:`Ketcham_PS_2006`, denoted as 'additive', or on the method proposed
% by Muench et al. :cite:`Muench_OE_2009`, denoted as 'wavelet'.
%
% Parameters
% ----------
% sino : numerical array
%     sinogram from which horizontal stripes have to be removed
% settings : structure, optional
%     contains additional settings, see *Other Parameters*
%
% Other Parameters
% ----------------
% method : Default = 'additive'
%     Defines which ringremove method is used: 'additive' or 'wavelet' 
% additive_SmoothMethod : Default = 'mean'
%     Smoothing method for averaged sinogram when using method 'additive': moving
%     averaging window ('mean'), moving median window ('median') or Gaussian
%     filtering ('gauss').
% additive_WindowSize : Default = 10
%     Window size for moving averaging or median window when using method 'additive'.
% additive_GaussSigma : Default = 3
%     Standard deviation for the Gaussian filtering when using method 'additive'.
% additive_Strength : Default = 1
%     Strength of the subtraction of the identified stripe artifacts from the
%     original sinogram when using method 'additive'.
% additive_Region : Default = []
%     Region (of the radius) of the sinogram to be corrected when using method 
%     'additive'. [] corresponds to 1:size(sino,1).
% wavelet_Wname : Default = 'sym8'
%     Wavelet base to be used for the decomposition of the sinogram when using method
%     'wavelet'.
% wavelet_DecNum : Default = 3
%     Highest decomposition level in the wavelet decomposition when using method
%     'wavelet'.
% wavelet_Sigma : Default = 1
%     Width of the Gaussian filter kernel for the horizontal part of the
%     sinogram in Fourier space when using method 'wavelet'.
%
% Returns
% -------
% sinoCorrected : numerical array
%     corrected sinogram from which horizontal stripes were removed
%
% Example
% -------
%
% .. code-block:: matlab
%
%    slice = phantom(512);
%    tomoAngles = 0:0.2:180;
%    sino = radon(slice,tomoAngles);
% 
%    numRings = 250;
%    maxAddedValue = 0.5;
%    sinoRings = sino;
%    for i = 1:numRings
%        position = rand;
%        strength = -2*maxAddedValue*rand + maxAddedValue;
%        sinoRings(ceil(position*size(sino,1)),:) = ...
%            sinoRings(ceil(position*size(sino,1)),:) + strength;
%    end
% 
%    sliceRings = iradon(sinoRings,tomoAngles,'Ram-Lak',1,512);
% 
%    settings = ringremove;
%    settings.method = 'additive';
%    settings.additive_SmoothMethod = 'mean'; 
%    settings.additive_WindowSize = 5;
% 
%    sinoCorrected = ringremove(sinoRings,settings);
% 
%    sliceCorrected = iradon(sinoCorrected,tomoAngles,'Ram-Lak',1,512);
%
%    subplot(1,2,1)
%    showImage(sliceRings)
%    subplot(1,2,2)
%    showImage(sliceCorrected)

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

if nargin < 2
    settings = struct;
end

% defaults
defaults.method = 'additive';   

defaults.additive_SmoothMethod = 'mean'; 
defaults.additive_WindowSize = 10;      
defaults.additive_GaussSigma = 3;        
defaults.additive_Strength = 1;
defaults.additive_Region = [];

defaults.wavelet_Wname = 'sym8';
defaults.wavelet_DecNum = 3;    
defaults.wavelet_Sigma = 1;       

if (nargin == 0)
    sinoCorrected=defaults;
    return
end

settings = completeStruct(settings, defaults);


switch settings.method
    case 'additive'
        % averaging of sinogram along the angle
        sinoMean = mean(sino,2);
        
        % smoothing of the sinogram
        switch settings.additive_SmoothMethod
            case 'mean'
                sinoMeanSmoothed = smoothdata(sinoMean,'movmean',settings.additive_WindowSize);
            case 'median'
                sinoMeanSmoothed = smoothdata(sinoMean,'movmedian',settings.additive_WindowSize);
            case 'gauss'
                sinoMeanSmoothed = imgaussfilt(sinoMean,settings.additive_GaussSigma);
            otherwise
                error('Choose between "mean", "median" and "gauss" for setting "additive_SmoothMethod"');
        end

		if ~isempty(settings.additive_Region)
			region = settings.additive_Region;
		else
			region = 1:size(sino,1);
		end

        % identification of horizontal stripes
        correctionProfile = zeros(size(sinoMean));
        correctionProfile(region) = sinoMean(region) - sinoMeanSmoothed(region);
        % sinogram correction
        sinoCorrected = sino - settings.additive_Strength * correctionProfile;
       
    case 'wavelet'
		
        % perform multilevel wavelet decomposition		
		H = cell(settings.wavelet_DecNum, 1);
		V = cell(settings.wavelet_DecNum, 1);
		D = cell(settings.wavelet_DecNum, 1);
		S = cell(settings.wavelet_DecNum, 1);
				
        for l = 1:settings.wavelet_DecNum
			S{l} = size(sino);
            [sino, H{l}, V{l}, D{l}] = dwt2(sino,settings.wavelet_Wname);
        end
        
        % filter horizontal frequency bands
        for l = 1:settings.wavelet_DecNum
            % FFT
            horzFourier = fft(H{l}');
            
            % damping of horizontal stripe information
			m = size(horzFourier,1);
            k = fftfreq(m, 2*pi/m);
            dampGauss = 1 - exp(-1/2 * k.^2 / settings.wavelet_Sigma^2 );
            horzFourier = horzFourier .* dampGauss;
            
            % inverse FFT
            H{l} = ifft(horzFourier)';
        end
        
		% wavelet reconstruction
		sinoCorrected = sino;
		for l = settings.wavelet_DecNum:-1:1
			sinoCorrected = sinoCorrected(1:size(V{l},1),1:size(V{l},2));
			sinoCorrected = idwt2(sinoCorrected,H{l}, V{l}, D{l},settings.wavelet_Wname, S{l});
		end	
        
    otherwise
        error('Choose between ringremove based on "additive" or "wavelet" correction for setting "method"');
end

end



