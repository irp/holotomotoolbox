function filterFBP = getFBPFilter(len, settings)
% GETFBPFILTER returns the Fourier-space filter of the filtered back projection.
%
% Parameters
% ----------
% len : positive integer
%     number of samples
% settings : structure, optional
%     contains additional settings, see *Other Parameters*
%
% Other Parameters
% ----------------
% filter : Default = 'ram-lak'
%     Type of filter. Choose between 'ram-lak', 'shepp-logan', 'cosine', 'hamming'
%     and 'none'.
% cutoff : Default = 1
%     Relative cutoff frequency (possible range: 0-1). 
% angles : Default = []
%     Number of angles for few-angles approximation.
% cropFreq : Default = 1
%     Specifies normalized frequency at which to crop the frequency response of the
%     filter.
%
% Returns
% -------
% filterFBP : vector
%     Fourier-space filter
%
% Example
% -------
%
% .. code-block:: matlab
%
%     settings = getFBPFilter;
%     settings.filter = 'ram-lak';
%     filterFBP = getFBPFilter(100, settings);
% 
%     plot(filterFBP)

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

if nargin < 2
    settings = struct;
end

defaults.filter = 'ram-lak';
defaults.cutoff = 1;
defaults.angles = [];
defaults.cropFreq = 1;

if (nargin == 0)
    filterFBP = defaults;
    return
end

settings = completeStruct(settings, defaults);

order = max(64,2^nextpow2(2*len));

if strcmpi(settings.filter, 'none')
    filterFBP = ones(1, order);
    return;
end

if (settings.cutoff > 1 || settings.cutoff < 0)
    error('invalid cutoff setting')
end

% First create a bandlimited ramp filter (Eqn. 61 Chapter 3, Kak and
% Slaney) - go up to the next highest power of 2.

n = 0:(order/2); % 'order' is always even.
filtImpResp = zeros(1,(order/2)+1); % 'filtImpResp' is the bandlimited ramp's impulse response (values for even n are 0)
filtImpResp(1) = 1/4; % Set the DC term
filtImpResp(2:2:end) = -1./((pi*n(2:2:end)).^2); % Set the values for odd n

filtImpResp = [filtImpResp filtImpResp(end-1:-1:2)];
filterFBP = 2*real(fft(filtImpResp));
filterFBP = filterFBP(1:(order/2)+1);

w = 2*pi*(0:size(filterFBP,2)-1)/order;   % frequency axis up to Nyquist

switch settings.filter
    case 'ram-lak'
        % Do nothing
    case 'shepp-logan'
        % be careful not to divide by 0:
        filterFBP(2:end) = filterFBP(2:end) .* (sin(w(2:end)/(2*settings.cropFreq)) ./ ...
            (w(2:end)/(2*settings.cropFreq)));
    case 'cosine'
        filterFBP(2:end) = filterFBP(2:end) .* cos(w(2:end)/(2*settings.cropFreq));
    case 'hamming'
        filterFBP(2:end) = filterFBP(2:end) .* (.54 + .46 * cos(w(2:end)/settings.cropFreq));
    case 'hann'
        filterFBP(2:end) = filterFBP(2:end) .*(1+cos(w(2:end)./settings.cropFreq)) / 2;
    otherwise
        error('invalid filter')
end

filterFBP(w > pi*settings.cutoff) = 0;              % Crop the frequency response
filterFBP = [filterFBP' ; filterFBP(end-1:-1:2)'];  % Symmetrize the filter

% fewangle cutoff by Aike Ruhland
if (~isempty(settings.angles))
    cutoff = settings.angles / (len*pi/2);
    filterFBP(filterFBP > cutoff) = cutoff;
end

end
