function projs = radon3Darray(vol, tomoAngles, settings)
% RADON3DARRAY performs the radon transform for an entire 3d object.
%
%    ``projs = radon3Darray(vol, tomoAngles, settings)``
%
% Performs the radon transform for an entire 3d object along a specified axis by
% performing the individual 2D radon transforms via a for-loop over the respective
% dimension of the object. This results in 2d projections as, e.g., acquired by area
% detectors.
%
% Parameters
% ----------
% vol : numerical array
%     volume of which the projections are created
% tomoAngles : vector 
%     angles at which the projections are created
% settings : structure, optional
%     contains additional settings, see *Other Parameters*
%
% Other Parameters
% ----------------
% outputSize : Default = []
%     Horizontal size of the projections. The default of [] results in the default
%     size of Matlab's radon transform.
% rotationAxis : Default = 3
%     Axis along which the object is rotated. 1: along y-axis, 2: along x-axis, 3:
%     along z-axis
%
% Returns
% -------
% projs : numerical array
%     projections of the given volume along the specified rotation axis
%
%
% Example
% -------
%
% .. code-block:: matlab
%
%     vol = ones([132,112,84]);
%     vol = padarray(vol, ([200,200,200]-size(vol))/2);
%     angles = (0:179);
%     settings.rotationAxis = 1;
%     projs = radon3Darray(vol, angles, settings);
% 
%     showImage(projs(:,:,randi(numel(angles))));


% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

if (nargin < 3)
    settings = struct;
end

% defaults
defaults.outputSize = [];       
defaults.rotationAxis = 3;    

% return default settings
if (nargin == 0)
    projs = defaults;
    return
end

settings = completeStruct(settings, defaults);

% Input and output dimensions
sizeVol = size(vol);
numSlices = sizeVol(settings.rotationAxis);
sizeSlice = sizeVol(setdiff([1,2,3], settings.rotationAxis));
sizeRadon = 2*ceil(norm(sizeSlice-floor((sizeSlice-1)/2)-1))+3;
if isempty(settings.outputSize)
    settings.outputSize = sizeRadon;
end

% Compute Radon transform for each slice
projs = zeros([numSlices, settings.outputSize, numel(tomoAngles)], class(vol));
for sliceIdx = 1:numSlices
    if settings.outputSize ~= sizeRadon
        projs(sliceIdx,:,:) = padToSize(radon(squeeze(getSlice(vol, sliceIdx, ...
            settings.rotationAxis)), tomoAngles),[settings.outputSize length(tomoAngles)]);
    else
        projs(sliceIdx,:,:) = radon(squeeze(getSlice(vol, sliceIdx, settings.rotationAxis)), ...
            tomoAngles);
    end
    
end

end

