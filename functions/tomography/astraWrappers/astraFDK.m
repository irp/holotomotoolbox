function vol = astraFDK(projs,tomoAngles,z01,z02,dx,settings)
% ASTRAFDK performs FDK tomography reconstruction via the ASTRA Toolbox.
%
%    ``vol = astraFDK(projs,tomoAngles,z01,z02,dx,settings)``
%
% Reconstructs a tomographic volume which was recorded in cone-beam geometry using
% the FDK implementation of the ASTRA tomography toolbox
% (https://www.astra-toolbox.com/) :cite:`Aarle_OE_2016,Aarle_U_2015,Palenstijn_JoSB_2011`. Hence, the ASTRA tomography
% toolbox has to be installed and added to the Matlab path in order for this function
% to work. Important: All input parameters (z01, z02 and dx) must have the
% same dimensions (e.g. all mm or all m). If the reconstruction still shows
% artifacts, especially at the rim of the volume, try changing the sign of the input
% parameter tomoAngles.
%
% Parameters
% ----------
% projs : numerical array
%     set of tomographic projections
% tomoAngles : vector
%     angles at which the projections were recorded
% z01 : number
%     distance between the source and the sample
% z02 : number
%     distance between the source and the detector
% dx : number
%     physical pixel size of the detector
% settings : structure, optional
%     contains additional settings, see *Other Parameters*
%
% Other Parameters
% ----------------
% outputSize : Default = []
%     Size of the reconstructed slices. The default of [] results in the same size as
%     the output of Matlab's iradon function.
% shortScan : Default = 1
%     If the angular range is smaller than 360 deg, shortScan has to be 1, which
%     applies additional weighting accounting for the restricted scan range.
%     Otherwise the parameter has to be 0.
% numSlices : Default = 1
%     Number of slices to be reconstructed
% offset : Default = 0
%     Offset of the reconstructed volume to the central slice of detector.
%     Can be used to compensate a shift between source and detector height or
%     to reconstruct a subvolume.
% iradonOrientation : Default = true
%     Orientation of the returned volume. iradonOrientation = true returns
%     projections with the same orientation as Matlab's iradon function.
%
% Returns
% -------
% vol : numerical array
%     reconstructed volume
%
% See also
% --------
% functions.tomography.astraWrappers.astraConeProjection,
% functions.tomography.astraWrappers.astraParProjection2D,
% functions.tomography.astraWrappers.astraParProjection3D, functions.tomography.astraWrappers.astraFBP,
% functions.tomography.astraWrappers.astraBackproj3D
%
% Example
% -------
%
% .. code-block:: matlab
%
%     phantom = create3Dphantom(256);
%     N = size(phantom,2);
%     dx = 10e-6;
%     magnification = 3;
%     dx_eff = dx/magnification;
%     sourceSample = 0.1;
%     sourceDetector = sourceSample*magnification;
%     coneAngle = 2*atand((N/2*dx_eff)/((sourceSample)));
%     fprintf('Full cone angle: %4.2f degree\n',coneAngle)
%     numProjs = 400;
%     tomoAngles = linspace(0,180 + ceil(coneAngle),numProjs);
%
%     settingsProjs = astraConeProjection;
%     settingsProjs.radonOrientation = true;
%     projs = astraConeProjection(phantom,tomoAngles,sourceSample,...
%         sourceDetector,dx,settingsProjs);
%
%     settingsVol = astraFDK;
%     settingsVol.outputSize = size(phantom,1);
%     settingsVol.numSlices = size(projs,1);
%     settingsVol.offset = 0;
%     settingsVol.shortScan = 1;
%     settingsVol.iradonOrientation = true;
%
%     vol = astraFDK(projs,tomoAngles,sourceSample,sourceDetector,...
%         dx,settingsVol);
%
%     showImage(vol(:,:,150))
%
% See also ASTRACONEPROJECTION, ASTRAPARPROJECTION2D, ASTRAPARPROJECTION3D, ASTRAFBP,
% ASTRABACKPROJ3D

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

if (nargin < 6)
    settings = struct;
end

% defaults
defaults.outputSize = [];
defaults.shortScan = 1;
defaults.numSlices = 1;
defaults.offset = 0;
defaults.iradonOrientation = true;

% return default settings
if (nargin == 0)
    vol = defaults;
    return
end

settings = completeStruct(settings, defaults);

% dependent settings
if isempty(settings.outputSize)
    % as in iradon
    settings.outputSize = 2*floor(size(projs,2)/(2*sqrt(2)));
end

% check and correct size of projs
if ndims(projs)<3
    projs= reshape(projs, [1 size(projs,1) size(projs,2)]);
end

%% check usage
if numel(tomoAngles) ~= size(projs,3)
    error('Number of tomographic angles does not match number of projections');
end


% Special case: complex-valued arrays are processed separately for real- imaginary parts
if ~isreal(projs)
    vol = astraFDK(real(projs),tomoAngles,z01,z02,dx,settings) ...
         + 1i * astraFDK(imag(projs),tomoAngles,z01,z02,dx,settings);
    return;
end


% create volume geometry
% input:
% size of volume in y, size of volume in x, size of volume in z,
% minimum coordinate in x, maximum coordinate in x, minimum coordinate in y, maximum
% coordinate in y, minimum coordinate in z, maximum coordinate in z
volGeom = astra_create_vol_geom(settings.outputSize, settings.outputSize, ...
    settings.numSlices, -settings.outputSize/2, settings.outputSize/2, ...
    -settings.outputSize/2, settings.outputSize/2, settings.offset - settings.numSlices/2, ...
    settings.offset + settings.numSlices/2);
% create initial volume for the reconstruction
volID = astra_mex_data3d('create', '-vol', volGeom);

% create projection geometry
M = z02/z01;
dx_eff = dx/M;
projGeom = astra_create_proj_geom('cone', M, M, size(projs,1), size(projs,2), ...
    -tomoAngles/180*pi, z01/dx_eff, (z02 - z01)/dx_eff);

% Adjust x/y/angle-axis-ordering to ASTRA's standard 
projs = permute(projs,[2 3 1]);


% load projection data into memory 
projID = astra_mex_data3d('create','-proj3d', projGeom, projs);

% set up the parameters for a reconstruction algorithm using the GPU
cfg = astra_struct('FDK_CUDA');
cfg.ReconstructionDataId = volID;
cfg.ProjectionDataId = projID;
cfg.option.ShortScan = settings.shortScan;

% create the algorithm object from the configuration structure
algID = astra_mex_algorithm('create', cfg);
% perform the operation
astra_mex_algorithm('run',algID);
% get the result
vol = astra_mex_data3d('get_single',volID);

% same orientation as ouput of iradon
if settings.iradonOrientation
    vol = permute(vol,[2 1 3]);
end

% clean up
astra_mex_algorithm('delete', algID);
astra_mex_data3d('delete', projID);
astra_mex_data3d('delete', volID);
end
