function vol = astraFBP(projs,tomoAngles,settings)
% ASTRAFDK performs filtered backprojection via the ASTRA Toolbox.
%
%    ``vol = astraFBP(projs,tomoAngles,settings)``
%
% Reconstructs a tomographic slice which was recorded in parallel-beam geometry using
% the FBP implementation of the ASTRA tomography toolbox
% (https://www.astra-toolbox.com/) :cite:`Aarle_OE_2016,Aarle_U_2015,Palenstijn_JoSB_2011`. Hence, the ASTRA tomography
% toolbox has to be installed and added to the Matlab path in order for this function
% to work.
%
% Parameters
% ----------
% projs : numerical array
%     sinogram/projections of the slice(s) to be reconstructed
% tomoAngles : vector
%     angles at which the projections were recorded
% settings : structure, optional
%     contains additional settings, see *Other Parameters*
%
% Other Parameters
% ----------------
% outputSize : Default = []
%     Size of the reconstructed slices. The default of [] results in the same size as
%     the output of Matlab's iradon function.
% filter : Default = 'Ram-Lak'
%     Specifies which filter is used for the filtered backprojection.
%
% Returns
% -------
% vol : numerical array
%     reconstructed slice(s)
%
% See also
% --------
% functions.tomography.astraWrappers.astraBackproj3D,
% functions.tomography.astraWrappers.astraParProjection2D,
% functions.tomography.astraWrappers.astraParProjection3D, functions.tomography.astraWrappers.astraFDK,
% functions.tomography.astraWrappers.astraConeProjection
%
% Example
% -------
%
% .. code-block:: matlab
%
%     %% 2d example
%     slice2D = phantom(256);
%     tomoAngles = linspace(0,180,300);
%     sinogram = astraParProjection2D(slice2D,tomoAngles);
%
%     sliceReconstructed = astraFBP(sinogram,tomoAngles);
%
%     showImage(sliceReconstructed)
%
%     %% 3d example
%     vol = create3Dphantom(256);
%     tomoAngles = linspace(0,180,300);
%     sinogram = astraParProjection3D(vol,tomoAngles);
%
%     volReconstructed = astraFBP(sinogram,tomoAngles);
% 
%     showOrthoslices(volReconstructed)
%
% See also ASTRABACKPROJ3D, ASTRAPARPROJECTION2D, ASTRAPARPROJECTION3D, ASTRAFDK,
% ASTRACONEPROJECTION

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

if (nargin < 3)
    settings = struct;
end

% defaults
defaults.outputSize = [];
defaults.filter = 'Ram-Lak';

% return default settings
if (nargin == 0)
    vol = defaults;
    return
end

settings = completeStruct(settings, defaults);

% dependent settings
if isempty(settings.outputSize)
    % as in iradon
    settings.outputSize = 2*floor(size(projs,ndims(projs)-1)/(2*sqrt(2)));
end

%% check usage
if numel(tomoAngles) ~= size(projs,ndims(projs))
    error('Number of tomographic angles does not match size of projections');
end

% Special case: complex-valued arrays are processed separately for real- imaginary parts
if ~isreal(projs)
    vol = astraFBP(real(projs),tomoAngles,settings) ...
         + 1i * astraFBP(imag(projs),tomoAngles,settings);
    return;
end

%% here it starts
% create volume geometry
volGeom = astra_create_vol_geom(settings.outputSize,settings.outputSize);
% create initial volume for the reconstruction
volID = astra_mex_data2d('create', '-vol', volGeom);

% create projection geometry
projGeom = astra_create_proj_geom('parallel', 1, size(projs,ndims(projs)-1), tomoAngles/180*pi);
% load projection (sinogram) data into memory
projID = astra_mex_data2d('create', '-sino', projGeom);

% set up the parameters for a reconstruction algorithm using the GPU
cfg = astra_struct('FBP_CUDA');
cfg.ReconstructionDataId = volID;
cfg.ProjectionDataId = projID;
cfg.FilterType = settings.filter;
% possible values for FilterType:
% none, ram-lak, shepp-logan, cosine, hamming, hann, tukey, lanczos,
% triangular, gaussian, barlett-hann, blackman, nuttall, blackman-harris,
% blackman-nuttall, flat-top, parzen

% permute projections and preallocate memory for the reconstructed volume
projs = squeeze(permute(projs, [3 2 1]));
vol = zeros([settings.outputSize, settings.outputSize, size(projs,3)], 'single');
% perform reconstruction slice by slice
for indSlice = 1:size(projs,3)
    astra_mex_data2d('set', projID, projs(:,:,indSlice));

    % create, run and delete FBP-algorithm on slice
    % (deleting for every slice is important to prevent memory-leaks)
    algID = astra_mex_algorithm('create', cfg);
    astra_mex_algorithm('run', algID);
    astra_mex_algorithm('delete', algID);

    % get the result
    vol(:,:,indSlice) = astra_mex_data2d('get_single', volID);
end

% clean up
astra_mex_data2d('delete', projID);
astra_mex_data2d('delete', volID);

end
