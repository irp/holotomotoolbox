function vol = astraBackproj3D(projs,tomoAngles,settings)
% ASTRABACKPROJ3D performs backprojection of 3d array via the ASTRA Toolbox.
%
%    ``vol = astraBackproj3D(projs,tomoAngles,settings)``
%
% Backprojects a tomographic volume without filtering which was recorded in
% parallel-beam geometry using the ASTRA tomography toolbox
% (https://www.astra-toolbox.com/) :cite:`Aarle_OE_2016,Aarle_U_2015,Palenstijn_JoSB_2011`. Hence, the ASTRA tomography
% toolbox has to be installed and added to the Matlab path in order for this function
% to work.
%
% Parameters
% ----------
% projs : numerical array
%     set of tomographic projections
% tomoAngles : vector
%     angles at which the projections were recorded
% settings : structure, optional
%     contains additional settings, see *Other Parameters*
%
% Other Parameters
% ----------------
% outputSize : Default = []
%     Size of the reconstructed slices. The default of [] results in the same size as
%     the output of Matlab's iradon function.
% iradonOrientation : Default = true
%     Orientation of the returned volume. iradonOrientation = true returns
%     projections with the same orientation as Matlab's iradon function.
%
% Returns
% -------
% vol : numerical array
%     reconstructed volume
%
%
% See also
% --------
% functions.tomography.astraWrappers.astraParProjection2D,
% functions.tomography.astraWrappers.astraParProjection3D, functions.tomography.astraWrappers.astraFBP,
% functions.tomography.astraWrappers.astraFDK, functions.tomography.astraWrappers.astraConeProjection
%
% Example
% -------
%
% .. code-block:: matlab
%
%     phantom = create3Dphantom(256);
%     tomoAngles = linspace(0,180,400);
%     projs = astraParProjection3D(phantom,tomoAngles);
%
%     vol = astraBackproj3D(projs,tomoAngles);
%
%     showImage(vol(:,:,150))
%
% See also ASTRAPARPROJECTION2D, ASTRAPARPROJECTION3D, ASTRAFBP, ASTRAFDK,
% ASTRACONEPROJECTION

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

if (nargin < 3)
    settings = struct;
end

% defaults
defaults.outputSize = [];
defaults.iradonOrientation = true;

% return default settings
if (nargin == 0)
    vol = defaults;
    return
end

settings = completeStruct(settings, defaults);

% dependent settings
if isempty(settings.outputSize)
    % as in iradon
    settings.outputSize = 2*floor(size(projs,2)/(2*sqrt(2)));
end

%% check usage
if numel(tomoAngles) ~= size(projs,3)
    error('Number of tomographic angles does not match number of projections');
end

% Special case: complex-valued arrays are processed separately for real- imaginary parts
if ~isreal(projs)
    vol = astraBackproj3D(real(projs),tomoAngles,settings) ...
         + 1i * astraBackproj3D(imag(projs),tomoAngles,settings);
    return;
end

%% here it starts
% create projection geometry
projGeom = astra_create_proj_geom('parallel3d', 1.0, 1.0, size(projs,1), size(projs,2), ...
    -tomoAngles/180*pi);

% Adjust x/y/angle-axis-ordering to ASTRA's standard
projs = permute(projs,[2 3 1]);

% load projection data into memory 
if isa(projs,'single')
    % read-only link
    projID = astra_mex_data3d('link','-proj3d', projGeom, projs, 1);
else
    % create initial volume for the projections and load data into memory
    projID = astra_mex_data3d('create','-proj3d', projGeom, projs);
end

% create volume geometry
volGeom = astra_create_vol_geom(settings.outputSize,settings.outputSize,size(projs,1));
volID = astra_mex_data3d('create', '-vol', volGeom);

% set up the parameters for a reconstruction algorithm using the GPU
cfg = astra_struct('BP3D_CUDA');
cfg.ReconstructionDataId = volID;
cfg.ProjectionDataId = projID;

% create the algorithm object from the configuration structure
algID = astra_mex_algorithm('create', cfg);
% perform the operation
astra_mex_algorithm('run', algID);
% get the result
vol = astra_mex_data3d('get_single', volID);

% same orientation as output of iradon
if settings.iradonOrientation
    vol = permute(vol,[2 1 3]);
end

% clean up
astra_mex_algorithm('delete', algID);
astra_mex_data3d('delete', projID);
astra_mex_data3d('delete', volID);

end

