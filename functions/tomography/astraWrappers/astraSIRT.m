function vol = astraSIRT(projs,tomoAngles,settings)
% ASTRASIRT performs SIRT tomography reconstruction via the ASTRA Toolbox.
%
%    ``vol = astraSIRT(projs,tomoAngles,settings)``
%
% Reconstructs a tomographic volume which was recorded in cone-beam geometry using
% the implementation of the SIRT algorithm of the ASTRA tomography toolbox
% (https://www.astra-toolbox.com/) :cite:`Aarle_OE_2016,Aarle_U_2015,Palenstijn_JoSB_2011`. 
% Hence, the ASTRA tomography toolbox has to be installed and added to the Matlab path
% in order for this function to work.
%
% Parameters
% ----------
% projs : numerical array
%     set of tomographic projections
% tomoAngles : vector
%     angles at which the projections were recorded
%
% Other Parameters
% ----------------
% iterations : Default = 100
%     Number of SIRT-iterations to be performed.
% outputSize : Default = []
%     Size of the reconstructed slices. The default of [] results in the same size as
%     the output of Matlab's iradon function.
% initialGuess : Default = []
%     Initial guess for the reconstructed volume. The SIRT algorithm is initialized
%     with this array. If empty, the algorithm is initialized with zero.
%     Must satisfy size(initialGuess) == [outputSize, outputSize, size(projs,1)]
% minVal : Default = -inf
%     Optional minimum constraint for the values of vol: it is imposed that
%     all(real(vol)) >= minVal.
% maxVal : Default = inf
%     Optional maximum constraint for the values of vol: it is imposed that
%     all(real(vol)) <= maxVal.
% minImag : Default = -inf
%     Optional minimum constraint for the imaginary part of vol: if the array projs is
%     complex-valued, it is imposed that all(imag(vol)) >= minImag. If projs is real-valued
%     this setting has no effect.
% maxImag : Default = inf
%     Optional maximum constraint for the imaginary part of vol: if the array projs is
%     complex-valued, it is imposed that all(imag(vol)) <= maxImag. If projs is real-valued
%     this setting has no effect.
% iradonOrientation : Default = false
%     Orientation of the returned volume. iradonOrientation = true returns
%     projections with the same orientation as Matlab's iradon function.
%
% Returns
% -------
% vol : numerical array
%     reconstructed volume
%
% See also
% --------
% functions.tomography.astraWrappers.astraConeProjection,
% functions.tomography.astraWrappers.astraParProjection2D,
% functions.tomography.astraWrappers.astraParProjection3D, functions.tomography.astraWrappers.astraFBP,
% functions.tomography.astraWrappers.astraBackproj3D
% functions.tomography.astraWrappers.astraFDK
%
% Example
% -------
%
% .. code-block:: matlab
%
%     phantomSettings.numObjects = 50;
%     phantom = create3Dphantom(256, phantomSettings);
%     tomoAngles = (0:179);
%     projs = astraParProjection3D(phantom, tomoAngles);
%
%     reconSettings.minVal = 0;
%     volRecon = astraSIRT(projs, tomoAngles, reconSettings);
%
%     plotSettings.figIdx = figure('name', 'Phantom'); 
%     showOrthoslices(phantom, plotSettings);
%
%     plotSettings.figIdx = figure('name', 'SIRT-reconstruction'); 
%     showOrthoslices(volRecon, plotSettings);
%     
%
% See also ASTRACONEPROJECTION, ASTRAPARPROJECTION2D, ASTRAPARPROJECTION3D, ASTRAFBP,
% ASTRABACKPROJ3D, ASTRAFDK

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

if (nargin < 3)
    settings = struct;
end

% defaults
defaults.iterations = 100;
defaults.outputSize = [];
defaults.initialGuess =  [];
%defaults.support = [];
defaults.minVal = -inf;
defaults.maxVal = inf;
defaults.minImag = -inf;
defaults.maxImag = inf;
defaults.iradonOrientation = false;

% return default settings
if (nargin == 0)
    vol = defaults;
    return
end

settings = completeStruct(settings, defaults);

% dependent settings
if isempty(settings.outputSize)
    % as in iradon
    settings.outputSize = 2*floor(size(projs,2)/(2*sqrt(2)));
end

% check usage
if numel(tomoAngles) ~= size(projs,3)
    error('Number of tomographic angles does not match number of projections');
end



% Special case: complex-valued arrays are processed separately for real- imaginary parts
if ~isreal(projs)
    settingsImag = settings;
    settingsImag.minVal = settings.minImag;
    settingsImag.maxVal = settings.maxImag;
    vol = astraSIRT(real(projs), tomoAngles, settings) ...
         + 1i * astraSIRT(imag(projs), tomoAngles, settingsImag);
    return;
end



% create volume geometry
volGeom = astra_create_vol_geom(settings.outputSize, settings.outputSize, size(projs,1));
%
% initialize volume with initial guess if given
if isempty(settings.initialGuess)
    volID = astra_mex_data3d('create', '-vol', volGeom);
else
    if isa(settings.initialGuess, 'single')
        % read-only link
        volID = astra_mex_data3d('link', '-vol', volGeom, settings.initialGuess, 1);
    else
        % create initial volume for the projections and load data into memory
        volID = astra_mex_data3d('create', '-vol', volGeom, settings.initialGuess);
    end
end


% create projection geometry
projGeom = astra_create_proj_geom('parallel3d', 1.0, 1.0, size(projs,1), size(projs,2), ...
    -tomoAngles/180*pi);

% Adjust x/y/angle-axis-ordering to ASTRA's standard
projs = permute(projs,[2 3 1]);

% load projection data into memory 
if isa(projs,'single')
    % read-only link
    projID = astra_mex_data3d('link','-proj3d', projGeom, projs, 1);
else
    % create initial volume for the projections and load data into memory
    projID = astra_mex_data3d('create','-proj3d', projGeom, projs);
end


% configure SIRT algorithm
cfg = astra_struct('SIRT3D_CUDA');
cfg.ReconstructionDataId = volID;
cfg.ProjectionDataId = projID;
%
% Optional minimum-constraint
if settings.minVal > -inf;
    cfg.option.MinConstraint = settings.minVal;
end
%
% Optional maximum-constraint
if settings.maxVal < inf;
    cfg.option.MaxConstraint = settings.maxVal;
end


% create and run SIRT algorithm
algID = astra_mex_algorithm('create', cfg);
astra_mex_algorithm('iterate', algID, settings.iterations);
astra_mex_algorithm('delete', algID);
vol = astra_mex_data3d('get_single', volID);

% same orientation as ouput of iradon
if settings.iradonOrientation
    vol = permute(vol,[2 1 3]);
end


% clean up
astra_mex_algorithm('delete', algID);
astra_mex_data3d('delete', projID);
astra_mex_data3d('delete', volID);


end
