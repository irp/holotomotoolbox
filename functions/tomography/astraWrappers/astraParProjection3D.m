function projs = astraParProjection3D(vol,tomoAngles,settings)
% ASTRAPARPROJECTION3D creates projections from a 3d volume via the ASTRA toolbox.
%
%    ``projs = astraParProjection3D(vol,tomoAngles,settings)``
%
% Creates projections of a given volume in parallel-beam geometry for a specified
% angular range using the ASTRA tomography toolbox (https://www.astra-toolbox.com/)
% :cite:`Aarle_OE_2016,Aarle_U_2015,Palenstijn_JoSB_2011`. Hence, the ASTRA tomography toolbox has to be installed and
% added to the Matlab path in order for this function to work.
%
% Parameters
% ----------
% vol : numerical array
%     volume of which the projections are created
% tomoAngles : vector
%     angles at which the projections are created
% settings : structure, optional
%     contains additional settings, see *Other Parameters*
%
% Other Parameters
% ----------------
% outputSize : Default = []
%     Size of the projections in second direction (first direction is given by the
%     size of the volume along the rotation axis). The default of [] results in the
%     same size as the output of Matlab's radon transform.
% radonOrientation : Default = true
%     Orientation of the returned projections. radonOrientation = true returns
%     projections with the same orientation as Matlab's radon transform.
%
% Returns
% -------
% projs : numerical array
%     projections of the given volume in parallel-beam geometry
%
% See also
% --------
% functions.tomography.astraWrappers.astraParProjection2D, functions.tomography.astraWrappers.astraFBP,
% functions.tomography.astraWrappers.astraBackproj3D,
% functions.tomography.astraWrappers.astraConeProjection, functions.tomography.astraWrappers.astraFDK
%
% Example
% -------
%
% .. code-block:: matlab
%
%     phantom = create3Dphantom(256);
%
%     settings = astraParProjection3D;
%     settings.radonOrientation = true;
%     projs = astraParProjection3D(phantom,linspace(0,180,400),settings);
%
%     showImage(projs(:,:,400))
%
% See also ASTRAPARPROJECTION2D, ASTRAFBP, ASTRABACKPROJ3D, ASTRACONEPROJECTION,
% ASTRAFDK

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

if (nargin < 3)
    settings = struct;
end

% defaults
defaults.outputSize = [];
defaults.radonOrientation = true;

% return default settings
if (nargin == 0)
    projs = defaults;
    return
end

settings = completeStruct(settings, defaults);

% dependent settings
if isempty(settings.outputSize)
    % as in radon
    settings.outputSize = 2*ceil(norm([size(vol,1) size(vol,2)] - ...
        floor(([size(vol,1) size(vol,2)] - 1)/2) - 1)) + 3;
end

settings.outputSize = [size(vol,3) settings.outputSize];

% Special case: complex-valued arrays are processed separately for real- imaginary parts
if ~isreal(vol)
    projs = astraParProjection3D(real(vol),tomoAngles,settings) ...
           + 1i * astraParProjection3D(imag(vol),tomoAngles,settings);
    return;
end

%% here it starts
% same orientation as output of iradon
if settings.radonOrientation
    vol = permute(vol,[2 1 3]);
end

% create volume geometry
volGeom = astra_create_vol_geom(size(vol,2),size(vol,1),size(vol,3));
if isa(vol,'single')
    % read-only link
    volID = astra_mex_data3d('link','-vol', volGeom, vol, 1);
else
    % create initial volume for the input volume and load data into memory
    volID = astra_mex_data3d('create','-vol', volGeom, vol);
end

% create projection geometry
projGeom = astra_create_proj_geom('parallel3d', 1.0, 1.0, settings.outputSize(1), ...
    settings.outputSize(2), -tomoAngles/180*pi);
% create initial volume for the projections
projID = astra_mex_data3d('create', '-sino', projGeom);

% set up the parameters for a reconstruction algorithm using the GPU
cfg = astra_struct('FP3D_CUDA');
cfg.ProjectionDataId = projID;
cfg.VolumeDataId = volID;

% create the algorithm object from the configuration structure
algID = astra_mex_algorithm('create', cfg);
% perform the operation
astra_mex_algorithm('run', algID);
% get the result
projs = astra_mex_data3d('get_single', projID);

% clean up
astra_mex_algorithm('delete', algID);
astra_mex_data3d('delete', projID);
astra_mex_data3d('delete', volID);

projs = permute(projs,[3 1 2]);
end

