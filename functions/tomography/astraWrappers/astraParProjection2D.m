function sinogram = astraParProjection2D(slice2D,tomoAngles,settings)
% ASTRAPARPROJECTION creates sinogram of a 2d slice via the ASTRA toolbox.
%
%    ``sinogram = astraParProjection2D(slice2D,tomoAngles,settings)``
%
% Creates sinogram of a given 2d slice in parallel-beam geometry  for a specified
% angular range using the ASTRA tomography toolbox (https://www.astra-toolbox.com/)
% :cite:`Aarle_OE_2016,Aarle_U_2015,Palenstijn_JoSB_2011`. Hence, the ASTRA tomography toolbox has to be installed and
% added to the Matlab path in order for this function to work.
%
% Parameters
% ----------
% slice2D : numerical array
%     slice for which the sinogram is created
% tomoAngles : vector
%     angles for which the sinogram is created
% settings : structure, optional
%     contains additional settings, see *Other Parameters*
%
% Other Parameters
% ----------------
% outputSize : Default = []
%     Size of the sinogram in second direction (not along tomographic angle). The
%     default of [] results in the same size as the output of Matlab's radon
%     transform.
% radonOrientation : Default = true
%     Orientation of the returned sinogram. radonOrientation = true returns
%     a sinogram with the same orientation as Matlab's radon transform.
%
% Returns
% -------
% sinogram : numerical array
%     sinogram of the given slice in parallel-beam geometry
%
% See also
% --------
% functions.tomography.astraWrappers.astraParProjection3D, functions.tomography.astraWrappers.astraFBP,
% functions.tomography.astraWrappers.astraBackproj3D,
% functions.tomography.astraWrappers.astraConeProjection, functions.tomography.astraWrappers.astraFDK
%
% Example
% -------
%
% .. code-block:: matlab
%
%     slice2D = phantom(256);
%     tomoAngles = linspace(0,180,300);
%     sinogram = astraParProjection2D(slice2D,tomoAngles);
%
%     showImage(sinogram)
%
% See also ASTRAPARPROJECTION3D, ASTRAFBP, ASTRABACKPROJ3D, ASTRACONEPROJECTION,
% ASTRAFDK

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

if (nargin < 3)
    settings = struct;
end

% defaults
defaults.outputSize = [];
defaults.radonOrientation = true;

% return default settings
if (nargin == 0)
    sinogram = defaults;
    return
end

settings = completeStruct(settings, defaults);

% dependent settings
if isempty(settings.outputSize)
    % as in radon
    settings.outputSize = 2*ceil(norm([size(slice2D,1) size(slice2D,2)] - ...
        floor(([size(slice2D,1) size(slice2D,2)] - 1)/2) - 1)) + 3;
end

% Complex-valued arrays are processed separately for real- imaginary parts
if ~isreal(slice2D)
    sinogram = astraParProjection2D(real(slice2D),tomoAngles,settings) ...
              + 1i * astraParProjection2D(imag(slice2D),tomoAngles,settings);
    return;
end

%% here it starts
% create volume geometry
volGeom = astra_create_vol_geom(size(slice2D,1),size(slice2D,2));
% create initial volume for the input slice
volID = astra_mex_data2d('create', '-vol', volGeom, slice2D);

% create projection (sinogram) geometry
projGeom = astra_create_proj_geom('parallel', 1.0, settings.outputSize, tomoAngles/180*pi);
% create initial volume for the projections (sinogram) and load data into memory
projID = astra_mex_data2d('create','-sino', projGeom);

% set up the parameters for a reconstruction algorithm using the GPU
cfg = astra_struct('FP_CUDA');
cfg.ProjectionDataId = projID;
cfg.VolumeDataId = volID;

% create the algorithm object from the configuration structure
fpID = astra_mex_algorithm('create', cfg);
% perform the operation
astra_mex_algorithm('run', fpID);
% get the result
sinogram = astra_mex_data2d('get_single', projID);

% clean up
astra_mex_algorithm('delete', fpID);
astra_mex_data2d('delete', projID);
astra_mex_data2d('delete', volID);

% same orientation as output of radon
if settings.radonOrientation
    sinogram = sinogram.';
end

end

