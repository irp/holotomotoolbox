%runtests('unitTests_tomography.m')

function tests = unitTests_tomography
tests = functiontests(localfunctions);
end

% This is how a test environement looks like:
%
% the function name has to start with test_...
% to run the test, use the command runtests('unitTests_tomography.m')
%
%function test_functionName(testCase)
% https://de.mathworks.com/help/matlab/ref/matlab.unittest.qualifications.verifiable.verifyequal.html
% verifyEqual(verifiable,actual,expected)
% verifyEqual(___,Name,Value)
% verifyEqual(___,diagnostic)
%end

%------------------------- alignProfiles---------------------------------%

function test_alignProfiles(testCase)
% get current filename and path and add toolbox to genpath
currfile = mfilename('fullpath') ;
currpath = fileparts(currfile) ;
pathToTools = fullfile(currpath,'..') ;
addpath(genpath(pathToTools )) ;

% create original profile
original = rand([100,1]);

% create a shifted profile
profShifted = circshiftSubPixel(original,[3 0]);

% apply alignProfiles
shiftval = alignProfiles(profShifted,original);
correctedProfile = circshiftSubPixel(profShifted,[shiftval 0]);

% Compare result and original image
verifyEqual(testCase,correctedProfile,original,'RelTol', 0.1 );

end

%------------------------- ringremove---------------------------------%

function test_ringremove(testCase)
% get current filename and path and add toolbox to genpath
currfile = mfilename('fullpath') ;
currpath = fileparts(currfile) ;
pathToTools = fullfile(currpath,'..') ;
addpath(genpath(pathToTools )) ;

% create original sinogram
sinoOriginal = ones(180,500);

% create a corrupted sinogram
numRings = 100;
maxAddedValue = 0.5;
sinoRings = sinoOriginal;
for i = 1:numRings
    position = rand;
    strength = -2*maxAddedValue*rand + maxAddedValue;
    sinoRings(ceil(position*size(sinoOriginal,1)),:) = ...
        sinoRings(ceil(position*size(sinoOriginal,1)),:) + strength;
end

% apply ringremove
settings.method = 'additive';
settings.additive_SmoothMethod = 'mean';
settings.additive_WindowSize = 100;
sinoCorrected = ringremove(sinoRings,settings);

% Compare result and original image
verifyEqual(testCase,sinoCorrected,sinoOriginal,'RelTol', 0.1 );

end

%------------------------- radon3Darray---------------------------------%

function test_radon3Darray(testCase)
% get current filename and path and add toolbox to genpath
currfile = mfilename('fullpath') ;
currpath = fileparts(currfile) ;
pathToTools = fullfile(currpath,'..') ;
addpath(genpath(pathToTools )) ;

% original projection
projectionOriginal = 128*ones(128,128);

% create projection
phantom = ones(128,128,128);
tomoAngle = 1;
settings.outputSize = size(phantom,2);
projection = radon3Darray(phantom, tomoAngle,settings);

% Compare result and original image
verifyEqual(testCase,projection(3:end-2,3:end-2),...
    projectionOriginal(3:end-2,3:end-2),'RelTol', 0.1 );

end

%------------------------- astraConeProjection ---------------------------------%

function test_astraConeProjection(testCase)
% get current filename and path and add toolbox to genpath
currfile = mfilename('fullpath') ;
currpath = fileparts(currfile) ;
pathToTools = fullfile(currpath,'..') ;
addpath(genpath(pathToTools )) ;

% original projection
projectionOriginal = 128*ones(128,128);

% create projection
phantom = ones(128,128,128);
magnification = 1;
dx = 10e-6;
dx_eff = dx/magnification;
sourceSample = 0.1;
sourceDetector = sourceSample*magnification;

tomoAngle = 1;

settings.outputSize = size(phantom,2);
projection = astraConeProjection(phantom,tomoAngle,sourceSample,...
    sourceDetector,dx,dx_eff,settings);

% Compare result and original image
verifyEqual(testCase,projection(2:end-1,2:end-1),...
    projectionOriginal(2:end-1,2:end-1),'RelTol', 0.1 );

end

%------------------------- astraParProjection2D ---------------------------------%

function test_astraParProjection2D(testCase)
% get current filename and path and add toolbox to genpath
currfile = mfilename('fullpath') ;
currpath = fileparts(currfile) ;
pathToTools = fullfile(currpath,'..') ;
addpath(genpath(pathToTools )) ;

% original projection
projectionOriginal = 128*ones(128,1);

% create projection
phantom = ones(128,128);
tomoAngle = 1;

settings.outputSize = size(phantom,2);
projection = astraParProjection2D(phantom,tomoAngle,settings);

% Compare result and original image
verifyEqual(testCase,projection(2:end-1),projectionOriginal(2:end-1),'RelTol', 0.1 );

end

%------------------------- astraParProjection3D ---------------------------------%

function test_astraParProjection3D(testCase)
% get current filename and path and add toolbox to genpath
currfile = mfilename('fullpath') ;
currpath = fileparts(currfile) ;
pathToTools = fullfile(currpath,'..') ;
addpath(genpath(pathToTools )) ;

% original projection
projectionOriginal = 128*ones(128,128);

% create projection
phantom = ones(128,128,128);
tomoAngle = 1;

settings.outputSize = size(phantom,2);
projection = astraParProjection3D(phantom,tomoAngle,settings);

% Compare result and original image
verifyEqual(testCase,projection(2:end-1,2:end-1),...
    projectionOriginal(2:end-1,2:end-1),'RelTol', 0.1 );

end

%------------------------- astraFBP ---------------------------------%

function test_astraFBP(testCase)
% get current filename and path and add toolbox to genpath
currfile = mfilename('fullpath') ;
currpath = fileparts(currfile) ;
pathToTools = fullfile(currpath,'..') ;
addpath(genpath(pathToTools )) ;

% original slice
sliceOriginal = phantom(256)+1;

% create projections
tomoAngles = linspace(0,180,400);
projections = astraParProjection2D(sliceOriginal,tomoAngles);

settings.outputSize = 256;
sliceReconstructed = astraFBP(projections,tomoAngles,settings);

% Compare result and original image
verifyEqual(testCase,cropToCenter(sliceReconstructed,[128 128]),...
    cropToCenter(sliceOriginal,[128 128]),'RelTol', 0.1);

end

%------------------------- astraFDK ---------------------------------%

function test_astraFDK(testCase)
% get current filename and path and add toolbox to genpath
currfile = mfilename('fullpath') ;
currpath = fileparts(currfile) ;
pathToTools = fullfile(currpath,'..') ;
addpath(genpath(pathToTools )) ;

% original phantom
volOriginal = repmat(phantom(256)+1,[1 1 30]);

% create projections
N = size(volOriginal,2);
dx = 10e-6;
magnification = 2;
dx_eff = dx/magnification;
sourceSample = 0.1;
sourceDetector = sourceSample*magnification;
coneAngle = 2*atand((N/2*dx_eff)/((sourceSample)));
tomoAngles = linspace(0,180 + ceil(coneAngle),400);
projections = astraConeProjection(volOriginal,tomoAngles,sourceSample,...
    sourceDetector,dx,dx_eff);

% reconstruct 3d array
settings.outputSize = size(volOriginal,1);
settings.numSlices = size(volOriginal,3);
volReconstructed = astraFDK(projections,tomoAngles,sourceSample,sourceDetector,...
    dx,dx_eff,settings);

% Compare result and original image
verifyEqual(testCase,cropToCenter(volReconstructed(:,:,15),[128 128]),...
    cropToCenter(volOriginal(:,:,15),[128 128]),'RelTol', 0.2);

end

%------------------------- astraBackproj3D ---------------------------------%

function test_astraBackproj3D(testCase)
% get current filename and path and add toolbox to genpath
currfile = mfilename('fullpath') ;
currpath = fileparts(currfile) ;
pathToTools = fullfile(currpath,'..') ;
addpath(genpath(pathToTools )) ;

% original phantom
volOriginal = repmat(phantom(256)+1,[1 1 30]);

% create projections
tomoAngles = linspace(0,180,400);
projections = astraParProjection3D(volOriginal,tomoAngles);

% reconstruct single slice with astraFBP without filter
settings.filter = 'none';
sliceOriginal = astraFBP(squeeze(projections(15,:,:)),tomoAngles,settings);

% reconstruct volume with astraBackproj3D
volReconstructed = astraBackproj3D(projections,tomoAngles);

% Compare result and original image
verifyEqual(testCase,volReconstructed(:,:,15),sliceOriginal,'RelTol', 0.1);

end