% test the padding routine
% test defaults - cosine padding
image = rand(128);
settings = padFadeout();
[image_pad, window, avg_values] = padFadeout(image, [256 256], settings);

figure(1);
imagesc(image)

figure(2);
imagesc(image_pad)
%% test defaults for ellipse method

image = rand(128);
settings = padFadeout();
settings.fadeoutMethod = 'ellipse';
[image_pad, window, avg_values] = padFadeout(image, [256 256], settings);

figure(1);
imagesc(image)

figure(2);
imagesc(image_pad)

%% test defaults for ellipse method

image = load('mandrill', 'X');
image = image.X;
settings = padFadeout();
settings.fadeoutMethod = 'ellipse';
settings.numSegments = 3;
settings.offsetSegments = 0.2;

[image_pad, window, avg_values] = padFadeout(image, [1024 1024], settings);

figure(1);
imagesc(image)

figure(2);
imagesc(image_pad)
