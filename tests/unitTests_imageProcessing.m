%runtests('unitTests_imageProcessing.m')

function tests = unitTests_imageProcessing
tests = functiontests(localfunctions);
end

% This is how a test environement looks like:
%
% the function name has to start with test_...
% to run the test, use the command runtests('unitTests_imageProcessing.m')
%
%function test_functionName(testCase)
% https://de.mathworks.com/help/matlab/ref/matlab.unittest.qualifications.verifiable.verifyequal.html
% verifyEqual(verifiable,actual,expected)
% verifyEqual(___,Name,Value)
% verifyEqual(___,diagnostic)
%end

%------------------------- removeOutliers---------------------------------%

function test_removeOutliers(testCase)

% get current filename and path and add toolbox to genpath
currfile = mfilename('fullpath') ;
currpath = fileparts(currfile) ;
pathToTools = fullfile(currpath,'..') ;
addpath(genpath(pathToTools )) ;

% phantom image
original = phantom(100)  ;
original  = original - 0.5 ;

% create a corrupted image
image = original ;

% hot pixels
image(10,10) = 100000 ;
image(80,80) = -100000 ;

% infs
image(30,30) = inf;
image(1,1) = -inf;

% NaNs
image(50,50) = NaN;
image(100,100) = NaN;

% apply removeOutliers
correctedImage = removeOutliers(image) ;

% Compare result and original image
verifyEqual(testCase ...
    , correctedImage ...
    , original ...
    ,'RelTol', 0.1 );

end

%------------------------- centerOfMass-----------------------------------%

function test_centerOfMass(testCase)

% get current filename and path and add toolbox to genpath
currfile = mfilename('fullpath') ;
currpath = fileparts(currfile) ;
pathToTools = fullfile(currpath,'..') ;
addpath(genpath(pathToTools )) ;

% phantom image
nx = 20; ny = 41 ;
com_x = 10 ; com_y = 15 ;
image = zeros([ny,nx]) ;
image(com_y, com_x ) = 1 ;

% compute center of mass
foundCom = centerOfMass(image) ;

% Compare result and original image
verifyEqual(testCase ...
    , foundCom(2) ...
    , com_x ...
    ,'RelTol', 0.1 );

verifyEqual(testCase ...
    , foundCom(1) ...
    , com_y );

end

%------------------------- angularAverage---------------------------------%

function test_angularAverage(testCase)

% get current filename and path and add toolbox to genpath
currfile = mfilename('fullpath') ;
currpath = fileparts(currfile) ;
pathToTools = fullfile(currpath,'..') ;
addpath(genpath(pathToTools )) ;

% phantom with even number of pixels
nx = 100 ;
ny = 100 ;

x = (-floor(nx/2):floor((nx-1)/2)).' ;
y = (-floor(ny/2):floor((ny-1)/2)) ;

rho = sqrt(x.^2 + y.^2) ;

expected = x(nx/2+1:end);
[found , ~] = angularAverage(rho) ;

verifyEqual(testCase ...
    , found ...
    , expected ...
    ,'AbsTol', 0.2 );

% phantom with odd number of pixels
nx = 101 ;
ny = 101 ;

x = (-floor(nx/2):floor((nx-1)/2)).' ;
y = (-floor(ny/2):floor((ny-1)/2)) ;

rho = sqrt(x.^2 + y.^2) ;

expected = x(ceil(nx/2):end) ;
[found , ~] = angularAverage(rho) ;

verifyEqual(testCase ...
    , found ...
    , expected...
    ,'AbsTol', 0.2 );

% phantom with even and odd number of pixels
nx = 101 ;
ny = 50 ;

x = (-floor(nx/2):floor((nx-1)/2)).' ;
y = (-floor(ny/2):floor((ny-1)/2)) ;

rho = sqrt(x.^2 + y.^2) ;

expected = y(ny/2+1:end).' ;
[found , ~] = angularAverage(rho) ;

verifyEqual(testCase ...
    , found ...
    , expected ...
    ,'AbsTol', 0.2 );

end

%------------------------- circshiftSubPixel------------------------------%

function test_circshiftSubPixel(testCase)

% get current filename and path and add toolbox to genpath
currfile = mfilename('fullpath') ;
currpath = fileparts(currfile) ;
pathToTools = fullfile(currpath,'..') ;
addpath(genpath(pathToTools )) ;

% phantom image
nx = 20; ny = 41 ;
com_x = 10 ; com_y = 15 ;
image = zeros([ny,nx]) ;
image(com_y, com_x ) = 1 ;

% compute center of mass
shift = [2.5 , -3.7] ;
imageShifted = circshiftSubPixel(image, shift) ;
com = centerOfMass(imageShifted) ;

% Compare theoretical center of mass and found center of mass
verifyEqual(testCase ...
    , com(2)  ...
    , shift(2) + com_x ...
    ,'RelTol', 0.1 );

verifyEqual(testCase ...
    , com(1) ...
    , shift(1) + com_y );

end

%------------------------- alignImages------------------------------------%

function test_alignImages(testCase)

% reference image
imReference = phantom(512);

% shift image
shift = [20.3 -30.5] ;
imToAlign = circshiftSubPixel(imReference,shift);

% align shifted phantom to reference
[imAligned,foundShift, ~] = alignImages(imToAlign,imReference);

% Compare found shift and theoretical shift
verifyEqual(testCase ...
    , foundShift(1)  ...
    , -shift(1) ...
    ,'RelTol', 0.1 );

verifyEqual(testCase ...
    , foundShift(2)  ...
    , -shift(2) ...
    ,'RelTol', 0.1 );

% Compare reference image center of mass and aligend Image center of mass
comReference= centerOfMass(imReference) ;
comAligend = centerOfMass(imAligned) ;
verifyEqual(testCase ...
    , comAligend  ...
    , comReference ...
    ,'RelTol', 0.1 );

end

%------------------------- combindInFourierSpace--------------------------%


%------------------------- cropToCenterSubPixel-----------------------------%

function test_cropToCenterSubPixelr(testCase)

% reference image
imReference = phantom(100);
imReference = padarray(imReference, [100,100],'both') ;

% shift image
shift = [20 -20] ;
imReference  = circshiftSubPixel(imReference,shift);

% get center of mass relative to center
com = centerOfMass(imReference ) ;
com = com - size(imReference) /2 ;

%-------------------nx even ny even---------------------------------------%

%size of cropped image
sizeCropped= [ 150 , 150 ] ;
imCropped = cropToCenterSubPixel(imReference, sizeCropped ) ;

% get center of mass relative to center
comCropped = centerOfMass(imCropped) ;
comCropped = comCropped - sizeCropped/2 ;

verifyEqual(testCase ...
    , comCropped  ...
    , com ...
    ,'RelTol', 0.01 );

%-------------------nx odd ny even----------------------------------------%

%size of cropped image
sizeCropped = [ 151 , 150 ] ;
imCropped = cropToCenterSubPixel(imReference, sizeCropped ) ;

% get center of mass relative to center
comCropped = centerOfMass(imCropped) ;
comCropped = comCropped - sizeCropped/2 ;

verifyEqual(testCase ...
    , comCropped  ...
    , com ...
    ,'RelTol', 0.01 );

%-------------------nx even ny odd----------------------------------------%

%size of cropped image
sizeCropped= [ 150 , 151 ] ;
imCropped = cropToCenterSubPixel(imReference, sizeCropped ) ;

% get center of mass relative to center
comCropped = centerOfMass(imCropped) ;
comCropped = comCropped - sizeCropped/2 ;

verifyEqual(testCase ...
    , comCropped  ...
    , com ...
    ,'RelTol', 0.01 );

%-------------------nx odd ny odd-----------------------------------------%

%size of cropped image
sizeCropped = [ 151 , 151 ] ;
imCropped = cropToCenterSubPixel(imReference, sizeCropped ) ;

% get center of mass relative to center
comCropped = centerOfMass(imCropped) ;
comCropped = comCropped - sizeCropped/2 ;

verifyEqual(testCase ...
    , comCropped  ...
    , com ...
    ,'RelTol', 0.01 );

end

%------------------------- replaceCenterSubPixel----------------------------------%

function test_replaceCenterSubPixel(testCase)

% image with replaced center
image = ones(10,10);
expectedCom = centerOfMass(image) ;

%-------------------nx even ny even---------------------------------------%

sizeCenter = [2,2] ;
newCenter = ones(sizeCenter) * 5 ;
imageWithReplCenter = replaceCenterSubPixel(image, newCenter) ;

% get center of mass
com = centerOfMass(imageWithReplCenter) ;

verifyEqual(testCase ...
    , com  ...
    , expectedCom ...
    ,'RelTol', 0.01 );

%-------------------nx odd ny even----------------------------------------%

sizeCenter = [3,2] ;
newCenter = ones(sizeCenter) * 5 ;
imageWithReplCenter = replaceCenterSubPixel(image, newCenter) ;

% get center of mass
com = centerOfMass(imageWithReplCenter) ;

verifyEqual(testCase ...
    , com  ...
    , expectedCom ...
    ,'RelTol', 0.01 );

%-------------------nx even ny odd----------------------------------------%

sizeCenter = [2,3] ;
newCenter = ones(sizeCenter) * 5 ;
imageWithReplCenter = replaceCenterSubPixel(image, newCenter) ;

% get center of mass
com = centerOfMass(imageWithReplCenter) ;

verifyEqual(testCase ...
    , com  ...
    , expectedCom ...
    ,'RelTol', 0.01 );

%-------------------nx odd ny odd-----------------------------------------%

sizeCenter = [3,3] ;
newCenter = ones(sizeCenter) * 5 ;
imageWithReplCenter = replaceCenterSubPixel(image, newCenter) ;

% get center of mass
com = centerOfMass(imageWithReplCenter) ;

verifyEqual(testCase ...
    , com  ...
    , expectedCom ...
    ,'RelTol', 0.01 );

end

%------------------------- imageToPolarCoords-----------------------------%

function test_imageToPolarCoords(testCase)

% Different image-sizes: [even,even], [even,odd], [odd,even] and [odd,odd]
imageSizes = {[100,100], [100,101], [101,100], [101,101]};

for testIdx = 1:4
    imageSize = imageSizes{testIdx};
    
    % test image (smoothly varying radially symmetric function)
    x = linspace(-imageSize(2)/2, imageSize(2)/2 , imageSize(2) ).' / imageSize(2) ;
    y = linspace(-imageSize(1)/2, imageSize(1)/2 , imageSize(1) )   / imageSize(1) ;
    image = cos((4*pi)*sqrt(x.^2 + y.^2));
    
    [imPolar, R, Phi] = imageToPolarCoords(image);
    
    expected  = cos((4*pi)*R(:,1)) ;
    found = imPolar(:,1) ;
    
    verifyEqual(testCase ...
        , found  ...
        , expected ...
        ,'AbsTol', 0.1 );
end
end

%------------------------- removeStripes----------------------------------%

function test_removeStripes(testCase)

referenceImage = phantom(512) + 1;

gaussSettings = gaussian;
gaussSettings.sigma = 10;
stripe = repmat(gaussian(512,gaussSettings),[512 1]);

%-------------------additive stripes--------------------------------------%

image = referenceImage + 0.6*stripe + 0.5*stripe';

clear settings
settings.direction = 'both';
settings.method = 'additive';

[image,~] = removeStripes(image , settings);

verifyEqual(testCase ...
    , image  ...
    , referenceImage ...
    ,'RelTol', 0.2 );

%-------------------multiplicative stripes--------------------------------%

image = referenceImage .*( 0.6*(1+stripe) + 0.5*(1+stripe)');

clear settings
settings.direction = 'both';
settings.method = 'multiplicative';

[image, ~] = removeStripes(image , settings);

verifyEqual(testCase ...
    , image  ...
    , referenceImage ...
    ,'RelTol', 0.2 );

end

%------------------------- shiftRotateImage-------------------------------%

function test_shiftRotateImage(testCase)

% parameters for phantom
n = 64;
rotAngleDegree = 360*rand();
shifts = (n/4) * (rand([1,2])-0.5);

% phantom to be rotated
im = padarray(ones(n/2), [n/4,n/4]);
im(n/8+1:n/4,n/4+1:3*n/8) = 1;

% test for correct orientation

combinedProcedure = shiftRotateImage(im, shifts, rotAngleDegree);
classicalProcedure = circshiftSubPixel( ...
    imrotate(im, rotAngleDegree, 'bilinear', 'Crop'), shifts);

classicalCOM = centerOfMass(classicalProcedure) ;
combinedCOM = centerOfMass(combinedProcedure) ;

verifyEqual(testCase ...
    , combinedCOM  ...
    , classicalCOM ...
    ,'RelTol', 0.05 );

% test for better resolution

numOnesClassical = sum(sum(classicalProcedure == 1)) ;
numOnesCombined = sum(sum(combinedProcedure == 1)) ;
numZerosClassical = sum(sum(classicalProcedure == 0)) ;
numZerosCombined = sum(sum(combinedProcedure  == 0 )) ;

verifyGreaterThan(testCase,numOnesCombined,numOnesClassical)
verifyGreaterThan(testCase,numZerosCombined,numZerosClassical)

end

%------------------------- scale01----------------------------------------%

function test_scale01(testCase)

% create phantom
upperlim = 50 ;
lowerlim = -50 ;
im = (upperlim-lowerlim).*rand([20,41]) + lowerlim ;

% scale the phantom
scaledIm = scale01(im) ;

% check scaled phantom: it should be [0 1]
verifyGreaterThanOrEqual(testCase,scaledIm,zeros(size(im))) ;
verifyLessThanOrEqual(testCase,scaledIm,ones(size(im))) ;

end

%------------------------- padToSize--------------------------------------%

function test_padToSize(testCase)

% specify modes and sizes
modes = { 'zero', 'symmetric', 'replicate', 'fadeOut' } ;
sizes = { [ 50 60 ] , [ 51 60 ] , [ 50 61 ] , [ 51 61 ] } ;

% create phantom
im  = rand(20) ;

% check results of padToSize
for curr_mode = 1 : numel(modes)
    for curr_size = 1 : numel(sizes)
        tmp = padToSize(im, sizes{curr_size}, modes{curr_mode}) ;
        verifySize(testCase,tmp,sizes{curr_size});
    end
end

end


%------------------------- fadeOut----------------------------------------%

function test_fadeOut(testCase)

pixelsToAdd = { [ 50 60 ] , [ 51 60 ] , [ 50 61 ] , [ 51 61 ] } ;

phantom  = rand(20) + 0.1  ;

for iii = 1 : numel(pixelsToAdd)
    
    fadedPhantom = fadeOut(phantom, pixelsToAdd{iii}) ;
    fadedRegion = fadedPhantom - padarray(phantom, pixelsToAdd{iii}) ;
    
    numZeros = numel(nonzeros(fadedPhantom - fadedRegion)) ;
    verifyEqual(testCase , numZeros, numel(phantom));
    verifyEqual(testCase , size(fadedPhantom), pixelsToAdd{iii}*2+size(phantom));
end

end


