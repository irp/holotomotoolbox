%runtests('unitTests_phaseRetrieval.m')

function tests = unitTests_phaseRetrieval
tests = functiontests(localfunctions);
end

% This is how a test environement looks like:
%
% the function name has to start with test_...
% to run the test, use the command runtests('unitTests_phaseRetrieval.m')
%
%function test_functionName(testCase)
% https://de.mathworks.com/help/matlab/ref/matlab.unittest.qualifications.verifiable.verifyequal.html
% verifyEqual(verifiable,actual,expected)
% verifyEqual(___,Name,Value)
% verifyEqual(___,diagnostic)
%end

%------------------------- rescaleDefocusSeries ---------------------------------%

function test_rescaleDefocusSeries(testCase)
% get current filename and path and add toolbox to genpath
currfile = mfilename('fullpath') ;
currpath = fileparts(currfile) ;
pathToTools = fullfile(currpath,'..') ;
addpath(genpath(pathToTools )) ;

% create projections
projsOriginal = zeros(512,512,2);
magnifications = [1.5 3];
fresnelnumbers = 0.01./magnifications;
shift = rand(1,4)*5;
for i = 1:2
    tmp = circshiftSubPixel(imresize(phantom(512)+0.1,magnifications(i)),...
        [shift(i) -shift(i)]);
    projsOriginal(:,:,i) = cropToCenter(tmp,[512 512]);
end

% apply rescaleDefocusSeries
settings.alignMethod = 'dftreg';
settings.medfilt = 1;
projsAligned = rescaleDefocusSeries(projsOriginal,fresnelnumbers,...
    magnifications,settings);

% Compare result and original image
verifyEqual(testCase,projsAligned(:,:,1),projsAligned(:,:,2),'AbsTol', 0.2);
  
end

