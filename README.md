# HoloTomoToolbox



## Documentation

See the [online-documentation](https://irp.pages.gwdg.de/holotomotoolbox/) and code examples in [examples/](examples/).

## Installation

Download or clone toolbox.

Clone by running
```
git clone https://gitlab.gwdg.de/irp/holotomotoolbox
```

Add to MATLAB path
```MATLAB
addpath(genpath('PATH_TO_TOOLBOX/functions/'))
```
In order to use the toolbox with OCTAVE, you additionally have to execute the script ```initOctave``` right after adding the path.

### Updating HoloTomoToolbox

In the directory you cloned the HoloTomoToolbox via `git` you can run
```
git pull
```
to update to the most recent version. 

## Build documentation
The documentation is built with [SPHINX](http://www.sphinx-doc.org) and
[sphinxcontrib-matlabdomain](https://pypi.org/project/sphinxcontrib-matlabdomain/).

### Install requirements
Install sphinx and the required extensions. It has to be Python 3! 
```
pip3 install --user -U sphinx sphinx_rtd_theme sphinxcontrib-matlabdomain sphinxcontrib-bibtex
```

### Compile the documentation
From the toolbox directory run
```
cd docs/
make html
```

## How to cite ?

When using this toolbox in an academic work, please cite: 

L. M. Lohse, A.-L. Robisch, M. Töpperwien, S. Maretzke, M. Krenkel, J. Hagemann, and T. Salditt, **A phase-retrieval toolbox for X-ray holography and tomography**,
Journal of Synchrotron Radiation **27**, 852-859 (2020). doi:https://doi.org/10.1107/S1600577520002398

```
@article{Lohse_2020,
	doi = {10.1107/s1600577520002398},
	year = 2020,
	month = {apr},
	publisher = {International Union of Crystallography ({IUCr})},
	volume = {27},
	number = {3},
	pages = {852--859},
	author = {Leon M. Lohse and Anna-Lena Robisch and Mareike Töpperwien and Simon Maretzke and Martin Krenkel and Johannes Hagemann and Tim Salditt},
	title = {A phase-retrieval toolbox for X-ray holography and tomography},
	journal = {Journal of Synchrotron Radiation}
}
```


## Contributing

[MATLAB Style Guidelines 2.0](http://www.datatool.com/downloads/MatlabStyle2%20book.pdf)

Push your changes into a new branch (with a useful name) and open a Merge Request afterwards.

In your development clone of the HoloTomoToolbox create a new branch
```
git checkout -b USEFUL_NAME
```
add and commit your changes. Then publish and push to the (new) remote branch
```
git push -u origin HEAD
```
