% This script is a tutorial for phase retrieval in the near field on optical stretcher data (barium stained cells) acquired by Jan-David Nicolas using the RAAR phase-retrieval algorithm.

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
% Original author: Johannes Hagemann 20171010

% workingPath = '~/Documents/' % your home dict.
workingPath = '.';
%toolbox path
TBPath = '~/Documents/holotomotoolbox/';
addpath(genpath(TBPath));
cd(workingPath)
% setup figures

set(0,'DefaultFigureColor','w')
cmap = ((colormap('bone(512)')));
set(0,'DefaultFigureColormap', cmap);

cmd = sprintf(['axis image; colorbar;  set(gcf,''PaperPosition'',[0 0 5 5], ''PaperSize'', [5 5]);',...
    ' set(gca,''xtick'',[],''ytick'',[], ''XColor'', [0 0 0], ''YColor'', [0 0 0])']);
set(groot,{'DefaultAxesXColor','DefaultAxesYColor','DefaultAxesZColor'},{'k','k','k'})
set(0,'DefaultImageCreateFcn', cmd)
set(groot,'defaultLineLineWidth',1.5)
set(0,'defaultAxesFontSize',15)
clear cmap cmd;
close 1



%% load raw data from matfile
% load('/home/AGSalditt/ProjectsX-rayImaging/Toolbox/release/numerical_experiments/iterative_phase_reconstruction/raw_input.mat', 'Icrop')
load('./rawImage.mat')
%% parameters from experiment

% use a for loop if defocal distance should be changed
E = 13.8;                   % energy
lambda = 12.398/E*1e-10;    % wavelength
dx = 6.54e-6;               % pandora pixel sixe

% detector and sample distance
z02 = 5.046;                % [m], where?
stx = 20;                   % [mm]

% calibration motor vs. real distance to wg	
corrStx = 0;
corrCx = 0;
corrX = corrStx+corrCx;

% calculate geometry parameters and transform images

% calculate parameters for each distance
z01   = (stx+corrX)/1000;
z12   = z02-z01;
M     = z02/z01;
zEff = z12/M;
dxeff = dx/M;
F     = dxeff.^2/(zEff*lambda);
%% draw support for cell in the figure

f = figure(1); 
imagesc(sqrt(imresize(rawImage,1)));  drawnow;


test = imfreehand();
suppMask = createMask(test);
%% single distance RAAR
raarsettings = RAAR();
raarsettings.useGPU = 0;
raarsettings.b_0 = 0.9;            % b0 = bm = 0.5 = GS
raarsettings.bM = 0.5;            % measurement (1: full measurement)
raarsettings.bS = 50;              % transition width 
% raarsettings.numIterations = 100; 
raarsettings.height = size(rawImage,1);
raarsettings.width = size(rawImage,2);
raarsettings.recHeight = 2840; % these numbers are chose to match to neccessary oversampling
raarsettings.recWidth = 2840;
% important!!!: in this example fresnel number and pixel size do not match!
% the images have to be enlarged to ensure proper propagation by a factor
% of 4 to match the condition N = 1/F.
% This enlargement should be done outside the algorithm. the algorithm
% itself does not do data preprocessing, thus oversamling should be forced
% to 1 in order to prevent miss behavior
raarsettings.oversample = 1; 

raarsettings.AmpValid = logical(ones(raarsettings.recHeight, raarsettings.recWidth)); % valid amplitudes (logical, 1: valid, 0: invalid)
% raarsettings.AmpValid = padToSize(raarsettings.Amp_valid, raarsettings.rec_height, raarsettings.rec_width, 'zero');

raarsettings.supp = logical(suppMask);           % support (logical, 1: in support, 0: outside)
raarsettings.supp = padToSize(raarsettings.supp, raarsettings.recHeight, raarsettings.rec_width, 'zero');
guess = exp(1i*zeros(size(rawImage(:,:,1)))); % this is a plane wave, amplitude 1 phase 0
guess = padToSize(guess,  raarsettings.recHeight, raarsettings.rec_width);
numIterations = 100;

raarsettings.F = F; % Fresnel number
raarsettings.doMErrors = 1; % errors with respect to measurement or equivalent 
raarsettings.doGap = 1; %|PS(xN) - P_M(x_n)|
raarsettings.doConvergenceHistory = 1; %|xN - x_(n-1)|

%% process measured intensities
% idea: the measuremant has to be enlarged.
% simply adding zeros induces an edge which is not physically meaningful
% Remember : a good hologram oscillates around 1! so normalization is
% important. also intensities ramps in the background can be hindering,
% these can be removed by high pass filtering.
% further for the padding: a replcate pad also induces artifacts in the
% reconstruction. if you use HIO with a replicate padded input, you still
% get a reconstruction, but it is flawed. (Example below)
% The GS and RAAR algorithm do not yield a reconstruction with this kind of
% input. So for these algorithm it is obligatory to use 'nicely padded &
% corrected' inputs. This is not only important if you wrongly sized inputs
% as in this case here but also if you have correctly sized inputs but you
% only have noise at the borders of your measurement. In this cases a kaiser 
% bessel window can be used to create a nicely decay towards the edges (example below)
% For now we want to pad the measurement with a noise region from the
% measurement itself:
% get the image from the stack
Itmp = rawImage; 
figure; imagesc(Itmp);
% select a noisy region - here lower left corner
roi = imrect;
mask = createMask(roi);
padVals = ones(size(mask));
padVals = Itmp(mask);
tmp = sum(mask);
height = find(tmp > 0, 1, 'last') - find(tmp > 0, 1, 'first') + 1;
tmp = sum(mask,2);
width = find(tmp > 0, 1, 'last') - find(tmp > 0, 1, 'first') + 1;

padVals = reshape(padVals, [width, height]);
%%
padVals = repmat(padVals, [15 15]);
padVals = mid(padVals, raarsettings.recHeight, raarsettings.rec_width);
% enlarge measurement
Itmp = padToSize(Itmp, raarsettings.recHeight, raarsettings.rec_width);
% copy the mesurement into the noise padding
Itmp = copyMid(Itmp, padVals, 710, 1200);
figure; imagesc(Itmp)
%% now we can reconstruct
raarsettings.measurements = sqrt(double(Itmp));

[reconRAAR, error, gap, convergence] =...
              RAAR('PFresnelMagnitude' ,... % input as amplitudes!
                   'PNegativePhaseSupport',...
                   guess, ...
                   raarsettings);
%% show result
figure; imagesc(mid(angle(reconRAAR), raarsettings)); axis image; colormap(bone)
%%
figure
subplot(1, 3, 1)
plot(error)
ax = gca; ax.YScale = 'log';
title('M error')
subplot(1, 3, 2)
plot(gap)
ax = gca; ax.YScale = 'log';
title('Gap')
subplot(1, 3, 3)
plot(convergence)
ax = gca; ax.YScale = 'log';
title('Convergence')
%% example Kaiser bessel for Kaiser Bessel (KB) preprocessing
Itmp = padToSize(rawImage, raarsettings.rec_height, raarsettings.rec_width);

frame = 0.8;
kbWindow = kaiserBessel(5,...
    ones(floor([frame*raarsettings.recWidth, frame*raarsettings.recWidth])));

kbWindow = padToSize(kb_window, raarsettings.rec_height, raarsettings.rec_width);
% convex combinations of kb window to value 1
Itmp = sqrt(double(Itmp .* kbWindow + (1-kbWindow)));
Itmp(isnan(Itmp)) = 1;
Itmp(isinf(Itmp)) = 1;
figure; imagesc(Itmp)
% note the KB- window cuts in the data, this can change results, carefully
% choose frame.
% other possibility: make a logical disk, filter it with a gaussian to
% create smooth edges and use that as window.
%% now we can reconstruct
raarsettings.measurements = sqrt(double(Itmp));

[reconRAAR, error, gap, convergence] =...
              RAAR('PFresnelMagnitude' ,... % input as amplitudes!
                   'PNegativePhaseSupport',...
                   guess, ...
                   raarsettings);
%% show result
figure; imagesc(mid(angle(reconRAAR), raarsettings))

figure
subplot(1, 3, 1)
plot(error)
ax = gca; ax.YScale = 'log';
title('M error')
subplot(1, 3, 2)
plot(gap)
ax = gca; ax.YScale = 'log';
title('Gap')
subplot(1, 3, 3)
plot(convergence)
ax = gca; ax.YScale = 'log';
title('Convergence')
%% HIO 
%% single distance modified HIO
hiosettings = hioFresnel;
hiosettings.C = 0.01;
hiosettings.useGpu = 1;
hiosettings.D = 0.00575; % Discrepancy value, depends on noise level
hiosettings.plotlive = 500;
hiosettings.phi0 = -0.08;
% hiosettings.startsol = exp(1i*squeeze(phiCtf(:,:,bildSel)));
hiosettings.startsol = exp(1i * zeros(size(rawImage)));
hiosettings.NIt = 200;
hiosettings.supp = logical(suppMask);     
hiosettings.beta = 0.001;
hiosettings.gamma = 0.1;
hiosettings.padfactor = 4;
% hiosettings.betaDelta

[test,tmp] = hioFresnel(rawImage,...
                      F, F,...
                      hiosettings);
reconHio = gather(tmp.psiUdp);
% phiHio = test;
% clf
figure
imagesc(imresize(angle(reconHio),1)); 


