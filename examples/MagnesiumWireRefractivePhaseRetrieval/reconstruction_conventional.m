function reco = reconstruction_conventional(p)
% suppguess add the regions above and below the wire to the reconstruction.
% This is again for mitigation of boundary artifacts.
load( 'supp_guess.mat')
%% projectors for conventional reconstruchtion in amplitude and phase
guess = gpuArray(complex(ones(p.recSize.*p.scale_fac, 'single')));

PSuppAdapt = PSupportAdaption(gpuArray(p.supp), p.suppAdaptSet);

PM = PFresnelMagnitude(gpuArray(single(sqrt(p.holo))),...
    p.F_calc, p.PMset);

PS = PConstraints(p.conSet) * PSuppAdapt;

%% 1st pass
starttime = datetime('now');
p.algSet.iterations = 1000;

projAlg = projectionAlgorithm(PM, PS, guess, p.algSet);
reco = projAlg.execute(p.algSet.iterations);


seq_conv_hist{1} = reco.stats.convergenceHistory;

%% we refinie the support from the final reconstruchtion
if(1)
p.supp = abs(angle(reco.x)) > 0.2;
p.supp = padToSize(cropToCenter(p.supp, p.data_size), p.recSize, 0);
se = strel('disk', 70);
p.supp = p.supp | imdilate(supp, se);
p.supp = bwareafilt(p.supp, 1, 'largest');

se = strel('disk', 10);
p.supp = imdilate(p.supp, se);
se = strel('disk', 10);
p.supp = imerode(p.supp, se);


p.supp = imfill(p.supp, 'holes');
p.supp = bwareafilt(p.supp, 1, 'largest');
p.supp = imgaussfilt(single(p.supp), 10/2.35);
figure(1002)
imagesc(p.supp)
end
%%
if(1)
PSupp = PSupport(gpuArray(p.supp), 1);

p.algSet.eta = 1.2;
p.algSet.gamma = 0.9;

% set or unset phase unwrapping, be warned it take time...
p.conSet.unwrapPhases = 1;
if(p.conSet.unwrapPhases)
    p.conSet.smoothAmplitudesBy = 0;
    p.conSet.smoothPhasesBy = 0;
    p.conSet.projectOnNegativity = 0;
end

PS = PConstraints(p.conSet) * PSupp;% * PStat;
p.algSet.iterations = 10000;

projAlg = projectionAlgorithm(PM, PS, guess, p.algSet);
reco = projAlg.execute(p.algSet.iterations);
seq_conv_hist{2} = reco.stats.convergenceHistory;
end

%% done
starttime - datetime('now')
end