%% example for iterative refractive phaseretrieval
% This script is a supplement to the paper: "A phase retrieval framework to
% directly reconstruct the projected refractive index". Here we demonstrate the 
% reconstruction of the holograms of the magnesium wire which have been obtained 
% at P05. 
%%
% change your current folder to the folder of this script
addpath(genpath('../../functions'));

addpath('.')
set(0,'DefaultFigureColor','w')
cmap = ((colormap('bone(512)')));
set(0,'DefaultFigureColormap', cmap);
% set defaults for figure
cmd = sprintf(['axis image; colorbar;  set(gcf,''PaperPosition'',[0 0 5 5], ''PaperSize'', [5 5]);',...
    ' set(gca,''xtick'',[],''ytick'',[], ''XColor'', [0 0 0], ''YColor'', [0 0 0]);']);

set(groot,{'DefaultAxesXColor','DefaultAxesYColor','DefaultAxesZColor'},{'k','k','k'})
set(groot,'defaultAxesCreateFcn',@(ax,~)set(ax.Toolbar,'Visible','off'))
 set(0,'DefaultImageCreateFcn', cmd)
set(groot,'defaultLineLineWidth',1.5)
set(0,'defaultAxesFontSize',15)
clear cmap cmd fig_cmd;
addToolsToToolbar();
close 1
%% ok lets look at the hologramm we are going to reconstruct
data = load('mg_wire_holo.mat');
holo = data.holo;

figure(1)
imagesc(holo)
colorbarSet = addColorbarLabel;
colorbarSet.Interpreter = 'latex';
addColorbarLabel('$I/I_0$')
% Here we see the flat-field corrected hologram  padded to a size of
% 4096x4096 pixels. The rays outside are a 'continuation' of the hologram to 
% mitigate gradients in the intensity. This square root of this array is
% used as input for the reconstruction algorithms
p.holo = holo;
% p is a general parameter struct
%% refractive reco
do_refractive = 1;
% algorithm_options is a seperate script which sets the parameters of the
% reconstruction. It takes care of calculating the propagation and algoritm specific
% parameters for the reconstruction. This populates p further.
algorithm_options
ref_reco = reconstruction_refractive(p);
%% plot reconstruction
% the reconstruction is ends with a projection on M, thus the sample
% constraints are not perfectly satisfied.
figure
imagesc(real(ref_reco.x))
addColorbarLabel('$\phi$ (rad)')
title('reconstructed \delta')

figure
imagesc(imag(ref_reco.x))
addColorbarLabel('$\beta$')
title('reconstructed \beta')

%% conventional reco
do_refractive = 0;
algorithm_options
conv_reco = reconstruction_conventional(p);
%% plot reconstruction
% the reconstruction is ends with a projection on M, thus the sample
% constraints are not perfectly satisfied.
figure
imagesc(angle(conv_reco.x))
addColorbarLabel('$\phi$ (rad)')
title('reconstructed phase angle')

% unwrap phases
tmp = conv_reco.x;
% tmp = pha
pha = (gpuArray(eval_unwrap(gather(cropToCenter(angle(tmp), [1700 1700])),...
    gather(cropToCenter(abs(tmp), [1700 1700])), [850 850])));

figure()
imagesc(pha)
addColorbarLabel('$\phi$ (rad)')
title('reconstructed phase angle unwrapped')


figure
imagesc(abs(conv_reco.x))
addColorbarLabel('$A$')
title('reconstructed amplitude')


