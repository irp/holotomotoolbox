%%
p.data_size = [2048 2048];
p.roiSize = [1700 1700]*1;
p.recSize = [2048 2048]*2;
p.positions = [519 521]-133; %mm  slider positions, - sliderpos1 + defocus distance
% 4070  = 509 mm  distance to goldi, 133 focal length
p.num_distances = 1;
% p.num_images = 1720;
p.z02 = 19.661e9;
p.lambda = 1.2398/11; %nm
p.det_pixel = 6500;
p.xcorr(1:4) = 51;
p.scale_fac = 1;
p = calc_Fresnel_geometry(p);
p.M./p.M(1)
p.xcorr
%%
p.fadeSet = fadeoutImage();
p.fadeSet.method = 'rectangle';
p.fadeSet.ellipseSize = [0.49 0.49];
p.fadeSet.windowShift = [0 0];
p.fadeSet.transitionLength = 20;
p.fadeSet.numSegments = 100;
% p.fadeSet.fadeToVal = 1;
%%
if(do_refractive) %refractive settings
%% magnitude
p.PMset = PFresnelMagnitude.defaultSettings();
p.PMset.projectionOrder = 'averagedRefractive';
% p.PMset.projectionOrder = 'cyclicRefractive';
% PMset.projectionOrder = 'sequential';
% tmp = true(p.data_size);
% tmp = padToSize(tmp, p.recSize, 0);
% p.PMset.applicationMask = repmat(tmp, [1 1 1]);
p.PMset.useGPU = 1;
p.PMset.singlePrecision = true;
p.PMset.representation = 'refractive';
p.PMset.doMErrors = 1;
p.PMset.plotIterates = 0;
p.PMset.plotInterval = 100;
%% refractive
p.refractiveSet = PRefractive.defaultSettings();
p.refractiveSet.ImagNegativeScaleFactor = 0;
p.refractiveSet.deltaOverBeta = [];
p.refractiveSet.minReal = -100;
p.refractiveSet.maxReal = 0;
p.refractiveSet.minImag = -0;
p.refractiveSet.maxImag = 1e-3;
p.refractiveSet.fwhmImag = 2;
p.refractiveSet.fwhmReal = 2;
p.refractiveSet.periodicBoundaries = 0;
p.refractiveSet.roiSize = p.roiSize*1.1;
p.refractiveSet.recSize = p.recSize;

%% support adaption
p.suppAdaptSet = PSupportAdaption.defaultSettings();
p.suppAdaptSet.doRefractive = 1;
p.suppAdaptSet.plotIterates = 1;
p.suppAdaptSet.plotInterval = 10;
p.suppAdaptSet.valueOutsideSupp = 0;
% p.suppAdaptSet.supportThreshold = 0.06;
p.suppAdaptSet.supportThreshold = 0.04;
p.suppAdaptSet.firstAdaption = 10;
p.suppAdaptSet.dilateValue = 5;
p.suppAdaptSet.erodeValue = 5;
% p.suppAdaptSet.
%% algorithm options
p.algSet = projectionAlgorithm.defaultSettings();
p.algSet.doRefractive = true;
p.algSet.doMErrors = 0;
p.algSet.roiSize = p.roiSize;
p.algSet.doConvergenceHistory = 1;
p.algSet.iterations = 2000;
p.algSet.useGPU = 1;
p.algSet.plotIterates = 1;
p.algSet.addMomentum = 0;
p.algSet.plotInterval = 100;
p.algSet.algorithm = 'NAG';%%
p.algSet.abortThreshold = -7.0;
p.algSet.eta = 1;
p.algSet.gamma = 1;
p.algSet.accelerationFrequency = 1;
end

%% amp/pha settings
if(~do_refractive) 
%% magnitude
p.PMset = PFresnelMagnitude.defaultSettings();
p.PMset.projectionOrder = 'averaged';
p.PMset.useGPU = 1;
p.PMset.singlePrecision = true;
p.PMset.doMErrors = 1;
p.PMset.plotIterates = 0;
p.PMset.plotInterval = 10;
%% constraints
p.conSet = PConstraints.defaultSettings();
p.conSet.upperAmplitude = 1.0101;
p.conSet.lowerAmplitude = 0.553;
p.conSet.smoothAmplitudesBy = 0;
p.conSet.smoothPhasesBy = 0;
p.conSet.projectOnNegativity = 0;
p.conSet.unwrapPhases = 0;

%% support adaption
p.suppAdaptSet = PSupportAdaption.defaultSettings();
p.suppAdaptSet.plotIterates = 1;
p.suppAdaptSet.plotInterval = 50;
p.suppAdaptSet.valueOutsideSupp = 1;
p.suppAdaptSet.supportThreshold = 0.01;
p.suppAdaptSet.firstAdaption = 50;
p.suppAdaptSet.dilateValue = 5;
p.suppAdaptSet.erodeValue = 5;

%% algorithm options
p.algSet = projectionAlgorithm.defaultSettings();
p.algSet.doMErrors = 0;
p.algSet.roiSize = p.roiSize;
p.algSet.doConvergenceHistory = 1;
p.algSet.iterations = 2000;
p.algSet.useGPU = 1;
p.algSet.plotIterates = 1;
p.algSet.addMomentum = 1;
p.algSet.plotInterval = 100;
p.algSet.algorithm = 'NAG';%%
p.algSet.abortThreshold = -10.0;
p.algSet.eta = 1.1;
p.algSet.gamma = 0.9;
p.algSet.accelerationStartAt = 2;
p.algSet.accelerationFrequency = 1;
end