% this function calculates the common parameters to calculate the Fresnel
% transformation for a multi distance propagation data set.
% Author: JH 58408 mJD
function p = calc_Fresnel_geometry(p)
if ~isfield(p, 'num_distances')
    p.num_distances = numel(p.positions);
end

if ~isfield(p, 'scale_fac')
    p.scale_fac = 1;
end

% initialize cell arrays
p.M = zeros(1, p.num_distances);
p.F_calc = zeros(1, p.num_distances);
p.z01 = zeros(1, p.num_distances);
p.z12 = zeros(1, p.num_distances);
p.z_eff = zeros(1, p.num_distances);
p.dxeff = zeros(1, p.num_distances);

if(~isfield(p, 'xcorr'))
    p.xcorr = zeros(p.num_distances, 1);
end

% calculate parameters for each distance
for k=1:p.num_distances
    p.z01(1, k)   = (p.positions(1, k) + p.xcorr(1, k))*1e6;
    p.z12(1, k)   = p.z02-p.z01(1, k);
    p.M(1, k)     = p.z02/p.z01(1, k);
    p.z_eff(1, k) = p.z12(1, k)/p.M(1, k);
    p.dxeff(1, k) = (p.det_pixel* 1/p.scale_fac)/p.M(1, k);
    p.F_calc(1, k)= p.dxeff(1, k).^2/(p.z_eff(1, k)*p.lambda);
end
p.F_rescaled = p.F_calc .* (p.M ./ max(p.M)).^2;
end