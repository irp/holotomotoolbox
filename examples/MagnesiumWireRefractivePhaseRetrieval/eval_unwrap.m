% wrapper function to evaluate phaseunwrapper.
% TO DO: add syntax for single argument function call
function [unwrap] = eval_unwrap(p_psi, a_psi, seed)

% p_psi = gather(angle(psi));
% a_psi = gather(abs(psi));

chunk_size = 2048;
num_steps = ceil(size(p_psi,1)/chunk_size);
num_bins = 2000;
max_num_workers = 16;
ppool = gcp('nocreate');
if(isempty('ppool'))
    ppool = parpool(max_num_workers);
    pctRunOnAll(maxNumCompThreads(2))
end

shifts = zeros(num_steps*num_steps, 4);
if num_steps > 1
    for jj = 1:num_steps
        for ii = 1:num_steps
            shifts(ii + (jj-1)*num_steps, :) = ...
                [ 1+(ii-1)*chunk_size, ...
                (ii-1)*chunk_size+chunk_size,...
                1+(jj-1)*chunk_size, ...
                (jj-1)*chunk_size+chunk_size];
        end
    end
else
    shifts(1, :) = [1 size(a_psi,1) 1 size(a_psi,2)];
end


target_roi = [1 chunk_size 1 chunk_size];
pha_psi = cell(size(shifts,1),1);
abs_psi = cell(size(shifts,1),1);
tmp = zeros(chunk_size);
if num_steps > 1
    for ii = 1:size(shifts,1)
        tmp = double(transplant_image_region(a_psi, tmp, shifts(ii,:), target_roi));
        abs_psi{ii} = ((tmp));
        
        tmp = double(transplant_image_region(p_psi, tmp, shifts(ii,:), target_roi));
        pha_psi{ii} = ((tmp));
    end
    
    phase_tiles = cell(size(shifts,1),1);
    
    if nargin < 3
        seed = [chunk_size/2 chunk_size/2];
    end
    
    for ii = 1:size(shifts,1)
        %    phase_tiles{ii} = robustunwrap_cpp(seed,...
        %             pha_psi(shifts(ii,1):shifts(ii,2), shifts(ii,3):shifts(ii,4)),...
        %             abs_psi(shifts(ii,1):shifts(ii,2), shifts(ii,3):shifts(ii,4)), 2000);
        phase_tiles{ii} = robustunwrap_cpp(seed,...
            pha_psi{ii},...
            abs_psi{ii}, num_bins);
    end
    
    phase_tiles = reshape(phase_tiles, num_steps, num_steps);
    unwrap = cell2mat(phase_tiles);
    
else
     seed = [size(p_psi,1)/2 size(p_psi,2)/2];
    unwrap = robustunwrap_cpp(seed,...
        double(p_psi),...
        double(a_psi), num_bins);
end


end