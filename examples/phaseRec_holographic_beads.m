%%
clear all;
close all;
addpath(genpath('../functions'));
if isOctave
	initOctave;
end

% Load data
data = getHologram('beads');
holograms = double(data.holograms);
fresnelNumbers = double(data.fresnelNumbers);
clear 'data';


cax = [-3.5,2];



%% Standard CTF-based phase reconstruction
settingsCTF.lim1 = 1e-3;
settingsCTF.lim2 = 1e-2;

tic; 
phaseRecCTF = phaserec_ctf(holograms, fresnelNumbers, settingsCTF); 
computeTimeCTF = toc;

figure('name', 'standard CTF-reconstruction'); showImage(phaseRecCTF);
imagesc(phaseRecCTF); colormap bone; colorbar; caxis(cax);


%% CTF-reconstruction with negative-phase-constraint
settingsCTFConstr = settingsCTF;
settingsCTFConstr.maxPhase = 0;

tic; 
phaseRecCTFConstr = phaserec_ctf(holograms, fresnelNumbers, settingsCTFConstr); 
computeTimeCTFConstr = toc;

figure('name', 'CTF-reconstruction with negative-phase-constraint');
imagesc(phaseRecCTFConstr); colormap bone; colorbar; caxis(cax);


%% Nonlinear Tikhonov reconstruction without constraints
settingsNLTikh = settingsCTF;

tic; 
phaseRecNLTikh = phaserec_nonlinTikhonov(holograms, fresnelNumbers, settingsNLTikh); 
computeTimeNLTikh = toc;

figure('name', 'Nonlinear Tikhonov reconstruction');
imagesc(phaseRecNLTikh); colormap bone; colorbar; caxis(cax);


%% Nonlinear Tikhonov reconstruction with negative-phase-constraint
settingsNLTikhConstr = settingsCTFConstr;

tic; 
phaseRecNLTikhConstr = phaserec_nonlinTikhonov(holograms, fresnelNumbers, settingsNLTikhConstr); 
computeTimeNLTikhConstr = toc;

figure('name', 'Nonlinear Tikhonov with negative-phase-constraint');
imagesc(phaseRecNLTikhConstr); colormap bone; colorbar; caxis(cax);


%% RAAR-reconstruction with pure-phase-object constraint (if minAbsorption ~= maxAbsorption, then also absorption is reconstructed) 
settingsRAAR.iterations = 100;
settingsRAAR.maxAbsorption = 1;
settingsRAAR.minAbsorption = 1;
tic; 
phaseRecRAAR = phaserec_RAAR(holograms, fresnelNumbers, settingsRAAR); 
computeTimeRAAR = toc;

figure('name', 'RAAR-reconstruction'); 
imagesc(phaseRecRAAR); colormap bone; colorbar; caxis(cax);


%% Alternating projections / Gerchberg-Saxton reconstruction with pure-phase-object constraint
settingsAP = settingsRAAR;
tic; 
phaseRecAP = phaserec_AP(holograms, fresnelNumbers, settingsAP); 
computeTimeAP = toc;

figure('name', 'Alternating projections / Gerchberg-Saxton reconstruction');
imagesc(phaseRecAP); colormap bone; colorbar; caxis(cax);


%% Modified reconstruction with pure-phase-object constraint
settingsMHIO = settingsRAAR;
tic; phaseRecMHIO = phaserec_mHIO(holograms, fresnelNumbers, settingsMHIO); computeTimeMHIO = toc;

figure('name', 'Modified Hybrid-Input-Output reconstruction');
imagesc(phaseRecMHIO); colormap bone; colorbar; caxis(cax);




