function [beam] = modelWaveField(phaPath, ampPath, phaseRange,...
                    amplitudeRange, imageSize, outputImageSize, gaussFiltFwhm)
% this function takes 2 paths as arguments which are interpreted as
% amplitude and phase of a wavefield(probe or object or whatever). The user
% can set the ranges of amplitude and phase. p is a parameter struture, its
% purpose here is to give the size of the desired wavefield.
%
%    ``beam = modelWaveField(phaPath, ampPath, lowerPhase, upperPhase, lowerAmp, upperAmp, p, gauss_filt_fwhm)``
% 
%
% Parameters
% ----------
% phaPath : string
%     Path to the image to be interpreted as the phases of the wave field.
%     The file format can be anything that can be read by matlab's imread.
% ampPath : string
%     Path to the image to be interpreted as the amplitudes of the wave field.
%     The file format can be anything that can be read by matlab's imread.
% phaseRange : [lowerPhase upperPhase] tupil
%     Range of phases in the output in rad. Mapped to the values in the
%     input image.
% amplitudeRange : [lowerAmp upperAmp] tupil
%     Range of amplitudes in the output. Mapped to the values in the
%     input image.
% imageSize : [height width] tupil
%     The input images will be scaled to this size.
% outputImageSize : [height width] tupil
%     The output will have this size. the input images will be placed in
%     the middle and a smooth transition to the surrounding will be added.
% gaussFiltFwhm : number
%      optional, input images can be smoothed by a gaussian.
%
% Example
% -------
%
% .. code-block:: matlab
%
%     TBPath = 'YOUR TOOLBOX DIRECTORY'
%     % e.g. a phase only phantom of a sample, i.e. no variation in the phase
%     sample = prepareProbe([TBPath '/phantoms/dictySketch.png'],...
%                [TBPath '/phantoms/dictySketch.png'],...
%                [0 1], [1 1], [512 512], [1024 1024]);
%     % e.g. an amplitude only phantom of a sample. 
%     sample = prepareProbe([TBPath '/phantoms/dictySketch.png'],...
%                [TBPath '/phantoms/dictySketch.png'],...
%                [0 0], [0.8 1], [512 512], [1024 1024]);
%     % e.g. an arbitrary phantom e.g. for the probe
%     sample = prepareProbe([TBPath '/phantoms/mandrill.png'],...
%                [TBPath '/phantoms/durer.png'],...
%                [0.2 -0.2], [0.8 1.2], [512 512], [1024 1024]);
%
     
% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
% Original author: Johannes Hagemann

%%

lowerPhase = phaseRange(1);
upperPhase = phaseRange(2);
lowerAmplitude = amplitudeRange(1);
upperAmplitude = amplitudeRange(2);

if nargin < 8
    gaussFiltFwhm = 0;
end
% enhancement: test ampPath with isa() for matrix or matfile and include these cases                
% read in and convert to grayscale
% amp = load(ampPath);
amp = getPhantom(ampPath);
% amp = amp.phantom;

% convert only to grayscale if we have a color image, otherwise rgb2gray
% throws an error
if ndims(amp) > 2
   amp = rgb2gray(amp);
end
   
% pha = load(phaPath);
pha = getPhantom(phaPath);
% pha = pha.phantom;


if ndims(pha) > 2
    pha = rgb2gray(pha);
end

amp = imresize(amp, imageSize);
pha = imresize(pha, imageSize);

if gaussFiltFwhm > 1e-6
    amp = imgaussfilt(amp, gaussFiltFwhm/(2*sqrt(2*log(2))));
    pha = imgaussfilt(pha , gaussFiltFwhm/(2*sqrt(2*log(2))));
end

% scaled amplitudes
ampRange = double(max(amp(:)) - min(amp(:)));
amp = lowerAmplitude + (double(amp) ./ (ampRange)) .* (upperAmplitude - lowerAmplitude);
if (outputImageSize(1)-imageSize(1)) > 0
    padSet.padVal = lowerAmplitude;
%     padSet.transitionLength =  (outputImageSize(1)-imageSize(1))*0.03;
    amp = padFadeout(amp, outputImageSize, padSet);
end
% scaled phases
phaRange = double(max(pha(:)) - min(pha(:)));
pha = lowerPhase + (double(pha) ./ (phaRange)) .* (upperPhase - lowerPhase);

if (outputImageSize(1)-imageSize(1)) > 0
    padSet.padVal = 0;
    padSet.transitionLength =  (outputImageSize(1)-imageSize(1))*0.03;
    pha = padFadeout(pha, outputImageSize, padSet);
end
beam = amp .* exp(1i .* pha);
% beam = padFadeout(beam, [p.embeddedHeight, p.embeddedWidth],(p.embeddedWidth-p.width)*0.01, exp(1i*0));
end
