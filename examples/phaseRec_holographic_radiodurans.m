%%
clear all;
close all;
addpath(genpath('../functions'));
if isOctave
    initOctave;
end

data = getHologram('radiodurans');
holograms = double(data.holograms);
fresnelNumbers = double(data.fresnelNumbers);
support = data.support;
clear 'data';

cax = [-0.4,0.05];



%% Standard CTF-based phase reconstruction
settingsCTF.lim1 = 1e-4;
settingsCTF.lim2 = 1e-2;

tic; phaseRecCTF = phaserec_ctf(holograms, fresnelNumbers, settingsCTF); computeTimeCTF = toc;

figure('name', 'standard CTF-reconstruction'); 
imagesc(phaseRecCTF); colormap bone; colorbar; caxis(cax);


%% CTF-reconstruction with with support- and negative-phase-constraint
settingsCTFConstr = settingsCTF;
settingsCTFConstr.support = support;
settingsCTFConstr.maxPhase = 0;

tic; phaseRecCTFConstr = phaserec_ctf(holograms, fresnelNumbers, settingsCTFConstr); computeTimeCTFConstr = toc;

figure('name', 'CTF-reconstruction with support- and negative-phase-constraint');
imagesc(phaseRecCTFConstr); colormap bone; colorbar; caxis(cax);


%% Nonlinear Tikhonov reconstruction without constraints
settingsNLTikh = settingsCTF;

tic; phaseRecNLTikh = phaserec_nonlinTikhonov(holograms, fresnelNumbers, settingsNLTikh); computeTimeNLTikh = toc;

figure('name', 'Nonlinear Tikhonov reconstruction');
imagesc(phaseRecNLTikh); colormap bone; colorbar; caxis(cax);


%% Nonlinear Tikhonov reconstruction with support- and negative-phase-constraint
settingsNLTikhConstr = settingsCTFConstr;

tic; phaseRecNLTikhConstr = phaserec_nonlinTikhonov(holograms, fresnelNumbers, settingsNLTikhConstr); computeTimeNLTikhConstr = toc;

figure('name', 'Nonlinear Tikhonov with support- and negative-phase-constraint');
imagesc(phaseRecNLTikhConstr); colormap bone; colorbar; caxis(cax);


%% RAAR-reconstruction with support- and negative-phase-constraint
settingsRAAR.iterations = 100;
settingsRAAR.support = support;
settingsRAAR.maxPhase = 0;
tic; phaseRecRAAR = phaserec_RAAR(holograms, fresnelNumbers, settingsRAAR); computeTimeRAAR = toc;

figure('name', 'RAAR-reconstruction with constraints');
imagesc(phaseRecRAAR); colormap bone; colorbar; caxis(cax);


%% Alternating projections / Gerchberg-Saxton reconstruction with support- and negative-phase-constraint
settingsAP = settingsRAAR;
tic; phaseRecAP = phaserec_AP(holograms, fresnelNumbers, settingsAP); computeTimeAP = toc;

figure('name', 'Alternating projections / Gerchberg-Saxton reconstruction with constraints');
imagesc(phaseRecAP); colormap bone; colorbar; caxis(cax);



%% Modified reconstruction with support- and negative-phase-constraint
settingsMHIO = settingsRAAR;
tic; phaseRecMHIO = phaserec_mHIO(holograms, fresnelNumbers, settingsMHIO); computeTimeMHIO = toc;

figure('name', 'Modified Hybrid-Input-Output reconstruction with support- and negative-phase-constraint');
imagesc(phaseRecMHIO); colormap bone; colorbar; caxis(cax);

