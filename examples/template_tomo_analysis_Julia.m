%% start from scratch
clear all; close all; clc
set(0,'DefaultFigureColor','w');


%% parameters of the scan
year = '2019';
runname = '2019_05_02';
detector = 'argos';
filePrefix = 'HH_TAB1_C3-3_tomo1_unten';
rotMotor = 'szrot';

angRange = 181;
numProjs = 1000;

numAverage = 1;
numFlats = 25;
numDarks = 10;
numAngles = numProjs+1;

%% source-detector distance and correction factor for source-sample distance
z02 = 185.74;
z01Corr = 0; % in mm; correction factor for source-sample distance wrt to sx motor position


%% Paths for tomographic data and toolboxes to be loaded (modify if loading of toolboxes or data fails)
if ispc
    toolboxPath = 'S:/Projects_X-ray_Imaging/holotomotoolbox/functions';
    astraPath = 'S:/Projects_X-ray_Imaging/ASTRAToolbox/windows';
    prePath = 'J:/';
elseif isunix
    toolboxPath = '/home/AG_Salditt/Projects_X-ray_Imaging/holotomotoolbox/functions';
    astraPath = '/home/AG_Salditt/Projects_X-ray_Imaging/ASTRAToolbox/linux';
    prePath = '/home/Labdata/AG_Salditt/julialab/';
end

% change if not using julia
dataPath = fullfile(prePath, year, 'julia' , runname, 'detectors', detector, filePrefix);
saveDir = fullfile(prePath, year, 'julia', runname, 'analysis', filePrefix, 'Slices');

if ~exist(toolboxPath,'dir')
    error(['Assigned path to the holotomotoolbox ''', toolboxPath, ''' does not exist.']);
end    

if ~exist(astraPath,'dir')
    error(['Assigned path to the ASTRA-toolbox ''', astraPath, ''' does not exist.']);
end

if ~exist(dataPath,'dir')
    error(['Assumed data path ''', dataPath, ''' does not exist. Scan parameters might be wrong. Please fix the path manually.']);
end

%% Add holotomotoolbox to path
addpath(genpath(toolboxPath));

%% Add ASTRA-toolbox to path
addpath(genpath(astraPath));

%% Set up image-reader for the specified detector
imageReader = getImageReaderIRP(detector);

switch(lower(detector))
    case 'argos'
        fileEnding = '_%04i.tif'; dumpEnding = '_%04i.tif.txt';
        dx = 0.54e-6;
    case 'talos'
        fileEnding = '_%04i.tif'; dumpEnding = '_%04i.tif.txt';
        dx = 75e-6;
    case 'pirra'
        fileEnding = '_%i.tif'; dumpEnding = '_%04i.tif.txt';
        dx = 6.5e-6;
    case 'thyia'
        fileEnding = '_%i.tif'; dumpEnding = '_%04i.tif.txt';
        dx = 4.5e-6;
    case 'iris'
        fileEnding = '_%04i.img'; dumpEnding = '_%04i.img.txt';
        dx = 6.5e-6;
    case 'timepix'
        fileEnding = '_%04i.raw'; dumpEnding = '_%04i.raw.txt';
        dx = 55e-6;
    case 'zyla'
        fileEnding = '_%04i.tif'; dumpEnding = '_%04i.tif.txt';
        dx = 6.5e-6;
end

fileName = [filePrefix,fileEnding];
dumpName = [filePrefix,dumpEnding];

getFileName = @(number) fullfile(dataPath, sprintf(fileName,number));
getDumpName = @(number) fullfile(dataPath,'dump',sprintf(dumpName,number));


%% Read one image to check if path is correct
number = numFlats+1;
raw0 = imageReader(getFileName(number));

showImage(raw0);
colormap gray


%% image numbers
clear emptynumbers;

% empty images before tomographic scan
emptynumbers{1} = 1:numFlats;

% first and last image of the tomographic scan
numFirstangle = numFlats+1;
numLastangle = numFlats+numAngles*numAverage;

% first image at each angle
rawnumbers = numFirstangle:numAverage:numLastangle;

% empty images after tomographic scan
emptynumbers{2} = numLastangle+1:numLastangle+numFlats;

% darkfield images after tomographic scan
darknumbers = 2*numFlats+numAngles*numAverage+1:2*numFlats+numAngles*numAverage+numDarks;


%% read in empty images
emptys = cell(numel(emptynumbers),1);
for indEmpty = 1:numel(emptynumbers) 
    emptyTmp = zeros(size(raw0,1),size(raw0,2),numFlats);
    for indImg = 1:numFlats
        emptyTmp(:,:,indImg) = imageReader(getFileName(emptynumbers{indEmpty}(indImg)));      
        fprintf('.');
    end
    % take median of all emptys
    emptys{indEmpty} = median(emptyTmp,3);
    fprintf('\n');
end

%% read in darkfields
darkTmp = zeros(size(raw0,1),size(raw0,2),numDarks);
for indImg = 1:numDarks
    darkTmp(:,:,indImg) = imageReader(getFileName(darknumbers(indImg)));
    fprintf('.');
end
% take median of all darkfield images
dark = median(darkTmp,3);
fprintf('\n');

%% read in tomographic scan data
binningFactor = 2;  % factor for binning of detector pixels, factor 1 does nothing
projs = zeros(size(raw0,1)/binningFactor,size(raw0,2)/binningFactor,numAngles,'single');
thetas = zeros(numAngles,1);

parfor indAngle = 1:numAngles
    fprintf('Reading angle %4i of %4i\n',indAngle,numAngles);
    
    % read images and tka median over all images
    number = rawnumbers(indAngle);
    imgTmp = zeros(size(raw0,1),size(raw0,2),numAverage);
    for indImage = 1:numAverage
        imgTmp(:,:,indImage) = imageReader(getFileName(number+(indImage-1))); %#ok<PFBNS>
    end
    % average over all images
    raw = median(imgTmp,3);
    
    % empty correction with linear interpolation
    share = (indAngle-1)/(numAngles-1);
    empty = (1-share)*emptys{1} + share*emptys{2};
    
    % store in array projs
    if binningFactor~=1
        projs(:,:,indAngle) = imresize((raw-dark)./(empty-dark),1/binningFactor);
    else
        projs(:,:,indAngle) = (raw-dark)./(empty-dark);
    end
    
    % read tomographic angle from motor position
    thetas(indAngle) = wwas(getDumpName(number+(indImage-1)),rotMotor);
end


%% remove hotpixel errors
parfor indProj = 1:numAngles
    projs(:,:,indProj) = removeOutliers(projs(:,:,indProj));
    disp(indProj);
end


%% show random projection to check results
showImage(projs(:,:,randi(size(projs,3))));


%% align rotation axis automatically
proj0Degrees = projs(:,:,1);
[~, idx180Degrees] = min(abs((thetas-thetas(1))-180));
proj180Degrees = projs(:,:,idx180Degrees);

% perform automatic alignment of shift and rotation (might fail for too large cone angles)
settings = alignImages;
settings.registrationMode = 'shiftRotation';
settings.registrationAccuracy = 20;
settings.sigmaLowpass = 0;
settings.medfilt = true;
settings.sigmaHighpass = 50;    % High-pass filtering before alignment to avoid
                                % errors due to low-frequency artifacts
[~,shift,tiltAngle,~] = alignImages(fadeoutImage(proj0Degrees), fadeoutImage(fliplr(proj180Degrees)), settings);
shiftAxis = shift(2)/2;
detectorTilt = tiltAngle/2;

% check result
showImage(shiftRotateImage(proj0Degrees, [0,shiftAxis], detectorTilt) - fliplr(shiftRotateImage(proj180Degrees, [0,shiftAxis], detectorTilt)))
colormap gray
caxis([-0.4 0.4])
title('Check that images cancel each other out in the center! If not, automatic rotation-axis alignment has possibly failed.')

%% if automatic alignment fails: pre-align rotation axis manually (for fine alignment use tomographic reconstruction)
% 
% % manually determined parameters
% shiftAxis = 0;
% shifty = 0;
% detectorTilt = 0;
% 
% settings.shiftx = shiftAxis;
% settings.shifty = shifty;
% settings.tiltAngle = detectorTilt;
% 
% % check result
% showImage(checkRotaxis(proj0Degrees,proj180Degrees,settings));
% caxis([-0.4 0.4])

%% correct for position of rotation axis and detector tilt (only if known!! otherwise find out manually, see bottom of this script)
parfor indProj = 1:size(projs,3)
    disp(indProj);
    projs(:,:,indProj) = shiftRotateImage(projs(:,:,indProj), [0,shiftAxis], detectorTilt);
end


%% determine geometric parameters
% source-sample distance
z01 = wwas(getDumpName(numFirstangle),'sx') + z01Corr;

% effective pixel size
M = z02/z01;
dx = binningFactor*dx;	% consider change in detector pixel size due to binning
dx_eff = dx/M;

% effective propagation distance
z12 = (z02-z01)/1000;
z_eff = z12/M;

% main energy peak of the x-ray spectrum
E = 9.25;					% gallium

% wavelength of the main energy peak
lambda = 12.398/E*1e-10;

% Fresnel number
F = dx_eff^2/(lambda*z_eff);
fprintf('Fresnel number: %4.5f\n',F);


%% PHASE RECONSTRUCTION
%% First determine suitable method and parameters for an exemplary projection
projIdx = 1; %randi(size(projs,3));
projExample = projs(:,:,projIdx);

figure('name', 'Original image'); showImage(projExample); colormap gray;

settingsPhaseRecon = struct;
settingsPhaseRecon.padx = 500;
settingsPhaseRecon.pady = 500;
settingsPhaseRecon.reg_alpha = 0.005;

% Reconstruction with MBA-algorithm
projRecMBA = phaserec_mba(projExample, F, settingsPhaseRecon);
figure('name', 'MBA-reconstruction'); showImage(projRecMBA); colormap gray;

% Reconstruction with BAC-algorithm
settingsPhaseRecon.reg_gamma = 1;
projRecBAC = phaserec_bac(projExample, settingsPhaseRecon);
figure('name', 'BAC-reconstruction'); showImage(projRecBAC); colormap gray;

%% Reconstruct all projections angles with BAC using the determined settings
parfor indProj = 1:numAngles
    disp(indProj);
    projs(:,:,indProj) = phaserec_bac(projs(:,:,indProj), settingsPhaseRecon);
end

%% show stack of reconstructed projections
showStack(projs(11:end-10,11:end-10,:));


%% Determine settings for ringremoval 
sino = squeeze(projs(ceil(size(projs,1)/2),:,:));

sliceOriginal = astraFBP(sino,thetas);
figure('name', 'Original slice'); showImage(sliceOriginal); colormap gray;

% additive approach for ring removal
settingsRingremove = ringremove;
settingsRingremove.method = 'additive';
settingsRingremove.additive_SmoothMethod = 'mean';
settingsRingremove.additive_WindowSize = 7;
settingsRingremove.additive_GaussSigma = 1;

sinoAdditive = ringremove(sino,settingsRingremove);
sliceAdditive = astraFBP(sinoAdditive,thetas);
figure('name', 'Additive ring removal'); showImage(sliceAdditive); colormap gray;

% wavelet-based approach for ring removal
settingsRingremove.method = 'wavelet';
settingsRingremove.wavelet_DecNum = 3;
settingsRingremove.wavelet_Wname = 'sym8';
settingsRingremove.wavelet_Sigma = 1;

sinoWavelet = ringremove(sino,settingsRingremove);
sliceWavelet = astraFBP(sinoWavelet,thetas);
figure('name', 'Wavelet-based ring removal'); showImage(sliceWavelet); colormap gray;

%% OPTIONAL: Perform ring-removal with determined method and settings (only reasonable if sliceAdditive or sliceWavelet looks cleaner than sliceOriginal)
settingsRingremove.method = 'additive';
parfor sliceIdx = 1:size(projs,1)
    disp(sliceIdx);
    projs(sliceIdx,:,:) = ringremove(squeeze(projs(sliceIdx,:,:)), settingsRingremove);
end


%% TOMOGRAPHIC RECONSTRUCTION
%% First determine suitable reconstruction parameters by trial-reconstruction of the central tomographic slice
slicenumber = ceil(size(projs,1)/2);

sinoCrop = 0;               % Number of pixels cropped from the edges of the sinogram before reconstruction
sinoPad = 0;                % Number of pixels padded to the edges of the sinogram before reconstruction
sliceSize = size(projs,2);  % Size of the reconstructed slices

settingsTomoRec = astraFDK;
settingsTomoRec.outputSize = sliceSize;
settingsTomoRec.shortScan = 1;  % short scan should be enabled when the scan range is smaller than 360 degree
settingsTomoRec.numSlices = 1;  % reconstruct only one slice
settingsTomoRec.offset = 0;     % position of the slice relative to the equator of the circular cone beam
                                % geometry. The value 0 corresponds to reconstructing the central slice

sliceTest = astraFDK(padarray(projs(slicenumber,sinoCrop+1:end-sinoCrop,:), [0,sinoPad,0], 'replicate'), thetas, z01, z02, dx*1000, settingsTomoRec);

% View results to see whether parameters have to be adjusted
showImage(sliceTest); colormap gray;


%% Tomographic reconstruction of all slices using the determined parameters
settingsTomoRec.offset = 0;                 
settingsTomoRec.numSlices = size(projs,1);  % all slices
slices = astraFDK(padarray(projs(:,sinoCrop+1:end-sinoCrop,:), [0,sinoPad,0], 'replicate'), thetas, z01, z02, dx*1000 , settingsTomoRec);


%% show the reconstructed slices
fprintf('... show reconstructed slices... \n' ) ;
showStack(slices);


%% save results as raw
if exist(saveDir,'dir') ~= 7
    mkdir(saveDir)
end
savePrefix = [filePrefix, '_reconstructedSlices'];
saveraw(slices, fullfile(saveDir, savePrefix));


%% OPTIONAL: filtering with Gaussian for better SNR
fprintf('... filter slices with a Gaussian function... \n' ) ;
filterSigma = 1; % standard deviation in pixels
slices = imgaussfilt3(slices,filterSigma);


%% save filtered results as raw
savePrefix = [filePrefix, '_reconstructedSlices_filtered_sigma=',num2str(filterSigma)];
saveraw(slices, fullfile(saveDir, savePrefix));



%% %%%%%%%%%%%%%%%%%%%%% OPTIONAL: manually determine shift and tilt of rotation axis %%%%%%%%%%%%%%%%%%%%%%%
%% align rotation axis manually (for fine alignment use tomographic reconstruction)
proj0Degrees = projs(:,:,1);
[~, idx180Degrees] = min(abs((thetas-thetas(1))-180));
proj180Degrees = projs(:,:,idx180Degrees);

% manually determined parameters
shiftx = 0;
shifty = 0;

settings.shiftx = shiftx;
settings.shifty = shifty;

% check result
showImage(checkRotaxis(proj0Degrees,proj180Degrees,settings));
caxis([-0.4 0.4])

%% align rotation axis via tomographic reconstruction
x = 0;
for xxx = -2:1:2
    %% position of rotation axis and detector tilt
    %find out the shift of the rotation axis by comparing different shifts for
    %the central slice
    shiftAxis = shiftx+xxx;
    
    % find out the detector tilt by determining the shift of the rotation axis
    % at two different z positions ( angle=atand((shift1-shift2)/(z1-z2)) )
    detectorTilt = 0;
    
    projsAstra = circshiftSubPixel(projsPad,[0 shiftAxis 0]);
    
    if detectorTilt ~= 0
        projsAstra = imrotate(projsAstra,detectorTilt,'bilinear','crop');
    end
    
    %% actual reconstruction
    settingsAstra = astraFDK;
    
    % size of the reconstructed slices
    settingsAstra.outputSize = 1500;
    
    % short scan should be enabled when the scan range is smaller than 360?
    settingsAstra.shortScan = 1;
    
    % number of reconstructed slices
    settingsAstra.numSlices = 1;
    
    % if you want to reconstruct the whole volume set offset to 0, otherwise it
    % indicates the position relative to the central line
    settingsAstra.offset = 0;
    
    % do the first reconstruction
    tic
    slicesAstra = astraFDK(projsAstra,thetas,z01,z02,dx*1000,settingsAstra);
    toc
    %
    x = x+1;
    disp(x)
    
    slicesTmp{x} = slicesAstra(:,:,1);
end

%% find out the correct rotation axis shift via manual inspection
showImage(-slicesTmp{3})
title(sprintf('Current rotation axis shift: %4.1f + %4.1f',shiftx,xxx))
colormap gray
zoom(1)

