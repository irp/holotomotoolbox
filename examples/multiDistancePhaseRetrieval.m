% This script is a tutorial for multi distance data generation and
% iterative phase retrieval with different algorithm/projector combinations.

% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
% Original author: Johannes Hagemann 20190430

% you should be in the examples folder
TBPath = '../functions/';
addpath(genpath(TBPath))
addpath(genpath('.'))


%% set some figure defaults
set(0,'DefaultFigureColor','w')
cmap = ((colormap('bone(512)')));
set(0,'DefaultFigureColormap', cmap);
cmd = sprintf(['axis image; colorbar;  set(gcf,''PaperPosition'',[0 0 5 5], ''PaperSize'', [5 5]);',...
    ' set(gca,''xtick'',[],''ytick'',[], ''XColor'', [0 0 0], ''YColor'', [0 0 0])']);
set(groot,{'DefaultAxesXColor','DefaultAxesYColor','DefaultAxesZColor'},{'k','k','k'})
% fix the toolbar of the figures. starting with matlabR2018a
if ~verLessThan('matlab','9.4')
    set(groot,'defaultFigureCreateFcn',@(fig,~)addToolbarExplorationButtons(fig))
    set(groot,'defaultAxesCreateFcn',@(ax,~)set(ax.Toolbar,'Visible','off'))
end

set(0,'DefaultImageCreateFcn', cmd)
set(groot,'defaultLineLineWidth',1.5)
set(0,'defaultAxesFontSize',15)
clear cmap cmd;
close 1
%% Simulation parameters
% size of the region of interest i.e. where the actual sample lies
p.size = [512 512];
% for the datageneration it can be necessary to add some padding for
% artifact free forward simulation
p.embeddedSize = [2048 2048];
% but the array size for the reconstruction can be different again. Think
% e.g.  about a detector which is too small to capture the whole hologram
p.recSize = [2048 2048];
% Fresnel number with respect to one pixel for the hologram generation.
% Keep in mind that for proper sampling the number of pixels (either
% embeddedSize or recSize) should be 1/p.F or more. Here we generate 4
% measurements with slightly varied F-number.
% Things you can try out: 
% -change the base F-number. Is the variation still sufficient for
% reconstruction
% -change the number of F-numbers
% -choose different modification values (smaller, larger range)
p.F = [0.98 0.99 1 1.01]'.* 1e-3;
%% Sample Setup
% use the sketch of a cell as phantom for a phase-only object

maxPhase = 1; %rad
sample = modelWaveField('dicty',...
    'dicty',...
    [0 maxPhase], [1 1], p.size, p.embeddedSize);
 sample = abs(sample) .* exp(1i.*(angle(conj(sample))));

% conrol sample and embedding
figure()
% imagesc(angle(cropToCenter(sample,p.size)));
imagesc(angle((sample)));
addColorbarLabel('$\phi$ in rad.');


%% calculate hologram
% set up a propagator object 'prop'
propSet = FresnelPropagator.defaultSettings();
propSet.method = 'fourier'; % actually this is the default ...
prop = FresnelPropagator(p.embeddedSize, p.F, propSet);
% apply free space propagation using transfer function approach to yield
% the hologram at the detector
% for ii = 1:numel(p.F)
hologram = abs(prop.propagate(sample));
% we can not measure phases, so convert complex amplitudes into measured
% intensities
% end
% show hologram
for ii = 1:numel(p.F)
figure;
imagesc(hologram(:,:,ii))
end
% Normally the intersting part starts now, where you would deliberatley
% distort this ideal measurement to make it more real world like. 
% For experimental data you would start with some pre-processing... this is covered
% in another example.
%%
recPar = phaserec_iterative();
guess = complex(single(ones([p.recSize numel(p.F)])));
recPar.useGPU = 1;
recPar.verbose = 1;
recPar.algorithm = 'RAAR';
recPar.PFresnelMagnitudeOpts.projectionOrder = 'averaged';
recPar.PFresnelMagnitudeOpts.internalP = 'PPhase';
recPar.PFresnelMagnitudeOpts.PPhaseOpts.projectOnNegativity = 1;
% recPar.PS = 'PProductSpace';
recPar.PS = 'PNothing';
recPar.PPhaseOpts.projectOnNegativity = 1;
recPar.doConvergenceHistory = 1;
[reco, recPar] = phaserec_iterative(hologram, p.F, guess, recPar);
figure;
imagesc(angle(cropToCenter(reco.x(:,:,1), p.size)))

%%
tic
recPar = phaserec_AP();
recPar.useGPU = 1;
recPar.PFresnelMagnitudeOpts.projectionOrder = 'sequential';
recPar.PPhaseOpts.projectOnNegativity = 1;
recPar.PPhaseOpts.keepAmplitude = 1;
recPar.iterations = 100;
recPar.doConvergenceHistory = 0;
recPar.verbose = 0;
guess = complex(single(ones([p.recSize 1])));
[reco, recPar] = phaserec_AP(hologram, p.F, guess, recPar);
toc
% figure;
% imagesc(angle(cropToCenter(reco.x(:,:,1), p.size)))

%%
tic
recPar = phaserec_RAAR();
recPar.useGPU = 1;
% recPar.PFresnelMagnitudeOpts.projectionOrder = 'sequential';
% recPar.PPhaseOpts.projectOnNegativity = 1;
recPar.iterations = 500;
recPar.bS = 5;
recPar.doConvergenceHistory = 1;
recPar.verbose = 1;
guess = complex(single(ones([p.recSize 1])));
[reco, recPar] = phaserec_RAAR(hologram, p.F, guess, recPar);
toc
figure;
imagesc(angle(cropToCenter(reco.x(:,:,1), p.size)))
%%
%%
tic
recPar = phaserec_RAAR();
recPar.useGPU = 1;
% recPar.PFresnelMagnitudeOpts.projectionOrder = 'parallel';
% recPar.PS = 'PProductSpace';
% recPar.PFresnelMagnitudeOpts.internalP = 'PSupportAdaption';
% recPar.PFresnelMagnitudeOpts.PSupportAdaptionOpts.PPhaseOpts.projectOnNegativity = 1;
% recPar.PFresnelMagnitudeOpts.PSupportAdaptionOpts.verbose = 1;
% recPar.PFresnelMagnitudeOpts.PPhaseOpts.projectOnNegativity = 1;
% % recPar.PPhaseOpts.projectOnNegativity = 1;
% recPar.iterations = 200;
% % recPar.bS = 5;
% recPar.doConvergenceHistory = 1;
% recPar.verbose = 1;
% % guess = complex(single(ones([p.recSize numel(p.F)])));
guess = complex(single(ones([p.recSize 1])));
[reco, recPar] = phaserec_RAAR(hologram, p.F, guess, recPar);
toc
figure;
imagesc(angle(cropToCenter(reco.x(:,:,1), p.size)))


%% restrict to negative phases
p = AP('PFresnelMagnitude', 'PNegativePhase', p);
reco = AP('PFresnelMagnitude', 'PNegativePhase', guess, p);
figure; 
imagesc(angle(cropToCenter(reco, p.size)))
title('reconstruction with PNegativePhase')
% This is already getting better. On one hand we have already the correct
% phase range recovered, especially that is only negative. 
% But on the outside there are still some distortions, so called twin image
% artifacts. Next we will make use of the fact, that our object is
% spatially bounded or supported.
% Note: the reconstructions can be in general only considered to be correct 
% up to a global phase factor.
% Note 2: Try to increase the phase range of the sample over pi in the setup
% section and run this reconstruction again. It will fail horribly...
%% use a support
% luckily this is a simulation, so we know our support.
supp = load('../functions/phantoms/phantomDicty.mat');
supp = supp.support;
supp = imresize(supp, (p.size(1)/2048), 'box');

supp = padToSize(supp, p.recSize);
% the support we are going to use for the reconstruction
figure()
imagesc(supp);

%% setup algorithm with support constraint
p = AP('PFresnelMagnitude', 'PSupport', p);
p.supp = supp;
reco = AP('PFresnelMagnitude', 'PSupport', guess, p);
figure; 
imagesc(angle(cropToCenter(reco, p.size)))
title('reconstruction with PSupport')
% Now we have removed the twin image artifacts. yay! But the phases are not
% aligned anymore. The reason for that is that we JUST have applied
% the support. Obviously there are cases where one wants to combine some
% constraints. For these cases the projectors offer the option to choose an
% internal projector (p.*projector name*_opts.internalP). Most often that
% will be PPurePhase or PNegativePhase or some idea of your own!? So
% lets try that.
%% setup algorithm with support and negativity constraint
p = AP('PFresnelMagnitude', 'PSupport', p);
p.supp = supp;
p.PSupportOpts.internalP = 'PNegativePhase';
reco = AP('PFresnelMagnitude', 'PSupport', guess, p);
figure; 
imagesc(angle(cropToCenter(reco, p.size)))
title('reconstruction with PSupport with internal projection on negativity')
% Isn't it a beauty! But the low spatial frequency have not converged yet. 
% They always need longest to converge. Let's invest some more iterations
% and look at the convergence
%%
p.iterations = 1000;
% there are also other errors to measure but this one suffices for now.
p.doConvergenceHistory = 1;
p.PSupportOpts.internalP = 'PNegativePhase';
[reco, p, ~,~, convHist] = AP('PFresnelMagnitude', 'PSupport', guess, p);
figure; 
imagesc(angle(cropToCenter(reco, p.size)))
title('PSupport with internal projection on negativity')

figure();
plot((convHist))
title('Convergence Graph')
ax = gca;
ax.XScale = 'log';
ax.YScale = 'log';
ylabel('$\|x_n - x_{n+1}\|$', 'Interpreter', 'latex')
xlabel('iterations')
% Note: if you reach a value of 10^-4 (for float, double is 10^-8) you can expect a nearly perfect
% reconstruction.
%% But seriously when do you know your support in real world experiments?
% Real phase retrieval finds the support on its own!
% For that we will use PSupportAdaption. It uses thresholding combined
% with morphological operations to estimate the support.
p = AP('PFresnelMagnitude', 'PSupportAdaption', p);
%%
p.supp = []; % forget the support
p.doConvergenceHistory = 1;
p.PSupportAdaptionOpts.supportThreshold = -0.05; %rad
p.PSupportAdaptionOpts.erodeValue = 10; %pixel
p.PSupportAdaptionOpts.dilateValue = 11; % you should add more then you remove...
p.PSupportAdaptionOpts.verbose = 1;
[reco, p, ~,~, convHist] = AP('PFresnelMagnitude', 'PSupportAdaption', guess, p);
figure; 
imagesc(angle(cropToCenter(reco, p.size)))
title('PSupport with internal projection on negativity')

figure; 
imagesc(cropToCenter(p.supp, p.size))
title('automatically determined support')

figure();
plot((convHist))
title('Convergence Graph')
ax = gca;
ax.XScale = 'log';
ax.YScale = 'log';
ylabel('$\|xN - x_{n+1}\|$', 'Interpreter', 'latex')
xlabel('iterations')
% what a useful tool ;). The spikey part in the middle of the convegence
% graph shows the activity of the support adaption. In this ideal setting
% it stops at some point. For real data this is often not the case. Then
% one should only a fix number of iterations to determine a decent support
% and then switch to a reconstruction strategy with static support.
%% Conclusion
% In this tutorial we have covered the generation of synthetic holograms
% and single distance iterative phase retrieval. We have seen the effects
% of different (increasingly restringent) constraints on the
% reconstruction. In another tutorial XXXXXXXXX we cover multiplane phase
% retrieval and in XXXXXX the application to experimental data. For a brief
% overview of other algorithms look at XXXXX. 
% Spoiler  warning: all the algorithms have the same structure as AP we have 
% used here. Just put another name and you are good to go. 
% If you liked this tutorial don't hesitate to use the like button below.
% or comments to johannes.hagemann@desy.de














