function res = imgaussfilt(im, sigma, padValue)
% OCTAVE-function that implements part of the MATLAB-function imgaussfilt.
% See the corresponding MATLAB-documentation for details.
    
% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

if ~isOctave
    rmpath(fileparts(mfilename('fullpath')));
    error(['This function should not be called from MATLAB because it shadows MATLAB''s ', ...
           'native implementation of it. Be careful NOT to include the directory ', ...
           '''octave'' to the search path when using MATLAB. The directory ', ...
           'The directory has now been removed from the search path, so the error should ', ...
           'fixed for the current session. Please try to re-run the last operation.']);
end

if nargin < 3
    padValue = 'replicate';
end
sizeIm = size(im);
filtDims = (sizeIm ~= 1 & (1:ndims(im)) < 3);
pad_by = zeros([1,ndims(im)]);
pad_by(filtDims) = ceil(3*sigma);
sizeFilt = 1 + (6*ceil(sigma)) .* filtDims;
sizePad = makeSizeFFTfriendly(sizeIm + 2*pad_by);
im = padToSize(im, sizePad, padValue);

filt = padToSize(fspecial('gaussian', sizeFilt, sigma), sizePad(1:2), 0);

res = cropToCenter(ifft2(fft2(ifftshift(filt)) .* fft2(im)), sizeIm);
if isreal(im)
    res = real(res);
end

end
