function array = gather(array) 
% OCTAVE-function that provides a dummy implementation of the MATLAB-function gather.
% Does nothing, but ensures that OCTAVE does not raise an error when gather is called.
% This makes it easier to write code that may run both on CPU and GPU (in MATLAB)
% without running into compatibility problems with OCTAVE.
    
% HoloTomoToolbox
% Copyright (C) 2019  Institut fuer Roentgenphysik, Universitaet Goettingen
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

if ~isOctave
    rmpath(fileparts(mfilename('fullpath')));
    error(['This function should not be called from MATLAB because it shadows MATLAB''s ', ...
           'native implementation of it. Be careful NOT to include the directory ', ...
           '''octave'' to the search path when using MATLAB. The directory ', ...
           '''octave'' has now been removed from the search path, so the error should ', ...
           'fixed for the current session. Please try to re-run the last operation.']);
end

end
